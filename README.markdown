# ovopen-tools

Provides several tools which support backend functionality of ovopen.

* dbc_dump - Tool which converts "dbc" files into JSON or TorqueScript datablock dumps
* node - Provides a node module which emulates basic location server responses for testing
* ovopen-build - Provides a toolset for building asset bundles
* patch_tool - Allows the creation of patch bundles

## Building

You will need a checkout of `ovopen-frontend` to build most of the tools. This should be 
placed in the parent folder, i.e.:

	/
		ovopen-tools
		ovopen-frontend

Then to build all the tools:

	mkdir build
	cmake -DOPENSSL_DIR=/usr/lib\
	-DBOOST_DIR=/usr/lib

	make



#### Licensing ####

All ovopen-tools code should be considered AGPL licensed. Everything in ext/ retains its original licensing.
