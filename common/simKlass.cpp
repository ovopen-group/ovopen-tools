#include "common.h"
#include "dbc_dump/dataBlocks.h"

#include "simKlass.h"
#include "jansson.h"

#include "math/mAngAxis.h"
#include "math/mMatrix.h"
#include "math/mQuat.h"
#include "ts/tsShape.h"
#include "ts/tsMaterial.h"
#include "ts/tsMaterialManager.h"
#include "ts/tsRenderState.h"


SimpleStringTable smSTInstance;

// FIELD TYPES

S32FieldType  S32Field;
S32FieldTickScaledType  S32FieldTickScaled;
U32FieldType  U32Field;
F32FieldType  F32Field;
BoolFieldType  BoolField;
S8FieldType  S8Field;
U8FieldType  U8Field;
StringFieldType  StringField;
STDStringFieldType  STDStringField;
STStringFieldType  STStringField;
Point2FFieldType  Point2FField;
Point2IFieldType  Point2IField;
Point3FFieldType  Point3FField;
Point3IFieldType  Point3IField;
Point4FFieldType  Point4FField;
ColorFFieldType  ColorFField;
ColorIFieldType  ColorIField;
SimRefFieldType  SimRefField;
SimWeakRefFieldType  SimWeakRefField;
MatrixPositionFieldType  MatrixPositionField;
MatrixRotationFieldType  MatrixRotationField;

static std::unordered_map<std::string, SimKlassInfo*> smKlassLookupTable;
SimKlassInfo* SimKlassInfo::smKlassList;


void StringFieldType::setString(int elementIdx, void* ptr, char* data)
{
   ((const char**)ptr)[elementIdx] = smSTInstance.getSTEntry(data);
}

void STStringFieldType::setString(int elementIdx, void* ptr, char* data)
{
   ((const char**)ptr)[elementIdx] = smSTInstance.getSTEntry(data);
}

const char* SimRefFieldType::getString(int elementIdx, void* ptr)
{
	char buf[64];
	SimDataBlock* obj = ((SimDataBlock**)ptr)[elementIdx];
	if (obj)
	  snprintf(buf, sizeof(buf), "%u", obj->mId);
	else
	  return "";
	return StringReturnBuf::instance()->alloc(buf);
}

void SimRefFieldType::setString(int elementIdx, void* ptr, char* data)
{
   ((SimDataBlock**)ptr)[elementIdx] = Sim::findObject((uint32_t)std::atoll(data));
}

const char* MatrixPositionFieldType::getString(int elementIdx, void* ptr)
{
  char buf[256];
  F32* col = (F32*)&((MatrixF*)ptr)[elementIdx] + 3;
  snprintf(buf, 256, "%g %g %g", col[0], col[4], col[8]);
  return StringReturnBuf::instance()->alloc(buf);
}

void MatrixPositionFieldType::setString(int elementIdx, void* ptr, char* data)
{
  F32* col = (F32*)&((MatrixF*)ptr)[elementIdx] + 3;
   col[0] = 0;
   col[4] = 0;
   col[8] = 0;
  sscanf(data, "%f %f %f", &col[0], &col[4], &col[8]);
}

const char* MatrixRotationFieldType::getString(int elementIdx, void* ptr)
{
  char buf[256];
  void* ofsPtr = &((MatrixF*)ptr)[elementIdx];
  DTShape::AngAxisF aa(*(DTShape::MatrixF *) ofsPtr);
  aa.axis.normalize();

  snprintf(buf,sizeof(buf),"%g %g %g %g",aa.axis.x,aa.axis.y,aa.axis.z,DTShape::mRadToDeg(aa.angle));
  return StringReturnBuf::instance()->alloc(buf);
}

void MatrixRotationFieldType::setString(int elementIdx, void* ptr, char* data)
{
   DTShape::AngAxisF aa(DTShape::Point3F(0,0,1), 0.0f);

   F32 ang = 0.0f;
  sscanf(data, "%f %f %f %f",&aa.axis.x,&aa.axis.y,&aa.axis.z,&ang);
   aa.angle = DTShape::mRadToDeg(aa.angle);
   aa.setMatrix(&((DTShape::MatrixF*)ptr)[elementIdx]);
}

void SimKlassInfo::buildKlassLookup()
{
   for (SimKlassInfo* itr = smKlassList; itr != NULL; itr = itr->mNextKlass)
   {
      smKlassLookupTable[itr->mClassName] = itr;
   }
   
   for (SimKlassInfo* itr = smKlassList; itr != NULL; itr = itr->mNextKlass)
   {
      itr->enumerateFields();
   }
   
   memset(__NetClassBitSize, '\0', sizeof(__NetClassBitSize));
   __NetClassBitSize[NetClassGroupGame][NetClassTypeObject] = 7;
   __NetClassBitSize[NetClassGroupGame][NetClassTypeDataBlock] = 6;
   __NetClassBitSize[NetClassGroupGame][NetClassTypeEvent] = 5;
}

SimKlassInfo* SimKlassInfo::lookupKlass(const char* className)
{
   return smKlassLookupTable[className];
}

SimpleStringTable* SimpleStringTable::inst()
{
   return &smSTInstance;
}


static StringReturnBuf srBufInstance;

StringReturnBuf* StringReturnBuf::instance()
{
  return &srBufInstance;
}


void JSONKlassDumper::dump(void* ptr, SimKlassInfo* info)
{
	if (!info || !ptr)
	{
	  assert(false);
	  return;
	}

	SimFieldInfo* nameField = info->findField("name");
	SimFieldInfo* classField = info->findField("class");
	SimFieldInfo* idField = info->findField("id");

	assert(idField != NULL);
	assert(classField != NULL);

	json_t *root = json_object();
	json_incref(root);

	for (auto itr : info->mFields)
	{
	  uint8_t* fieldPtr = ((uint8_t*)ptr) + itr.offset;

	  if (!info->shouldEnumerate(ptr, fieldPtr, -1))
	    continue;

	  int numElements = info->getFieldElements(&itr, ptr);
	  if (numElements >= 0)
	  {
	    json_t* arr = json_object();
	    bool didEnum = false;
	    for (int i=0; i<numElements; i++)
	    {
	      if (!info->shouldEnumerate(ptr, fieldPtr, i))
	        continue;
	      char buf[64];
	      snprintf(buf, 64, "%u", i);
	      json_object_set_new(arr, buf, getValue(itr, ptr, i, info));
	      didEnum=true;
	    }

	    if (didEnum)
	    {
	      json_object_set_new(root, itr.name, arr);
	    }
	    else
	    {
	      json_decref(arr);
	    }
	  }
	  else
	  {
	    json_object_set_new(root, itr.name, getValue(itr, ptr, 0, info));
	  }
	}

	// Dynamic fields
	std::vector<SimFieldInfo> dynamicFields;
	info->enumerateDynamicFields(ptr, dynamicFields);

	if (dynamicFields.size() > 0)
	{
	  json_t *fields = json_object();
	  json_incref(fields);
	  for (auto itr : dynamicFields)
	  {
	    json_object_set_new(fields, itr.name, json_string(info->getDynamicFieldValue(ptr, itr.name)));
	  }
	  json_object_set_new(root, "dynamicFields", fields);
	  json_decref(fields);
	}

	char *out = json_dumps(root, JSON_PRESERVE_ORDER|JSON_INDENT(3));

	if (strcasecmp(out, "(null)") == 0)
	{
	  printf("WTF!!!!!");
	}
	printf("%s", out);
	free(out);

	json_decref(root);
}


StringTableEntry BitStream::readSTString()
{
   char str[256];
   str[0] = '\0';
   readString(str);
   return SimpleStringTable::getSTEntry(str);
}


namespace Sim
{
  std::unordered_map<uint32_t, SimDataBlock*> smLoadedObjects;

  SimDataBlock* findObject(uint32_t id)
  {
    auto itr = smLoadedObjects.find(id);
    if (itr != smLoadedObjects.end())
      return itr->second;
    else
      return NULL;
  }
}

