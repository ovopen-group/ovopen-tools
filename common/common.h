/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <vector>
#include <time.h>
//#include <unistd.h>
#include <assert.h>
#include <stdarg.h>


#undef M_PI
#undef M_SQRT2

#define M_PI         3.14159265358979323846
#define M_SQRT2      1.41421356237309504880

#define M_2PI        (3.1415926535897932384626433 * 2.0)
#define M_SQRTHALF   0.7071067811865475244008443

#define M_PI_F         3.14159265358979323846f
#define M_SQRT2_F      1.41421356237309504880f

#define M_2PI_F        (3.1415926535897932384626433f * 2.0f)
#define M_SQRTHALF_F   0.7071067811865475244008443f

#define BIT(x) (1 << (x))                       ///< Returns value with bit x set (2^x)

#define DECLARE_OVERLOADED_READ(type)      \
   bool read(type* out_read) {             \
      return read(sizeof(type), out_read); \
   }
#define DECLARE_OVERLOADED_WRITE(type)       \
   bool write(type in_write) {               \
      return write(sizeof(type), &in_write); \
   }

#define DECLARE_ENDIAN_OVERLOADED_READ(type)       \
   bool read(type* out_read) {                     \
      type temp;                                   \
      bool success = read(sizeof(type), &temp);    \
      *out_read = (temp);      \
      return success;                              \
   }
#define DECLARE_ENDIAN_OVERLOADED_WRITE(type)      \
   bool write(type in_write) {                     \
      type temp = (in_write);  \
      return write(sizeof(type), &temp);           \
   }

typedef int64_t U64;
typedef unsigned int U32;
typedef signed int S32;
typedef unsigned char U8;
typedef unsigned short U16;
typedef short S16;
typedef char S8;
typedef float F32;

inline void AssertFatal(bool cond, const char *str)
{
	if (!cond)
	{
		printf("%s\n", str);
      assert(false);
	}
}

inline F32 mSqrt(const F32 val)
{
   return (F32) sqrt(val);
}

inline F32 mFabs(const F32 val)
{
   return (F32) fabs(val);
}

inline F32 mCos(const F32 angle)
{
   return (F32) cos(angle);
}

inline F32 mSin(const F32 angle)
{
   return (F32) sin(angle);
}

inline F32 mAsin(const F32 val)
{
   return (F32) asin(val);
}

inline F32 mAtan(const F32 x, const F32 y)
{
   return (F32) atan2(x, y);
}

inline U32 getNextPow2(U32 io_num);

/// Determines if the given U32 is some 2^n
/// @returns true if in_num is a power of two, otherwise false
inline bool isPow2(const U32 in_num)
{
   return (in_num == getNextPow2(in_num));
}

// note: impl from T2D
inline U32 getNextPow2(U32 io_num)
{
   S32 oneCount   = 0;
   S32 shiftCount = -1;
   while (io_num) {
      if(io_num & 1)
         oneCount++;
      shiftCount++;
      io_num >>= 1;
   }
   if(oneCount > 1)
      shiftCount++;
   
   return U32(1 << shiftCount);
}


// note: impl from T2D
inline U32 getBinLog2(U32 io_num)
{
   AssertFatal(io_num != 0 && isPow2(io_num) == true,
               "Error, this only works on powers of 2 > 0");
   
   S32 shiftCount = 0;
   while (io_num) {
      shiftCount++;
      io_num >>= 1;
   }
   
   return U32(shiftCount - 1);
}

/// Returns the lesser of the two parameters: a & b.
inline U32 getMin(U32 a, U32 b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline U16 getMin(U16 a, U16 b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline U8 getMin(U8 a, U8 b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline S32 getMin(S32 a, S32 b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline S16 getMin(S16 a, S16 b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline S8 getMin(S8 a, S8 b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline float getMin(float a, float b)
{
   return a>b ? b : a;
}

/// Returns the lesser of the two parameters: a & b.
inline double getMin(double a, double b)
{
   return a>b ? b : a;
}

/// Returns the greater of the two parameters: a & b.
inline U32 getMax(U32 a, U32 b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline U16 getMax(U16 a, U16 b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline U8 getMax(U8 a, U8 b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline S32 getMax(S32 a, S32 b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline S16 getMax(S16 a, S16 b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline S8 getMax(S8 a, S8 b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline float getMax(float a, float b)
{
   return a>b ? a : b;
}

/// Returns the greater of the two parameters: a & b.
inline double getMax(double a, double b)
{
   return a>b ? a : b;
}

inline S32 mClamp(S32 val, S32 low, S32 high)
{
   return getMax(getMin(val, high), low);
}

inline F32 mClampF(F32 val, F32 low, F32 high)
{
   return (F32) getMax(getMin(val, high), low);
}

namespace Torque
{
   class NetAddress;
}

/// A convenience class to manipulate a set of bits.
///
/// Notice that bits are accessed directly, ie, by passing
/// a variable with the relevant bit set or not, instead of
/// passing the index of the relevant bit.
class BitSet32
{
private:
   /// Internal representation of bitset.
   U32 mbits;

public:
   BitSet32()                         { mbits = 0; }
   BitSet32(const BitSet32& in_rCopy) { mbits = in_rCopy.mbits; }
   BitSet32(const U32 in_mask)        { mbits = in_mask; }

   operator U32() const               { return mbits; }
   U32 getMask() const                { return mbits; }

   /// Set all bits to true.
   void set()                         { mbits  = 0xFFFFFFFFUL; }

   /// Set the specified bit(s) to true.
   void set(const U32 m)              { mbits |= m; }

   /// Masked-set the bitset; ie, using s as the mask and then setting the masked bits
   /// to b.
   void set(BitSet32 s, bool b)       { mbits = (mbits&~(s.mbits))|(b?s.mbits:0); }

   /// Clear all bits.
   void clear()                       { mbits  = 0; }

   /// Clear the specified bit(s).
   void clear(const U32 m)            { mbits &= ~m; }

   /// Toggle the specified bit(s).
   void toggle(const U32 m)           { mbits ^= m; }

   /// Are any of the specified bit(s) set?
   bool test(const U32 m) const       { return (mbits & m) != 0; }

   /// Are ALL the specified bit(s) set?
   bool testStrict(const U32 m) const { return (mbits & m) == m; }

   /// @name Operator Overloads
   /// @{
   BitSet32& operator =(const U32 m)  { mbits  = m;  return *this; }
   BitSet32& operator|=(const U32 m)  { mbits |= m; return *this; }
   BitSet32& operator&=(const U32 m)  { mbits &= m; return *this; }
   BitSet32& operator^=(const U32 m)  { mbits ^= m; return *this; }

   BitSet32 operator|(const U32 m) const { return BitSet32(mbits | m); }
   BitSet32 operator&(const U32 m) const { return BitSet32(mbits & m); }
   BitSet32 operator^(const U32 m) const { return BitSet32(mbits ^ m); }
   /// @}
};

class HuffmanProcessor;

template<typename T> struct Point3TPL
{
   union
   {
	  T x;
     T red;
   };
   union
   {
     T y;
     T green;
   };
   union
   {
     T z;
     T blue;
   };

	Point3TPL<T>() : x(0), y(0), z(0) {;}
	Point3TPL<T>(T _x, T _y, T _z) : x(_x), y(_y), z(_z) { ; }

	inline Point3TPL<T> operator-(const Point3TPL<T>& in) const
	{
		return Point3TPL<T>(x - in.x, y - in.y, z - in.z);
	}

	inline Point3TPL<T> operator-=(const Point3TPL<T>& in)
	{
		x -= in.x;
		y -= in.y;
		z -= in.z;
		return *this;
	}

   inline void set(T _x, T _y, T _z)
   {
      x = _x;
      y = _y;
      z = _z;
   }

	inline F32 len()
	{
		return mSqrt(x*x + y*y + z*z);
	}

   void normalize()
   {
      F32 squared = F32(x)*F32(x) + F32(y)*F32(y) + F32(z)*F32(z);
      if (squared != 0.0f) {
         F32 factor = 1.0f / mSqrt(squared);
         x = F32(x) * factor;
         y = F32(y) * factor;
         z = F32(z) * factor;
      } else {
         x = 0;
         y = 0;
         z = 1;
      }
   }
};

typedef Point3TPL<F32> Point3F;
typedef Point3TPL<S32> Point3I;


template<typename T> struct Point4TPL
{
   union
   {
     T x;
     T red;
   };
   union
   {
     T y;
     T green;
   };
   union
   {
     T z;
     T blue;
   };
   union
   {
     T w;
     T alpha;
   };

   Point4TPL<T>() : x(0), y(0), z(0), w(0) {;}
   Point4TPL<T>(T _x, T _y, T _z, T _w) : x(_x), y(_y), z(_z), w(_w) { ; }

   inline Point4TPL<T> operator-(const Point4TPL<T>& in) const
   {
      return Point4TPL<T>(x - in.x, y - in.y, z - in.z, w - in.w);
   }

   inline Point4TPL<T> operator-=(const Point4TPL<T>& in)
   {
      x -= in.x;
      y -= in.y;
      z -= in.z;
      w -= in.w;
      return *this;
   }

   inline void set(T _x, T _y, T _z, T _w=1)
   {
      x = _x;
      y = _y;
      z = _z;
      w = _w;
   }

   inline F32 len()
   {
      return mSqrt(x*x + y*y + z*z + w*w);
   }

   void normalize()
   {
      F32 squared = F32(x)*F32(x) + F32(y)*F32(y) + F32(z)*F32(z) + F32(w)*F32(w);
      if (squared != 0.0f) {
         F32 factor = 1.0f / mSqrt(squared);
         x = F32(x) * factor;
         y = F32(y) * factor;
         z = F32(z) * factor;
         w = F32(w) * factor;
      } else {
         x = 0;
         y = 0;
         z = 0;
         w = 1;
      }
   }

   template<typename Y> Point4TPL<T> operator=( const Point4TPL<Y> & inv )
   {
       Point4TPL<T> out;
       out.x = inv.x;
       out.y = inv.y;
       out.z = inv.z;
       out.w = inv.w;
       return out;
   }
};

inline void m_quatF_set_matF_C( F32 x, F32 y, F32 z, F32 w, F32* m )
{
#define qidx(r,c) (r*4 + c)
      F32 xs = x * 2.0f;
      F32 ys = y * 2.0f;
      F32 zs = z * 2.0f;
      F32 wx = w * xs;
      F32 wy = w * ys;
      F32 wz = w * zs;
      F32 xx = x * xs;
      F32 xy = x * ys;
      F32 xz = x * zs;
      F32 yy = y * ys;
      F32 yz = y * zs;
      F32 zz = z * zs;
      m[qidx(0,0)] = 1.0f - (yy + zz);
      m[qidx(1,0)] = xy - wz;
      m[qidx(2,0)] = xz + wy;
      m[qidx(3,0)] = 0.0f;
      m[qidx(0,1)] = xy + wz;
      m[qidx(1,1)] = 1.0f - (xx + zz);
      m[qidx(2,1)] = yz - wx;
      m[qidx(3,1)] = 0.0f;
      m[qidx(0,2)] = xz - wy;
      m[qidx(1,2)] = yz + wx;
      m[qidx(2,2)] = 1.0f - (xx + yy);
      m[qidx(3,2)] = 0.0f;

      m[qidx(0,3)] = 0.0f;
      m[qidx(1,3)] = 0.0f;
      m[qidx(2,3)] = 0.0f;
      m[qidx(3,3)] = 1.0f;
#undef qidx
}


struct MatrixF
{
   F32 m[16];
   operator F32*() { return (m); }
   operator const F32*() const { return (F32*)(m); }

   void identity()
   {
      m[0] = 1;
      m[1] = 0;
      m[2] = 0;
      m[3] = 0;
      
      m[4] = 0;
      m[5] = 1;
      m[6] = 0;
      m[7] = 0;
      
      m[8] = 0;
      m[9] = 0;
      m[10] = 1;
      m[11] = 0;

      m[12] = 0;
      m[13] = 0;
      m[14] = 0;
      m[15] = 1;
   }

   void setColumn(S32 col, const Point3F& cptr)
   {
      m[col]   = cptr.x;
      m[col+4] = cptr.y;
      m[col+8] = cptr.z;
   }
};


struct QuatF : public Point4TPL<F32>
{
   MatrixF * setMatrix( MatrixF * mat ) const
   {
      if( x*x + y*y + z*z < 10E-20f) // isIdentity() -- substituted code a little more stringent but a lot faster
         mat->identity();
      else
         m_quatF_set_matF_C( x, y, z, w, *mat );
      return mat;
   }
};

typedef Point4TPL<F32> Point4F;
typedef Point4TPL<S32> Point4I;

struct ColorI;

struct ColorF : public Point4TPL<F32>
{
   ColorF() : Point4TPL<F32>(0,0,0,1) {;}
   ColorF(F32 _x, F32 _y, F32 _z, F32 _w) : Point4TPL<F32>(_x,_y,_z,_w)  { ; }
   inline operator ColorI() const;
};

struct ColorI : public Point4TPL<U8>
{
   ColorI() : Point4TPL<U8>(0,0,0,255) {;}
   ColorI(U8 _x, U8 _y, U8 _z, U8 _w) : Point4TPL<U8>(_x,_y,_z,_w) { ; }
   inline operator ColorF() const;
};

inline ColorF::operator ColorI() const
{
   return ColorI(U8(red   * 255.0f + 0.5),
                  U8(green * 255.0f + 0.5),
                  U8(blue  * 255.0f + 0.5),
                  U8(alpha * 255.0f + 0.5));
}

inline ColorI::operator ColorF() const
{
   const F32 inv255 = 1.0f / 255.0f;

   return ColorF(F32(red)   * inv255,
                 F32(green) * inv255,
                 F32(blue)  * inv255,
                 F32(alpha) * inv255);
}

template<typename T> struct Point2TPL
{
   T x,y;

   Point2TPL<T>() : x(0), y(0) {;}
   Point2TPL<T>(T _x, T _y) : x(_x), y(_y) { ; }

   inline Point2TPL<T> operator-(const Point2TPL<T>& in) const
   {
      return Point2TPL<T>(x - in.x, y - in.y);
   }

   inline Point2TPL<T> operator-=(const Point2TPL<T>& in)
   {
      x -= in.x;
      y -= in.y;
      return *this;
   }

   inline void set(T _x, T _y)
   {
      x = _x;
      y = _y;
   }

   inline F32 len()
   {
      return mSqrt(x*x + y*y);
   }

   void normalize()
   { 
      F32 factor = 1.0f / mSqrt(x*x + y*y );
      x *= factor;
      y *= factor;
   }
};

typedef Point2TPL<F32> Point2F;
typedef Point2TPL<S32> Point2I;

template<typename T> struct RectTPL
{
   Point2TPL<T> point;
   Point2TPL<T> extent;
};

typedef RectTPL<F32> RectF;
typedef RectTPL<S32> RectI;

class Stream
{
   // Public structs and enumerations...
  public:
     /// Status constantants for the stream
   enum Status {
      Ok = 0,           ///< Ok!
      IOError,          ///< Read or Write error
      EOS,              ///< End of Stream reached (mostly for reads)
      IllegalCall,      ///< An unsupported operation used.  Always w/ accompanied by AssertWarn
      Closed,           ///< Tried to operate on a closed stream (or detached filter)
      UnknownError      ///< Catchall
   };

   enum Capability {
      StreamWrite    = BIT(0), ///< Can this stream write?
      StreamRead     = BIT(1), ///< Can this stream read?
      StreamPosition = BIT(2)  ///< Can this stream position?
   };

   // Accessible only through inline accessors
  private:
   Status m_streamStatus;


  public:

   virtual bool _read (const U32 size,void* d) = 0;
   virtual bool _write(const U32 size,const void* d) = 0;

   // Overloaded write and read ops..
  public:
   bool read(const U32 in_numBytes,  void* out_pBuffer) {
      return _read(in_numBytes, out_pBuffer);
   }
   bool write(const U32 in_numBytes, const void* in_pBuffer) {
      return _write(in_numBytes, in_pBuffer);
   }

   bool write(const ColorI& rColor)
   {
      bool success = write(rColor.red);
      success     |= write(rColor.green);
      success     |= write(rColor.blue);
      success     |= write(rColor.alpha);

      return success;
   }

   bool write(const ColorF& rColor)
   {
     ColorI temp = rColor;
     return write(temp);
   }

   bool read(ColorI* pColor)
   {
      bool success = read(&pColor->red);
      success     |= read(&pColor->green);
      success     |= read(&pColor->blue);
      success     |= read(&pColor->alpha);

      return success;
   }

   bool read(ColorF* pColor)
   {
      ColorI temp;
      bool success = read(&temp);

      *pColor = temp;
      return success;
   }


   DECLARE_OVERLOADED_WRITE(S8)
   DECLARE_OVERLOADED_WRITE(U8)

   DECLARE_ENDIAN_OVERLOADED_WRITE(S16)
   DECLARE_ENDIAN_OVERLOADED_WRITE(S32)
   DECLARE_ENDIAN_OVERLOADED_WRITE(U16)
   DECLARE_ENDIAN_OVERLOADED_WRITE(U32)
   DECLARE_ENDIAN_OVERLOADED_WRITE(F32)

   DECLARE_OVERLOADED_READ(S8)
   DECLARE_OVERLOADED_READ(U8)

   DECLARE_ENDIAN_OVERLOADED_READ(S16)
   DECLARE_ENDIAN_OVERLOADED_READ(S32)
   DECLARE_ENDIAN_OVERLOADED_READ(U16)
   DECLARE_ENDIAN_OVERLOADED_READ(U32)
   DECLARE_ENDIAN_OVERLOADED_READ(F32)

   bool read(bool* out_pRead) {
      U8 translate;
      bool success = read(&translate);
      if (success == false)
         return false;

      *out_pRead = translate != 0;
      return true;
   }
   bool write(const bool& in_rWrite) {
      U8 translate = in_rWrite ? U8(1) : U8(0);
      return write(translate);
   }
};

typedef const char* StringTableEntry;

// should be 52 bytes
class BitStream : public Stream
{
public:
   U8 *dataPtr;
   S32  bitNum;
   S32  bufSize;
   bool error;
   S32  maxReadBitNum;
   S32  maxWriteBitNum;
   char *stringBuffer;
   bool mCompressRelative;
   Point3F mCompressPoint;

   friend class HuffmanProcessor;
public:
   static BitStream *getPacketStream(U32 writeSize = 0);
   static void sendPacketStream(const Torque::NetAddress *addr);

   void setBuffer(void *bufPtr, S32 bufSize, S32 maxSize = 0);
   U8*  getBuffer() { return dataPtr; }
   U8*  getBytePtr();

   U32 getReadByteSize();

   S32  getCurPos() const;
   void setCurPos(const U32);

   BitStream(void *bufPtr, S32 bufSize, S32 maxWriteSize = -1) { setBuffer(bufPtr, bufSize,maxWriteSize); stringBuffer = NULL; }
   void clear();

   inline void setStringBuffer(char buffer[256]);
   void writeInt(S32 value, S32 bitCount);
   S32  readInt(S32 bitCount);

   /// Use this method to write out values in a concise but ass backwards way...
   /// Good for values you expect to be frequently zero, often small. Worst case
   /// this will bloat values by nearly 20% (5 extra bits!) Best case you'll get
   /// one bit (if it's zero).
   ///
   /// This is not so much for efficiency's sake, as to make life painful for
   /// people that want to reverse engineer our network or file formats.
   void writeCussedU32(U32 val)
   {
      // Is it zero?
      if(writeFlag(val == 0))
         return;

      if(writeFlag(val <= 0xF)) // 4 bit
         writeRangedU32(val, 0, 0xF);
      else if(writeFlag(val <= 0xFF)) // 8 bit
         writeRangedU32(val, 0, 0xFF);
      else if(writeFlag(val <= 0xFFFF)) // 16 bit
         writeRangedU32(val, 0, 0xFFFF);
      else if(writeFlag(val <= 0xFFFFFF)) // 24 bit
         writeRangedU32(val, 0, 0xFFFFFF);
      else
         writeRangedU32(val, 0, 0xFFFFFFFF);
   }

   U32 readCussedU32()
   {
      if(readFlag())
         return 0;

      if(readFlag())
         return readRangedU32(0, 0xF);
      else if(readFlag())
         return readRangedU32(0, 0xFF);
      else if(readFlag())
         return readRangedU32(0, 0xFFFF);
      else if(readFlag())
         return readRangedU32(0, 0xFFFFFF);
      else
         return readRangedU32(0, 0xFFFFFFFF);
   }

   void writeSignedInt(S32 value, S32 bitCount);
   S32  readSignedInt(S32 bitCount);

   void writeRangedU32(U32 value, U32 rangeStart, U32 rangeEnd);
   U32  readRangedU32(U32 rangeStart, U32 rangeEnd);
   
   /// Writes a clamped signed integer to the stream using 
   /// an optimal amount of bits for the range.
   void writeRangedS32( S32 value, S32 min, S32 max );

   /// Reads a ranged signed integer written with writeRangedS32.
   S32 readRangedS32( S32 min, S32 max );

   // read and write floats... floats are 0 to 1 inclusive, signed floats are -1 to 1 inclusive

   F32  readFloat(S32 bitCount);
   F32  readSignedFloat(S32 bitCount);

   void writeFloat(F32 f, S32 bitCount);
   void writeSignedFloat(F32 f, S32 bitCount);

   /// Writes a clamped floating point value to the 
   /// stream with the desired bits of precision.
   void writeRangedF32( F32 value, F32 min, F32 max, U32 numBits );

   /// Reads a ranged floating point value written with writeRangedF32.
   F32 readRangedF32( F32 min, F32 max, U32 numBits );

   void writeClassId(U32 classId, U32 classType, U32 classGroup);
   S32 readClassId(U32 classType, U32 classGroup); // returns -1 if the class type is out of range

   // writes a normalized vector
   void writeNormalVector(const Point3F& vec, S32 bitCount);
   void readNormalVector(Point3F *vec, S32 bitCount);

   void clearCompressionPoint();
   void setCompressionPoint(const Point3F& p);

   // Matching calls to these compression methods must, of course,
   // have matching scale values.
   void writeCompressedPoint(const Point3F& p,F32 scale = 0.01f);
   void readCompressedPoint(Point3F* p,F32 scale = 0.01f);

   // Uses the above method to reduce the precision of a normal vector so the server can
   //  determine exactly what is on the client.  (Pre-dumbing the vector before sending
   //  to the client can result in precision errors...)
   static Point3F dumbDownNormal(const Point3F& vec, S32 bitCount);


   // writes a normalized vector using alternate method
   void writeNormalVector(const Point3F& vec, S32 angleBitCount, S32 zBitCount);
   void readNormalVector(Point3F *vec, S32 angleBitCount, S32 zBitCount);

   void readVector(Point3F * vec, F32 minMag, F32 maxMag, S32 magBits, S32 angleBits, S32 zBits);
   void writeVector(Point3F vec, F32 minMag, F32 maxMag, S32 magBits, S32 angleBits, S32 zBits);

   virtual void writeBits(S32 bitCount, const void *bitPtr);
   virtual void readBits(S32 bitCount, void *bitPtr);
   virtual bool writeFlag(bool val);
   virtual bool readFlag();

   void writeAffineTransform(const MatrixF&);
   void readAffineTransform(MatrixF*);

   void setBit(S32 bitCount, bool set);
   bool testBit(S32 bitCount);

   bool isFull() { return bitNum > (bufSize << 3); }
   bool isValid() { return !error; }

   bool _read (const U32 size,void* d);
   bool _write(const U32 size,const void* d);

   void readString(char stringBuf[256]);
   void writeString(const char *stringBuf, S32 maxLen=255);

   bool hasCapability(const Capability) const { return true; }
   U32  getPosition() const;
   bool setPosition(const U32 in_newPosition);
   U32  getStreamSize();

   static void copyState(BitStream* in, BitStream* out)
   {
   	out->dataPtr = in->dataPtr;
   	out->bitNum = in->bitNum;
   	out->bufSize = in->bufSize;
   	out->error = in->error;
   	out->maxReadBitNum = in->maxReadBitNum;
   	out->maxWriteBitNum = in->maxWriteBitNum;
   	out->stringBuffer = in->stringBuffer;
   	out->mCompressRelative = in->mCompressRelative;
   	out->mCompressPoint = in->mCompressPoint;
   }

   StringTableEntry readSTString();
};

//------------------------------------------------------------------------------
//-------------------------------------- INLINES
//
inline S32 BitStream::getCurPos() const
{
   return bitNum;
}

inline void BitStream::setCurPos(const U32 in_position)
{
   AssertFatal(in_position < (U32)(bufSize << 3), "Out of range bitposition");
   bitNum = S32(in_position);
}

inline bool BitStream::readFlag()
{
   if(bitNum > maxReadBitNum)
   {
      error = true;
      AssertFatal(false, "Out of range read");
      return false;
   }
   S32 mask = 1 << (bitNum & 0x7);
   bool ret = (*(dataPtr + (bitNum >> 3)) & mask) != 0;
   bitNum++;
   return ret;
}

// bitstream utility functions

inline void BitStream::setStringBuffer(char buffer[256])
{
   stringBuffer = buffer;
}

typedef BitStream*  (*fpGetPacketStream)(void);

inline BitStream *BitStream::getPacketStream(U32 writeSize)
{
	// hook into orig ver
	// @ 0x000adc02
	const fpGetPacketStream _func = (fpGetPacketStream)0x53780;
	return _func();
}

typedef const char*  (*fpSendPacketStream)(const Torque::NetAddress*);

inline void BitStream::sendPacketStream(const Torque::NetAddress *addr)
{
	// hook into orig ver
	const fpSendPacketStream _func = (fpSendPacketStream)0x53aa0;
	_func(addr);
}

// FIXMEFIXMEFIXME MATH

inline bool IsEqual(F32 a, F32 b) { return a == b; }


class HuffmanProcessor
{
   static const U32 csm_charFreqs[256];
   bool   m_tablesBuilt;

   void buildTables();

   struct HuffNode {
      U32 pop;

      S16 index0;
      S16 index1;
   };
   struct HuffLeaf {
      U32 pop;

      U8  numBits;
      U8  symbol;
      U32 code;   // no code should be longer than 32 bits.
   };
   // We have to be a bit careful with these, mSince they are pointers...
   struct HuffWrap {
      HuffNode* pNode;
      HuffLeaf* pLeaf;

     public:
      HuffWrap() : pNode(NULL), pLeaf(NULL) { }

      void set(HuffLeaf* in_leaf) { pNode = NULL; pLeaf = in_leaf; }
      void set(HuffNode* in_node) { pLeaf = NULL; pNode = in_node; }

      U32 getPop() { if (pNode) return pNode->pop; else return pLeaf->pop; }
   };

   std::vector<HuffNode> m_huffNodes;
   std::vector<HuffLeaf> m_huffLeaves;

   S16 determineIndex(HuffWrap&);

   void generateCodes(BitStream&, S32, S32);

  public:
   HuffmanProcessor() : m_tablesBuilt(false) { }

   static HuffmanProcessor g_huffProcessor;

   bool readHuffBuffer(BitStream* pStream, char* out_pBuffer);
   bool writeHuffBuffer(BitStream* pStream, const char* out_pBuffer, S32 maxLen);
};


//-----------------------------------------------------------------------------

enum NetClassTypes {
   NetClassTypeObject = 0,
   NetClassTypeDataBlock,
   NetClassTypeEvent,
   NetClassTypesCount,
};

//-----------------------------------------------------------------------------

enum NetClassGroups {
   NetClassGroupGame = 0,
   NetClassGroupCommunity,
   NetClassGroup3,
   NetClassGroup4,
   NetClassGroupsCount,
};
extern U32 __NetClassBitSize[NetClassGroupsCount][NetClassTypesCount];


enum SimObjectsConstants
{
   DataBlockObjectIdFirst = 3,
   DataBlockObjectIdBitSize = 14, // NOTE: internal = 13, 0.9 = 14!!
   DataBlockObjectIdLast = DataBlockObjectIdFirst + (1 << DataBlockObjectIdBitSize) - 1,

   DynamicObjectIdFirst = DataBlockObjectIdLast + 1,
   InvalidEventId = 0,
   RootGroupId = 0xFFFFFFFF,
};


inline char* dStrcpyl(char *dst, size_t dstSize, ...)
{
   const char* src;
   char *p = dst;

   AssertFatal(dstSize > 0, "dStrcpyl: destination size is set zero");
   dstSize--;  // leave room for string termination

   va_list args;
   va_start(args, dstSize);

   // concatenate each src to end of dst
   while ( (src = va_arg(args, const char*)) != NULL )
   {
      while( dstSize && *src )
      {
         *p++ = *src++;
         dstSize--;   
      }
   }

   va_end(args);

   // make sure the string is terminated 
   *p = 0;

   return dst;
}

struct SimpleVec
{
   U32 mElementCount;
   U32 mArraySize;
   void* mArray;
};

template<typename T> struct Vector
{
   U32 mElementCount;
   U32 mArraySize;
   //void* mArray;

   std::vector<T> *impl;

   Vector<T>()
   {
      impl = new std::vector<T>();
   }

   ~Vector<T>()
   {
      delete impl;
   }

   void setSize(uint32_t size)
   {
      impl->resize(size);
   }

   S32 size() const { return impl->size(); }
   bool empty() const { return impl->empty(); }

   T&       front() { return impl->front(); }
   const T& front() const  { return impl->front(); }
   T&       back()  { return impl->back(); }
   const T& back() const  { return impl->back(); }

   void push_front(const T&a) { return impl->push_front(a); }
   void push_back(const T&a) { return impl->push_back(a); }
   void pop_front() { return impl->pop_front(); }
   void pop_back() { return impl->pop_back(); }

   T& operator[](U32 a) { return (*impl)[a]; }
   const T& operator[](U32 a) const { return (*impl)[a]; }

   T& operator[](S32 i)              { return operator[](U32(i)); }
   const T& operator[](S32 i ) const { return operator[](U32(i)); }

   void reserve(U32 i) { return impl->reserve(i); }
};



inline bool mathRead(Stream& stream, Point2I* p)
{
   bool success = stream.read(&p->x);
   success     &= stream.read(&p->y);
   return success;
}

inline bool mathRead(Stream& stream, Point3I* p)
{
   bool success = stream.read(&p->x);
   success     &= stream.read(&p->y);
   success     &= stream.read(&p->z);
   return success;
}

inline bool mathRead(Stream& stream, Point2F* p)
{
   bool success = stream.read(&p->x);
   success     &= stream.read(&p->y);
   return success;
}

inline bool mathRead(Stream& stream, Point3F* p)
{
   bool success = stream.read(&p->x);
   success     &= stream.read(&p->y);
   success     &= stream.read(&p->z);
   return success;
}

inline bool mathRead(Stream& stream, Point4F* p)
{
   bool success = stream.read(&p->x);
   success     &= stream.read(&p->y);
   success     &= stream.read(&p->z);
   success     &= stream.read(&p->w);
   return success;
}

inline bool mathRead(Stream& stream, RectI* r)
{
   bool success = mathRead(stream, &r->point);
   success     &= mathRead(stream, &r->extent);
   return success;
}

inline bool mathRead(Stream& stream, RectF* r)
{
   bool success = mathRead(stream, &r->point);
   success     &= mathRead(stream, &r->extent);
   return success;
}

inline bool mathRead(Stream& stream, MatrixF* m)
{
   bool success = true;
   F32* pm    = *m;
   for (U32 i = 0; i < 16; i++)
      success &= stream.read(&pm[i]);
   return success;
}

inline bool mathRead(Stream& stream, QuatF* q)
{
   bool success = stream.read(&q->x);
   success     &= stream.read(&q->y);
   success     &= stream.read(&q->z);
   success     &= stream.read(&q->w);
   return success;
}


//#undef DECLARE_OVERLOADED_READ
//#undef DECLARE_OVERLOADED_WRITE
//#undef DECLARE_ENDIAN_OVERLOADED_READ
//#undef DECLARE_ENDIAN_OVERLOADED_WRITE

