/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "common.h"
#include "dbc_dump/dataBlocks.h"

#include "simKlass.h"
#include "jansson.h"

#include "math/mAngAxis.h"
#include "math/mMatrix.h"
#include "math/mQuat.h"
#include "ts/tsShape.h"
#include "ts/tsMaterial.h"
#include "ts/tsMaterialManager.h"
#include "ts/tsRenderState.h"


ShapeBaseData gShapeBaseDataProto;

KlassFieldInfo<AIPetData> klAIPetData("AIPetData");
KlassFieldInfo<MountablePlayerData> klMountablePlayerData("MountablePlayerData");
KlassFieldInfo<PowerupData> klPowerupData("PowerupData");
KlassFieldInfo<ShoulderPetData> klShoulderPetData("ShoulderPetData");
KlassFieldInfo<ToolImageData> klToolImageData("ToolImageData");
KlassFieldInfo<HomePointData> klHomePointData("HomePointData");
KlassFieldInfo<InteractiveObjectData> klInteractiveObjectData("InteractiveObjectData");
KlassFieldInfo<ClothingData> klClothingData("ClothingData");
KlassFieldInfo<sgLightObjectData> klsgLightObjectData("sgLightObjectData");
KlassFieldInfo<ParticleData> klParticleData("ParticleData");
KlassFieldInfo<ParticleEmitterData> klParticleEmitterData("ParticleEmitterData");
KlassFieldInfo<TSShapeConstructor> klTSShapeConstructor("TSShapeConstructor");
KlassFieldInfo<DecalData> klDecalData("DecalData");
KlassFieldInfo<WheeledVehicleData> klWheeledVehicleData("WheeledVehicleData");
KlassFieldInfo<WheeledVehicleSpring> klWheeledVehicleSpring("WheeledVehicleSpring");
KlassFieldInfo<WheeledVehicleTire> klWheeledVehicleTire("WheeledVehicleTire");
KlassFieldInfo<HoverVehicleData> klHoverVehicleData("HoverVehicleData");
KlassFieldInfo<FlyingVehicleData> klFlyingVehicleData("FlyingVehicleData");
KlassFieldInfo<TriggerData> klTriggerData("TriggerData");
KlassFieldInfo<StaticShapeData> klStaticShapeData("StaticShapeData");
KlassFieldInfo<ShapeBaseImageData> klShapeBaseImageData("ShapeBaseImageData");
KlassFieldInfo<ShapeBaseData> klShapeBaseData("ShapeBaseData");
KlassFieldInfo<ProjectileData> klProjectileData("ProjectileData");
KlassFieldInfo<PlayerData> klPlayerData("PlayerData");
KlassFieldInfo<ItemData> klItemData("ItemData");
KlassFieldInfo<GameBaseData> klGameBaseData("GameBaseData");
KlassFieldInfo<WeatherLightningData> klWeatherLightningData("WeatherLightningData");
KlassFieldInfo<SplashData> klSplashData("SplashData");
KlassFieldInfo<PrecipitationData> klPrecipitationData("PrecipitationData");
KlassFieldInfo<ParticleEmitterNodeData> klParticleEmitterNodeData("ParticleEmitterNodeData");
KlassFieldInfo<LightningData> klLightningData("LightningData");
KlassFieldInfo<fxLightData> klfxLightData("fxLightData");
KlassFieldInfo<ExplosionData> klExplosionData("ExplosionData");
KlassFieldInfo<DebrisData> klDebrisData("DebrisData");
KlassFieldInfo<CameraData> klCameraData("CameraData");
KlassFieldInfo<AIPlayerData> klAIPlayerData("AIPlayerData");
KlassFieldInfo<AudioProfile> klAudioProfile("AudioProfile");
KlassFieldInfo<AudioDescription> klAudioDescription("AudioDescription");
KlassFieldInfo<MissionMarkerData> klMissionMarkerData("MissionMarkerData");


//

const char* packetNames[] = {
   "Data",
   "Ping",
   "Ack"
};

enum PacketTypes
{
   DataPacket,
   PingPacket,
   AckPacket,
   InvalidPacketType,
};

class SimDataBlock;

// ClientEdit* versions of normal objects have : <normal update> <ClientEditSceneObject<kind>::unpackUpdate>
// Datablocks are EXACTLY THE SAME

PacketReadClass sReadClasses[] = {
   13, NetClassTypeEvent, "GeneralTransmitField", NULL,
   7, NetClassTypeEvent, "ClientTransmitPersistField", NULL,
   6, NetClassTypeObject, "ClientEditAIPlayerObject", NULL,
   26, NetClassTypeEvent, "UpdateClientAIPlayerPersistField", NULL,
   52, NetClassTypeObject, "MountablePlayer", NULL,
   21, NetClassTypeDataBlock, "MountablePlayerData", &doReadFuncTPL<MountablePlayerData>,
   18, NetClassTypeObject, "ClientEditLightning", NULL,
   38, NetClassTypeObject, "GameScriptObject", NULL,
   12, NetClassTypeObject, "ClientEditGameScriptObject", NULL,
   27, NetClassTypeEvent, "UpdateClientGameScriptPersistField", NULL,
   39, NetClassTypeObject, "GiftItem", NULL,
   28, NetClassTypeDataBlock, "PowerupData", &doReadFuncTPL<PowerupData>,
   5, NetClassTypeObject, "CameraBlocker", NULL,
   59, NetClassTypeObject, "PlayerBlocker", NULL,
   31, NetClassTypeObject, "ClientEditVehicleBlocker", NULL,
   9, NetClassTypeObject, "ClientEditCameraBlocker", NULL,
   20, NetClassTypeObject, "ClientEditPlayerBlocker", NULL,
   8, NetClassTypeObject, "ClientEditCamera", NULL,
   22, NetClassTypeObject, "ClientEditPrecipitation", NULL,
   40, NetClassTypeObject, "GuidedProjectile", NULL,
   73, NetClassTypeObject, "ShoulderPet", NULL,
   35, NetClassTypeDataBlock, "ShoulderPetData", &doReadFuncTPL<ShoulderPetData>,
   0, NetClassTypeObject, "AIPet", NULL,
   0, NetClassTypeDataBlock, "AIPetData", &doReadFuncTPL<AIPetData>,
   36, NetClassTypeObject, "GameArea", NULL,
   11, NetClassTypeObject, "ClientEditGameArea", NULL,
   40, NetClassTypeDataBlock, "ToolImageData", &doReadFuncTPL<ToolImageData>,
   30, NetClassTypeObject, "ClientEditTrigger", NULL,
   79, NetClassTypeObject, "StoreArea", NULL,
   69, NetClassTypeObject, "PurchaseWheeledVehicle", NULL,
   66, NetClassTypeObject, "PurchaseMountablePlayer", NULL,
   67, NetClassTypeObject, "PurchaseShoulderPet", NULL,
   63, NetClassTypeObject, "PurchaseAIPet", NULL,
   68, NetClassTypeObject, "PurchaseToolObject", NULL,
   64, NetClassTypeObject, "PurchaseClothingObject", NULL,
   65, NetClassTypeObject, "PurchaseInteractiveObject", NULL,
   25, NetClassTypeObject, "ClientEditStoreArea", NULL,
   14, NetClassTypeObject, "ClientEditHomePoint", NULL,
   13, NetClassTypeObject, "ClientEditHomeArea", NULL,
   42, NetClassTypeObject, "HomePoint", NULL,
   15, NetClassTypeDataBlock, "HomePointData", &doReadFuncTPL<HomePointData>,
   41, NetClassTypeObject, "HomeArea", NULL,
   28, NetClassTypeEvent, "UpdateClientInteractivePersistField", NULL,
   15, NetClassTypeObject, "ClientEditInteractiveObject", NULL,
   7, NetClassTypeDataBlock, "ClientEditInteractiveObjectData", &doReadFuncTPL<InteractiveObjectData>,
   44, NetClassTypeObject, "InteractiveObject", NULL,
   17, NetClassTypeDataBlock, "InteractiveObjectData", &doReadFuncTPL<InteractiveObjectData>,
   29, NetClassTypeEvent, "UpdatePersistField", NULL,
   27, NetClassTypeObject, "ClientEditSunLight", NULL,
   19, NetClassTypeObject, "ClientEditParticleEmitterNode", NULL,
   17, NetClassTypeObject, "ClientEditLightObject", NULL,
   7, NetClassTypeObject, "ClientEditAudioEmitter", NULL,
   60, NetClassTypeObject, "PoiPoint", NULL,
   21, NetClassTypeObject, "ClientEditPoiPoint", NULL,
   29, NetClassTypeObject, "ClientEditTerrainBlock", NULL,
   3, NetClassTypeEvent, "ClientTerrainAckStart", NULL,
   5, NetClassTypeEvent, "ClientTerrainFilesChanged", NULL,
   6, NetClassTypeEvent, "ClientTerrainTransactionData", NULL,
   4, NetClassTypeEvent, "ClientTerrainBeginTransaction", NULL,
   8, NetClassTypeDataBlock, "ClothingData", &doReadFuncTPL<ClothingData>,
   2, NetClassTypeEvent, "ClientObjectTransformChanged", NULL,
   32, NetClassTypeObject, "ClientEditWaterBlock", NULL,
   28, NetClassTypeObject, "ClientEditTSStatic", NULL,
   26, NetClassTypeObject, "ClientEditSun", NULL,
   24, NetClassTypeObject, "ClientEditSky", NULL,
   23, NetClassTypeObject, "ClientEditShapeReplicator", NULL,
   16, NetClassTypeObject, "ClientEditInteriorInstance", NULL,
   10, NetClassTypeObject, "ClientEditFoliageReplicator", NULL,
   91, NetClassTypeObject, "fxRenderObject", NULL,
   99, NetClassTypeObject, "volumeLight", NULL,
   97, NetClassTypeObject, "sgMissionLightingFilter", NULL,
   48, NetClassTypeDataBlock, "sgMissionLightingFilterData", NULL,
   98, NetClassTypeObject, "sgUniversalStaticLight", NULL,
   49, NetClassTypeDataBlock, "sgUniversalStaticLightData", NULL,
   96, NetClassTypeObject, "sgLightObject", NULL,
   47, NetClassTypeDataBlock, "sgLightObjectData", &doReadFuncTPL<sgLightObjectData>,
   95, NetClassTypeObject, "sgDecalProjector", NULL,
   53, NetClassTypeObject, "ParticleEmitter", NULL,
   22, NetClassTypeDataBlock, "ParticleData", &doReadFuncTPL<ParticleData>,
   23, NetClassTypeDataBlock, "ParticleEmitterData", &doReadFuncTPL<ParticleEmitterData>,
   70, NetClassTypeObject, "RigidShape", NULL,
   31, NetClassTypeDataBlock, "RigidShapeData", NULL,
   39, NetClassTypeDataBlock, "TSShapeConstructor", &doReadFuncTPL<TSShapeConstructor>,
   85, NetClassTypeObject, "WaterBlock", NULL,
   82, NetClassTypeObject, "TerrainBlock", NULL,
   80, NetClassTypeObject, "Sun", NULL,
   75, NetClassTypeObject, "Sky", NULL,
   49, NetClassTypeObject, "Marker", NULL,
   17, NetClassTypeEvent, "PathManagerEvent", NULL,
   14, NetClassTypeEvent, "GhostAlwaysObjectEvent", NULL,
   11, NetClassTypeEvent, "FileNoneExistEvent", NULL,
   19, NetClassTypeEvent, "RequestResendFileChunkEvent", NULL,
   0, NetClassTypeEvent, "AckFileChunkEvent", NULL,
   9, NetClassTypeEvent, "FileChunkEvent", NULL,
   10, NetClassTypeEvent, "FileDownloadRequestEvent", NULL,
   12, NetClassTypeEvent, "FileVerifyCheckSum", NULL,
   8, NetClassTypeEvent, "ConnectionMessageEvent", NULL,
   10, NetClassTypeDataBlock, "DecalData", &doReadFuncTPL<DecalData>,
   16, NetClassTypeEvent, "NetStringEvent", NULL,
   26, NetClassTypeDataBlock,"PathedInteriorData", NULL,
   56, NetClassTypeDataBlock,"PathedInterior", NULL,
   46, NetClassTypeObject, "InteriorMap", NULL,
   45, NetClassTypeObject, "InteriorInstance", NULL,
   88, NetClassTypeObject, "WheeledVehicle", NULL,
   43, NetClassTypeDataBlock, "WheeledVehicleData", &doReadFuncTPL<WheeledVehicleData>,
   44, NetClassTypeDataBlock, "WheeledVehicleSpring", &doReadFuncTPL<WheeledVehicleSpring>,
   45, NetClassTypeDataBlock, "WheeledVehicleTire", &doReadFuncTPL<WheeledVehicleTire>,
   84, NetClassTypeObject, "VehicleBlocker", NULL,
   43, NetClassTypeObject, "HoverVehicle", NULL,
   16, NetClassTypeDataBlock,"HoverVehicleData", &doReadFuncTPL<HoverVehicleData>,
   35, NetClassTypeObject, "FlyingVehicle", NULL,
   13, NetClassTypeDataBlock,"FlyingVehicleData", &doReadFuncTPL<FlyingVehicleData>,
   81, NetClassTypeObject, "TSStatic", NULL,
   83, NetClassTypeObject, "Trigger", NULL,
   41, NetClassTypeDataBlock,"TriggerData", &doReadFuncTPL<TriggerData>,
   78, NetClassTypeObject, "StaticShape", NULL,
   38, NetClassTypeDataBlock, "StaticShapeData", &doReadFuncTPL<StaticShapeData>,
   34, NetClassTypeDataBlock, "ShapeBaseImageData", &doReadFuncTPL<ShapeBaseImageData>,
   72, NetClassTypeObject, "ShapeBase", NULL,
   33, NetClassTypeDataBlock, "ShapeBaseData", &doReadFuncTPL<ShapeBaseData>,
   71, NetClassTypeObject, "ScopeAlwaysShape", NULL,
   32, NetClassTypeDataBlock, "ScopeAlwaysShapeData", NULL,
   62, NetClassTypeObject, "Projectile", NULL,
   30, NetClassTypeDataBlock, "ProjectileData", &doReadFuncTPL<ProjectileData>,
   58, NetClassTypeObject, "Player", NULL,
   27, NetClassTypeDataBlock, "PlayerData", &doReadFuncTPL<PlayerData>,
   57, NetClassTypeObject, "PhysicalZone", NULL,
   55, NetClassTypeObject, "PathCamera", NULL,
   25, NetClassTypeDataBlock, "PathCameraData", NULL,
   74, NetClassTypeObject, "SimpleNetObject", NULL,
   25, NetClassTypeEvent, "SimpleMessageEvent", NULL,
   18, NetClassTypeEvent, "RemoteCommandEvent", NULL,
   76, NetClassTypeObject, "SpawnSphere", NULL,
   86, NetClassTypeObject, "WayPoint", NULL,
   51, NetClassTypeObject, "MissionMarker", NULL,
   20, NetClassTypeDataBlock, "MissionMarkerData", &doReadFuncTPL<MissionMarkerData>,
   50, NetClassTypeObject, "MissionArea", NULL,
   47, NetClassTypeObject, "Item", NULL,
   18, NetClassTypeDataBlock, "ItemData", &doReadFuncTPL<ItemData>,
   1,  NetClassTypeEvent, "ClientFurnitureTransformChanged", NULL,
   20, NetClassTypeEvent, "SetMissionCRCEvent", NULL,
   22, NetClassTypeEvent, "Sim3DAudioEvent", NULL,
   21, NetClassTypeEvent, "Sim2DAudioEvent", NULL,
   23, NetClassTypeEvent, "SimDataBlockAckEvent", NULL,
   24, NetClassTypeEvent, "SimDataBlockEvent", NULL,
   37, NetClassTypeObject, "GameBase", NULL,
   14, NetClassTypeDataBlock, "GameBaseData", &doReadFuncTPL<GameBaseData>,
   87, NetClassTypeObject, "WeatherLightning", NULL,
   42, NetClassTypeDataBlock, "WeatherLightningData", &doReadFuncTPL<WeatherLightningData>,
   30, NetClassTypeEvent, "WeatherLightningStrikeEvent", NULL,
   77, NetClassTypeObject, "Splash", NULL,
   37, NetClassTypeDataBlock, "SplashData", &doReadFuncTPL<SplashData>,
   29, NetClassTypeDataBlock, "PrecipitationData", &doReadFuncTPL<PrecipitationData>,
   61, NetClassTypeObject, "Precipitation", NULL,
   54, NetClassTypeObject, "ParticleEmitterNode", NULL,
   24, NetClassTypeDataBlock, "ParticleEmitterNodeData", &doReadFuncTPL<ParticleEmitterNodeData>,
   15, NetClassTypeEvent, "LightningStrikeEvent", NULL,
   48, NetClassTypeObject, "Lightning", NULL,
   19, NetClassTypeDataBlock, "LightningData", &doReadFuncTPL<LightningData>,
   94, NetClassTypeObject, "fxSunLight", NULL,
   92, NetClassTypeObject, "fxShapeReplicatedStatic", NULL,
   93, NetClassTypeObject, "fxShapeReplicator", NULL,
   90, NetClassTypeObject, "fxLight", NULL,
   46, NetClassTypeDataBlock, "fxLightData", &doReadFuncTPL<fxLightData>,
   89, NetClassTypeObject, "fxFoliageReplicator", NULL,
   11, NetClassTypeDataBlock, "ExplosionData", &doReadFuncTPL<ExplosionData>,
   12, NetClassTypeDataBlock, "FireballAtmosphereData", NULL,
   34, NetClassTypeObject, "FireballAtmosphere", NULL,
   33, NetClassTypeObject, "Debris", NULL,
   9, NetClassTypeDataBlock, "DebrisData", &doReadFuncTPL<DebrisData>,
   4, NetClassTypeObject, "Camera", NULL,
   6, NetClassTypeDataBlock, "CameraData", &doReadFuncTPL<CameraData>,
   3, NetClassTypeObject, "AudioEmitter", NULL,
   2, NetClassTypeObject, "AIWheeledVehicle", NULL,
   1, NetClassTypeObject, "AIPlayer", NULL,
   1, NetClassTypeDataBlock, "AIPlayerData", &doReadFuncTPL<AIPlayerData>,
   36, NetClassTypeDataBlock, "SimDataBlock", NULL,
   4, NetClassTypeDataBlock, "AudioProfile", &doReadFuncTPL<AudioProfile>,
   2, NetClassTypeDataBlock, "AudioDescription", &doReadFuncTPL<AudioDescription>,
   5, NetClassTypeDataBlock, "AudioSampleEnvironment", NULL,
   3, NetClassTypeDataBlock, "AudioEnvironment", NULL
};


PacketReadClass* findClassInfo(U32 idx, U32 typeID)
{
   for (int i=0; i<sizeof(sReadClasses) / sizeof(sReadClasses[0]); i++)
   {
      if (sReadClasses[i].index == idx && sReadClasses[i].typeID == typeID)
         return sReadClasses + i;
   }
   
   return NULL;
}


