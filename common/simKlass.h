#pragma once


#include <string>
#include <functional>
#include <unordered_map>
#include <vector>
#include <algorithm>

#include "math/mQuat.h"
#include "math/mMatrix.h"
#include "common.h"

typedef const char* StringTableEntry;
typedef std::unordered_map<std::string, std::string> STHashMap;


class SimpleStringTable
{
public:
	STHashMap mHashes;

	static StringTableEntry getSTEntry(const char* str)
	{
		std::string val(str);
		SimpleStringTable* stInst = inst();
		auto stStringInst = stInst->mHashes.find(val);
		if (stStringInst != stInst->mHashes.end())
		{
			return stStringInst->second.c_str();
		}
		stInst->mHashes[str] = val;
		return stInst->mHashes[str].c_str();
	}

	static SimpleStringTable* inst();
};

class SimFieldDictionary
{
public:
  std::unordered_map<const char*, std::string> mKeys;
};

namespace Torque
{
class Notify;
class Namespace;
class SimGroup;
class SimObject;
class SimFieldDictionary;
}


class StringReturnBuf
{
public:
  enum
  {
    MAX_BUFFERS = 5
  };
  std::string data[MAX_BUFFERS];
  U32 idx;

  StringReturnBuf() : idx(0)
  {
  }

  const char* alloc(const char* str)
  {
    idx++;
    idx = idx % MAX_BUFFERS;
    data[idx] = str;

    return data[idx].c_str();
  }

  static StringReturnBuf* instance();
};

class SimDataBlock;

namespace Sim
{
  SimDataBlock* findObject(uint32_t id);
};

template<class T> class SimWeakRef
{
public:
  SimWeakRef() {;}
  SimWeakRef(U32 theId) { mId = theId; }
  SimWeakRef(T* ref) { mId = ref ? ref->mId : 0; }

  operator T*() {
    return Sim::findObject(mId);
  }

  bool operator==(const SimWeakRef<T>& lhs) {
    return mId == lhs.mId;
  }

  SimWeakRef<T>& operator=(const uint32_t rhs)
  {
    mId = rhs;
    return *this; // return the result by reference
  }

  SimWeakRef<T>& operator=(const int32_t rhs)
  {
    mId = rhs;
    return *this; // return the result by reference
  }

  uint32_t mId;
};

class SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)=0;
  virtual void setString(int elementIdx, void* ptr, char* data)=0;
   
  virtual int getElements(int baseElements, void* ptr)
  {
    return baseElements;
  }
  virtual int setElements(int baseElements, void* ptr)
  {
    return baseElements;
  }
   
  virtual bool needsCopy() { return false; }
  virtual SimFieldType* copy() { return NULL; }

  virtual ~SimFieldType() { ; }
};

class SimFieldInfo
{
public:

  SimFieldInfo() {;}
  SimFieldInfo(const char* theName, uint32_t theOffset, int elems=-1) : name(theName), offset(theOffset), numElements(elems) {;}

  const char* name;
  uint32_t offset;
  int numElements;

  SimFieldType* type;
};

// Basic field types

class S32FieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    snprintf(buf, sizeof(buf), "%i", ((S32*)ptr)[elementIdx]);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     ((S32*)ptr)[elementIdx] = atoi(data);
  }
};
extern S32FieldType  S32Field;


class S32FieldTickScaledType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    S32 val = ((S32*)ptr)[elementIdx];
    S32 sval = val * 32;
    snprintf(buf, sizeof(buf), "%i", sval);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    ((S32*)ptr)[elementIdx] = atoi(data) / 32;
  }
};
extern S32FieldTickScaledType  S32FieldTickScaled;

class U32FieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    snprintf(buf, sizeof(buf), "%u", ((U32*)ptr)[elementIdx]);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    ((U32*)ptr)[elementIdx] = (U32)std::atoll(data);
  }
};
extern U32FieldType  U32Field;

class F32FieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    snprintf(buf, sizeof(buf), "%g", ((F32*)ptr)[elementIdx]);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     ((F32*)ptr)[elementIdx] = atof(data);
  }
};
extern F32FieldType  F32Field;

class BoolFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    bool val = ((bool*)ptr)[elementIdx];
    return val ? "1" : "0";
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     ((bool*)ptr)[elementIdx] = atoi(data) == 1;
  }
};
extern BoolFieldType  BoolField;

class S8FieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    snprintf(buf, sizeof(buf), "%i", (int32_t)((S8*)ptr)[elementIdx]);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    ((U8*)ptr)[elementIdx] = (S8)atoi(data);
  }
};
extern S8FieldType  S8Field;

class U8FieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    snprintf(buf, sizeof(buf), "%u", (uint32_t)((U8*)ptr)[elementIdx]);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    ((U8*)ptr)[elementIdx] = (U8)atoi(data);
  }
};
extern U8FieldType  U8Field;

class StringFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    const char* realPtr = ((const char**)ptr)[elementIdx];
    return realPtr ? realPtr : "";
  }

   virtual void setString(int elementIdx, void* ptr, char* data);
};
extern StringFieldType  StringField;

class STDStringFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    const char* realPtr = ((std::string*)ptr)[elementIdx].c_str();
    return realPtr ? realPtr : "";
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     ((std::string*)ptr)[elementIdx] = data;
  }
};
extern STDStringFieldType  STDStringField;

class STStringFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    const char* realPtr = ((const char**)ptr)[elementIdx];
    return realPtr ? realPtr : "";
  }

   virtual void setString(int elementIdx, void* ptr, char* data);
};
extern STStringFieldType  STStringField;

class Point2FFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    Point2F value = ((Point2F*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%g %g", value.x, value.y);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    Point2F &value = ((Point2F*)ptr)[elementIdx];
     value.x = 0;
     value.y = 0;
     sscanf(data, "%f %f", &value.x, &value.y);
  }
};
extern Point2FFieldType  Point2FField;

class Point2IFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    Point2I value = ((Point2I*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%i %i", value.x, value.y);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    Point2I &value = ((Point2I*)ptr)[elementIdx];
     value.x = 0;
     value.y = 0;
     sscanf(data, "%i %i", &value.x, &value.y);
  }
};
extern Point2IFieldType  Point2IField;

class Point3FFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    Point3F value = ((Point3F*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%g %g %g", value.x, value.y, value.z);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     Point3F &value = ((Point3F*)ptr)[elementIdx];
      value.x = 0;
      value.y = 0;
      value.z = 0;
      sscanf(data, "%f %f %f", &value.x, &value.y, &value.z);
  }
};
extern Point3FFieldType  Point3FField;

class Point3IFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    Point3I value = ((Point3I*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%i %i %i", value.x, value.y, value.z);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    Point3I &value = ((Point3I*)ptr)[elementIdx];
     value.x = 0;
     value.y = 0;
     value.z = 0;
     sscanf(data, "%i %i %i", &value.x, &value.y, &value.z);
  }
};
extern Point3IFieldType  Point3IField;

class Point4FFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    Point4F value = ((Point4F*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%g %g %g %g", value.x, value.y, value.z, value.w);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    Point4F &value = ((Point4F*)ptr)[elementIdx];
     value.x = 0;
     value.y = 0;
     value.z = 0;
     value.w = 0;
     sscanf(data, "%f %f %f %f", &value.x, &value.y, &value.z, &value.w);
  }
};
extern Point4FFieldType  Point4FField;

class ColorFFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    ColorF value = ((ColorF*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%g %g %g %g", value.red, value.green, value.blue, value.alpha);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
    ColorF &value = ((ColorF*)ptr)[elementIdx];
    value.red = 0;
    value.green = 0;
    value.blue = 0;
    value.alpha = 1;
     sscanf(data, "%f %f %f %f", &value.red, &value.green, &value.blue, &value.alpha);
  }
};
extern ColorFFieldType  ColorFField;

class ColorIFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    ColorI value = ((ColorI*)ptr)[elementIdx];
    snprintf(buf, sizeof(buf), "%i %i %i %i", value.red, value.green, value.blue, value.alpha);
    return StringReturnBuf::instance()->alloc(buf);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     ColorI &value = ((ColorI*)ptr)[elementIdx];
      value.red = 0;
      value.green = 0;
      value.blue = 0;
      value.alpha = 255;
     
     int r,g,b,a;
     r=g=b=0;
     a=255;
      sscanf(data, "%i %i %i %i", &r, &g, &b, &a);
     
     value.red=r;
     value.green=g;
     value.blue=b;
     value.alpha=a;
    
  }
};
extern ColorIFieldType  PointColorIField;

class MatrixPositionFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr);
  virtual void setString(int elementIdx, void* ptr, char* data);
};
extern MatrixPositionFieldType  MatrixPositionField;

class MatrixRotationFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr);
  virtual void setString(int elementIdx, void* ptr, char* data);
};
extern MatrixRotationFieldType  MatrixRotationField;

template<typename T, typename F> class FuncFieldType : public SimFieldType
{
public:
  std::function<const char*(int elementIdx, void*ptr)> getFunc;
  std::function<const char*(int elementIdx, void*ptr, char*data)> setFunc;
  std::function<int(void*ptr)> elemFunc;
  std::function<int(int numEls, void*ptr)> setElemFunc;

  virtual const char* getString(int elementIdx, void* ptr)
  {
    return getFunc(elementIdx, ptr);
  }

  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     setFunc(elementIdx, ptr, data);
  }

  virtual int setElements(int numElements, void* ptr)
  {
    return setElemFunc(numElements, ptr);
  }

  virtual int getElements(int baseElements, void* ptr)
  {
    return elemFunc(ptr);
  }

  virtual bool needsCopy() { return true; }

  virtual SimFieldType* copy()
  {
    auto ptr = new FuncFieldType<T,F>();
    ptr->getFunc = getFunc;
    ptr->setFunc = setFunc;
    ptr->elemFunc = elemFunc;
    ptr->setElemFunc = setElemFunc;
    return ptr;
  }
};

class BaseEnumType : public SimFieldType
{
public:
  struct EnumTable
  {
    S32 value;
    const char* key;
  };

  virtual EnumTable* getEnumTable(uint32_t &count)=0;

  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[128];
    S32 value = ((S32*)ptr)[elementIdx];
    uint32_t numEnums = 0;
    EnumTable* table = getEnumTable(numEnums);
    for (int i=0; i<numEnums; i++)
    {
      if (table[i].value == value)
      {
        return table[i].key;
      }
    }
    return table[0].key;
  }
  virtual void setString(int elementIdx, void* ptr, char* data)
  {
     char buf[128];
     ((S32*)ptr)[elementIdx] = 0;
     uint32_t numEnums = 0;
     EnumTable* table = getEnumTable(numEnums);
     for (int i=0; i<numEnums; i++)
     {
       if (strcasecmp(table[i].key, data) == 0)
       {
          ((S32*)ptr)[elementIdx] = table[i].value;
          return;
       }
     }
  }
};

#define DECLARE_ENUM_TYPE(name)\
class name##EnumType : public BaseEnumType { public: EnumTable* getEnumTable(uint32_t &count); };\
extern name##EnumType name;

#define IMPLEMENT_ENUM_TYPE(name, table)\
BaseEnumType::EnumTable* name##EnumType::getEnumTable(uint32_t &count) { count = sizeof(table) / sizeof(table[0]); return &table[0]; }\
name##EnumType name;


//

// Field info for class
class SimKlassInfo
{
public:
  // basic info
  std::vector<SimFieldInfo> mFields;
  std::vector<SimFieldType*> mTempKinds;
  const char* mClassName;

  // List
  SimKlassInfo* mNextKlass;
  static SimKlassInfo* smKlassList;

  SimKlassInfo(const char* className) : mClassName(className)
  {
    mNextKlass = smKlassList;
    smKlassList = this;
  }

  virtual ~SimKlassInfo()
  {
    for (auto itr : mTempKinds)
    {
      delete itr;
    }
  }

  virtual void enumerateFields() {;}
  static void buildKlassLookup();
  static SimKlassInfo* lookupKlass(const char* className);

  void addField(const char* name, SimFieldType &kind, uint32_t offset, int numElements=-1)
  {
    SimFieldType* type = NULL;
    if (kind.needsCopy())
    {
      type = kind.copy();
      mTempKinds.push_back(type);
    }
    else
    {
      type = &kind;
    }

    SimFieldInfo info;
    info.name = name;
    info.offset = offset;
    info.type = type;
    info.numElements = numElements;
    mFields.push_back(info);
  }

  template<typename T1, typename T2> void addFuncField(const char* name, T1 func1, T2 func2)
  {
    FuncFieldType<T1,T2>* ffield = new FuncFieldType<T1,T2>();
    ffield->getFunc = func1;
    ffield->elemFunc = func2;
    mTempKinds.push_back(ffield);

    SimFieldInfo info;
    info.name = name;
    info.offset = 0;
    info.type = ffield;
    info.numElements = -1;
    mFields.push_back(info);
  }

  SimFieldInfo* findField(const char* name)
  {
    std::vector<SimFieldInfo>::iterator itr = std::find_if(mFields.begin(), mFields.end(), [name](SimFieldInfo& info){
        return strcmp(name, info.name) == 0;
    });

    if (itr == mFields.end())
      return NULL;
    else
      return &(*itr);
  }

  const char* getFieldValue(SimFieldInfo* info, void* objptr, int elementIdx)
  {
    uint8_t* ptr = (uint8_t*)(objptr);
    assert(info != NULL);
    return info->type->getString(elementIdx, ptr + info->offset);
  }

  int getFieldElements(SimFieldInfo* info, void* objptr)
  {
    uint8_t* ptr = (uint8_t*)(objptr);
    return info->type->getElements(info->numElements, ptr + info->offset);
  }

  //virtual void enumerateFields() = 0;
  virtual bool shouldEnumerate(void* inst, void* field, int elementIdx)=0;
  virtual const char* getDynamicFieldValue(void* ptr, const char* key)=0;
  virtual void enumerateDynamicFields(void* ptr, std::vector<SimFieldInfo> &fields)=0;
};

template<typename T> class KlassFieldInfo : public SimKlassInfo
{
public:
  KlassFieldInfo(const char* name) : SimKlassInfo(name) {;}

  void enumerateFields()
  {
    return T::enumerateFields(*this);
  }

  const char* getDynamicFieldValue(void* ptr, const char* key)
  {
    return ((T*)ptr)->getDynamicFieldValue(key);
  }

   void enumerateDynamicFields(void* ptr, std::vector<SimFieldInfo> &fields)
   {
    return ((T*)ptr)->enumerateDynamicFields(fields);
   }

  bool shouldEnumerate(void* inst, void* field, int elementIdx)
  {
    return ((T*)(inst))->shouldEnumerate(field, elementIdx);
  }
};

#ifndef Offset
#if defined(TORQUE_COMPILER_GCC) && (__GNUC__ > 3) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1))
#define Offset(m,T) ((int)(&((T *)1)->m) - 1)
#else
#define Offset(x, cls) ((size_t)((const char *)&(((cls *)0)->x)-(const char *)0))
#endif
#endif


class SimRefFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr);
  virtual void setString(int elementIdx, void* ptr, char* data);
};
extern SimRefFieldType  SimRefField;

class SimWeakRefFieldType : public SimFieldType
{
public:
  virtual const char* getString(int elementIdx, void* ptr)
  {
    char buf[64];
    U32 num = ((U32*)ptr)[elementIdx];
    if (num != 0)
      snprintf(buf, sizeof(buf), "DB_%u", num);
    else
      return "";
    return StringReturnBuf::instance()->alloc(buf);
  }
   
   virtual void setString(int elementIdx, void* ptr, char* data)
   {
     ((U32*)ptr)[elementIdx] = (U32)std::atoll(data);
   }
};
extern SimWeakRefFieldType  SimWeakRefField;

template<typename T> class Resource
{
	T *obj;
};

class TSShape
{

};

class TextureHandle
{
  void* ptr;
};

typedef Point3F VectorF;

struct Box3F
{
	Point3F min,max;
};





template<class OutIter>
OutIter write_escaped(std::string const& s, OutIter out) {
  *out++ = '"';
  for (std::string::const_iterator i = s.begin(), end = s.end(); i != end; ++i) {
    unsigned char c = *i;
    if (' ' <= c and c <= '~' and c != '\\' and c != '"') {
      *out++ = c;
    }
    else {
      *out++ = '\\';
      switch(c) {
      case '"':  *out++ = '"';  break;
      case '\\': *out++ = '\\'; break;
      case '\t': *out++ = 't';  break;
      case '\r': *out++ = 'r';  break;
      case '\n': *out++ = 'n';  break;
      default:
        char const* const hexdig = "0123456789ABCDEF";
        *out++ = 'x';
        *out++ = hexdig[c >> 4];
        *out++ = hexdig[c & 0xF];
      }
    }
  }
  *out++ = '"';
  return out;
}

// TODO: output something like:
/*
{
  
  'class': 'ClothingData',
  'id': 123,
  'array' : {'0': 123, '2': 456},
  'dynamicFields': { ... }
}
*/

class KlassDumper
{
public:
  virtual void dump(void* ptr, SimFieldInfo* info) = 0;
};

#include "jansson.h"

class JSONKlassDumper
{
public:

  template<typename T> json_t* getValue(T &itr, void* ptr, int idx, SimKlassInfo* info)
  {
    uint8_t* realPtr = ((uint8_t*)ptr) + itr.offset;
    if (dynamic_cast<F32FieldType*>(itr.type) != NULL)
    {
      realPtr += (idx*sizeof(float));
      return json_real(((F32*)realPtr)[0]);
    }
    else if (dynamic_cast<U32FieldType*>(itr.type) != NULL)
    {
      realPtr += (idx*sizeof(int));
      return json_integer(((U32*)realPtr)[0]);
    }
    else if (dynamic_cast<S32FieldType*>(itr.type) != NULL)
    {
      realPtr += (idx*sizeof(int));
      return json_integer(((S32*)realPtr)[0]);
    }
    else if (dynamic_cast<BoolFieldType*>(itr.type) != NULL)
    {
      realPtr += (idx*sizeof(int));
      return json_boolean(((bool*)realPtr)[0]);
    }
    else
    {
      return json_string(info->getFieldValue(&itr, ptr, idx));
    }
  }

  void dump(void* ptr, SimKlassInfo* info);
};

class TSKlassDumper
{
public:

  const char* escapeValue(const char* value)
  {
    char* outS = new char[(strlen(value)*2)+1];
    std::string s = value;
    char* endS = write_escaped(s, outS);
    *endS++ = '\0';
    const char* ret = StringReturnBuf::instance()->alloc(outS);
    delete[] outS;
    return ret;
  } 

  void dump(void* ptr, SimKlassInfo* info)
  {
    if (!info || !ptr)
    {
      assert(false);
      return;
    }

    SimFieldInfo* nameField = info->findField("name");
    SimFieldInfo* classField = info->findField("class");
    SimFieldInfo* idField = info->findField("id");

    assert(idField != NULL);
    assert(classField != NULL);

    const char* klass = info->getFieldValue(classField, ptr, 0);

    if (nameField)
    {
      printf("datablock %s(%s) {\n", info->getFieldValue(classField, ptr, 0), info->getFieldValue(nameField, ptr, 0));
    }
    else
    {
      printf("datablock %s(DB_%s) {\n", info->getFieldValue(classField, ptr, 0), info->getFieldValue(idField, ptr, 0));
    }

    for (auto itr : info->mFields)
    {
      uint8_t* fieldPtr = ((uint8_t*)ptr) + itr.offset;

      if (strcmp(itr.name, "id") == 0)
        continue;
      if (strcmp(itr.name, "name") == 0)
        continue;
      if (strcmp(itr.name, "class") == 0)
        continue;
      if (!info->shouldEnumerate(ptr, fieldPtr, -1))
        continue;

      int numElements = info->getFieldElements(&itr, ptr);
      if (numElements >= 0)
      {
        for (int i=0; i<numElements; i++)
        {
          if (!info->shouldEnumerate(ptr, fieldPtr, i))
            continue;
          printf("\t%s[%i] = %s;\n", itr.name, i,  escapeValue(info->getFieldValue(&itr, ptr, i)));
        }
      }
      else
      {
        printf("\t%s = %s;\n", itr.name, escapeValue(info->getFieldValue(&itr, ptr, 0)));
      }
    }
    
    // Dynamic fields
    std::vector<SimFieldInfo> dynamicFields;
    info->enumerateDynamicFields(ptr, dynamicFields);

    for (auto itr : dynamicFields)
    {
      printf("\t%s = %s;\n", itr.name, escapeValue(info->getDynamicFieldValue(ptr, itr.name)));
    }

    printf("};\n\n");
  }
};

typedef SimDataBlock*  (*fpUnpackDataType)(BitStream&);

struct PacketReadClass
{
   U32 index;
   U32 typeID;
   const char* name;
   fpUnpackDataType readFunc;
};



