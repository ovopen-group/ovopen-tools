/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
// hello.cc
#include <node.h>
#include <node_buffer.h>
#include "common.h"


namespace torqueutil {

using v8::Exception;
using v8::FunctionCallbackInfo;
using v8::Isolate;
using v8::Local;
using v8::NewStringType;
using v8::Number;
using v8::Object;
using v8::String;
using v8::Value;

void parseConnectRequest(const FunctionCallbackInfo<Value>& args)
{
	uint32_t len = node::Buffer::Length(args[0]->ToObject());
	char* buffer = (char*) node::Buffer::Data(args[0]->ToObject());

	v8::Handle<v8::Object> obj = v8::Object::New(args.GetIsolate());

      BitStream pStream(buffer, len);
      U32 _digest[4];
      U32 classGroup, classCRC;
      U8 num; U32 connectSequence;
      
      pStream.readBits(8, &num);
      pStream.readBits(32, &connectSequence);
      pStream.readBits(4*32, &_digest);
      //printf("num=%i seq=%i digest=%08x,%08x,%08x,%08x\n", num, connectSequence, _digest[0], _digest[1], _digest[2], _digest[3]); _digest[0] = 0;

      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "connectSequence", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::Number::New(args.GetIsolate(), connectSequence));

      v8::Handle<v8::Object> buf = node::Buffer::New(args.GetIsolate(), 4*32).ToLocalChecked();
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "digest", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       buf); 
      memcpy( (char*) node::Buffer::Data(buf), _digest, 4*32);

      char __klass[512];
      char __buf[512];
      char __uname[512];
      pStream.readString(__klass);
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "className", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::String::NewFromUtf8(args.GetIsolate(), __klass, v8::NewStringType::kInternalized).ToLocalChecked()); 

      //printf("klass=%s\n", __klass);
      pStream.read(&classGroup);
      pStream.read(&classCRC);
      //printf("%i %i %i", num, classGroup, classCRC);

      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "classGroup", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::Number::New(args.GetIsolate(), classGroup)); 
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "classCRC", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::Number::New(args.GetIsolate(), classCRC)); 


      U32 v1,v2;
      pStream.read(&v1);
      pStream.read(&v2);
      //printf("V: %u,%u\n", v1, v2);
      pStream.readString(__uname);

      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "v1", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::Number::New(args.GetIsolate(), v1)); 
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "v2", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::Number::New(args.GetIsolate(), v2)); 
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "username", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       v8::String::NewFromUtf8(args.GetIsolate(), __uname, v8::NewStringType::kInternalized).ToLocalChecked()); 

      // SHA1(AddrDigest + SHA1(PASSWORD))
      pStream.readBits(20*8, __buf);

      buf = node::Buffer::New(args.GetIsolate(), 20).ToLocalChecked();
      memcpy( (char*) node::Buffer::Data(buf), __buf, 20);
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "password_hash", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       buf); 

      U32 _argc;
      const char* _argv[32];
      pStream.read(&_argc);
      v8::Handle<v8::Array> arr = v8::Array::New(args.GetIsolate(), _argc);
      obj->Set(v8::String::NewFromUtf8(args.GetIsolate(), "args", v8::NewStringType::kInternalized).ToLocalChecked(), 
      	       arr); 

      for (int i=0; i<_argc; i++)
      {
         pStream.readString(__buf); 
         _argv[i] = __buf;

  		arr->Set(v8::Number::New(args.GetIsolate(), i), 
  			    v8::String::NewFromUtf8(args.GetIsolate(), __buf));
      }

      args.GetReturnValue().Set(obj);
}

std::string ObjectToString(v8::Isolate* isolate, Local<Value> value) {
  String::Utf8Value utf8_value(isolate, value);
  return std::string(*utf8_value);
}

void constructConnectRequest(const FunctionCallbackInfo<Value>& args)
{
      v8::Handle<v8::Object> obj = args[0]->ToObject();
      v8::Handle<v8::Number> nodeNumber;
      char streamBuffer[1024];
      BitStream pStream(streamBuffer, sizeof(streamBuffer));
      U32 _digest[4];
      U32 classGroup, classCRC;
      U8 num; U32 connectSequence;

      num = 32;
      connectSequence = obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "connectSequence"))->IntegerValue();

      //printf("connectSequence=%u\n", connectSequence);

      char* digestBuffer = (char*) node::Buffer::Data(obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "digest")));
      pStream.writeBits(8, &num);
      pStream.writeBits(32, &connectSequence);
      pStream.writeBits(4*32, digestBuffer);
		
	  //printf("writing className\n");
      
      pStream.writeString(ObjectToString(args.GetIsolate(), obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "className"))).c_str());

      //printf("wrote className\n");

      classGroup = obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "classGroup"))->IntegerValue();
      classCRC = obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "classCRC"))->IntegerValue();
      pStream.write(classGroup);
      pStream.write(classCRC);
      //printf("%i %i %i\n", classGroup, classCRC);


      U32 v1 = obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "v1"))->IntegerValue();
      U32 v2 = obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "v2"))->IntegerValue();
      pStream.write(v1);
      pStream.write(v2);
      //printf("V: %u,%u\n", v1, v2);
      pStream.writeString(ObjectToString(args.GetIsolate(), obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "username"))).c_str());

      // SHA1(AddrDigest + SHA1(PASSWORD))
      pStream.writeBits(20*8, node::Buffer::Data(obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "password_hash"))));


      v8::Handle<v8::Array> arr = v8::Handle<v8::Array>::Cast(obj->Get(v8::String::NewFromUtf8(args.GetIsolate(), "args")));

      U32 len = arr->Length();
      pStream.write(len);

      for (int i=0; i<len; i++)
      {
         v8::Handle<v8::String> value = v8::Handle<v8::String>::Cast(arr->Get(v8::Number::New(args.GetIsolate(), i)));
         pStream.writeString(ObjectToString(args.GetIsolate(), value).c_str());
      }

      U32 outBufferBytes = (pStream.getCurPos() + 7) / 8;
      auto nodeBuffer = node::Buffer::New(args.GetIsolate(), outBufferBytes).ToLocalChecked();
      memcpy(node::Buffer::Data(nodeBuffer), streamBuffer, outBufferBytes);
      args.GetReturnValue().Set(nodeBuffer);
}

void Initialize(Local<Object> exports) {
  NODE_SET_METHOD(exports, "parseConnectRequest", parseConnectRequest);
  NODE_SET_METHOD(exports, "constructConnectRequest", constructConnectRequest);
}

NODE_MODULE(NODE_GYP_MODULE_NAME, Initialize)

}  // namespace demo