# COMMON STUFF
# ovopen-tools
# Copyright (C) 2020  mangofusi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

$asset_types = ['.txt', '.jpg', '.png']
$patch_tool = ENV["OVOPEN_PATCHER"]||"patch_tool_Debug64"
$compile_script = ENV["OVOPEN_COMPLER"]||"compile_script"
$build_folder = ENV["BUILD_FOLDER"]||"build"

def common_exclude(fl, build_folder)
	fl.exclude(/^#{build_folder}\//)
	fl.exclude(/^out\//)
end

def import_files(build_folder, &block)
	script_files = Rake::FileList.new("**/*.cs", &block)
	gui_files = Rake::FileList.new("**/*.gui", &block)

	fexts = $asset_types.map{|f| "**/*#{f}"}
	data_files = Rake::FileList.new(*fexts, &block).pathmap("#{build_folder}/%p")
	compiled_script_files = script_files.ext(".cs.dso").pathmap("#{build_folder}/%p")
	compiled_gui_files = gui_files.ext(".gui.dso").pathmap("#{build_folder}/%p")
	dest_files = compiled_script_files.to_ary + compiled_gui_files.to_ary + data_files.to_ary

	source_for_cs = Proc.new{|file|
		file = file.match(/#{build_folder}\/(.*)/)[1]
		script_files.detect{|f| f == file.ext('')}
	}

	source_for_gui = Proc.new{|file|
		file = file.match(/#{build_folder}\/(.*)/)[1]
		gui_files.detect{|f| f == file.ext('')}
	}

	copy_asset = Proc.new{|file|
		#puts "\tcopy_asset(#{file})"
		file.match(/#{build_folder}\/(.*)/)[1]
	}

	rule '.cs.dso' => source_for_cs do |t|
		sh $compile_script, t.source, t.name
	end

	rule '.gui.dso' => source_for_gui do |t|
		sh $compile_script, t.source, t.name
	end

	directory_list = {'out' => 0}
	dest_files.each do |t|
		directory_list[t.pathmap('%d')] ||= 0
	end

	directory_list.keys.each { |d| directory d }

	$asset_types.each do |asset|
		rule /#{build_folder}\/.*#{asset}/ => copy_asset do |t|
			puts "copy S=#{t.source} D=#{t.name}"
			sh 'cp', '-vp', t.source, t.name
		end
	end

	return {'files' => dest_files,
		'dirs' => directory_list.keys}
end

