/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _PATCHFILE_H_
#define _PATCHFILE_H_

#include "common.h"
#include "packet.hpp"
#include <string>



#include "ghc/filesystem.hpp"
namespace fs = ghc::filesystem;

class PatchDataSource
{
public:
	virtual ~PatchDataSource() {;}
	virtual uint32_t getSize()=0;
	virtual void read(void* dest, uint32_t offset, uint32_t bytes)=0;

	uint32_t calcCRC();
	void calcMD5(uint8_t* outMD5);
};

class PatchMemStream : public MemStream, public std::enable_shared_from_this<PatchMemStream>
{
public:
	PatchMemStream(uint32_t size) : MemStream(size) {;}
};

typedef std::shared_ptr<PatchMemStream> PatchMemStreamPtr;

class PatchFileSource : public PatchDataSource
{
public:
	FILE* mFP;
	uint32_t mSize;
	std::string mFilename;

	bool open()
	{
		if (!mFP)
		{
			mFP = fopen(mFilename.c_str(), "rb");
			if (mFP)
			{
				fseek(mFP, 0, SEEK_END);
				mSize = ftell(mFP);
				fseek(mFP, 0, SEEK_SET);
			}
		}
		return mFP != NULL;
	}

	void close()
	{
		fclose(mFP);
		mFP = NULL;
	}

	PatchFileSource() : mFP(NULL) {;}
	~PatchFileSource()
	{
		if (mFP)
			fclose(mFP);
	}

	virtual uint32_t getSize()
	{
		if (!mFP)
		{
			if (!open())
				return 0;
		}
		return mSize;
	}

	virtual void read(void* dest, uint32_t offset, uint32_t bytes)
	{
		if (!mFP)
		{
			if (!open())
				return;
		}

		fseek(mFP, offset, SEEK_SET);
		fread(dest, bytes, 1, mFP);
	}
};

class PatchMemStreamSource : public PatchDataSource
{
public:
	PatchMemStreamPtr s;
	uint32_t mSrcOffset;
	uint32_t mSize;

	PatchMemStreamSource() : s(NULL), mSrcOffset(0), mSize(0) {;}

	~PatchMemStreamSource()
	{
	}

	virtual uint32_t getSize()
	{
		return mSize;
	}

	virtual void read(void* dest, uint32_t offset, uint32_t bytes)
	{
		s->setPosition(mSrcOffset+offset);
		s->read(bytes, dest);
	}
};

class PatchFile
{
public:
	// File entry kind
	enum
	{
		KIND_NOP = 0,
		KIND_PATCH = 1,
		KIND_ADD = 2,
		KIND_DELETE = 3
	};

	// Main kind
	enum
	{
		PATCHFILE_PATCH=1,
		PATCHFILE_REPLACE=2
	};

	enum
	{
		VERSION=2
	};

	struct ListRange
	{
		uint32_t start;
		uint32_t num;
		ListRange() : start(0), num(0) {;}
		ListRange(uint32_t a, uint32_t b) : start(a), num(b) {;}
		uint32_t getEnd() const { return start+num; }
	};

	struct FileInfo
	{
		uint8_t kind;
		bool isExecutable;
		bool isLink;

		std::string fileName;
		std::string pathName;

		uint32_t crc;
		uint32_t patch_crc;

		uint8_t md5[16];
		uint8_t patch_md5[16];

		uint32_t ctime;
		uint32_t mtime;

		uint32_t offset;
		uint32_t size;

		uint32_t parentFolderIdx;

		uint32_t dataSrcIdx;

		FileInfo() : kind(0), isExecutable(0), isLink(0) {;}

		bool isSameAs(const FileInfo& other)
		{
			return crc == other.crc && memcmp(md5, other.md5, 16) == 0;
		}
		bool isRegularFile() { return !(isExecutable || isLink); }
	};

	struct FolderInfo
	{
		uint8_t kind;
		uint8_t modCount;
		std::string folderName;
		uint32_t startFile;
		uint32_t numFiles;
		uint32_t startFolder;
		uint32_t numFolders;
		uint32_t endOffset;
		uint32_t parentFolderIdx;

		union { uint32_t parentCount; int32_t srcIdx; };
		int32_t destIdx;

		FolderInfo() : kind(0), startFile(0), numFiles(0), startFolder(0), numFolders(0), endOffset(0), parentFolderIdx(0), parentCount(0), modCount(0), destIdx(0) {;}
		ListRange getFileRange()   { return ListRange(startFile, numFiles); }
		ListRange getFolderRange() { return ListRange(startFolder, numFolders); }
		void markModified() { modCount = 1; }
	};

	std::vector<FileInfo> mFiles;
	std::vector<FolderInfo> mFolders;
	std::vector<PatchDataSource*> mDataSources;

	uint32_t mCurrentDataStart;
	uint32_t mCurrentDataEnd;

	uint8_t  mPatchKind;

	static std::vector<std::string> sIgnoreFiles;

	~PatchFile();
	void clear();

	bool populateFromDirectory(const fs::path &path, uint32_t rootFolderIdx);
	static PatchDataSource* generateFileDiff(PatchDataSource* d1, PatchDataSource* d2);
	static PatchDataSource* applyFileDiff(PatchDataSource* data, PatchDataSource* patch);

	void beginEmpty(fs::path &root);

	// In
	bool read(PatchMemStream &s);
	bool readFolder(PatchMemStream &s, FolderInfo &info, uint32_t idx);
	bool readFile(PatchMemStream &s, FileInfo &info);

	// Out
	bool write(MemStream &s);
	bool writeFolder(MemStream &s, FolderInfo &info, uint32_t idx);
	bool writeFile(MemStream &s, FileInfo &info);
	bool writeFileData(MemStream &s);

	uint32_t updateFileOffsets(uint32_t newOffset);
	bool apply(fs::path &destPath);

	// IO from file
	bool readFromFile(PatchMemStream &s, fs::path &path);
	bool compressToFile(MemStream &s, fs::path &path);

	static PatchFile* generateDiff(PatchFile* src, PatchFile* dest);

	void printListing();
	void dumpFiles(fs::path &root);
};

#endif