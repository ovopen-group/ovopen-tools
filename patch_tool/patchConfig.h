/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef _PATCHCONFIG_H_
#define _PATCHCONFIG_H_

#include "common.h"
#include "packet.hpp"
#include <string>
#include "jansson.h"
#include "patchFile.h"

class PatchConfig
{
public:

	struct KeyValue
	{
		int32_t parentID;
		std::string key;
		std::string value;

		KeyValue() : parentID(-1) {;}
	};

	struct BundleInfo
	{
		std::string name;
		std::string version;
	};

	std::string mLastPatchServer;

	std::vector<BundleInfo> mBundleInfo;
	std::vector<KeyValue> mBundleKeys;
	std::vector<KeyValue> mBundleOSKeys;
	std::vector<KeyValue> mGlobalKeys;

	PatchConfig();
	~PatchConfig();

	void clear();

	bool read(MemStream &s);
	bool write(MemStream &s);

	void fromJSON(json_t* root);
	void toJSON(json_t* root);

	int32_t getBundleIdx(const std::string &key);
	BundleInfo* getBundleInfo(const std::string &key);

	std::string getBundleValue(int32_t bundleIdx, const std::string &key, bool *isPresent);
	void setBundleValue(int32_t bundleIdx, const std::string &key, const std::string &value);

	std::string getBundleOSValue(int32_t bundleIdx, const std::string &key, bool *isPresent);
	void setBundleOSValue(int32_t bundleIdx, const std::string &key, const std::string &value);

	std::string getGlobalValue(int32_t bundleIdx, const std::string &key, bool *isPresent);
	void setGlobalValue(int32_t bundleIdx, const std::string &key, const std::string &value);


	bool readFromFile(MemStream &s, fs::path &path);
	bool writeToFile(MemStream &s, fs::path &path);
};

#endif