/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define BOOST_TEST_MODULE OvOpenTest
#define BOOST_TEST_NO_MAIN
#define BOOST_TEST_ALTERNATIVE_INIT_API

#include <asio.hpp>
#include <atomic>
#include <iostream>
#include <unordered_map>

#include "patchFile.h"
#include "patchDiff.h"

#define CATCH_CONFIG_MAIN
#ifdef __GNUC__
#define DO_NOT_USE_WMAIN
#endif
#include "catch.hpp"

using namespace PatchDiff;
fs::path gTestDataPath = "./test";

PatchFile::FolderInfo* CheckFolderTreeDirectory(PatchFile* f, const char* path)
{
	fs::path root(path);
	uint32_t currentFolder = 0;

	for (auto itr = root.begin(), itrEnd = root.end(); itr != itrEnd; itr++)
	{
		auto nextItr = itr;
		nextItr++;

		int folderIdx = GetIndexOfFolderInList(itr->filename().string(), f->mFolders, f->mFolders[currentFolder].getFolderRange());
		if (folderIdx >= 0)
		{
			if (nextItr == itrEnd)
			{
				return &f->mFolders[folderIdx];
			}
			currentFolder = folderIdx;
		}
		else
		{
			return NULL;
		}
	}

	return NULL;
}

PatchFile::FileInfo* CheckFolderTreeFile(PatchFile* f, const char* path)
{
	fs::path root(path);
	uint32_t currentFolder = 0;
	//printf("CheckFolderTreeFile %s\n", path);

	for (auto itr = root.begin(), itrEnd = root.end(); itr != itrEnd; itr++)
	{
		auto nextItr = itr;
		nextItr++;
		//printf("cur == %s\n", itr->filename().c_str());

		if (nextItr == itrEnd)
		{
			//printf("NEXT IS END, ^^ SHOULD EXIST IN FOLDER %u FROM %u TO %u\n", currentFolder, f->mFolders[currentFolder].getFileRange().start, f->mFolders[currentFolder].getFileRange().getEnd());
			int fileIdx = GetIndexOfFileInList(itr->filename().string(), f->mFiles, f->mFolders[currentFolder].getFileRange());
			//printf("\tfileIdx ended up being %i\n", fileIdx);
			if (fileIdx >= 0)
			{
				return &f->mFiles[fileIdx];
			}
			else
			{
				return NULL;
			}
		}
		else
		{
			int folderIdx = GetIndexOfFolderInList(itr->filename().c_str(), f->mFolders, f->mFolders[currentFolder].getFolderRange());
			if (folderIdx >= 0)
			{
				currentFolder = folderIdx;
			}
			else
			{
				return NULL;
			}
		}
	}
	return NULL;
}

void MakeSimpleFolderTree(PatchDiff::FolderList &list, fs::path root)
{
	PatchFile::FolderInfo subdir;
	subdir.kind = PatchFile::KIND_NOP;
	subdir.folderName = "";
	subdir.parentFolderIdx = 0;
	subdir.numFolders = 0;
	subdir.startFolder = 1;
	list.push_back(subdir);
	uint32_t count = 1;

	for (auto itr = root.begin(), itrEnd = root.end(); itr != itrEnd; itr++)
	{
		PatchFile::FolderInfo subdir;
		subdir.kind = PatchFile::KIND_ADD;
		subdir.folderName = itr->filename().c_str();
		subdir.startFolder = list.size()+1; // i.e. next one
		subdir.parentFolderIdx = list.size()-1; // prev one
		subdir.parentCount = count++;
		list.push_back(subdir);

		list[subdir.parentFolderIdx].numFolders = 1;
	}
}

void InsertSimpleFolderTree(PatchDiff::FolderList &list, fs::path root, uint32_t rootFolderIdx)
{
	uint32_t parentIdx = rootFolderIdx;
	uint32_t folderListStart;
	uint32_t oldSize = list.size();

	if (list[rootFolderIdx].numFolders == 0)
	{
		folderListStart = list.size();
		list[rootFolderIdx].startFolder = folderListStart;
	}
	else
	{
		folderListStart = (list[rootFolderIdx].startFolder + list[rootFolderIdx].numFolders);

		// Adjust ranges to account for new root folder to be inserted
		for (uint32_t i=0; i<oldSize; i++)
		{
			if (list[i].startFolder >= folderListStart)
				list[i].startFolder++;
			if (list[i].parentFolderIdx >= folderListStart)
				list[i].parentFolderIdx++;
		}
	}

	uint32_t nextPlace = folderListStart;

	for (auto itr = root.begin(), itrEnd = root.end(); itr != itrEnd; itr++)
	{
		PatchFile::FolderInfo subdir;
		subdir.kind = PatchFile::KIND_ADD;
		subdir.folderName = itr->filename().c_str();
		subdir.startFolder = list.size()+1; // i.e. next one at the end
		subdir.parentFolderIdx = parentIdx; // prev one
		list.insert(list.begin()+nextPlace, subdir);

		// Adjust counts
		list[nextPlace].parentCount = list[subdir.parentFolderIdx].parentCount+1;
		list[subdir.parentFolderIdx].numFolders++;

		// Future stuff will be added on end
		nextPlace = list.size();
		parentIdx = list.size()-1;
	}
}

void AddDummyFile(PatchFile* outFile, PatchFile::FolderInfo &folder, uint32_t folderId, const char *filename, const char *data)
{
	PatchFile::FileInfo info;
	info.fileName = std::string(filename);
	info.kind = PatchFile::KIND_NOP;
	info.parentFolderIdx = folderId;
	info.size = strlen(data);
	outFile->mFiles.push_back(info);

	PatchMemStreamSource* s1 = new PatchMemStreamSource();
	PatchMemStreamPtr stream1 = std::make_shared<PatchMemStream>(4096);
	s1->s = stream1;
	s1->mSize = strlen(data);
	stream1->write(strlen(data), data);

	outFile->mDataSources.push_back(s1);
	info.dataSrcIdx = outFile->mDataSources.size()-1;

	folder.numFiles++;
}

// Finds a folder in target which matches folderId in src
TEST_CASE( "FindMatchingFolderTest" )
{
	PatchDiff::FolderList folders1;
	PatchDiff::FolderList folders2;
	PatchDiff::FolderList folders3;

	MakeSimpleFolderTree(folders1, "one/two/three/four");
	MakeSimpleFolderTree(folders2, "one/two/three");
	MakeSimpleFolderTree(folders3, "one/two");

	/*for (auto folder : folders1)
	{
		printf("::Folder(%s) parentCount=%u\n", folder.folderName.c_str(), folder.parentCount);
	}*/

	auto itr = PatchDiff::FindMatchingFolder(folders1, folders3, 1);
	REQUIRE((itr != folders1.end() && itr->folderName == "one"));
	itr = PatchDiff::FindMatchingFolder(folders1, folders3, 2);
	REQUIRE((itr != folders1.end() && itr->folderName == "two"));

	itr = PatchDiff::FindMatchingFolder(folders1, folders2, 1);
	REQUIRE((itr != folders1.end() && itr->folderName == "one"));
	itr = PatchDiff::FindMatchingFolder(folders1, folders2, 2);
	REQUIRE((itr != folders1.end() && itr->folderName == "two"));


	itr = PatchDiff::FindMatchingFolder(folders3, folders1, 1);
	REQUIRE((itr != folders3.end() && itr->folderName == "one"));
	itr = PatchDiff::FindMatchingFolder(folders3, folders1, 2);
	REQUIRE((itr != folders3.end() && itr->folderName == "two"));
	itr = PatchDiff::FindMatchingFolder(folders3, folders1, 3);
	REQUIRE(itr == folders3.end());

	InsertSimpleFolderTree(folders2, "drei/four", 2);

	/*printf("INSERT CHECK\n");
	for (auto folder : folders2)
	{
		printf("::Folder(%s) startChild=%u numChild=%u parentCount=%u parent=%u\n", folder.folderName.c_str(), folder.startFolder, folder.numFolders, folder.parentCount, folder.parentFolderIdx);
	}
	printf("--\n");*/

	itr = PatchDiff::FindMatchingFolder(folders2, folders1, 4);
	REQUIRE(itr == folders2.end());


	InsertSimpleFolderTree(folders2, "four", 3);

	/*printf("INSERT CHECK\n");
	for (auto folder : folders2)
	{
		printf("::Folder(%s) startChild=%u numChild=%u parentCount=%u parent=%u\n", folder.folderName.c_str(), folder.startFolder, folder.numFolders, folder.parentCount, folder.parentFolderIdx);
	}*/

	itr = PatchDiff::FindMatchingFolder(folders2, folders1, 4);
	REQUIRE((itr != folders2.end() && itr->folderName == "four" && folders2[itr->parentFolderIdx].folderName == "three"));
}

// Check any modified directory trees are marked as modified
TEST_CASE( "FixModCountsTest" )
{
	PatchDiff::FolderList folders1;
	PatchDiff::FileList files1;

	MakeSimpleFolderTree(folders1, "one/two/three/four");

	REQUIRE(folders1[0].modCount == 0);
	REQUIRE(folders1[1].modCount == 0);
	REQUIRE(folders1[2].modCount == 0);
	REQUIRE(folders1[3].modCount == 0);

	folders1[3].modCount = 1;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	folders1[0].modCount = 0;
	folders1[0].kind = PatchFile::KIND_NOP;
	folders1[1].modCount = 0;
	folders1[1].kind = PatchFile::KIND_NOP;
	folders1[2].modCount = 0;
	folders1[2].kind = PatchFile::KIND_NOP;
	folders1[3].modCount = 0;
	folders1[3].kind = PatchFile::KIND_NOP;
	folders1[4].modCount = 0;
	folders1[4].kind = PatchFile::KIND_NOP;
	REQUIRE(folders1.size() == 5);

	PatchFile::FileInfo testInfo;
	testInfo.fileName = "addfile";
	testInfo.kind = PatchFile::KIND_ADD;
	testInfo.parentFolderIdx = 3;
	files1.push_back(testInfo);

	testInfo.fileName = "delfile";
	testInfo.kind = PatchFile::KIND_DELETE;
	testInfo.parentFolderIdx = 3;
	files1.push_back(testInfo);

	testInfo.fileName = "patchfile";
	testInfo.kind = PatchFile::KIND_PATCH;
	testInfo.parentFolderIdx = 3;
	files1.push_back(testInfo);


	// =ADD FILES CHECK=
	folders1[3].startFile = 0;
	folders1[3].numFiles = 1;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount == 0);
	REQUIRE(folders1[1].modCount == 0);
	REQUIRE(folders1[2].modCount == 0);
	REQUIRE(folders1[3].modCount == 0);

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, false);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	// =DEL FILES CHECK=

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[3].startFile = 1;
	folders1[3].numFiles = 1;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, false);

	REQUIRE(folders1[0].modCount == 0);
	REQUIRE(folders1[1].modCount == 0);
	REQUIRE(folders1[2].modCount == 0);
	REQUIRE(folders1[3].modCount == 0);

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	// =PATCH FILES CHECK=

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[3].startFile = 2;
	folders1[3].numFiles = 1;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, false);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	// ADDED FOLDER

	folders1[3].numFiles = 0;
	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].kind = PatchFile::KIND_ADD;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, false);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].kind = PatchFile::KIND_ADD;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount == 0);
	REQUIRE(folders1[1].modCount == 0);
	REQUIRE(folders1[2].modCount == 0);
	REQUIRE(folders1[3].modCount == 0);

	// DELETED FOLDER
	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].kind = PatchFile::KIND_DELETE;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].kind = PatchFile::KIND_DELETE;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, false);

	REQUIRE(folders1[0].modCount == 0);
	REQUIRE(folders1[1].modCount == 0);
	REQUIRE(folders1[2].modCount == 0);
	REQUIRE(folders1[3].modCount == 0);

	// PATCHED FOLDER
	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].kind = PatchFile::KIND_PATCH;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, true);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);

	folders1[0].modCount = 0;
	folders1[1].modCount = 0;
	folders1[2].modCount = 0;
	folders1[3].modCount = 0;
	folders1[4].kind = PatchFile::KIND_PATCH;
	folders1[4].modCount = 0;

	PatchDiff::FixModCounts(folders1, files1, false);

	REQUIRE(folders1[0].modCount > 0);
	REQUIRE(folders1[1].modCount > 0);
	REQUIRE(folders1[2].modCount > 0);
	REQUIRE(folders1[3].modCount > 0);
}

// Gets index of filename
TEST_CASE( "GetIndexOfFileInListTest" )
{
	std::string test1 = "test1";
	std::string test2 = "test2";
	std::string test3 = "test3";
	std::string test4 = "test4";
	FileList list;
	PatchFile::FileInfo info;
	info.fileName = test1;
	list.push_back(info);
	info.fileName = test2;
	list.push_back(info);
	info.fileName = test3;
	list.push_back(info);
	info.fileName = test4;
	list.push_back(info);

	int idx = GetIndexOfFileInList(test1, list, PatchFile::ListRange(0, 0));
	REQUIRE(idx == -1);
	idx = GetIndexOfFileInList(test1, list, PatchFile::ListRange(0, 1));
	REQUIRE(idx == 0);
	idx = GetIndexOfFileInList(test1, list, PatchFile::ListRange(0, 2));
	REQUIRE(idx == 0);
	idx = GetIndexOfFileInList(test1, list, PatchFile::ListRange(1, 3));
	REQUIRE(idx == -1);

	idx = GetIndexOfFileInList(test2, list, PatchFile::ListRange(0, 0));
	REQUIRE(idx == -1);
	idx = GetIndexOfFileInList(test2, list, PatchFile::ListRange(0, 1));
	REQUIRE(idx == -1);
	idx = GetIndexOfFileInList(test2, list, PatchFile::ListRange(0, 2));
	REQUIRE(idx == 1);
	idx = GetIndexOfFileInList(test2, list, PatchFile::ListRange(1, 3));
	REQUIRE(idx == 1);

	idx = GetIndexOfFileInList(test1, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 0);
	idx = GetIndexOfFileInList(test2, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 1);
	idx = GetIndexOfFileInList(test3, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 2);
	idx = GetIndexOfFileInList(test4, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 3);
}

// Gets index of folder
TEST_CASE( "GetIndexOfFolderInListTest" )
{
	std::string test1 = "test1";
	std::string test2 = "test2";
	std::string test3 = "test3";
	std::string test4 = "test4";
	FolderList list;
	PatchFile::FolderInfo info;
	info.folderName = test1;
	list.push_back(info);
	info.folderName = test2;
	list.push_back(info);
	info.folderName = test3;
	list.push_back(info);
	info.folderName = test4;
	list.push_back(info);

	int idx = GetIndexOfFolderInList(test1, list, PatchFile::ListRange(0, 0));
	REQUIRE(idx == -1);
	idx = GetIndexOfFolderInList(test1, list, PatchFile::ListRange(0, 1));
	REQUIRE(idx == 0);
	idx = GetIndexOfFolderInList(test1, list, PatchFile::ListRange(0, 2));
	REQUIRE(idx == 0);
	idx = GetIndexOfFolderInList(test1, list, PatchFile::ListRange(1, 3));
	REQUIRE(idx == -1);

	idx = GetIndexOfFolderInList(test2, list, PatchFile::ListRange(0, 0));
	REQUIRE(idx == -1);
	idx = GetIndexOfFolderInList(test2, list, PatchFile::ListRange(0, 1));
	REQUIRE(idx == -1);
	idx = GetIndexOfFolderInList(test2, list, PatchFile::ListRange(0, 2));
	REQUIRE(idx == 1);
	idx = GetIndexOfFolderInList(test2, list, PatchFile::ListRange(1, 3));
	REQUIRE(idx == 1);

	idx = GetIndexOfFolderInList(test1, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 0);
	idx = GetIndexOfFolderInList(test2, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 1);
	idx = GetIndexOfFolderInList(test3, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 2);
	idx = GetIndexOfFolderInList(test4, list, PatchFile::ListRange(0, 4));
	REQUIRE(idx == 3);
}

// Creates a new file in patch for any modified file in src
TEST_CASE( "GenerateFilePatchesTest" )
{
	PatchFile* outPatch = new PatchFile();
	auto root = fs::path("folder");
	outPatch->beginEmpty(root);
	REQUIRE(outPatch->mFolders.size() == 2);

	PatchFile* srcPatch = new PatchFile();
	srcPatch->beginEmpty(root);
	PatchFile* destPatch = new PatchFile();
	destPatch->beginEmpty(root);

	AddDummyFile(srcPatch, srcPatch->mFolders[1], 1, "file1.txt", "FILE 1");
	AddDummyFile(srcPatch, srcPatch->mFolders[1], 1, "file2.txt", "FILE 2");
	AddDummyFile(destPatch, destPatch->mFolders[1], 1, "file1.txt", "FILE 1");
	AddDummyFile(destPatch, destPatch->mFolders[1], 1, "file3.txt", "FILE 3");

	REQUIRE(srcPatch->mFolders[1].numFiles == 2);
	REQUIRE(destPatch->mFolders[1].numFiles == 2);
	REQUIRE(outPatch->mFolders[1].numFiles == 0);

	srcPatch->mFiles[0].kind = PatchFile::KIND_PATCH;
	srcPatch->mFiles[1].kind = PatchFile::KIND_NOP;
	destPatch->mFiles[0].kind = PatchFile::KIND_PATCH;
	destPatch->mFiles[1].kind = PatchFile::KIND_ADD;

	PatchDiff::GenerateFilePatches(outPatch, srcPatch, destPatch, 1, 1, 1);
	outPatch->mFolders[1].numFiles = outPatch->mFiles.size() - outPatch->mFolders[1].startFile;

	REQUIRE(outPatch->mFiles.size() == 1);
	REQUIRE(outPatch->mFiles[0].parentFolderIdx == 1);
	REQUIRE(outPatch->mFolders[1].startFile == 0);
	REQUIRE(outPatch->mFolders[1].numFiles == 1);
	REQUIRE(outPatch->mFiles[0].fileName == "file1.txt");
}

// Recursively zips together two patch files into a new patch file
TEST_CASE( "RecurseAddFoldersInFolderTest" )
{
}

// Should create a patch which reasonably reflects the directory tree
TEST_CASE( "populateFromDirectoryTest" )
{
	fs::path path(gTestDataPath);
	REQUIRE(fs::is_directory(path));

	if (!fs::is_directory(path))
		return;

	path += fs::path("/deep_list/");
	REQUIRE(fs::is_directory(path));

	if (!fs::is_directory(path))
		return;

	PatchFile::sIgnoreFiles.clear();
	PatchFile::sIgnoreFiles.push_back(".DS_Store");
	PatchFile::sIgnoreFiles.push_back(".git");

	PatchFile* file = new PatchFile();
	fs::path rootPath("");
	file->beginEmpty(rootPath);
	file->populateFromDirectory(path, 0);

	REQUIRE(file->mFiles.size() == 10);
	REQUIRE(file->mFolders.size() == 6);

	REQUIRE(CheckFolderTreeDirectory(file, "folder1/folder2") != NULL);
	REQUIRE(CheckFolderTreeDirectory(file, "folder1/folder3") == NULL);
	REQUIRE(CheckFolderTreeDirectory(file, "folder1/folder2/folder3") != NULL);
	REQUIRE(CheckFolderTreeDirectory(file, "folder1/folder2_2") != NULL);
	REQUIRE(CheckFolderTreeDirectory(file, "folder1/folder2_2/folder3") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/file1.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/file2.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/folder2/file1.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/folder2/file2.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/folder2/folder3/file1.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/folder2/folder3/file2.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/folder2_2/folder3/file1.txt") != NULL);
	REQUIRE(CheckFolderTreeFile(file, "folder1/folder2_2/folder3/file2.txt") != NULL);

	// Check props
	delete file;
	file = new PatchFile();
	path = fs::path(gTestDataPath);
	path += fs::path("/propcheck/");
	file->beginEmpty(rootPath);
	file->populateFromDirectory(path, 0);
	
	REQUIRE(file->mFiles.size() == 3);
	REQUIRE((CheckFolderTreeFile(file, "executable") != NULL && CheckFolderTreeFile(file, "executable")->isExecutable));
	REQUIRE((CheckFolderTreeFile(file, "file1.txt") != NULL && CheckFolderTreeFile(file, "file1.txt")->isRegularFile()));
	REQUIRE((CheckFolderTreeFile(file, "file2.txt") != NULL));
	#ifndef WIN32
	REQUIRE((CheckFolderTreeFile(file, "file2.txt") != NULL && CheckFolderTreeFile(file, "file2.txt")->isLink));
	#endif
	REQUIRE((CheckFolderTreeFile(file, "file2.txt") != NULL && CheckFolderTreeFile(file, "file2.txt")->pathName == "file1.txt"));

	for (auto f : file->mFiles)
	{
		printf("FILE: [%s] PATH [%s]\n", f.fileName.c_str(), f.pathName.c_str());
	}

	delete file;
}

// Should generate a working diff from two files
TEST_CASE( "generateDiffTest" )
{
	PatchMemStreamSource* s1 = new PatchMemStreamSource();
	PatchMemStreamSource* s2 = new PatchMemStreamSource();

	const char* test1 = "TEST FILE TEST FILE 1 ITS A TEST\n";
	const char* test2 = "TEST FILE TEST FILE 2 ITS A REAL TEST\n";

	PatchMemStreamPtr stream1 = std::make_shared<PatchMemStream>(4096);
	PatchMemStreamPtr stream2 = std::make_shared<PatchMemStream>(4096);

	s1->s = stream1;
	s1->mSize = strlen(test1);
	s2->s = stream2;
	s2->mSize = strlen(test2);

	stream1->write(strlen(test1), test1);
	stream2->write(strlen(test2), test2);

	PatchDataSource* patchFile = PatchFile::generateFileDiff(s1, s2);
	REQUIRE(patchFile != NULL);

	// TODO: See if we can apply this
	PatchDataSource* newDiff = PatchFile::applyFileDiff(s1, patchFile);
	REQUIRE(newDiff != NULL);

	char buffer[4096];
	newDiff->read(buffer, 0, newDiff->getSize());
	REQUIRE(memcmp(buffer, test2, strlen(test2)) == 0);
}

