/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "common.h"
#include "patchFile.h"
#include "easylzma/compress.h"
#include "easylzma/decompress.h"

#include "md5.h"
#include "PatchGenerator.h"
#include "FileFormat1.h"
#include "patchDiff.h"

// Everything here deals with calculating diffs between two patches

namespace PatchDiff
{
	// Find matching folder in targetFolderList corresponding to the one in srcFolderList
	FolderItr FindMatchingFolder(FolderList &targetFolderList, FolderList &srcFolderList, uint32_t folderId)
	{
		//printf("FindMatchingFolder(%u)\n", folderId);
		// Special case: target has no folders
		if (targetFolderList.size() == 0)
			return targetFolderList.end();

		// Special case: root
		if (folderId == 0)
		{
			if (srcFolderList[0].folderName != targetFolderList[0].folderName)
			{
				return targetFolderList.end();
			}
			else
			{
				return targetFolderList.begin();
			}
		}

		FolderItr entry = srcFolderList.begin() + folderId;

		// Get folder chain for this folder
		std::vector<std::string> folderChain;
		FolderItr itr;
		for (itr = srcFolderList.begin() + folderId; itr != srcFolderList.end(); itr = srcFolderList.begin() + itr->parentFolderIdx)
		{
			// Out at root
			if (itr == srcFolderList.begin())
				break;
			folderChain.push_back(itr->folderName);
		}

		// Find folder which matches chain
		itr = std::find_if(targetFolderList.begin(), targetFolderList.end(), 
			[entry, srcFolderList, targetFolderList, folderChain](const PatchFile::FolderInfo &needle){
			
			if (entry->parentCount == needle.parentCount)
			{
				// Each item in chain should match
				uint32_t idx = needle.parentFolderIdx;
				uint32_t count=1;

				assert(idx >= 0);

				if (folderChain.front() == needle.folderName)
				{
					//printf(">>>FOLDER CHAIN MATCH, CHECKING CHAIN...\n");
					while (idx != 0) // i.e. not root
					{
						//printf(">>>FOLDER %s EXPECTED %s\n", targetFolderList[idx].folderName.c_str(), folderChain[count].c_str());
						if (targetFolderList[idx].folderName != folderChain[count])
							break;
						count++;
						idx = targetFolderList[idx].parentFolderIdx;
					}

					if (count == folderChain.size())
					{
						return true;
					}
				}
			}

			return false;
		});

		printf("MATCHING FOUND %s\n", itr == targetFolderList.end() ? "NULL" : itr->folderName.c_str());
		return itr;
	}

	// Iterate backwards through folder trees summing up modCounts
	void FixModCounts(FolderList &targetFolderList, FileList &targetFileList, bool modFilesAreDeleted)
	{
		uint32_t maxParents = 0;
		for (auto info : targetFolderList)
		{
			maxParents = std::max(info.parentCount, maxParents);
		}

		uint32_t modCounts = 0;

		// Go backwards up tree summing up modCount,
		// also checking for deleted files
		for (uint32_t i=maxParents; i > 0; i--)
		{
			//printf("\tFixModCounts(%u, %s)\n", i, modFilesAreDeleted ? "DELETED_OR_PATCHED" : "ADDED_OR_PATCHED");
			for (auto &info : targetFolderList)
			{
				if (info.parentCount != i)
					continue;

				//printf("\t\tCHECKING FOLDER %s (mc=%u)...\n", info.folderName.c_str(), info.modCount);

				uint32_t numMod = 0;
				for (int j=info.startFolder; j<info.startFolder + info.numFolders; j++)
				{
					PatchFile::FolderInfo &childInfo = targetFolderList[j];
					//printf("\t\t\tFOLDER %s KIND=%u\n", childInfo.folderName.c_str(), childInfo.kind);
					if (modFilesAreDeleted && (childInfo.kind == PatchFile::KIND_DELETE || childInfo.kind == PatchFile::KIND_PATCH))
					{
						//printf("^^MARK MODIFIED FOLDER\n");
						info.markModified();
					}
					else if (!modFilesAreDeleted && (childInfo.kind == PatchFile::KIND_ADD || childInfo.kind == PatchFile::KIND_PATCH))
					{
						//printf("^^MARK MODIFIED FOLDER\n");
						info.markModified();
					}
					else if (childInfo.modCount > 0)
					{
						info.markModified();
					}
					numMod += childInfo.modCount;
				}

				// Mark ourselves as modified
				for (int j=info.startFile; j<info.startFile + info.numFiles; j++)
				{
					PatchFile::FileInfo &fileInfo = targetFileList[j];
					//printf("\t\t\tFILE %s KIND=%u\n", fileInfo.fileName.c_str(), fileInfo.kind);
					if (modFilesAreDeleted && (fileInfo.kind == PatchFile::KIND_DELETE || fileInfo.kind == PatchFile::KIND_PATCH))
					{
						//printf("^^MARK MODIFIED FILE\n");
						info.markModified();
					}
					else if (!modFilesAreDeleted && (fileInfo.kind == PatchFile::KIND_ADD || fileInfo.kind == PatchFile::KIND_PATCH))
					{
						//printf("^^MARK MODIFIED FILE\n");
						info.markModified();
					}
				}

				uint32_t newCount = info.modCount;
				//printf("nc==%u\n", newCount);

				newCount += numMod;
				if (newCount > 255)
					info.modCount = 255;
				else
					info.modCount += numMod;

				modCounts += info.modCount;
				//printf("New ModCount %u\n", info.modCount);
			}
		}

		targetFolderList[0].modCount = modCounts > 255 ? 255 : modCounts;
	}

	// Add files to patch from src which have been modified
	void AddModifiedFiles(PatchFile* patch, PatchFile* src, uint32_t folderIdx)
	{
		PatchFile::ListRange range = src->mFolders[folderIdx].getFileRange();

		FileList &patchFiles = patch->mFiles;
		FileList &srcFiles = src->mFiles;

		for (uint32_t i=range.start; i<range.getEnd(); i++)
		{
			PatchFile::FileInfo info = srcFiles[i];
			if (info.kind == PatchFile::KIND_NOP || info.kind == PatchFile::KIND_PATCH)
				continue;

			info.dataSrcIdx = patch->mDataSources.size();
			info.parentFolderIdx = folderIdx;
			patch->mDataSources.push_back(src->mDataSources[info.dataSrcIdx]);
			patchFiles.push_back(info);
		}
	}

	int GetIndexOfFileInList(const std::string &filename, FileList &fileList, PatchFile::ListRange range)
	{
		for (uint32_t i=range.start; i<range.getEnd(); i++)
		{
			if (fileList[i].fileName == filename)
				return i;
		}
		return -1;
	}

	int GetIndexOfFolderInList(const std::string &foldername, FolderList &folderList, PatchFile::ListRange range)
	{
		for (uint32_t i=range.start; i<range.getEnd(); i++)
		{
			if (folderList[i].folderName == foldername)
				return i;
		}
		return -1;
	}

	// For each file in srcFolderIdx marked patch, find corresponding entry in destFolderIdx and create a file in patch containing diff
	void GenerateFilePatches(PatchFile* patch, PatchFile* src, PatchFile* dest, uint32_t srcFolderIdx, uint32_t destFolderIdx, uint32_t patchFolderIdx)
	{ 
		printf("GenerateFilePatches(%u,%u,%u)\n", srcFolderIdx, destFolderIdx, patchFolderIdx);
		FileList &patchFiles = patch->mFiles;
		FileList &srcFiles = src->mFiles;
		FileList &destFiles = dest->mFiles;

		PatchFile::FolderInfo &srcFolder = src->mFolders[srcFolderIdx];
		PatchFile::FolderInfo &destFolder = dest->mFolders[destFolderIdx];
		PatchFile::ListRange range = srcFolder.getFileRange();

		for (uint32_t i=range.start; i<range.getEnd(); i++)
		{
			PatchFile::FileInfo info = srcFiles[i];
			if (info.kind != PatchFile::KIND_PATCH)
				continue;

			int destIdx = GetIndexOfFileInList(info.fileName, dest->mFiles, destFolder.getFileRange());
			if (destIdx < 0)
			{
				printf("File %s marked as patch but doesn't exist (folder %s vs %s)!!!\n", info.fileName.c_str(), srcFolder.folderName.c_str(), destFolder.folderName.c_str());
			}
			assert(destIdx >= 0);

			PatchFile::FileInfo &destInfo = destFiles[destIdx];
			PatchDataSource* newFile = PatchFile::generateFileDiff(src->mDataSources[info.dataSrcIdx], dest->mDataSources[destInfo.dataSrcIdx]);

			// patch_crc is crc of file before patching, crc is new crc.
			memcpy(info.patch_md5, info.md5, 16);
			info.patch_crc = info.crc;
			memcpy(info.md5, destInfo.md5, 16);
			info.crc = destInfo.crc;

			info.ctime = destInfo.ctime;
			info.mtime = destInfo.mtime;
			info.offset = 0;
			info.size = newFile->getSize();
			info.dataSrcIdx = patch->mDataSources.size();
			info.parentFolderIdx = patchFolderIdx;
			patch->mDataSources.push_back(newFile);
			patch->mFiles.push_back(info);
		}
	}

	// Resursively insert child folders of info into patch, keeping track of matching folders in srcFolderIdx and destFolderIdx
	void RecurseAddFoldersInFolder(PatchFile* patch, PatchFile::FolderInfo &info, int patchFolderIdx, PatchFile* src, PatchFile* dest, int32_t srcFolderIdx, int32_t destFolderIdx)
	{
		PatchFile::FolderInfo* srcInfo = NULL;
		PatchFile::FolderInfo* destInfo = NULL;

		if (srcFolderIdx < 0 && destFolderIdx < 0)
		{
			assert(false);
		}

		if (srcFolderIdx < 0)
		{
			info.kind = dest->mFolders[destFolderIdx].kind;
			info.folderName = dest->mFolders[destFolderIdx].folderName;
			destInfo = &dest->mFolders[destFolderIdx];
		}
		else if (destFolderIdx < 0)
		{
			info.kind = src->mFolders[srcFolderIdx].kind;
			info.folderName = src->mFolders[srcFolderIdx].folderName;
			srcInfo = &src->mFolders[srcFolderIdx];
		}
		else
		{
			info.kind = src->mFolders[srcFolderIdx].kind;
			info.folderName = src->mFolders[srcFolderIdx].folderName;
			srcInfo = &src->mFolders[srcFolderIdx];
			destInfo = &dest->mFolders[destFolderIdx];
		}

		info.startFolder = patch->mFolders.size();
		info.numFolders = 0;
		info.startFile = patch->mFiles.size();
		info.numFiles = 0;

		if (info.kind == PatchFile::KIND_DELETE)
		{
			return;
		}
		else
		{
			// Add files in this folder
			if (srcInfo) AddModifiedFiles(patch, src, srcFolderIdx);
			if (destInfo) AddModifiedFiles(patch, dest, destFolderIdx);

			// Generate patches
			if (srcInfo && destInfo)
			{
				//printf("GENERATING PATCHES FOR ANY FILES IN %s INTO PATCH FOLDER %u\n", srcInfo->folderName.c_str(), patchFolderIdx);
				GenerateFilePatches(patch, src, dest, srcFolderIdx, destFolderIdx, patchFolderIdx);
			}

			info.numFiles = patch->mFiles.size() - info.startFile;
			//printf("TOTAL FILES HERE: %u\n", info.numFiles);
		}

		PatchFile::ListRange range;

		// Add any NOP or DEL folders in src
		if (srcInfo)
		{
			range = srcInfo->getFolderRange();
			for (uint32_t i=range.start; i<range.getEnd(); i++)
			{
				if (src->mFolders[i].modCount == 0)
					continue;

				PatchFile::FolderInfo childFolder;
				childFolder.kind = src->mFolders[i].kind;
				childFolder.folderName = src->mFolders[i].folderName;
				childFolder.srcIdx = i;
				childFolder.destIdx = -1;
				childFolder.parentFolderIdx = patchFolderIdx;
				patch->mFolders.push_back(childFolder);
				info.numFolders++;
			}
		}

		// Add any new or modified folders in dest
		if (destInfo)
		{
			range = destInfo->getFolderRange();
			for (uint32_t i=range.start; i<range.getEnd(); i++)
			{
				if (dest->mFolders[i].modCount == 0)
					continue;

				// Ignore if folder already exists in patch
				if (srcInfo)
				{
					int patchIdx = GetIndexOfFolderInList(dest->mFolders[i].folderName, patch->mFolders, patch->mFolders[patchFolderIdx].getFolderRange());
					if (patchIdx >= 0)
					{
						patch->mFolders[patchIdx].destIdx = i;
						continue;
					}
				}

				PatchFile::FolderInfo childFolder;
				childFolder.kind = dest->mFolders[i].kind;
				childFolder.folderName = dest->mFolders[i].folderName;
				childFolder.parentFolderIdx = patchFolderIdx;
				childFolder.srcIdx = -1;
				childFolder.destIdx = i;
				patch->mFolders.push_back(childFolder);
				info.numFolders++;
			}
		}

		// Recurse into sub-folders
		range = info.getFolderRange();
		//printf("Iterating into patch folder range %i...%i\n", range.start, range.getEnd());
		for (int i=range.start; i<range.getEnd(); i++)
		{
			//printf("ITR FOLDER %s\n", patch->mFolders[i].folderName.c_str());
			//printf("RecurseAddFoldersInFolder(p=%i, s=%i, d=%i)\n", i, patch->mFolders[i].srcIdx, patch->mFolders[i].destIdx);
			RecurseAddFoldersInFolder(patch, patch->mFolders[i], i, src, dest, patch->mFolders[i].srcIdx, patch->mFolders[i].destIdx);
		}
	}
}

using namespace PatchDiff;

static void PrintFolderList(FolderItr folder, std::vector<PatchFile::FolderInfo> &folders)
{
	std::vector<FolderItr> dirs;
	dirs.push_back(folder);

	while (true)
	{
		folder = folders.begin() + folder->parentFolderIdx;
		if (folder == folders.begin())
			break;
		dirs.push_back(folder);
	}

	for (auto itr=dirs.rbegin(), itrEnd = dirs.rend(); itr != itrEnd; itr++)
	{
		printf("%s/", (*itr)->folderName.c_str());
	}
}

PatchFile* PatchFile::generateDiff(PatchFile* src, PatchFile* dest)
{
	// TODO: generate a third patch file which filters out all the differences between src and dest
	/*
		Algorithm:

		- Clear all files and folders in src to D
		- Clear all files and folders in dst to A
		- Iterate reursively through folders in src
			- For each file/folder, find corresponding entry in dst
			- If an entry exists, check the CRC and MD5. 
				- If they differ, mark both entries as P and replace data with patch data
				- If they match, mark both entries as N
			- Tally up the count of modified folders & files into each folder entry
		- Iterate through both src and dest, matching up parent folders
			- Write new set of REQUIRED entries to return patch (ignoring unmodified folders)
	*/

	if (src->mPatchKind != PATCHFILE_REPLACE && dest->mPatchKind != PATCHFILE_REPLACE)
	{
		// Can't calculate patches of patches
		return NULL;
	}

	// src and dest root need to be the same
	if (src->mFolders[0].folderName != dest->mFolders[0].folderName)
	{
		return NULL;
	}

	// Stage 1: initial state
	// Src is assumed to be DELETE
	// Dest is assumed to be ADD

	for (auto& file : src->mFiles)
	{
		file.kind = KIND_DELETE;
	}

	for (auto& folder : src->mFolders)
	{
		folder.kind = KIND_DELETE;
		folder.modCount = 0;
	}

	for (auto& file : dest->mFiles)
	{
		file.kind = KIND_ADD;
	}

	for (auto& folder : src->mFolders)
	{
		folder.kind = KIND_ADD;
		folder.modCount = 0;
	}

	printf("Stage 1: everthing marked\n");

	// Stage 2: mark nops, adds, and patches in both lists

	for (FolderItr destFolderItr = dest->mFolders.begin(), destFolderItrEnd = dest->mFolders.end(); destFolderItr != destFolderItrEnd; destFolderItr++)
	{
		printf("destF(%s)...\n", destFolderItr->folderName.c_str());
		printf("DEBUG DESTF=");
		PrintFolderList(destFolderItr, dest->mFolders);
		printf("\n");
		
		ListRange destFileRange = destFolderItr->getFileRange();
		FolderItr srcFolder = FindMatchingFolder(src->mFolders, dest->mFolders, destFolderItr - dest->mFolders.begin());

		if (srcFolder != src->mFolders.end())
		{
			printf("\tMATCH: found destF(%s) in srcF(%s) (srcIdx=%u)\n", destFolderItr->folderName.c_str(), srcFolder->folderName.c_str(), (uint32_t)(srcFolder - src->mFolders.begin()));
			// src still has this folder
			destFolderItr->kind = PatchFile::KIND_NOP;
			srcFolder->kind = PatchFile::KIND_NOP;


			// Compare files in dest with those in src
			for (FileItr destFileItr = dest->mFiles.begin()+(destFileRange.start), 
				destFileItrEnd = dest->mFiles.begin()+(destFileRange.getEnd()); 
				destFileItr != destFileItrEnd; destFileItr++)
			{
				int idx = GetIndexOfFileInList(destFileItr->fileName, src->mFiles, srcFolder->getFileRange());
				printf("\tsrcIdx(%s) == %i range=%u...%u\n", destFileItr->fileName.c_str(), idx, srcFolder->getFileRange().start, srcFolder->getFileRange().getEnd());
				
				printf("\t DEBUG FOLDER SHOULD BE ->");
				PrintFolderList(srcFolder, src->mFolders);
				printf("\n");

				for (uint32_t k=srcFolder->getFileRange().start; k<srcFolder->getFileRange().getEnd(); k++)
				{
					printf("\t\tDEBUG srcF[%u] == %s\n", k, src->mFiles[k].fileName.c_str());
				}

				PatchFile::FileInfo* srcFile = idx >= 0 ? &src->mFiles[idx] : NULL;

				if (srcFile)
				{
					// File exists in both src and dest, so they can either be NOP or PATCH
					if (srcFile->isSameAs(*destFileItr))
					{
						printf("\tsrc(%s) == dest(%s)\n", srcFile->fileName.c_str(), destFileItr->fileName.c_str());
						srcFile->kind = KIND_NOP;
						destFileItr->kind = KIND_NOP;
					}
					else
					{
						printf("\tsrc(%s) != dest(%s)\n", srcFile->fileName.c_str(), destFileItr->fileName.c_str());
						srcFile->kind = KIND_PATCH;
						destFileItr->kind = KIND_PATCH;
						srcFolder->markModified();
						destFolderItr->markModified();
					}
				}
				else
				{
					// Can only be an add
					printf("\tdest(%s) ADDED\n", destFileItr->fileName.c_str());
					destFileItr->kind = KIND_ADD;
					srcFolder->markModified();
				}
			}
		}
		else
		{
			printf("\tsrcF(%s) ADDED\n", destFolderItr->folderName.c_str());
			// In this case, all files in dest have to be new
			destFolderItr->kind = PatchFile::KIND_ADD;
			destFolderItr->markModified();
			
			// Also mark files as new
			for (FileItr destFileItr = dest->mFiles.begin()+(destFileRange.start), 
				destFileItrEnd = dest->mFiles.begin()+(destFileRange.getEnd()); destFileItr != destFileItrEnd; destFileItr++)
			{
				destFileItr->kind = PatchFile::KIND_ADD;
			}
		}
	}

	printf("Stage 2: everthing diff'd\n");

	printf("SRC...\n");
	src->printListing();
	printf("DEST...\n");
	dest->printListing();


	PatchFile* newPatch = new PatchFile();

	// Reserve folders and files
	newPatch->mFolders.reserve(src->mFolders.size() + dest->mFolders.size() + 1);
	newPatch->mFiles.reserve(src->mFiles.size() + dest->mFiles.size() + 1);

	printf("==FIXMODCOUNTS SRC==\n");
	FixModCounts(src->mFolders, src->mFiles, true);
	printf("==FIXMODCOUNTS DEST==\n");
	FixModCounts(dest->mFolders, dest->mFiles, false);

	for (auto &folder : src->mFolders)
	{
		printf("srcFolder[%s] MODCOUNT=%i\n", folder.folderName.c_str(), folder.modCount);
	}
	for (auto &folder : dest->mFolders)
	{
		printf("destFolder[%s] MODCOUNT=%i\n", folder.folderName.c_str(), folder.modCount);
	}

	// Stage 3: combine list into single list
	// NOTE: We ignore any NOP entries with modCount == 0
	FolderInfo root;
	root.startFolder = 1;
	newPatch->mFolders.push_back(root);
	RecurseAddFoldersInFolder(newPatch, newPatch->mFolders.back(), 0, src, dest, 0, 0);

	printf("Stage 3: everthing merged into new\n");

	// All folders left in patch which aren't DEL or NOP should be PATCH (TODO: check this esp with longer root paths)
	for (int i=1; i<newPatch->mFolders.size(); i++)
	{
		if (newPatch->mFolders[i].kind == PatchFile::KIND_NOP)
		{
			newPatch->mFolders[i].kind = PatchFile::KIND_PATCH;
		}
	}

	return newPatch;
}

