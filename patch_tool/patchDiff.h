/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
namespace PatchDiff
{
	typedef const std::vector<PatchFile::FolderInfo> CFolderList;
	typedef std::vector<PatchFile::FolderInfo> FolderList;
	typedef std::vector<PatchFile::FolderInfo>::iterator FolderItr;
	typedef std::vector<PatchFile::FolderInfo>::const_iterator CFolderItr;
	typedef const std::vector<PatchFile::FileInfo> CFileList;
	typedef std::vector<PatchFile::FileInfo> FileList;
	typedef std::vector<PatchFile::FileInfo>::iterator FileItr;
	typedef std::vector<PatchFile::FileInfo>::const_iterator CFileItr;


	// Find matching folder in targetFolderList corresponding to the one in srcFolderList
	FolderItr FindMatchingFolder(FolderList &targetFolderList, FolderList &srcFolderList, uint32_t folderId);

	// Iterate backwards through folder trees summing up modCounts
	void FixModCounts(FolderList &targetFolderList, FileList &targetFileList, bool modFilesAreDeleted);

	// Add files to patch from src which have been modified
	void AddModifiedFiles(PatchFile* patch, PatchFile* src, uint32_t folderIdx);

	int GetIndexOfFileInList(const std::string &filename, FileList &fileList, PatchFile::ListRange range);

	int GetIndexOfFolderInList(const std::string &foldername, FolderList &folderList, PatchFile::ListRange range);

	// For each file in srcFolderIdx marked patch, find corresponding entry in destFolderIdx and create a file in patch containing diff
	void GenerateFilePatches(PatchFile* patch, PatchFile* src, PatchFile* dest, uint32_t srcFolderIdx, uint32_t destFolderIdx, uint32_t patchFolderIdx);

	// Resursively insert child folders of info into patch, keeping track of matching folders in srcFolderIdx and destFolderIdx
	void RecurseAddFoldersInFolder(PatchFile* patch, PatchFile::FolderInfo &info, int folderIdx, PatchFile* src, PatchFile* dest, int32_t srcFolderIdx, int32_t destFolderIdx);
}