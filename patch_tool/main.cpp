/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include "common.h"
#include "patchFile.h"
#include "patchConfig.h"
#include <vector>
#include "optparse.h"

using namespace fs;

int main(int argc, char **argv)
{
	PatchFile::sIgnoreFiles.push_back(".DS_Store");
	PatchFile::sIgnoreFiles.push_back(".git");

    optparse::OptionParser parser = optparse::OptionParser().description("patch_tool");

    parser.add_option("-p", "--path").dest("path").set_default("")
          .help("Put files on root path");
    parser.add_option("-no-blank", "--no-blank").dest("no-blank").set_default("")
          .help("Dont write a blank patch");

    const optparse::Values options = parser.parse_args(argc, argv);
    const std::vector<std::string> args = parser.args();

    if (args.size() < 2)
    {
    	printf("Please specify method and input paths.\n");
    	return 1;
    }

	PatchFile srcPatch;
	PatchFile destPatch;
	PatchMemStreamPtr srcS = std::make_shared<PatchMemStream>(100*1024);
	PatchMemStreamPtr destS = std::make_shared<PatchMemStream>(100*1024);
	bool writeBlank = !options.get("no-blank");

    if (args[0] == "create")
    {
    	if (args.size() == 4)
    	{
			auto patchName = path(args[1]);
			auto srcLoc = path(args[2]);
			auto destLoc = path(args[3]);
			auto rootPath = path((const char*)options.get("path"));

			if (is_directory(srcLoc))
			{
				srcPatch.beginEmpty(rootPath);
				srcPatch.populateFromDirectory(srcLoc, srcPatch.mFolders.size()-1);
			}
			else
			{
				srcPatch.readFromFile(*srcS.get(), srcLoc);
			}

			if (is_directory(destLoc))
			{
				destPatch.beginEmpty(rootPath);
				destPatch.populateFromDirectory(destLoc, destPatch.mFolders.size()-1);
			}
			else
			{
				destPatch.readFromFile(*destS.get(), destLoc);
			}

			printf("SRC...\n");
			srcPatch.printListing();
			printf("DEST...\n");
			destPatch.printListing();
			
			PatchFile *diffPatch = PatchFile::generateDiff(&srcPatch, &destPatch);
			if (diffPatch)
			{
				printf("DIFF...\n");
				diffPatch->printListing();
			}

			if (!writeBlank && diffPatch->mFiles.size() == 0)
			{
				fprintf(stderr, "Patch is blank\n");
				return 1;
			}

			MemStream patchS(100*1024);
			diffPatch->compressToFile(patchS, patchName);
    	}
    	else if (args.size() == 3)
    	{
			auto patchName = path(args[1]);
			auto srcLoc = path(args[2]);
			auto rootPath = path((const char*)options.get("path"));

			if (is_directory(srcLoc))
			{
				MemStream patchS(100*1024);
				srcPatch.beginEmpty(rootPath);
				srcPatch.populateFromDirectory(srcLoc, srcPatch.mFolders.size()-1);

				if (!writeBlank && srcPatch.mFiles.size() == 0)
				{
					fprintf(stderr, "Patch is blank\n");
					return 1;
				}

				srcPatch.compressToFile(patchS, patchName);
			}
			else
			{
				printf("create needs a directory\n");
				return 1;
			}
    	}
    	else
    	{
    		printf("Usage: create <src_folder> <dest_folder> / <src_folder>\n");
    	}
    }
    else if (args[0] == "apply")
    {
    	if (args.size() == 3)
    	{
			auto srcLoc = path(args[1]);
			auto destLoc = path(args[2]);

			if (!is_regular_file(srcLoc) || !srcPatch.readFromFile(*(srcS.get()), destLoc))
			{
				printf("apply needs a valid patch file\n");
				return 1;
			}

			srcPatch.apply(destLoc);
    	}
    	else
    	{
    		printf("Usage: apply <patch> <root folder>\n");
    	}
    }
    else if (args[0] == "ls")
    {
    	if (args.size() == 2)
    	{
			auto srcLoc = path(args[1]);

			if (!is_regular_file(srcLoc))
			{
				printf("ls needs a file\n");
				return 1;
			}

			if (!srcPatch.readFromFile(*(srcS.get()), srcLoc))
			{
				printf("ls needs a valid patch file\n");
				return 1;
			}

			srcPatch.printListing();
    	}
    	else
    	{
    		printf("Usage: ls <patch>\n");
    	}
    }
    else if (args[0] == "extract")
    {
    	if (args.size() == 3)
    	{
			auto srcLoc = path(args[1]);
			auto destLoc = path(args[2]);

			if (!is_regular_file(srcLoc))
			{
				printf("extract needs a file\n");
				return 1;
			}

			if (!srcPatch.readFromFile(*(srcS.get()), srcLoc))
			{
				printf("extract needs a valid patch file\n");
				return 1;
			}

			srcPatch.dumpFiles(destLoc);
    	}
    	else
    	{
    		printf("Usage: extract <patch> <dest>\n");
    	}
    }
    else if (args[0] == "dumpcfg")
    {
    	PatchConfig srcCfg;
    	if (args.size() == 3)
    	{
			auto srcDat = path(args[1]);
			auto destJSON = path(args[2]);

			if (!is_regular_file(srcDat))
			{
				printf("dumpcfg needs a file\n");
				return 1;
			}

			if (!srcCfg.readFromFile(*(srcS.get()), srcDat))
			{
				printf("dumpcfg needs a valid patch file\n");
				return 1;
			}

			json_t* root = json_object();
			srcCfg.toJSON(root);
			json_incref(root);

			if (json_dump_file(root, destJSON.c_str(), JSON_INDENT(2)) < 0)
			{
				printf("dumpcfg failed to write JSON output\n");
				return 1;
			}

			json_decref(root);
    	}
    	else
    	{
    		printf("Usage: dumpcfg <patch> <patch.json>\n");
    	}
    }
    else if (args[0] == "savecfg")
    {
    	PatchConfig destCfg;
    	if (args.size() == 3)
    	{
			auto srcJSON = path(args[1]);
			auto destDat = path(args[2]);

			if (!is_regular_file(srcJSON))
			{
				printf("savecfg needs a file\n");
				return 1;
			}

			json_error_t error;
			json_t* root = json_load_file(srcJSON.c_str(), JSON_PRESERVE_ORDER, &error);
			if (root)
			{
				destCfg.fromJSON(root);
			}
			else
			{
				printf("savecfg needs a valid JSON file\n");
				return 1;
			}

			json_decref(root);

			if (!destCfg.writeToFile(*(srcS.get()), destDat))
			{
				printf("savecfg failed to write patch\n");
				return 1;
			}
    	}
    	else
    	{
    		printf("Usage: savecfg <patch> <patch.json>\n");
    	}

    }
    else
    {
    	printf("Please specify create, apply, ls, extract, dumpcfg or savecfg.\n");
    	return 1;
    }

    return 0;
}