/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "common.h"
#include "patchFile.h"
#include "easylzma/compress.h"
#include "easylzma/decompress.h"
#include "zlib.h"
#include "md5.h"
#include "PatchGenerator.h"
#include "FileFormat1.h"
#include "POSIXUtil.h"

std::vector<std::string> PatchFile::PatchFile::sIgnoreFiles;

/* 

Notes on lists:

- Root folder is always of kind NOP
- Folders with a modified child or modified file listing are all of kind PATCH (down to root)
- When symlinks change, they are simply added again (should also apply to replaced files)
- With an ADD patch, everything is ADD except for the root
- Deleted folders are marked with DEL. Children are omitted.
- All files appear to be listed in alphabetical order.

*/

PatchFile::~PatchFile()
{
	clear();
}

void PatchFile::clear()
{
	mFiles.clear();
	mFolders.clear();
	for (auto src : mDataSources)
	{
		delete src;
	}
	mDataSources.clear();
}

bool PatchFile::read(PatchMemStream &s)
{
	uint16_t magic;
	s.readUintBE(magic);

	if (magic != 20561)
		return false;

	uint8_t version;

	s.read(version);
	s.read(mPatchKind);

	if (version != 2)
		return false;

	s.readUintBE(mCurrentDataStart);

	mFiles.clear();
	mFolders.clear();

	mFolders.push_back(FolderInfo());
	
	if (!readFolder(s, mFolders[0], 0))
		return false;

	assert(mCurrentDataStart == s.getPosition());

	s.setPosition(mCurrentDataStart);
	s.readUintBE(mCurrentDataEnd);

	return true;
}

uint32_t PatchFile::updateFileOffsets(uint32_t newOffset)
{
	std::vector<FileInfo>::iterator itr;
	std::vector<FileInfo>::iterator itrEnd;

	for (itr=mFiles.begin(), itrEnd = mFiles.end(); itr != itrEnd; itr++)
	{
		if (itr->isLink || itr->kind == PatchFile::KIND_DELETE)
			continue;
		itr->offset = newOffset;
		newOffset += itr->size;
	}
	return newOffset;
}

bool PatchFile::write(MemStream &s)
{
	uint16_t magic=20561;
	s.writeUintBE(magic);

	s.write((uint8_t)VERSION);
	s.write(mPatchKind);

	s.writeUintBE(mCurrentDataStart);
	
	// Write folders so we get header size
	if (!writeFolder(s, mFolders[0], 0))
		return false;

	mCurrentDataStart = s.getPosition();
	mCurrentDataEnd = 0;
	s.write(mCurrentDataEnd);

	mCurrentDataEnd = updateFileOffsets(mCurrentDataStart+4);

	// Now write data so we get that size
	writeFileData(s);

	// Write folders again
	s.setPosition(4);
	s.writeUintBE(mCurrentDataStart);
	if (!writeFolder(s, mFolders[0], 0))
		return false;
	assert(mCurrentDataStart == s.getPosition());

	// Write offsets again
	s.setPosition(mCurrentDataStart);
	s.writeUintBE(mCurrentDataEnd);

	s.setPosition(mCurrentDataEnd);
	return true;
}

bool PatchFile::writeFileData(MemStream &s)
{
	std::vector<FileInfo>::iterator itr;
	std::vector<FileInfo>::iterator itrEnd;
	uint32_t offset = 0;
	for (itr=mFiles.begin(), itrEnd = mFiles.end(); itr != itrEnd; itr++)
	{
		if (itr->isLink || itr->kind == PatchFile::KIND_DELETE)
			continue;

		char buffer[4096];
		uint32_t fileOffset = 0;
		uint32_t bytesLeft = itr->size;
		PatchDataSource* dataSource = mDataSources[itr->dataSrcIdx];
		s.growPosition(itr->offset);

		printf("FILE %s offset %u\n", itr->fileName.c_str(), itr->offset);

		while (bytesLeft != 0)
		{
			uint32_t bytesToWrite = std::min<uint32_t>(bytesLeft, (uint32_t)sizeof(buffer));
			dataSource->read(buffer, fileOffset, bytesToWrite);
			s.write(bytesToWrite, buffer);
			bytesLeft -= bytesToWrite;
			fileOffset += bytesToWrite;
		}

		offset = std::max(itr->offset + itr->size, offset);
	}
	mCurrentDataEnd = offset;
	return true;
}

PatchDataSource* PatchFile::generateFileDiff(PatchDataSource* d1, PatchDataSource* d2)
{
	std::stringstream source;
	std::stringstream target;
	std::stringstream patch;

	char* buffer = new char[d1->getSize()];
	d1->read(buffer, 0, d1->getSize());
	source.write(buffer, d1->getSize());
	delete[] buffer;
	buffer = new char[d2->getSize()];
	d2->read(buffer, 0, d2->getSize());
	target.write(buffer, d2->getSize());
	delete[] buffer;

    PatchGenerator* gen = new PatchGenerator(source,d1->getSize(),target,d2->getSize(),patch);
    gen->blockSize = 16;
    gen->maxMatches = 500;

	// create sameBlock storage
	std::vector<SameBlock*> sameBlocks;
	// run the patch generator to find similar blocks
	gen->execute(sameBlocks);

	FileFormat1::writePatch(patch,target,sameBlocks);
	// cleanup sameblocks
	for(vector<SameBlock*>::iterator iter = sameBlocks.begin(); iter != sameBlocks.end(); iter++) {
		delete *iter;
		*iter = NULL;
	}

	uint32_t patchSize = patch.tellp();
	patch.seekp(0);

	PatchMemStreamSource* newSrc = new PatchMemStreamSource();
	newSrc->s = std::make_shared<PatchMemStream>(patchSize);
	newSrc->mSrcOffset = 0;
	newSrc->mSize = patchSize;
	newSrc->s->mSize = patchSize;
	patch.read((char*)newSrc->s->mPtr, patchSize);

	return newSrc;
}

class PatchBlockReader
{
public:
	uint32_t offset;

	StackMemStream<8> tmp;
	PatchDataSource* src;

	PatchBlockReader() : offset(0), src(NULL) {;}

	bool read(uint32_t size, void* data) {
		if (offset + size > src->getSize())
			return false;
		src->read(data, offset, size);
		offset += size;
		return true;
	}

	inline void readUintBE(uint16_t &out) { tmp.setPosition(0); src->read(tmp.mPtr, offset, 2); offset += 2; tmp.readUintBE(out); }
	inline void readUintBE(uint32_t &out) { tmp.setPosition(0); src->read(tmp.mPtr, offset, 4); offset += 4; tmp.readUintBE(out); }
};

PatchDataSource* PatchFile::applyFileDiff(PatchDataSource* data, PatchDataSource* patch)
{
	/*
	Format:

	NumBlocks
	BodySize

	Each block
		type
		num[type] blockSize
		if type < 4:
			uint32 srcOffset
			uint8 data[blockSize] (in src)
		else:
			uint8 data[blockSize] (in patch)

	*/
	uint8_t buffer[16384];
	PatchBlockReader patchReader;
	PatchBlockReader srcReader;
	patchReader.src = patch;
	srcReader.src = data;
	uint32_t magic = 0; patchReader.readUintBE(magic); // 1413566551
	uint32_t patch_blocks = 0; patchReader.readUintBE(patch_blocks);
	uint32_t eof = 0; patchReader.readUintBE(eof);
	//printf("MAGIC=%u. blocks=%u eof=%u\n", magic, patch_blocks, eof);

	PatchMemStreamPtr destPatchStream = std::make_shared<PatchMemStream>(data->getSize());
	MemStream &destStream = *destPatchStream.get();

	while (patch_blocks--) {
		unsigned char blocktype = 0;
		unsigned long blocksize = 0;
		if(!patchReader.read(1, &blocktype)) {
	      assert(false);
		  return NULL;
		}
		//printf("BlockType == %u\n", (uint32_t)blocktype);

		switch (blocktype) {
		case 1:
		case 2:
		case 3:
		  if (blocktype == 1)
		    { unsigned char x=0; patchReader.read(1, &x); blocksize = x; }
		  else if (blocktype == 2)
		    { unsigned short x=0; patchReader.readUintBE(x); blocksize = x; }
		  else
		    { uint32_t x=0;  patchReader.readUintBE(x); blocksize = x; }

		  if (!blocksize)
		  {
	      assert(false);
		    return NULL;
			}

		  srcReader.offset=0; patchReader.readUintBE(srcReader.offset);

		  do {
		  	uint32_t toRead = std::min<uint32_t>(sizeof(buffer), blocksize);
		    if(!srcReader.read(toRead, buffer)) {
	      assert(false);
		      return NULL;
		    }
		    if (!destStream.write(toRead, buffer))
		    {
	      		assert(false);
		    	return NULL;
		    }
		    blocksize -= toRead;
		  } while (blocksize);

		  break;

		case 5:
		case 6:
		case 7:
		  if (blocktype == 5)
		    { unsigned char x=0; patchReader.read(1, &x); blocksize = x; }
		  else if (blocktype == 6)
		    { unsigned short x=0; patchReader.readUintBE(x); blocksize = x; }
		  else
		    { uint32_t x=0;  patchReader.readUintBE(x); blocksize = x; }

		  if (!blocksize)
		  {
	      assert(false);
		    return NULL;
			}
		  
		  do {
		  	uint32_t toRead = std::min<uint32_t>(sizeof(buffer), blocksize);
		    if(!patchReader.read(toRead, &buffer)) {
	      assert(false);
		      return NULL;
		    }
		    destStream.write(toRead, buffer);
		    blocksize -= toRead;
		  } while (blocksize);

		  break;

		default:

	      assert(false);
		  return NULL;
		}
	}

	//printf("DEST FILE SIZE == %u\n", destStream.mSize);

	PatchMemStreamSource* newSrc = new PatchMemStreamSource();
	newSrc->s = destPatchStream;
	newSrc->mSrcOffset = 0;
	newSrc->mSize = destStream.getStreamSize();
	return newSrc;
}

uint32_t PatchDataSource::calcCRC()
{
	uint32_t crc=0;
	uint8_t buf[4096];
	uint32_t bytesLeft = getSize();
	uint32_t offset = 0;

	while (bytesLeft != 0)
	{
		uint32_t bytesToRead = bytesLeft > sizeof(buf) ? sizeof(buf) : bytesLeft;
		read(buf, offset, bytesToRead);
		bytesLeft -= bytesToRead;
		offset += bytesToRead;
		crc = crc32(crc, buf, bytesToRead);
	}

	return crc;
}

void PatchDataSource::calcMD5(uint8_t* outMD5)
{
	md5_state_t state;
	md5_init(&state);

	uint8_t buf[4096];
	uint32_t bytesLeft = getSize();
	uint32_t offset = 0;

	while (bytesLeft != 0)
	{
		uint32_t bytesToRead = bytesLeft > sizeof(buf) ? sizeof(buf) : bytesLeft;
		read(buf, offset, bytesToRead);
		md5_append(&state, buf, bytesToRead);
		bytesLeft -= bytesToRead;
		offset += bytesToRead;
	}
	md5_finish(&state, outMD5);
}

static void EnumDir(PatchFile &patch, const fs::path &path, uint32_t rootFolderIdx, uint32_t parentCount)
{
	fs::directory_iterator it(path);
	PatchFile::FolderInfo& info = patch.mFolders[rootFolderIdx];
	info.startFile = patch.mFiles.size();
	info.numFiles = 0;
	info.startFolder = patch.mFolders.size();
	info.numFolders = 0;

	// Stick everything in nice lists so we can do sorting and better iterate down the line
	std::vector<fs::path> directories;
	std::vector<fs::path> files;

	for (it = fs::directory_iterator(path); it != fs::directory_iterator{}; it++)
	{
		if (fs::is_regular_file(it->path()))
		{
			files.push_back(it->path());
		}
		else if (fs::is_directory(it->path()))
		{
			directories.push_back(it->path());
		}
	}

	auto pathSort = [](const fs::path& a, const fs::path& b) {
		return std::strcmp(a.c_str(), b.c_str()) < 0;	
	};

	std::sort(directories.begin(), directories.end(), pathSort);
	std::sort(files.begin(), files.end(), pathSort);

	// Do files first
	for (const fs::path &entry : files)
	{
		auto itr = std::find(PatchFile::sIgnoreFiles.begin(), PatchFile::sIgnoreFiles.end(), std::string(entry.filename().c_str()));
		if (itr != PatchFile::sIgnoreFiles.end())
			continue;

		PatchFile::FileInfo finfo;
		finfo.kind = PatchFile::KIND_ADD;
		finfo.parentFolderIdx = rootFolderIdx;

		fs::perms pm = fs::status(entry).permissions();
		if (((uint32_t)pm & (uint32_t)fs::perms::others_exec) != 0)
			finfo.isExecutable = true;
		else
			finfo.isExecutable = false;

		POSIX::ALT_FILETIME time = POSIX::getFileTime(entry.c_str());
		finfo.ctime = time.dwHighDateTime;
		finfo.mtime = time.dwLowDateTime;

		//if (finfo.isExecutable) { printf("FILE %s is executable!!\n", entry.c_str()); }

		if (fs::is_symlink(entry))
		{
			finfo.isLink = true;
			finfo.fileName = entry.filename().string();

			auto canon = fs::canonical(entry);
			finfo.pathName = fs::relative(canon, path).c_str();
			//printf("FILE %s is symlink!!\n", entry.c_str());
		}
		else
		{
			//printf("FILE %s is NOT symlink!!\n", entry.c_str());
			finfo.dataSrcIdx = patch.mDataSources.size()-1;
			PatchFileSource* fileSrc = new PatchFileSource();
			fileSrc->mFilename = entry.c_str();
			if (!fileSrc->open())
				continue;

			// Calc CRC, MD5
			finfo.crc = fileSrc->calcCRC();
			fileSrc->calcMD5(finfo.md5);

			finfo.size = fileSrc->getSize();

			finfo.fileName = entry.filename().c_str();

			finfo.dataSrcIdx = patch.mDataSources.size();
			patch.mDataSources.push_back(fileSrc);
		}

		patch.mFolders[rootFolderIdx].numFiles++;
		patch.mFiles.push_back(finfo);
	}

	// Now folders
	for (const fs::path &entry : directories)
	{
		if (fs::is_directory(entry))
		{
      		auto itr = std::find(PatchFile::sIgnoreFiles.begin(), PatchFile::sIgnoreFiles.end(), std::string(entry.filename().c_str()));
      		if (itr != PatchFile::sIgnoreFiles.end())
      			continue;

			PatchFile::FolderInfo subdir;
			subdir.kind = PatchFile::KIND_ADD;
			subdir.folderName = entry.filename().c_str();
			subdir.parentCount = parentCount;
			subdir.parentFolderIdx = rootFolderIdx;
			patch.mFolders.push_back(subdir);
			patch.mFolders[rootFolderIdx].numFolders++;
		}
	}

	// Recurse into folders (should be same order)
	int ofs=info.startFolder;
	for (const fs::path &entry : directories)
	{
  		auto itr = std::find(PatchFile::sIgnoreFiles.begin(), PatchFile::sIgnoreFiles.end(), std::string(entry.filename().c_str()));
  		if (itr != PatchFile::sIgnoreFiles.end())
  			continue;

		if (fs::is_directory(entry))
		{
			EnumDir(patch, entry, ofs++, parentCount+1);
		}
	}
}

void EnumPath(PatchFile& patch, const fs::path &root)
{
	uint32_t count = 1;
	for (auto itr = root.begin(), itrEnd = root.end(); itr != itrEnd; itr++)
	{
		PatchFile::FolderInfo subdir;
		subdir.kind = PatchFile::KIND_ADD;
		subdir.folderName = itr->filename().c_str();
		subdir.startFolder = patch.mFolders.size()+1; // i.e. next one
		subdir.parentFolderIdx = patch.mFolders.size()-1; // prev one
		subdir.parentCount = count++;
		patch.mFolders.push_back(subdir);

		patch.mFolders[subdir.parentFolderIdx].numFolders = 1;
	}
}

void PatchFile::beginEmpty(fs::path &root)
{
	clear();

	// All patch files begin with a blank root
	PatchFile::FolderInfo subdir;
	subdir.kind = PatchFile::KIND_NOP;
	subdir.folderName = "";
	subdir.parentFolderIdx = 0;
	subdir.numFolders = 0;
	subdir.startFolder = 1;
	mFolders.push_back(subdir);

	EnumPath(*this, root);
}

bool PatchFile::populateFromDirectory(const fs::path &path, uint32_t rootFolderIdx)
{
	if (!fs::is_directory(path))
		return false;

	mPatchKind = PATCHFILE_REPLACE;

	EnumDir(*this, path, mFolders.size()-1, mFolders.size());
	return true;
}

bool PatchFile::readFile(PatchMemStream &s, FileInfo &info)
{
	s.read(info.kind);
	s.readCPPString(info.fileName);
	s.read(info.isLink);

	if (info.isLink)
	{
		s.readCPPString(info.pathName);
		return true;
	}

	if (info.kind != KIND_DELETE)
	{
		s.read(info.isExecutable);
		s.readUintBE(info.crc);
		s.read(16, &info.md5[0]);

		if (info.kind == KIND_PATCH)
		{
			s.readUintBE(info.patch_crc);
			s.read(16, &info.patch_md5[0]);
		}

		s.readUintBE(info.ctime);
		s.readUintBE(info.mtime);
		s.readUintBE(info.offset);
		s.readUintBE(info.size);
	}

	// Setup data source
	PatchMemStreamSource* memSrc = new PatchMemStreamSource();
	memSrc->s = s.shared_from_this();
	memSrc->mSrcOffset = info.offset;
	memSrc->mSize = info.size;
	info.dataSrcIdx = mDataSources.size();
	mDataSources.push_back(memSrc);
	return true;
}

bool PatchFile::readFolder(PatchMemStream &s, FolderInfo &info, uint32_t idx)
{
	s.readUintBE(info.endOffset);
	s.readCPPString(info.folderName);
	s.read(info.kind);

	uint32_t num = 0;
	uint32_t folderIdx = info.startFolder; // idx of info
	s.readUintBE(num);
	info.startFile = mFiles.size();
	info.numFiles = num;
	mFiles.resize(mFiles.size() + num);

	num = mFiles.size();
	for (uint32_t i=info.startFile; i<num; i++)
	{
		FileInfo& fileInfo = mFiles[i];
		fileInfo.parentFolderIdx = folderIdx;
		if (!readFile(s, fileInfo))
			return false;
	}

	// Need to save these as resizing mFolders trashes info.
	uint32_t startFolderIdx = mFolders.size();
	uint32_t parentCount = info.parentCount;

	s.readUintBE(num);
	info.startFolder = startFolderIdx;
	info.numFolders = num;
	mFolders.resize(mFolders.size() + num); // info now trashed

	num = mFolders.size();
	for (uint32_t i=startFolderIdx; i<num; i++)
	{
		FolderInfo& childFolder = mFolders[i];
		childFolder.startFolder = i;
		childFolder.parentFolderIdx = folderIdx;
		childFolder.parentCount = parentCount + 1;
		if (!readFolder(s, childFolder, i))
			return false;
	}

	return true;
}

bool PatchFile::writeFile(MemStream &s, FileInfo &info)
{
	s.write(info.kind);
	s.writeCPPString(info.fileName);
	s.write(info.isLink);

	if (info.isLink)
	{
		s.writeCPPString(info.pathName);
		return true;
	}

	if (info.kind != KIND_DELETE)
	{
		s.write(info.isExecutable);
		s.writeUintBE(info.crc);
		s.write(16, &info.md5[0]);

		if (info.kind == KIND_PATCH)
		{
			s.writeUintBE(info.patch_crc);
			s.write(16, &info.patch_md5[0]);
		}

		s.writeUintBE(info.ctime);
		s.writeUintBE(info.mtime);
		s.writeUintBE(info.offset);
		s.writeUintBE(info.size);
	}
	return true;
}

bool PatchFile::writeFolder(MemStream &s, FolderInfo &info, uint32_t idx)
{
	s.writeUintBE(info.endOffset);
	s.writeCPPString(info.folderName);
	s.write(info.kind);

	uint32_t num = info.numFiles;
	s.writeUintBE(num);
	num += info.startFile;

	for (uint32_t i=info.startFile; i<num; i++)
	{
		FileInfo& fileInfo = mFiles[i];
		if (!writeFile(s, fileInfo))
			return false;
	}

	num = info.numFolders;
	s.writeUintBE(num);
	num += info.startFolder;

	for (uint32_t i=info.startFolder; i<num; i++)
	{
		FolderInfo& childFolder = mFolders[i];
		if (!writeFolder(s, childFolder, i))
			return false;
	}

	info.endOffset = s.getPosition(); // needs to be updated
	return true;
}

bool PatchFile::apply(fs::path &destPath)
{
	// TODO: apply a diff to an existing set of files
	return false;
}

// Compression

struct LZFileDataStream  
{  
    FILE* fp;  
    MemStream* data;
    uint32_t inputSize;
};


static int  
fileInputCallback(void *ctx, void *buf, size_t * size)  
{
    size_t rd = 0;
    struct LZFileDataStream * ds = (struct LZFileDataStream *) ctx;  
     
    rd = fread(buf, 1, *size, ds->fp);
    //printf("fileInput: sz=%u read=%u\n", *size, ((uint32_t)rd));
    *size = rd;

    return 0;  
}

static size_t  
fileOutputCallback(void *ctx, const void *buf, size_t size)  
{  
    size_t rd = 0;
    struct LZFileDataStream * ds = (struct LZFileDataStream *) ctx;  
     
    return fwrite(buf, 1, size, ds->fp);
}

static int  
memInputCallback(void *ctx, void *buf, size_t *size)  
{  
    size_t rd = 0;
    struct LZFileDataStream * ds = (struct LZFileDataStream *) ctx;  
    
    size_t newPos = ds->data->getPosition() + *size;
    newPos = newPos > ds->inputSize ? ds->inputSize : newPos;
    size_t readLeft = newPos - ds->data->getPosition();

    if (readLeft > 0)
    {
    	ds->data->read(readLeft, buf);
    }

    *size = readLeft;
    return 0;
}

static size_t  
memOutputCallback(void *ctx, const void *buf, size_t size)  
{  
    size_t rd = 0;
    struct LZFileDataStream * ds = (struct LZFileDataStream *) ctx;

    //printf("memOutput: sz=%u size is=%u pos is=%u\n", size, ds->data->getStreamSize(), ds->data->getPosition());
    ds->data->write(size, buf);
    //printf("memOutput: sz=%u pos now=%u pos is=%u\n", size, ds->data->getStreamSize(), ds->data->getPosition());

    return size;
}

bool PatchFile::readFromFile(PatchMemStream &s, fs::path &path)
{
	FILE* fp = fopen(path.c_str(), "rb");
	if (fp)
	{
	    int rc;  
	    elzma_decompress_handle hand;  
	   
	    /* allocate compression handle */  
	    hand = elzma_decompress_alloc();  
	    assert(hand != NULL); 
	   
	    /* now run the compression */  
	    {  
	        /* set up the context structure that will be passed to 
	         * stream callbacks */    
	        struct LZFileDataStream ds; 
	        ds.fp = fp;
	        ds.data = &s;
	  
	        /* run the streaming compression */   
	        rc = elzma_decompress_run(hand, fileInputCallback, (void *) &ds,  
	                                memOutputCallback, (void *) &ds, ELZMA_lzma);  
	        
	        //printf("rc == %i\n", rc);
	        if (rc != ELZMA_E_OK) {
	        	bool fail = false;

	        	if (s.getStreamSize() > 8)
	        	{
		        	// Check if we have enough data on the output
		        	s.setPosition(2 + 1 + 1);
		        	uint32_t dataStart=0;
		        	s.readUintBE(dataStart);
		        	//printf("dataStart=%u\n", dataStart);

		        	if (s.getStreamSize() >= dataStart+4)
		        	{
		        		s.setPosition(dataStart);
		        		s.readUintBE(dataStart);
		        		//printf("dataEnd=%u\n", dataStart);
		        		if (s.getStreamSize() < dataStart)
		        		{
		        			fprintf(stderr, "Not enough data outputted in stream!\n");
		        			fail = true;
		        		}
		        	}
		        	else
		        	{
		        		fail = true;
		        	}
		        }
		        else
		        {
		        	fail = true;
		        }

		        if (fail)
		        {
		        	fprintf(stderr, "Decompression failed!\n");
		            elzma_decompress_free(&hand);  
		            fclose(fp);
		            return false;  
		        }
	        }
	    }

	    fclose(fp);
	    s.setPosition(0);
	    return read(s); 
	}
	return false;
}

bool PatchFile::compressToFile(MemStream &s, fs::path &path)
{
	s.setPosition(0);
	if (!write(s))
		return false;

	s.setPosition(0);

	FILE* fp = fopen(path.c_str(), "wb");
	if (fp)
	{
	    int rc;
	    elzma_compress_handle hand;  
	   
	    /* allocate compression handle */  
	    hand = elzma_compress_alloc();  
	    assert(hand != NULL);

		rc = elzma_compress_config(hand, ELZMA_LC_DEFAULT,  
		                       ELZMA_LP_DEFAULT, ELZMA_PB_DEFAULT,  
		                       5, (1 << 20) /* 1mb */,  
		                       ELZMA_lzma, mCurrentDataEnd);  
	  
	    /* fail if we couldn't allocate */    
	    if (rc != ELZMA_E_OK) {  
	        elzma_compress_free(&hand);
	        fclose(fp);
	        return false;  
	    }
	   
	    /* now run the compression */  
	    {  
	        /* set up the context structure that will be passed to 
	         * stream callbacks */    
	        struct LZFileDataStream ds; 
	        ds.fp = fp;
	        ds.data = &s;
	        ds.inputSize = mCurrentDataEnd;
	  
	        /* run the streaming compression */   
	        rc = elzma_compress_run(hand, memInputCallback, (void *) &ds,  
	                                fileOutputCallback, (void *) &ds, NULL, NULL);  
	        
	        if (rc != ELZMA_E_OK) {  
	            elzma_compress_free(&hand);  
	            fclose(fp);
	            return false;  
	        }
	    }

	    fclose(fp);
	    return true;
	}
	return false;
}

#include <ctime>
static void ItrFolderListing(PatchFile* patch, uint32_t folderId, const uint32_t nestSpacing)
{
	const char* kindLookup[] = {
		"[NOP   ]",
		"[PATCH ]",
		"[ADD   ]",
		"[DELETE]"
	};

	PatchFile::ListRange fileRange = patch->mFolders[folderId].getFileRange();
	PatchFile::ListRange folderRange = patch->mFolders[folderId].getFolderRange();
	printf("%s %*s>%s\n", kindLookup[patch->mFolders[folderId].kind], nestSpacing, " ", patch->mFolders[folderId].folderName.c_str());

	for (int i=folderRange.start; i<folderRange.getEnd(); i++)
	{
		ItrFolderListing(patch, i, nestSpacing+6);
	}

	for (int i=fileRange.start; i<fileRange.getEnd(); i++)
	{
		PatchFile::FileInfo &info = patch->mFiles[i];
		char extraBuf[256];
		extraBuf[0] = '\0';

		static const uint64_t EPOCH = static_cast<uint64_t>(116444736000000000ULL);
		uint64_t ll = ((uint64_t)info.mtime) + (((uint64_t)info.ctime) << 32);
		ll = (ll - EPOCH) / 10000000ULL;
		time_t posix_time = (time_t)ll;

		if (info.kind == PatchFile::KIND_PATCH)
		{
			snprintf(extraBuf, sizeof(extraBuf), "(%x -> %x)  [@%s", info.patch_crc, info.crc, ctime(&posix_time));
			extraBuf[strlen(extraBuf)-1] = ']';
		}
		else
		{
			snprintf(extraBuf, sizeof(extraBuf), "(CRC %x) [@%s", info.crc, ctime(&posix_time));
			extraBuf[strlen(extraBuf)-1] = ']';
		}

		if (info.isLink)
		{
			printf("%s %*sL %s -> %s\n", kindLookup[info.kind], nestSpacing, " ", info.fileName.c_str(), info.pathName.c_str());
		}
		else if (info.isExecutable)
		{
			printf("%s %*sX %s%s\n", kindLookup[info.kind], nestSpacing, " ", info.fileName.c_str(), extraBuf);
		}
		else
		{
			printf("%s %*s  %s%s\n", kindLookup[info.kind], nestSpacing, " ", info.fileName.c_str(), extraBuf);
		}
	}
}

void PatchFile::printListing()
{
	if (mFolders.size() == 0)
		return;

	printf("%s\n", mPatchKind == PatchFile::PATCHFILE_PATCH ? "PATCH" : "REPLACE");
	ItrFolderListing(this, 0, 0);
}

void PatchFile::dumpFiles(fs::path &root)
{
	printf("TODO\n");
}

