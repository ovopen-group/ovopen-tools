/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "patchConfig.h"

PatchConfig::PatchConfig()
{

}

PatchConfig::~PatchConfig()
{
	
}

void PatchConfig::clear()
{
	mLastPatchServer = "";
	mBundleInfo.clear();
	mBundleKeys.clear();
	mBundleOSKeys.clear();
	mGlobalKeys.clear();
}

bool PatchConfig::read(MemStream &s)
{
	BundleInfo* currentBundle = NULL;
	int32_t currentBundleIdx = -1;
	std::string key;
	std::string value;

	uint16_t magic=0;
	uint8_t version=0;

	s.readUintBE(magic);

	if (magic != 21862)
		return false;

	s.read(version);

	if (version != 1)
		return false;

	while (s.getPosition() != s.getStreamSize())
	{
		uint8_t code=0xFF;
		//printf("pos=%u\n", s.getPosition());
		s.read(code);
		bool error = false;

		//printf("code=%u\n", (uint32_t)code);

		switch(code & 0xFF)
		{
			case 0:
				currentBundle = NULL;
				currentBundleIdx = -1;
				break;
			case 1:
				s.readCPPString(mLastPatchServer);
				break;
			case 2:
				mBundleInfo.push_back(BundleInfo());
				currentBundle = &mBundleInfo.back();
				currentBundleIdx = mBundleInfo.size()-1;
				s.readCPPString(currentBundle->name);
				break;
			case 3: 
				s.readCPPString(key);
				s.readCPPString(value);
				setGlobalValue(currentBundleIdx, key, value);
				break;
			case 0x33:
				if (currentBundle)
				{
					s.readCPPString(currentBundle->version);
				}
				else
				{
					error = true;
				}
				break;
			case 0x34: 
				if (currentBundle)
				{
					s.readCPPString(key);
					s.readCPPString(value);
					setBundleOSValue(currentBundleIdx, key, value);
				}
				else
				{
					error = true;
				}
				break;
			case 0x35: 
				if (currentBundle)
				{
					s.readCPPString(key);
					s.readCPPString(value);
					setBundleValue(currentBundleIdx, key, value);
				}
				else
				{
					error = true;
				}
				break;
			default:
				error = true;
			break;
		}

		if (error)
		{
			return false;
		}
	}

	return true;
}

bool PatchConfig::write(MemStream &s)
{
	uint16_t magic = 21862;
	uint8_t version = 1;

	s.writeUintBE(magic);
	s.write(version);

	uint8_t code;

	if (!mLastPatchServer.empty())
	{
		code=1;
		s.write(code);
		s.writeCPPString(mLastPatchServer);
	}

	int32_t bundleIdx = 0;
	for (auto &bundle : mBundleInfo)
	{
		code = 2;
		s.write(code);
		s.writeCPPString(bundle.name);
		code = 0x33;
		s.write(code);
		s.writeCPPString(bundle.version);

		code = 0x34;
		for (auto &osKey : mBundleOSKeys)
		{
			if (osKey.parentID == bundleIdx)
			{
				s.write(code);
				s.writeCPPString(osKey.key);
				s.writeCPPString(osKey.value);
			}
		}

		code = 0x35;
		for (auto &bndKey : mBundleKeys)
		{
			if (bndKey.parentID == bundleIdx)
			{
				s.write(code);
				s.writeCPPString(bndKey.key);
				s.writeCPPString(bndKey.value);
			}
		}
		
		code = 0;
		s.write(code);

		bundleIdx++;
	}

	code = 3;
	for (auto &osKey : mGlobalKeys)
	{
		if (osKey.parentID == -1)
		{
			s.write(code);
			s.writeCPPString(osKey.key);
			s.writeCPPString(osKey.value);
		}
	}

	code = 0;
	s.write(code);
	return true;
}

static void ImportKeyValues(int32_t parentID, json_t *values, std::vector<PatchConfig::KeyValue> &outList)
{
	const char *key;
	json_t *value;
	//printf("ImportKeyValues...\n");

	json_object_foreach(values, key, value) {
		//printf("ImportKeyValues key=%s\n", key);
		PatchConfig::KeyValue entry;
		entry.parentID = parentID;
		entry.key = key;
		entry.value = json_string_value(value);
		outList.push_back(std::move(entry));
	}
}

void PatchConfig::fromJSON(json_t* root)
{
	const char* key;
	json_t* value;

	mLastPatchServer = "";

	json_object_foreach(root, key, value) {
		//printf("ROOT key=%s, value=%s\n", key, json_string_value(value));

		if (strcmp("server", key) == 0)
		{
			const char* str = json_string_value(value);
			if (str)
			{
				mLastPatchServer = str;
			}
		}
		else if (strcmp("bundles", key ) == 0)
		{
			size_t index;
			json_t* avalue;

			json_array_foreach(value, index, avalue) {
				//printf("BUNDLE ITERATING\n");
				const char* bkey;
				json_t* bvalue;
				BundleInfo bundle;
				int32_t bundleIdx = mBundleInfo.size();
			
				json_object_foreach(avalue, bkey, bvalue) {
					//printf("BUNDLE key=%s, value=%s\n", bkey, json_is_object(bvalue) ? "object" : "invalid");
					if (strcmp("os", bkey) == 0)
					{
						ImportKeyValues(bundleIdx, bvalue, mBundleOSKeys);
					}
					else if (strcmp("keys", bkey) == 0)
					{
						ImportKeyValues(bundleIdx, bvalue, mBundleKeys);
					}
					else if (strcmp("name", bkey) == 0)
					{
						bundle.name = json_string_value(bvalue);
					}
					else if (strcmp("version", bkey) == 0)
					{
						bundle.version = json_string_value(bvalue);
					}
				}

				mBundleInfo.push_back(std::move(bundle));
			}
		}
		else if (strcmp("keys", key) == 0)
		{
			//printf("GLOBAL KEYS GETTING\n");
			ImportKeyValues(-1, value, mGlobalKeys);
		}
	}
}

void PatchConfig::toJSON(json_t* root)
{
	json_t* bundle_arr = json_array();
	json_t* global_keys = json_object();

	int32_t bundleIdx = 0;
	for (auto &bundleItr : mBundleInfo)
	{
		json_t* bundle_obj = json_object();
		json_t* bundle_os_keys = json_object();
		json_t* bundle_keys = json_object();

		json_object_set_new(bundle_obj, "name", json_string(bundleItr.name.c_str()));
		if (!bundleItr.version.empty())
		{
			json_object_set_new(bundle_obj, "version", json_string(bundleItr.version.c_str()));
		}

		for (auto& osKey : mBundleOSKeys)
		{
			if (osKey.parentID != bundleIdx)
				continue;
			json_object_set_new(bundle_os_keys, osKey.key.c_str(), json_string(osKey.value.c_str()));
		}

		for (auto& bndKey : mBundleKeys)
		{
			if (bndKey.parentID != bundleIdx)
				continue;
			json_object_set_new(bundle_keys, bndKey.key.c_str(), json_string(bndKey.value.c_str()));
		}

		json_object_set_new(bundle_obj, "os", bundle_os_keys);
		json_object_set_new(bundle_obj, "keys", bundle_keys);

		json_array_append(bundle_arr, bundle_obj);
		bundleIdx++;
	}

	for (auto& bndKey : mGlobalKeys)
	{
		if (bndKey.parentID != -1)
			continue;
		json_object_set_new(global_keys, bndKey.key.c_str(), json_string(bndKey.value.c_str()));
	}

	json_object_set_new(root, "bundles", bundle_arr);
	json_object_set_new(root, "keys", global_keys);
	json_object_set_new(root, "server", json_string(mLastPatchServer.c_str()));
}

int32_t PatchConfig::getBundleIdx(const std::string &name)
{
	for (auto itr = mBundleInfo.begin(), itrEnd = mBundleInfo.end(); itr != itrEnd; itr++)
	{
		if (itr->name == name)
			return itr - mBundleInfo.begin();
	}
	return -1;
}

PatchConfig::BundleInfo* PatchConfig::getBundleInfo(const std::string &key)
{
	int32_t idx = getBundleIdx(key);
	if (idx >= 0)
	{
		return &mBundleInfo[idx];
	}
	return NULL;
}

template<class T> PatchConfig::KeyValue* FindKeyInVec(int parentID, const std::string &key, T& vec)
{
	for (typename T::iterator itr = vec.begin(), itrEnd = vec.end();  itr != itrEnd;  itr++)
	{
		if (itr->parentID == parentID && itr->key == key)
			return &(*itr);
	}
	return NULL;
}

std::string PatchConfig::getBundleValue(int32_t bundleIdx, const std::string &key, bool *isPresent)
{
	PatchConfig::KeyValue* value = FindKeyInVec(bundleIdx, key, mBundleKeys);
	if (isPresent) *isPresent = (value != NULL);
	return value ? value->value : std::string();
}

void PatchConfig::setBundleValue(int32_t bundleIdx, const std::string &key, const std::string &svalue)
{
	PatchConfig::KeyValue* value = FindKeyInVec(bundleIdx, key, mBundleKeys);
	if (!value)
	{
		mBundleKeys.push_back(KeyValue());
		value = &mBundleKeys.back();
		value->parentID = bundleIdx;
		value->key = key;
	}
	value->value = svalue;
}

std::string PatchConfig::getBundleOSValue(int32_t bundleIdx, const std::string &key, bool *isPresent)
{
	PatchConfig::KeyValue* value = FindKeyInVec(bundleIdx, key, mBundleOSKeys);
	if (isPresent) *isPresent = (value != NULL);
	return value ? value->value : std::string();
}

void PatchConfig::setBundleOSValue(int32_t bundleIdx, const std::string &key, const std::string &svalue)
{
	PatchConfig::KeyValue* value = FindKeyInVec(bundleIdx, key, mBundleOSKeys);
	if (!value)
	{
		mBundleOSKeys.push_back(KeyValue());
		value = &mBundleOSKeys.back();
		value->parentID = bundleIdx;
		value->key = key;
	}
	value->value = svalue;
}

std::string PatchConfig::getGlobalValue(int32_t bundleIdx, const std::string &key, bool *isPresent)
{
	PatchConfig::KeyValue* value = FindKeyInVec(bundleIdx, key, mGlobalKeys);
	if (isPresent) *isPresent = (value != NULL);
	return value ? value->value : std::string();
}

void PatchConfig::setGlobalValue(int32_t bundleIdx, const std::string &key, const std::string &svalue)
{
	PatchConfig::KeyValue* value = FindKeyInVec(bundleIdx, key, mGlobalKeys);
	if (!value)
	{
		mGlobalKeys.push_back(KeyValue());
		value = &mGlobalKeys.back();
		value->parentID = bundleIdx;
		value->key = key;
	}
	value->value = svalue;
}

bool PatchConfig::readFromFile(MemStream &s, fs::path &path)
{
	FILE* fp = fopen(path.c_str(), "rb");
	if (fp)
	{
	    s.setPosition(0);
		fseek(fp, 0, SEEK_END);
		size_t size = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		char buffer[4096];
		while (size > 0)
		{
			size_t bytesLeft = size > sizeof(buffer) ? sizeof(buffer) : size;
			fread(buffer, 1, bytesLeft, fp);
			s.write(bytesLeft, buffer);
			size -= bytesLeft;
		}
	    fclose(fp);
	    s.setPosition(0);

	    return read(s); 
	}
	return false;
}

bool PatchConfig::writeToFile(MemStream &s, fs::path &path)
{
	s.setPosition(0);
	if (!write(s))
		return false;

	s.setPosition(0);

	FILE* fp = fopen(path.c_str(), "wb");
	if (fp)
	{
		fwrite(s.mPtr, 1, s.getStreamSize(), fp);
		fclose(fp);
		return true;
	}

	return false;
}
