@echo off
echo Cleaning up all non-source files
del /S *.dcu
del /S *.local
del /S *.obj
del /S *.identcache
del ..\*.TDS
echo done.