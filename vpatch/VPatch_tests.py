##---------------------------------------------------------------------------
## VPatch_tests.py: VPatch Unit Tests
##---------------------------------------------------------------------------
##                           -=* VPatch *=-
##---------------------------------------------------------------------------
## Copyright (C) 2001-2005 Koen van de Sande / Van de Sande Productions
##---------------------------------------------------------------------------
## Website: http://www.tibed.net/vpatch
##
## This software is provided 'as-is', without any express or implied
## warranty.  In no event will the authors be held liable for any damages
## arising from the use of this software.
##
## Permission is granted to anyone to use this software for any purpose,
## including commercial applications, and to alter it and redistribute it
## freely, subject to the following restrictions:
##
## 1. The origin of this software must not be misrepresented; you must not
##    claim that you wrote the original software. If you use this software
##    in a product, an acknowledgment in the product documentation would be
##    appreciated but is not required.
## 2. Altered source versions must be plainly marked as such, and must not be
##    misrepresented as being the original software.
## 3. This notice may not be removed or altered from any source distribution.

# This runs a number of tests on the GenPat patch generator and the stand-alone
# EXE runtime. Install Python 2.4+ to make this script work.

import unittest
import os
import time

ROOT = os.getcwd()
DATA = os.path.join(ROOT, "testdata")
EXE = os.path.normpath(os.path.join(ROOT, r".."))
GENPAT_EXE = os.path.join(EXE, "Genpat.exe")
VAPPEND_EXE = os.path.join(EXE, "VAppend.exe")
EXTERNAL_DATA = os.path.normpath(os.path.join(ROOT, "../../extended_data"))

try:
    if len(os.listdir(DATA)):
        raise Exception("Clean up testdata folder first!!!")
except WindowsError:
    pass
    
class VPatchTest(unittest.TestCase):
    def writeDataFile(self, filename, content):
        f = open(os.path.join(DATA, filename), "wb")
        f.write(content)
        f.close()

    def compareDataFile(self, f1, f2, checkDate=True):
        f1 = os.path.join(DATA, f1)
        f2 = os.path.join(DATA, f2)
        if checkDate:
            self.assertEqual(os.stat(f1).st_mtime, os.stat(f2).st_mtime)
        f = open(f1, "rb")
        g = open(f2, "rb")
        FD = f.read()
        GD = g.read()
        f.close()
        g.close()
        self.assertEqual(len(FD), len(GD))
        if FD != GD:
            lookupPosition = (i for i in range(len(FD)) if FD[i] != GD[i])
            for i in lookupPosition:
                self.fail("File contents mismatch: " + str(i) + " " + repr(FD[i:(i+30)]) + " " + repr(GD[i:(i+30)]))               
        # should be superflous
        self.assertEqual(FD, GD)
        
    def makePatch(self, f1, f2, pat, returnCode=0, replace=False):
        if replace:
            self.assertEqual(os.spawnl(os.P_WAIT, GENPAT_EXE, GENPAT_EXE, os.path.join(DATA, f1), os.path.join(DATA, f2), os.path.join(DATA, pat), "-R"), returnCode)
        else:
            self.assertEqual(os.spawnl(os.P_WAIT, GENPAT_EXE, GENPAT_EXE, os.path.join(DATA, f1), os.path.join(DATA, f2), os.path.join(DATA, pat)), returnCode)

    def performPatch(self, pat, f1, f2):
        patchEXE = os.path.join(DATA, pat + ".exe")
        self.assertEqual(os.spawnl(os.P_WAIT, VAPPEND_EXE, VAPPEND_EXE, os.path.join(DATA, pat), patchEXE, os.path.join(EXE, "VPatch.bin")), 0)
        self.assertEqual(os.spawnl(os.P_WAIT, patchEXE, patchEXE, os.path.join(DATA, f1), os.path.join(DATA, f2)), 0)
        self.cleanUp([patchEXE])
        
    def makeCRC32Patch(self, filename):
        f = open(os.path.join(DATA, filename), "wb")
        f.write("VPAT\x00\x00\x00\x00")
        f.close()

    def cleanUp(self, listing):
        for fn in listing:
            try:
                os.remove(os.path.join(DATA,fn))
            except:
                pass

    ## SIMPLE
    def doSimple(self, patchFilename):
        self.writeDataFile("a.txt", "Test123")
        self.writeDataFile("b.txt", "Test456")
        self.makePatch("a.txt", "b.txt", patchFilename)
        self.performPatch(patchFilename, "a.txt", "b.txt.check")
        self.compareDataFile("b.txt", "b.txt.check")
        self.cleanUp(["a.txt", "b.txt", patchFilename, "b.txt.check"])
        
    def testSimple(self):
        self.doSimple("simple.pat")

    def testSimpleCRC(self):
        self.makeCRC32Patch("simplecrc.pat")
        self.doSimple("simplecrc.pat")

    ## BIGGER SIMPLE
    def doBiggerSimple(self, patchFilename):
        self.writeDataFile("a.txt", "123. In the real world,\nthere is no such thing as a perfect use case\n\tRIGHT!?\nQuote by: not actually a quote. After all, what is the answer to life, the universe and everyhting?\n\tForty-two.\n\t\t42\n\nTHE END")
        self.writeDataFile("b.txt", "456. In the real world,\nthere is no such thing as a perfect use case.\n\tRIGHT!?\nWRONG\nQuote by: not actually a quote. After all, what is the answer to life, the universe and everything?\n\tForty-two\n\t\t42\n\nTHE END!!!")
        self.makePatch("a.txt", "b.txt", patchFilename)
        self.performPatch(patchFilename, "a.txt", "b.txt.check")
        self.compareDataFile("b.txt", "b.txt.check")
        self.cleanUp(["a.txt", "b.txt", patchFilename, "b.txt.check"])
        
    def testBiggerSimple(self):
        self.doBiggerSimple("simpleb.pat")

    def testBiggerSimpleCRC(self):
        self.makeCRC32Patch("simplebcrc.pat")
        self.doBiggerSimple("simplebcrc.pat")

    ## TRANSITIVE 1
    def doTransitive1(self, patchFilename):
        self.writeDataFile("a.txt", "Test123")
        self.writeDataFile("b.txt", "Test456")
        self.writeDataFile("c.txt", "Test789")
        self.makePatch("a.txt", "b.txt", patchFilename)
        self.makePatch("b.txt", "c.txt", patchFilename)
        self.performPatch(patchFilename, "a.txt", "b.txt.check")
        self.performPatch(patchFilename, "b.txt.check", "c.txt.check")
        self.compareDataFile("b.txt", "b.txt.check")
        self.compareDataFile("c.txt", "c.txt.check")
        self.cleanUp(["a.txt", "b.txt", "c.txt", patchFilename, "b.txt.check", "c.txt.check"])
        
    def testTransitive1(self):
        self.doTransitive1("trans1.pat")

    def testTransitive1CRC(self): 
        self.makeCRC32Patch("trans1crc.pat")
        self.doTransitive1("trans1crc.pat")
        
    ## TRANSITIVE 2
    def doTransitive2(self, patchFilename):
        self.writeDataFile("a.txt", "Test123")
        self.writeDataFile("b.txt", "Test456")
        self.writeDataFile("c.txt", "Test789")
        self.makePatch("a.txt", "c.txt", patchFilename)
        self.makePatch("b.txt", "c.txt", patchFilename)
        self.performPatch(patchFilename, "a.txt", "c1.txt.check")
        self.performPatch(patchFilename, "b.txt", "c2.txt.check")
        self.compareDataFile("c.txt", "c1.txt.check")
        self.compareDataFile("c.txt", "c2.txt.check")
        self.cleanUp(["a.txt", "b.txt", "c.txt", patchFilename, "c1.txt.check", "c2.txt.check"])

    def testTransitive2(self):
        self.doTransitive2("trans2.pat")

    def testTransitive2CRC(self): 
        self.makeCRC32Patch("trans2crc.pat")
        self.doTransitive2("trans2crc.pat")

    ## REPLACEMENT
    def doReplacement(self, patchFilename1, patchFilename2):
        self.writeDataFile("a.txt", "Test123")
        self.writeDataFile("b.txt", "Test456XX")
        self.writeDataFile("c.txt", open("../oldfile.txt").read())
        self.writeDataFile("d.txt", open("../newfile.txt").read())
        self.makePatch("a.txt", "c.txt", patchFilename1)
        self.makePatch("b.txt", "c.txt", patchFilename1)
        # now replace them (try the error first)
        self.makePatch("a.txt", "d.txt", patchFilename1, returnCode=3)
        self.makePatch("b.txt", "d.txt", patchFilename1, returnCode=3)
        # now replace them for real
        self.makePatch("a.txt", "d.txt", patchFilename1, replace=True)
        self.makePatch("b.txt", "d.txt", patchFilename1, replace=True)
        # confirm patch
        self.performPatch(patchFilename1, "a.txt", "d1.txt.check")
        self.performPatch(patchFilename1, "b.txt", "d2.txt.check")
        self.compareDataFile("d.txt", "d1.txt.check")
        self.compareDataFile("d.txt", "d2.txt.check")

        # again in different order
        self.makePatch("a.txt", "c.txt", patchFilename2)
        self.makePatch("b.txt", "c.txt", patchFilename2)
        # now replace them
        self.makePatch("b.txt", "d.txt", patchFilename2, replace=True)
        self.makePatch("a.txt", "d.txt", patchFilename2, replace=True)
        # confirm patch
        self.performPatch(patchFilename2, "a.txt", "d3.txt.check")
        self.performPatch(patchFilename2, "b.txt", "d4.txt.check")
        self.compareDataFile("d.txt", "d3.txt.check")
        self.compareDataFile("d.txt", "d4.txt.check")       
      
        # add an extra file
        self.makePatch("c.txt", "d.txt", patchFilename2)       
        self.makePatch("b.txt", "c.txt", patchFilename2, replace=True)
        self.performPatch(patchFilename2, "a.txt", "d5.txt.check")
        self.performPatch(patchFilename2, "b.txt", "c.txt.check")
        self.performPatch(patchFilename2, "c.txt", "d6.txt.check")
        self.compareDataFile("c.txt", "c.txt.check")
        self.compareDataFile("d.txt", "d5.txt.check")
        self.compareDataFile("d.txt", "d6.txt.check")       
        
        self.cleanUp(["a.txt", "b.txt", "c.txt", "d.txt", patchFilename1, 
                      patchFilename2, "c.txt.check", "d1.txt.check", 
                      "d2.txt.check", "d3.txt.check", "d4.txt.check", 
                      "d5.txt.check", "d6.txt.check"])

    def testReplacement(self):
        self.doReplacement("replace1.pat", "replace2.pat")

    def testReplacementCRC(self): 
        self.makeCRC32Patch("replace1crc.pat")
        self.makeCRC32Patch("replace2crc.pat")
        self.doReplacement("replace1crc.pat", "replace2crc.pat")

    ## SIMPLE BINARY
    def doSimpleBinary(self, patchFilename):
        self.writeDataFile("a.txt", "Test123\x18dkjfa;lksjdf;aslkjdfaskl;djf\x11\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99")
        self.writeDataFile("b.txt", "Test456\x99askdfja;ksldjf;aslkdfja;slkjdfl;kj\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\x99\xA5\x99\x99")
        self.makePatch("a.txt", "b.txt", patchFilename)
        self.performPatch(patchFilename, "a.txt", "b.txt.check")
        self.compareDataFile("b.txt", "b.txt.check")
        self.cleanUp(["a.txt", "b.txt", patchFilename, "b.txt.check"])

    def testSimpleBinary(self):
        self.doSimpleBinary("bin.pat")

    def testSimpleBinary2CRC(self): 
        self.makeCRC32Patch("bincrc.pat")
        self.doSimpleBinary("bincrc.pat")

    ## SIMPLE DATE
    def doSimpleDate(self, patchFilename):
        self.writeDataFile("a.txt", "Test123")
        self.writeDataFile("b.txt", "Test456")
        time.sleep(3)
        self.makePatch("a.txt", "b.txt", patchFilename)
        time.sleep(3)
        self.performPatch(patchFilename, "a.txt", "b.txt.check")
        time.sleep(3)
        self.compareDataFile("b.txt", "b.txt.check")
        self.cleanUp(["a.txt", "b.txt", patchFilename, "b.txt.check"])
        
    def testSimpleDate(self):
        self.doSimpleDate("simpledate.pat")

    def testSimpleDateCRC(self):
        self.makeCRC32Patch("simpledatecrc.pat")
        self.doSimpleDate("simpledatecrc.pat")
        
    ## EXTENDED TEST CASES - THESE NEED SPECIFIC EXTERNAL DATAFILES
    def testExtended(self):
        global DATA, EXTERNAL_DATA
        if not os.path.exists(EXTERNAL_DATA):
            raise Exception("External data not found. Cannot perform this specific test.")
        oDATA = DATA
        DATA = EXTERNAL_DATA

        # fix old crashes, which are annoying for this test
        self.cleanUp(["ex1.pat"])
        
        # go ahead
        self.makePatch("ra2_150.dta", "ra2_153.dta", "ex1.pat")
        self.makePatch("ra2_151.dta", "ra2_153.dta", "ex1.pat")
        self.makePatch("ra2_152.dta", "ra2_153.dta", "ex1.pat")
        self.performPatch("ex1.pat", "ra2_150.dta", "ra2_150.check")
        self.performPatch("ex1.pat", "ra2_151.dta", "ra2_151.check")
        self.performPatch("ex1.pat", "ra2_152.dta", "ra2_152.check")
        self.compareDataFile("ra2_153.dta", "ra2_150.check")
        self.compareDataFile("ra2_153.dta", "ra2_151.check")
        self.compareDataFile("ra2_153.dta", "ra2_152.check")
        # Patches by VC++/MinGW are slightly different from BC++
        #self.compareDataFile("ex1.pat", "ra2.pat", checkDate=False)
        self.cleanUp(["ra2_150.check", "ra2_151.check", "ra2_152.check", "ex1.pat"])
        
        self.makePatch("default2.ini", "deffire.ini", "ex2.pat")
        self.performPatch("ex2.pat", "default2.ini", "deffire.check")
        self.performPatch("def2.pat", "default2.ini", "deffire2.check")
        self.compareDataFile("deffire.ini", "deffire.check")
        self.compareDataFile("deffire.ini", "deffire2.check", checkDate=False)
        #self.compareDataFile("ex2.pat", "default2.pat", checkDate=False)
        self.cleanUp(["deffire.check", "deffire2.check", "ex2.pat"])
        
        DATA = oDATA

if __name__ == '__main__':
    try:
        os.makedirs(DATA)
    except:
        pass
    unittest.main()
