program VAppend;

//---------------------------------------------------------------------------
// VAppend.dpr: Delphi program to append patches to VPatch runtimes
//---------------------------------------------------------------------------
//                           -=* VPatch *=-
//---------------------------------------------------------------------------
// Copyright (C) 2001-2008 Koen van de Sande / Van de Sande Productions
//---------------------------------------------------------------------------
// Website: http://www.tibed.net/vpatch
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

{$APPTYPE CONSOLE}

uses
  SysUtils;

var
  fs, fo: File;
  Patch: String;
  OutFile: String = 'VPatch.exe';
  Runtime: String = 'VPATCH.BIN';
  o: LongWord;
  Buf: Array[0..1048575] of Byte;
  Size, BufSize: Integer;

begin
  WriteLn('VAppend v3.2');
  WriteLn('============');
  WriteLn;
  WriteLn('(c) 2001-2008 Van de Sande Productions');
  WriteLn('Website: http://www.tibed.net/vpatch');
  WriteLn;
  if ParamCount = 0 then begin
    WriteLn('Use this program to append .PAT files to the VPatch runtime.');
    WriteLn;
    WriteLn('  VAPPEND (patch file) [output file] [runtime]');
    WriteLn;
    WriteLn('By default, the output file is VPATCH.EXE and the runtime is VPATCH.BIN');
  end;
  if not FileExists(ParamStr(1)) then begin
    WriteLn('ERROR: Patch file not found');
    Exit;
  end;
  Patch := ParamStr(1);
  if ParamCount > 1 then OutFile := ParamStr(2);
  if ParamCount > 2 then Runtime := ParamStr(3);
  WriteLn('Patch:   '+Patch);
  WriteLn('Runtime: '+Runtime);
  if not FileExists(Runtime) then begin
    WriteLn('ERROR: Patch runtime (' + Runtime + ') not found');
    Exit;
  end;
  WriteLn('Output:  '+OutFile);

  AssignFile(fo,OutFile);
  Rewrite(fo,1);
  //copy the runtime
  AssignFile(fs,Runtime);
  FileMode:=fmOpenRead;
  Reset(fs,1);
  BufSize:=1048576;
  o:=FileSize(fs);            //patch start offset
  Size:=FileSize(fs);
  while Size>0 do begin
    if Size-BufSize<0 then BufSize:=Size;
    BlockRead(fs,Buf,BufSize);
    BlockWrite(fo,Buf,BufSize);
    Dec(Size,BufSize);
  end;
  CloseFile(fs);
  //do the patch
  AssignFile(fs,Patch);
  FileMode:=fmOpenRead;
  Reset(fs,1);
  BufSize:=1048576;
  Size:=FileSize(fs);
  while Size>0 do begin
    if Size-BufSize<0 then BufSize:=Size;
    BlockRead(fs,Buf,BufSize);
    BlockWrite(fo,Buf,BufSize);
    Dec(Size,BufSize);
  end;
  CloseFile(fs);

  BlockWrite(fo,o,SizeOf(o));
  CloseFile(fo);
  WriteLn('Created.');
  WriteLn;
  WriteLn('Example usage of your patch:');
  WriteLn('  ',OutFile,' ','OLDFILE.TXT OUTPUT.TXT');
  WriteLn('Note: input file (oldfile.txt) cannot be the same as the output file!');
end.
