SetCompressor /SOLID lzma

!include "MUI.nsh"
!define MUI_COMPONENTSPAGE_NODESC
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE license.rtf
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_LANGUAGE "English"

Name "VPatch 3.2"
OutFile ..\..\vpatch32.exe
ComponentText "This will install the VPatch patch generator on your computer."
ShowInstDetails show
AutoCloseWindow false
DirText "Please select a location to install VPatch to."

UninstallText "This will uninstall VPatch. Hit Next to uninstall, or Cancel to abort."

InstallDir $PROGRAMFILES\VPatch

Section "VPatch Core Files" SectionCore
SectionIn 1 RO
  SetOutPath $INSTDIR
  File ..\VPatch.htm
  File ..\vpatch.bin
  File ..\example.nsi
  File ..\oldfile.txt
  File ..\newfile.txt
  File ..\Readme.html
  File ..\GenPat.exe
  File ..\ListPat.exe
  File ..\patch.pat
  File ..\VPatchDLL.bin
  File ..\vpatchprompt.exe
  File ..\VAppend.exe
  File ..\VPatchLib.nsh
SectionEnd

Section "VPatch Graphical User Interface"
  SetOutPath $INSTDIR
  File ..\VPatchGUI.exe

  ;Create shortcuts
  SetOutPath $SMPROGRAMS\VPatch
  CreateShortCut "$SMPROGRAMS\VPatch\VG - VPatch GUI.lnk" "$INSTDIR\VPatchGUI.exe" "" "" 0
  CreateShortCut "$SMPROGRAMS\VPatch\VPatch Readme.lnk" "$INSTDIR\VPatch.htm" "" "" 0
  CreateShortCut "$SMPROGRAMS\VPatch\VPatch NSIS Readme.lnk" "$INSTDIR\Readme.html" "" "" 0
  CreateShortCut "$SMPROGRAMS\VPatch\Uninstall VPatch.lnk" "$INSTDIR\vpatch-uninst.exe" "" "" 0

  MessageBox MB_YESNO "Do you want to create a shortcut on your desktop?" IDNO NoDesktop

  ;Create shortcuts
  SetOutPath $DESKTOP
  CreateShortCut "$DESKTOP\VPatch GUI.lnk" "$INSTDIR\VPatchGUI.exe" "" "" 0

  NoDesktop:
SectionEnd

Section /o "VPatch Source"
  SetOutPath $INSTDIR\Sources
  File /r *.*
SectionEnd

Section -Last
  SetOutPath $INSTDIR
  WriteUninstaller vpatch-uninst.exe
SectionEnd

Section UnInstall
  Delete $INSTDIR\VPatch.htm
  Delete $INSTDIR\vpatch.bin
  Delete $INSTDIR\example.nsi
  Delete $INSTDIR\oldfile.txt
  Delete $INSTDIR\newfile.txt
  Delete $INSTDIR\Readme.html
  Delete $INSTDIR\GenPat.exe
  Delete $INSTDIR\ListPat.exe
  Delete $INSTDIR\patch.pat
  Delete $INSTDIR\VPatchDLL.bin
  Delete $INSTDIR\vpatchprompt.exe
  Delete $INSTDIR\VAppend.exe
  Delete $INSTDIR\VPatchGUI.exe
  Delete $INSTDIR\VPatchLib.nsh
  RMDir /r /REBOOTOK "$INSTDIR\Source"
  Delete "$DESKTOP\VPatch GUI.lnk"
  RMDir /r "$SMPROGRAMS\VPatch"
  Delete $INSTDIR\vpatch-uninst.exe
  RMDir /r "$INSTDIR"
  IfRebootFlag 0 +2
    MessageBox MB_OK "You should reboot your computer to finish uninstallation completely."
SectionEnd
