//---------------------------------------------------------------------------
// vpatchdll.c: C DLL version of the VPatch runtime
//---------------------------------------------------------------------------
//                           -=* VPatch *=-
//---------------------------------------------------------------------------
// Copyright (C) 2001-2005 Koen van de Sande / Van de Sande Productions
//---------------------------------------------------------------------------
// Website: http://www.tibed.net/vpatch
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "..\NSISPlugin\apply_patch.h"

/* ------------------------ Stand-alone DLL code ----------------- */

// main DLL function; this is the one you want to use externally
// dllname should be the name of the file containing the patch (typically
// attached to the DLL, so the name of the DLL), source the name of the input
// file to patch and dest the filename where the updated file has to be written
// to. Source and dest must be different, because VPatch does not copy files.
int __declspec(dllexport) __stdcall vpatchfile(char* dllname, char* source, char* dest) {
  HANDLE hPatch, hSource, hDest;
  int result;

  hPatch = CreateFile(dllname, GENERIC_READ, FILE_SHARE_READ, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if (hPatch == INVALID_HANDLE_VALUE) {
    return FILE_ERR_PATCH;
  }

  hSource = CreateFile(source, GENERIC_READ, FILE_SHARE_READ, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if (hSource == INVALID_HANDLE_VALUE) {
    CloseHandle(hPatch);
    return FILE_ERR_SOURCE;
  }
  
  hDest = CreateFile(dest, GENERIC_READ | GENERIC_WRITE, 0, NULL,
                                  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
  if (hDest == INVALID_HANDLE_VALUE) {
    CloseHandle(hPatch);
    CloseHandle(hSource);
    return FILE_ERR_DEST;
  }
  
  result = DoPatch(hPatch, hSource, hDest);

  CloseHandle(hDest);
  CloseHandle(hSource);
  CloseHandle(hPatch);

  if (result != PATCH_SUCCESS) {
    DeleteFile(dest);
  }    
  
  return result;
}

BOOL WINAPI DllMain(HANDLE hInst, ULONG ul_reason_for_call, LPVOID lpReserved) {
  return TRUE;
}
