//---------------------------------------------------------------------------
// vpatch.c: C version of the VPatch runtime
//---------------------------------------------------------------------------
//                           -=* VPatch *=-
//---------------------------------------------------------------------------
// Copyright (C) 2001-2005 Koen van de Sande / Van de Sande Productions
//---------------------------------------------------------------------------
// Website: http://www.tibed.net/vpatch
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include "..\NSISPlugin\apply_patch.h"

/* ------------------------ Stand-alone EXE utility functions ----- */

void strcopy(char *tgt, const char *src) {
    while (*tgt++ = *src++);
}

char* chop_arg(char **args) {
    char *save = *args, quote = **args;
    if (quote == '\"' || quote == '\'' || quote == '`') {
        (*args)++; save++;                    // skip open quote
        while (**args && **args != quote) (*args)++;
        if (**args == quote) *(*args)++ = 0;// nullify end quote
    } else
        while (**args > 32) (*args)++;
    while (**args && **args <= 32) *(*args)++ = 0;
    return save;
}

void ErrorBox(LPSTR msg) {
    MessageBox(0, msg, "VPatch", MB_ICONSTOP);
}

/* ------------------------ Stand-alone EXE code ----------------- */

int EntryPoint() {
  char argbuf[MAX_PATH];
  char* args;
  char* exename;
  char* source;
  char* dest;
  HANDLE hPatch, hSource, hDest;
  int result;

  // process command-line arguments
  strcopy(argbuf, GetCommandLine());
  args = argbuf;
  exename = chop_arg(&args);
  source = chop_arg(&args);
  dest = chop_arg(&args);

  hPatch = CreateFile(exename, GENERIC_READ, FILE_SHARE_READ, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if (hPatch == INVALID_HANDLE_VALUE) {
    ErrorBox("Unable to open patch file: be sure to use the full runtime filename (like vpatch.exe, not just vpatch).");
    ExitProcess(FILE_ERR_PATCH);
	return 0;
  }

  hSource = CreateFile(source, GENERIC_READ, FILE_SHARE_READ, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if (hSource == INVALID_HANDLE_VALUE) {
    ErrorBox("Unable to open source file");
    CloseHandle(hPatch);
    ExitProcess(FILE_ERR_SOURCE);
	return 0;
  }
  
  hDest = CreateFile(dest, GENERIC_READ | GENERIC_WRITE, 0, NULL,
                                  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
  if (hDest == INVALID_HANDLE_VALUE) {
    ErrorBox("Unable to open output file");
    CloseHandle(hPatch);
    CloseHandle(hSource);
    ExitProcess(FILE_ERR_DEST);
	return 0;
  }
  
  result = DoPatch(hPatch, hSource, hDest);

  CloseHandle(hDest);
  CloseHandle(hSource);
  CloseHandle(hPatch);

  if (result != PATCH_SUCCESS) {
    if (result == PATCH_ERROR)
      ErrorBox("An error occured while patching");
    else if (result == PATCH_CORRUPT)
      ErrorBox("Patch data is invalid or corrupt");
    else if (result == PATCH_NOMATCH)
      ErrorBox("No suitable patches were found");
    //else if (result == PATCH_UPTODATE)
    //    ErrorBox("You already have the latest version installled");
    DeleteFile(dest);
  }    
 
  ExitProcess(result);
  return 0;
}
