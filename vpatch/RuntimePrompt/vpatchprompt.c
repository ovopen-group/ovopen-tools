//---------------------------------------------------------------------------
// vpatch.c: C version of the VPatch runtime
//---------------------------------------------------------------------------
//                           -=* VPatch *=-
//---------------------------------------------------------------------------
// Copyright (C) 2001-2005 Koen van de Sande / Van de Sande Productions
//---------------------------------------------------------------------------
// Website: http://www.tibed.net/vpatch
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <commdlg.h>
#include "..\NSISPlugin\apply_patch.h"

/* ------------------------ Stand-alone EXE utility functions ----- */

void strcopy(char *tgt, const char *src) {
    while (*tgt++ = *src++);
}

char* chop_arg(char **args) {
    char *save = *args, quote = **args;
    if (quote == '\"' || quote == '\'' || quote == '`') {
        (*args)++; save++;                    // skip open quote
        while (**args && **args != quote) (*args)++;
        if (**args == quote) *(*args)++ = 0;// nullify end quote
    } else
        while (**args > 32) (*args)++;
    while (**args && **args <= 32) *(*args)++ = 0;
    return save;
}

void ErrorBox(LPSTR msg) {
    MessageBox(0, msg, "VPatch", MB_ICONSTOP);
}

/* ------------------------ Utility function for prompting ------- */

int HasPatch(HANDLE hPatch) {
  unsigned long temp = 0;
  unsigned long read;

  // special 'addition' for the dll: since the patch file is now
  // in a seperate file, the VPAT header might be right at the start
  // of the file, and a pointer at the end of the file is probably missing
  // (because all patch generator versions don't append it, the linker/gui
  //  does this).
  SetFilePointer(hPatch, 0, NULL, FILE_BEGIN);
  ReadFile(hPatch, &temp, 4, &read, NULL);
  // it's not at the start of file -> there must be a pointer at the end of
  // file then
  if (temp != 0x54415056) {
    SetFilePointer(hPatch, -4, NULL, FILE_END);
    ReadFile(hPatch, &temp, 4, &read, NULL);

    SetFilePointer(hPatch, temp, NULL, FILE_BEGIN);
    ReadFile(hPatch, &temp, 4, &read, NULL);
    if (temp != 0x54415056)
      return 0;
  }
  return 1;
}

int GetOpenFilename(char* title, char* filename) {
   OPENFILENAME fn;
   // fill in structure fields for Open File dialog box
   fn.lStructSize       = sizeof(OPENFILENAME);
   fn.hwndOwner         = NULL;
   fn.lpstrFilter       = NULL;
   fn.lpstrCustomFilter = NULL;
   fn.nFilterIndex      = 0;
   fn.lpstrFile         = NULL;
   fn.lpstrFile         = filename;
   fn.nMaxFile          = MAX_PATH;
   fn.lpstrFileTitle    = NULL;
   fn.lpstrInitialDir   = NULL;
   fn.lpstrTitle        = title;
   fn.Flags             = OFN_HIDEREADONLY | OFN_FILEMUSTEXIST;
   fn.lpstrDefExt       = NULL;
   filename[0] = '\0';
   // activate the Open File dialog box
   if (GetOpenFileName(&fn)) {
      return 1;
   } else {
      return 0;
   }
}

int GetSaveFilename(char* title, char* filename) {
   OPENFILENAME fn;
   // fill in structure fields for Save File dialog box
   fn.lStructSize       = sizeof(OPENFILENAME);
   fn.hwndOwner         = NULL;
   fn.lpstrCustomFilter = NULL;
   fn.nFilterIndex      = 0;
   fn.lpstrFile         = filename;
   fn.nMaxFile          = MAX_PATH;
   fn.lpstrFileTitle    = NULL;
   fn.lpstrInitialDir   = NULL;
   fn.lpstrTitle        = title;
   fn.Flags             = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT |
                          OFN_PATHMUSTEXIST;
   fn.lpstrDefExt       = NULL;
   fn.lpstrFilter       = NULL;
   filename[0] = '\0';
   // activate the Open File dialog box
   if (GetSaveFileName(&fn)) {
      return 1;
   } else {
      return 0;
   }
}

/* ------------------------ Stand-alone EXE code ----------------- */

int EntryPoint() {
  char argbuf[MAX_PATH];
  char promptbuf1[MAX_PATH];
  char promptbuf2[MAX_PATH];
  char promptbuf3[MAX_PATH];
  char* args;
  char* exename;
  char* source;
  char* dest;
  HANDLE hPatch, hSource, hDest;
  int result;

  // process command-line arguments
  strcopy(argbuf, GetCommandLine());
  args = argbuf;
  
  // patch
  exename = chop_arg(&args);
  hPatch = CreateFile(exename, GENERIC_READ, FILE_SHARE_READ, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if((hPatch == INVALID_HANDLE_VALUE) || !HasPatch(hPatch)) {
    if(hPatch != INVALID_HANDLE_VALUE) CloseHandle(hPatch);
    exename = chop_arg(&args);   // read patch as argument then
    if(strlen(exename) == 0) {
      // prompt for patch filename
      if(GetOpenFilename("Select patch file", promptbuf1)) {
        exename = promptbuf1;
      }  // upon cancel, the error below will trigger
    }
    hPatch = CreateFile(exename, GENERIC_READ, FILE_SHARE_READ, NULL,
                                        OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
    if (hPatch == INVALID_HANDLE_VALUE) {
      ErrorBox("Unable to open patch file.");
      ExitProcess(FILE_ERR_PATCH);
      return 0;
    }
  }

  // source
  source = chop_arg(&args);
  if(strlen(source) == 0) {
    // prompt for source filename
    if(GetOpenFilename("Select source file", promptbuf2)) {
      source = promptbuf2;
    }  // upon cancel, the error below will trigger
  }
  hSource = CreateFile(source, GENERIC_READ, FILE_SHARE_READ, NULL,
                                      OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
  if (hSource == INVALID_HANDLE_VALUE) {
    ErrorBox("Unable to open source file");
    CloseHandle(hPatch);
    ExitProcess(FILE_ERR_SOURCE);
    return 0;
  }

  // output
  dest = chop_arg(&args);
  if(strlen(dest) == 0) {
    // prompt for dest filename
    if(GetSaveFilename("Select output file", promptbuf3)) {
      dest = promptbuf3;
    }  // upon cancel, the error below will trigger
  }
  if(strcmp(source, dest) == 0) {
    ErrorBox("Source and output file cannot be the same. Use a temporary file for output and then rename the file.");
  }
  hDest = CreateFile(dest, GENERIC_READ | GENERIC_WRITE, 0, NULL,
                                  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
  if (hDest == INVALID_HANDLE_VALUE) {
    ErrorBox("Unable to open output file");
    CloseHandle(hPatch);
    CloseHandle(hSource);
    ExitProcess(FILE_ERR_DEST);
    return 0;
  }
  
  result = DoPatch(hPatch, hSource, hDest);

  CloseHandle(hDest);
  CloseHandle(hSource);
  CloseHandle(hPatch);

  if (result != PATCH_SUCCESS) {
    if (result == PATCH_ERROR)
      ErrorBox("An error occured while patching");
    else if (result == PATCH_CORRUPT)
      ErrorBox("Patch data is invalid or corrupt");
    else if (result == PATCH_NOMATCH)
      ErrorBox("No suitable patches were found");
    //else if (result == PATCH_UPTODATE)
    //    ErrorBox("You already have the latest version installled");
    DeleteFile(dest);
  }    
 
  ExitProcess(result);
  return 0;
}
