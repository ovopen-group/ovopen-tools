//---------------------------------------------------------------------------
// FileFormat1.cpp
//---------------------------------------------------------------------------
//                           -=* VPatch *=-
//---------------------------------------------------------------------------
// Copyright (C) 2001-2008 Koen van de Sande / Van de Sande Productions
//---------------------------------------------------------------------------
// Website: http://www.tibed.net/vpatch
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#include "FileFormat1.h"
#include "GlobalTypes.h"

#define MAGIC_VPAT 0x54415056

namespace FileFormat1 {
  void writeByte(bostream& patch, TFileOffset dw) {
    unsigned char b = dw & 0xFF;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
  }
  void writeWord(bostream& patch, TFileOffset dw) {
    unsigned char b = (dw & 0xFF00) >> 8;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
    b = dw & 0xFF;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
  }
  void writeDword(bostream& patch, TFileOffset dw) {
    unsigned char b = (dw & 0xFF000000) >> 24;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
    
    b = (dw & 0xFF0000) >> 16;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
    
    b = (dw & 0xFF00) >> 8;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
    
    b = dw & 0xFF;
    patch.write(reinterpret_cast<char*>(&b),sizeof(b));
  }

  void writePatch(bostream& patch, bistream& target, vector<SameBlock*>& sameBlocks) {
    // NOTE: modified to fit with OV patch variant
    TFileOffset bodySize = 0;
    TFileOffset noBlocks = 0;
    writeDword(patch, 0x54415057);
    TFileOffset noBlocksOffset = patch.tellp();
    writeDword(patch,noBlocks);
    TFileOffset bodySizeOffset = patch.tellp();
    writeDword(patch,bodySize);

    for(vector<SameBlock*>::iterator iter = sameBlocks.begin(); iter != sameBlocks.end(); iter++) {
      SameBlock* current = *iter;

      // store current block
      if(current->size > 0) {
        // copy block from sourceFile
        if(current->size < 256) {
          writeByte(patch,1);
          writeByte(patch,current->size);
          bodySize += 2;
        } else if(current->size < 65536) {
          writeByte(patch,2);
          writeWord(patch,current->size);
          bodySize += 3;
        } else {
          writeByte(patch,3);
          writeDword(patch,current->size);
          bodySize += 5;
        }
        writeDword(patch,current->sourceOffset);
        bodySize += 4;
        noBlocks++;
      }
      iter++;
      if(iter == sameBlocks.end()) break;
      SameBlock* next = *iter;
      iter--;

      // calculate area inbetween this block and the next
      TFileOffset notFoundStart = current->targetOffset+current->size;
      if(notFoundStart > next->targetOffset) {
        throw "makeBinaryPatch input problem: there was overlap";
      }
      TFileOffset notFoundSize = next->targetOffset - notFoundStart;
      if(notFoundSize > 0) {
        // we need to include this area in the patch directly
        if(notFoundSize < 256) {
          writeByte(patch,5);
          writeByte(patch,notFoundSize);
          bodySize += 2;
        } else if(notFoundSize < 65536) {
          writeByte(patch,6);
          writeWord(patch,notFoundSize);
          bodySize += 3;
        } else {
          writeByte(patch,7);
          writeDword(patch,notFoundSize);
          bodySize += 5;
        }
        // copy from target...
        target.seekg(notFoundStart,ios::beg);
#define COPY_BUF_SIZE 4096
        char copyBuffer[COPY_BUF_SIZE];
        for(TFileOffset i = 0; i < notFoundSize; i += COPY_BUF_SIZE) {
          TFileOffset j = notFoundSize - i;
          if(j > COPY_BUF_SIZE) j = COPY_BUF_SIZE;
          target.read(copyBuffer,j);
          patch.write(copyBuffer,j);
        }
        bodySize += notFoundSize;
        noBlocks++;
      }
    }

    TFileOffset curPos = patch.tellp();
    patch.seekp(noBlocksOffset,ios::beg);
    writeDword(patch,noBlocks);
    patch.seekp(bodySizeOffset,ios::beg);
    writeDword(patch,bodySize);
    patch.seekp(curPos,ios::beg);
  }
}
