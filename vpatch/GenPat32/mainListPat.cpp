//---------------------------------------------------------------------------
// mainListPat.cpp
//---------------------------------------------------------------------------
//                           -=* VPatch *=-
//---------------------------------------------------------------------------
// Copyright (C) 2001-2008 Koen van de Sande / Van de Sande Productions
//---------------------------------------------------------------------------
// Website: http://www.tibed.net/vpatch
//
// This software is provided 'as-is', without any express or implied
// warranty.  In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
//    claim that you wrote the original software. If you use this software
//    in a product, an acknowledgment in the product documentation would be
//    appreciated but is not required.
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
// 3. This notice may not be removed or altered from any source distribution.

#ifdef __BORLANDC__
  #pragma argsused
#endif

#include "GlobalTypes.h"
#include "FileFormat1.h"

#include <fstream>
#include <sstream>

int main( int argc, char * argv[] ) {
  cout << "ListPat v3.2\n";
  cout << "============\n\n(c) 2001-2008 Van de Sande Productions\n";
  cout << "Website: http://www.tibed.net/vpatch\n\n";

  if(argc != 2) {
    cout << "This program will take a (patchfile) and list its contents.\n";
    cout << "Command line info:\n";
    cout << "  LISTPAT (patchfile)\n\n";

    cout << "Possible exit codes:\n";
    cout << "  0  Success\n";
    cout << "  1  Arguments missing\n";
    cout << "  2  Other error\n";
    return 1;
  }

  string patchFileName(argv[1]);

  cout << "[PatchFile] " << patchFileName.c_str() << "\n";

  bifstream patch;
  patch.open(patchFileName.c_str(), std::ios_base::binary | std::ios_base::in);

  if(patch.good()) {
    try {
      FileFormat1::printPatchFileInfo(patch);
    }
    catch(string s) {
      cerr << "Error thrown: " << s.c_str();
      return 2;
    }
    catch(const char* s) {
      cerr << "Error thrown: " << s;
      return 2;
    }
  } else {
    cerr << "There was a problem opening the files.\n";
    return 2;
  }
  return 0;
}
