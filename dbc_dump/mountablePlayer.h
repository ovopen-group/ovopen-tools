/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "player.h"

// OK
class MountablePlayerData : public PlayerData
{
typedef PlayerData Parent;

public:
    S32 mCC;                       // @ 0x3c2c
    S32 mPP;                       // @ 0x3c30
    S32 mTier;                     // @ 0x3c34
    StringTableEntry mDispName;    // @ 0x3c38
    StringTableEntry mDesc;        // @ 0x3c3c
    StringTableEntry mIconFile;    // @ 0x3c40
    StringTableEntry mTextureName; // @ 0x3c44
    
   MountablePlayerData()
   {
    mCC = 0;
    mPP = 0;
    mTier = 0;
    mDispName = NULL;
    mDesc = NULL;
    mIconFile = NULL;
    mTextureName = NULL;
   }

   virtual ~MountablePlayerData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      stream->read(&mCC);
      stream->read(&mPP);

      if (stream->readFlag()) {
        mTextureName = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mIconFile = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mDesc = readStringTableString(stream);
      }
      mDispName = readStringTableString(stream);
      mTier = stream->readInt(3);
   }


   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("textureFile",    STStringField,       Offset(mTextureName, MountablePlayerData));
      
      fields.addField("CC",    U32Field,       Offset(mCC, MountablePlayerData));
      fields.addField("PP",    U32Field,       Offset(mPP, MountablePlayerData));

      fields.addField("displayName",    STStringField,       Offset(mDispName, MountablePlayerData));
      fields.addField("desc",    STStringField,       Offset(mDesc, MountablePlayerData));
      
      fields.addField("tier",    S32Field,       Offset(mTier, MountablePlayerData));

      fields.addField("iconFile",    STStringField,       Offset(mIconFile, MountablePlayerData));
   }

   virtual const char* getClassName()
   {
    return "MountablePlayerData";
   }
};
