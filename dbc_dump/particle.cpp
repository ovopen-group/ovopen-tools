/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "common.h"
#include "dataBlocks.h"

static ParticleData gDefaultParticleData;
static ParticleEmitterData gDefaultEmitterData;

void ParticleData::unpackData(BitStream* stream)
{
      Parent::packData(stream);

   dragCoefficient = stream->readFloat(10) * 5;

   if(stream->readFlag())
      stream->read(&windCoefficient);
   else
      windCoefficient = gDefaultParticleData.windCoefficient;

   gravityCoefficient = stream->readSignedFloat(12) * 10;
   inheritedVelFactor = stream->readFloat(9);

   if(stream->readFlag())
      stream->read(&constantAcceleration);
   else
      constantAcceleration = gDefaultParticleData.constantAcceleration;

   lifetimeMS = stream->readInt(10) << 5;
   lifetimeVarianceMS = stream->readInt(10) << 5;

   if(stream->readFlag())
      stream->read(&spinSpeed);
   else
      spinSpeed = gDefaultParticleData.spinSpeed;

   if(stream->readFlag())
   {
      spinRandomMin = stream->readInt(11) - 1000;
      spinRandomMax = stream->readInt(11) - 1000;
   }
   else
   {
      spinRandomMin = gDefaultParticleData.spinRandomMin;
      spinRandomMax = gDefaultParticleData.spinRandomMax;
   }

   useInvAlpha = stream->readFlag();

   S32 i;
   S32 count = stream->readInt(2) + 1;
   for(i = 0;i < count; i++)
   {
      colors[i].red   = stream->readFloat(7);
      colors[i].green = stream->readFloat(7);
      colors[i].blue  = stream->readFloat(7);
      colors[i].alpha = stream->readFloat(7);

      sizes[i] = stream->readFloat(14) * MaxParticleSize;
      times[i] = stream->readFloat(8);
   }
   count = stream->readInt(6);
   for(i = 0; i < count;i ++)
      textureNameList[i] = readStringTableString(stream);
      
      
   allowLighting = stream->readFlag();
}

void ParticleEmitterData::unpackData(BitStream* stream)
{
     Parent::unpackData(stream);

     ejectionPeriodMS = stream->readInt(12); // OV CHANGED
     periodVarianceMS = stream->readInt(12); // OV CHANGED
     ejectionVelocity = stream->readInt(16) / 100.0f;
     velocityVariance = stream->readInt(14) / 100.0f;
     if(stream->readFlag())
        ejectionOffset = stream->readInt(16) / 100.0f;
     else
        ejectionOffset = gDefaultEmitterData.ejectionOffset;

     thetaMin = stream->readRangedU32(0, 360) - 180; // DIFF IN OV!!
     thetaMax = stream->readRangedU32(0, 360) - 180; // DIFF IN OV!!

     if(stream->readFlag())
        phiReferenceVel = stream->readRangedU32(0, 0x3fff); // DIFF IN OV!!!
     else
        phiReferenceVel = gDefaultEmitterData.phiReferenceVel;

     if(stream->readFlag())
        phiVariance = stream->readRangedU32(0, 360);
     else
        phiVariance = gDefaultEmitterData.phiVariance;

     overrideAdvance = stream->readFlag();
     orientParticles = stream->readFlag();
     orientOnVelocity = stream->readFlag();
     lifetimeMS = stream->readInt(10) << 5;
     lifetimeVarianceMS = stream->readInt(10) << 5;
     useEmitterSizes = stream->readFlag();
     useEmitterColors = stream->readFlag();

     U32 size=0;
     stream->read(&size);
     assert(size < 200);
     dataBlockIds.setSize(size);
     for (U32 i = 0; i < dataBlockIds.size(); i++)
        stream->read(&dataBlockIds[i]);

    // Build particle string
    std::string build;
    for (S32 i = 0; i < dataBlockIds.size(); i++)
    {
      char buf[64];
      snprintf(buf, 64, i == dataBlockIds.size()-1 ? "DB_%u" : "DB_%u ", dataBlockIds[i]);
      build += buf;
    }
    particleString = SimpleStringTable::getSTEntry(build.c_str());
}
