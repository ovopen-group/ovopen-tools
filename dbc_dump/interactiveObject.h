/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

class SimFieldDictionary;
class ParticleEmitterData;

// OK
class InteractiveObjectData : public ShapeBaseData
{
typedef ShapeBaseData Parent;

public:

   struct ObjectEntry {
    S32 objectId;
    S32 type;
   };


    SimFieldDictionary *mPersistFieldDictionary;
    S32 mCC;
    S32 mPP;
    StringTableEntry mDispName; // @ 0x258
    StringTableEntry mDesc; // @ 0x25c
    bool mIsWall; // @ 0x260
    bool mIsFurniture; // @ 0x261
    bool mShowRightClick; // @ 0x262
    S32 mTier; // @ 0x264
    S32 mType;
    StringTableEntry mIconFile; // @ 0x26c
    StringTableEntry mTextureName; // @ 0x270
    ParticleEmitterData *mParticleEmitter;
    S32 mParticleEmitterId; // @ 0x278
    S32 mParticleMountPoint; // @ 0x27c
    S32 mLightMountPoint; // @ 0x280
    Vector<InteractiveObjectData::ObjectEntry> mRndVector[5];


   InteractiveObjectData()
   {
      mPersistFieldDictionary = NULL;
      mCC = 0;
      mPP = 0;
      mDispName = NULL;
      mDesc = NULL;
      mIsWall = false;
      mIsFurniture = false;
      mShowRightClick = true;
      mTier = 0;
      mType = 0;
      mIconFile = NULL;
      mTextureName = NULL;
      mParticleEmitter = NULL;
      mParticleEmitterId = 0;
      mParticleMountPoint = 0;
      mLightMountPoint = 0;
      mPersistFieldDictionary = new SimFieldDictionary();
   }

   virtual ~InteractiveObjectData()
   {
    delete mPersistFieldDictionary;
   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

    if (stream->readFlag()) {
      mParticleEmitterId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
    }
    
    mParticleMountPoint = stream->readRangedU32(0, 33) - 1;
    mLightMountPoint = stream->readRangedU32(0, 33) - 1;

    if (stream->readFlag()) {
      mTextureName = readStringTableString(stream);
    }
    
    if (stream->readFlag()) {
      mIconFile = readStringTableString(stream);
    }
    
    if (stream->readFlag()) {
      mDesc = readStringTableString(stream);
    }
    
    while (stream->readFlag()) {
      char fieldValue[256];
      StringTableEntry fieldName  = readStringTableString(stream);
      stream->readString(fieldValue);
      mPersistFieldDictionary->mKeys[fieldName] = std::string(fieldValue);
    }
    
    stream->read(&mCC);
    stream->read(&mPP);
    mDispName = readStringTableString(stream);

    mShowRightClick = stream->readFlag();
    mIsFurniture = stream->readFlag();
    mIsWall = stream->readFlag();

    mTier = stream->readInt(3);
    stream->read(&mType);
   }


   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("textureFile",    STStringField,       Offset(mTextureName, InteractiveObjectData));
      fields.addField("particleEmitter",    SimWeakRefField,       Offset(mParticleEmitterId, InteractiveObjectData));

      fields.addField("particleMountPoint",    S32Field,       Offset(mParticleMountPoint, InteractiveObjectData));
      fields.addField("lightMountPoint",    S32Field,       Offset(mLightMountPoint, InteractiveObjectData));
      
      fields.addField("CC",    S32Field,       Offset(mCC, InteractiveObjectData));
      fields.addField("PP",    S32Field,       Offset(mPP, InteractiveObjectData));

      fields.addField("displayName",    STStringField,       Offset(mDispName, InteractiveObjectData));
      fields.addField("desc",    STStringField,       Offset(mDesc, InteractiveObjectData));

      fields.addField("showRightClick",    BoolField,       Offset(mShowRightClick, InteractiveObjectData));
      fields.addField("isFurniture",    BoolField,       Offset(mIsFurniture, InteractiveObjectData));
      fields.addField("isWall",    BoolField,       Offset(mIsWall, InteractiveObjectData));
      fields.addField("tier",    S32Field,       Offset(mTier, InteractiveObjectData));
      fields.addField("furnType",    S32Field,       Offset(mType, InteractiveObjectData));
      fields.addField("iconFile",    STStringField,       Offset(mIconFile, InteractiveObjectData));
   }

   const char* getDynamicFieldValue(const char* key)
   {
    auto itr = mPersistFieldDictionary->mKeys.find(key);
    if (itr == mPersistFieldDictionary->mKeys.end())
      return NULL;
    else
      return itr->second.c_str();
   }

   void enumerateDynamicFields(std::vector<SimFieldInfo> &fields)
   {
    for (auto itr : mPersistFieldDictionary->mKeys)
    {
      SimFieldInfo info;
      info.name = itr.first;
      fields.push_back(info);
    }
   }

   virtual const char* getClassName()
   {
    return "InteractiveObjectData";
   }
};
