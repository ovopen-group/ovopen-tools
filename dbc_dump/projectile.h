/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

class AudioProfile;
class ExplosionData;
class SplashData;
class ParticleEmitterData;
class DecalData;

// OK
class ProjectileData : public GameBaseData
{
typedef GameBaseData Parent;

public:
   enum {
      NumDecals = 6,
   };

   enum ProjectileConstants {
      MaxLivingTicks       = 4095
   };

    const char *projectileShapeName; // @ 0x50
    bool hasLight;          // @ 0x54
    F32 lightRadius;
    ColorF lightColor;
    bool hasWaterLight;     // @ 0x6c
    ColorF waterLightColor; // @ 0x70
    bool faceViewer;        // @ 0x80
    Point3F scale;
    F32 velInheritFactor;
    F32 muzzleVelocity;
    bool isBallistic;     // @ 0x98
    bool explodeEndLife;  // @ 0x99 (not used?)
    bool explodeFirstHit; // @ 0x9a (not used?)
    bool showIndicator;   // @ 0x9b
    F32 bounceElasticity; // @ 0x9c
    F32 bounceFriction;
    F32 gravityMod;
    U32 lifetime;         // @ 0xa8
    S32 armingDelay;      // @ 0xac
    S32 fadeDelay;        // @ 0xb0
    ExplosionData *explosion;
    S32 explosionId;
    ExplosionData *waterExplosion;
    S32 waterExplosionId;
    SplashData *splash;
    S32 splashId;
    AudioProfile *sound;
    S32 soundId;
    DecalData *decals[NumDecals];
    S32 decalId[NumDecals];
    U32 decalCount;
    Resource<TSShape> projectileShape;
    S32 activateSeq;
    S32 maintainSeq;
    ParticleEmitterData *particleEmitter;
    S32 particleEmitterId;
    ParticleEmitterData *particleWaterEmitter;
    S32 particleWaterEmitterId;


   ProjectileData()
   {
      projectileShapeName = NULL;
      hasLight = false;
      lightRadius = 1;
      lightColor = ColorF(1,1,1,1);
      hasWaterLight = false;
      waterLightColor = ColorF(1,1,1,1);
      faceViewer = false;
      scale = Point3F(1,1,1);
      velInheritFactor = 1;
      muzzleVelocity = 50;
      isBallistic = false;
      explodeEndLife = false;
      explodeFirstHit = false;
      showIndicator = false;
      bounceElasticity = 0.999;
      bounceFriction = 0.3;
      gravityMod = 1;
      lifetime = 625;
      armingDelay = 0;
      fadeDelay = 625;
      explosion = NULL;
      explosionId = 0;
      waterExplosion = NULL;
      waterExplosionId = 0;
      splash = NULL;
      splashId = 0;
      sound = NULL;
      soundId = 0;
      memset(decals, '\0', sizeof(decals));
      memset(decalId, '\0', sizeof(decalId));
      decalCount = 0;
      activateSeq = -1;
      maintainSeq = -1;
      particleEmitter = NULL;
      particleEmitterId = 0;
      particleWaterEmitter = NULL;
      particleWaterEmitterId = 0;
   }

   virtual ~ProjectileData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);

     projectileShapeName = readStringTableString(stream);

     faceViewer = stream->readFlag();
     if(stream->readFlag())
     {
        stream->read(&scale.x);
        stream->read(&scale.y);
        stream->read(&scale.z);
     }
     else
        scale.set(1,1,1);

     if (stream->readFlag())
        particleEmitterId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     if (stream->readFlag())
        particleWaterEmitterId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     if (stream->readFlag())
        explosionId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     if (stream->readFlag())
        waterExplosionId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     if (stream->readFlag())
        splashId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     if (stream->readFlag())
        soundId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);

     for (U32 i = 0; i < NumDecals; i++)
     {
        if (stream->readFlag())
           decalId[i] = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     }

     hasLight = stream->readFlag();
     if(hasLight)
     {
        lightRadius = stream->readFloat(8) * 20;
        lightColor.red = stream->readFloat(7);
        lightColor.green = stream->readFloat(7);
        lightColor.blue = stream->readFloat(7);
     }
     hasWaterLight = stream->readFlag();
     if(hasWaterLight)
     {
        waterLightColor.red = stream->readFloat(7);
        waterLightColor.green = stream->readFloat(7);
        waterLightColor.blue = stream->readFloat(7);
     }
     lifetime = stream->readRangedU32(0, ProjectileData::MaxLivingTicks);
     armingDelay = stream->readRangedU32(0, ProjectileData::MaxLivingTicks);
     fadeDelay = stream->readRangedU32(0, ProjectileData::MaxLivingTicks);

     isBallistic = stream->readFlag();
     if(isBallistic)
     {
        stream->read(&gravityMod);
        stream->read(&bounceElasticity);
        stream->read(&bounceFriction);
     }

     // OV added
     showIndicator = stream->readFlag();
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);
    fields.addField("particleEmitter", SimWeakRefField, Offset(particleEmitterId, ProjectileData));
    fields.addField("particleWaterEmitter", SimWeakRefField, Offset(particleWaterEmitterId, ProjectileData));
    fields.addField("projectileShapeName", STStringField, Offset(projectileShapeName, ProjectileData));
    fields.addField("scale", Point3FField, Offset(scale, ProjectileData));
    fields.addField("sound", SimWeakRefField, Offset(soundId, ProjectileData));

    fields.addField("explosion", SimWeakRefField, Offset(explosionId, ProjectileData));
    fields.addField("waterExplosion", SimWeakRefField, Offset(waterExplosionId, ProjectileData));
    fields.addField("splash", SimWeakRefField, Offset(splashId, ProjectileData));
    fields.addField("decals", SimWeakRefField, Offset(decalId, ProjectileData), NumDecals);

    fields.addField("hasLight", BoolField, Offset(hasLight, ProjectileData));
    fields.addField("lightRadius", F32Field, Offset(lightRadius, ProjectileData));
    fields.addField("lightColor", ColorFField, Offset(lightColor, ProjectileData));

    fields.addField("hasWaterLight", BoolField, Offset(hasWaterLight, ProjectileData));
    fields.addField("waterLightColor", ColorFField, Offset(waterLightColor, ProjectileData));
    
    fields.addField("isBallistic", BoolField, Offset(isBallistic, ProjectileData));
    fields.addField("explodeEndLife", BoolField, Offset(explodeEndLife, ProjectileData));
    fields.addField("explodeFirstHit", BoolField, Offset(explodeFirstHit, ProjectileData));
    fields.addField("showIndicator", BoolField, Offset(showIndicator, ProjectileData));

    fields.addField("velInheritFactor", F32Field, Offset(velInheritFactor, ProjectileData));

    fields.addField("muzzleVelocity", F32Field, Offset(muzzleVelocity, ProjectileData));

    fields.addField("lifetime", S32FieldTickScaled, Offset(lifetime, ProjectileData));

    fields.addField("armingDelay", S32FieldTickScaled, Offset(armingDelay, ProjectileData));

    fields.addField("fadeDelay", S32FieldTickScaled, Offset(fadeDelay, ProjectileData));

    fields.addField("bounceElasticity", F32Field, Offset(bounceElasticity, ProjectileData));

    fields.addField("bounceFriction", F32Field, Offset(bounceFriction, ProjectileData));

    fields.addField("gravityMod", F32Field, Offset(gravityMod, ProjectileData));
   }

   virtual const char* getClassName()
   {
    return "ProjectileData";
   }
};