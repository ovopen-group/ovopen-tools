/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "simKlass.h"
#include "simDatablock.h"
#include "gameBase.h"
#include "shapeBase.h"
#include "staticShape.h"

#include "aiPet.h"
#include "aiPlayer.h"
#include "audioDatablocks.h"
#include "camera.h"
#include "clothing.h"
#include "debris.h"
#include "decal.h"
#include "explosion.h"
#include "flyingVehicle.h"
#include "fxLight.h"
#include "homePoint.h"
#include "hoverVehicle.h"
#include "interactiveObject.h"
#include "item.h"
#include "lightning.h"
#include "mountablePlayer.h"
#include "particle.h"
#include "player.h"
#include "powerup.h"
#include "projectile.h"
#include "sgLight.h"
#include "shoulderPet.h"
#include "toolImage.h"
#include "trigger.h"
#include "tsShape.h"
#include "vehicle.h"
#include "wheeledVehicle.h"
