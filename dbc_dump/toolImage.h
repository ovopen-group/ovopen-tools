/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

// OK
class ToolImageData : public ShapeBaseImageData
{
typedef ShapeBaseImageData Parent;

public:
    S32 mCC;                        // @ 0x1ae4
    S32 mPP;                        // @ 0x1ae8
    S32 mTier;                      // @ 0x1aec
    StringTableEntry mThumbShape;   // @ 0x1af0
    StringTableEntry mDispName;     // @ 0x1af4
    StringTableEntry mDesc;         // @ 0x1af8
    StringTableEntry mIconFile;     // @ 0x1afc
    StringTableEntry mTextureName;  // @ 0a1b00
    StringTableEntry mActionList;   // @ 0x1b04
    
   ToolImageData()
   {
    mCC = 0;
    mPP = 0;
    mTier = 0;
    mThumbShape = NULL;
    mDispName = NULL;
    mDesc = NULL;
    mIconFile = NULL;
    mTextureName = NULL;
    mActionList = NULL;
   }

   virtual ~ToolImageData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      stream->read(&mCC);
      stream->read(&mPP);

      if (stream->readFlag()) {
        mThumbShape = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mTextureName = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mIconFile = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mDesc = readStringTableString(stream);
      }
      mDispName = readStringTableString(stream);
      mTier = stream->readInt(3);
      mActionList = readStringTableString(stream);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("textureFile",    STStringField,       Offset(mTextureName, ToolImageData));
      
      fields.addField("CC",    U32Field,       Offset(mCC, ToolImageData));
      fields.addField("PP",    U32Field,       Offset(mPP, ToolImageData));

      fields.addField("thumbShape",    STStringField,       Offset(mThumbShape, ToolImageData));
      fields.addField("displayName",    STStringField,       Offset(mDispName, ToolImageData));
      fields.addField("desc",    STStringField,       Offset(mDesc, ToolImageData));
      
      fields.addField("tier",    S32Field,       Offset(mTier, ToolImageData));

      fields.addField("iconFile",    STStringField,       Offset(mIconFile, ToolImageData));

      fields.addField("actionList",    STStringField,       Offset(mActionList, ToolImageData));
    }

   virtual const char* getClassName()
   {
    return "ToolImageData";
   }
};
