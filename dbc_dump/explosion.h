/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

class AudioProfile;
class ParticleEmitterData;
class ExplosionData;
class ShockwaveData;

// OK
class ExplosionData : public GameBaseData
{
typedef GameBaseData Parent;


   enum ExplosionConsts
   {
      EC_NUM_DEBRIS_TYPES = 1,
      EC_NUM_EMITTERS = 4,
      EC_MAX_SUB_EXPLOSIONS = 5,
      EC_NUM_TIME_KEYS = 4,
   };

enum
{
  MaxLightRadius = 20
};

public:
    StringTableEntry dtsFileName;
    bool faceViewer;
    S32 particleDensity;
    F32 particleRadius;
    AudioProfile *soundProfile;
    ParticleEmitterData *particleEmitter;
    S32 soundProfileId;
    S32 particleEmitterId;
    Point3F explosionScale;
    F32 playSpeed;
    Resource<TSShape> explosionShape;
    S32 explosionAnimation;
    ParticleEmitterData *emitterList[4];
    S32 emitterIDList[4];
    ShockwaveData *shockwave;
    S32 shockwaveID;
    bool shockwaveOnTerrain;
    ExplosionData *debrisList[1];
    S32 debrisIDList[1];
    F32 debrisThetaMin;
    F32 debrisThetaMax;
    F32 debrisPhiMin;
    F32 debrisPhiMax;
    S32 debrisNum;
    S32 debrisNumVariance;
    F32 debrisVelocity;
    F32 debrisVelocityVariance;
    ExplosionData *explosionList[5];
    S32 explosionIDList[5];
    S32 delayMS;
    S32 delayVariance;
    S32 lifetimeMS;
    S32 lifetimeVariance;
    F32 offset;
    Point3F sizes[4];
    F32 times[4];
    bool shakeCamera;
    VectorF camShakeFreq;
    VectorF camShakeAmp;
    F32 camShakeDuration;
    F32 camShakeRadius;
    F32 camShakeFalloff;
    F32 lightStartRadius;
    F32 lightEndRadius;
    ColorF lightStartColor;
    ColorF lightEndColor;

   ExplosionData()
   {
        dtsFileName  = NULL;
        particleDensity = 10;
        particleRadius = 1.0f;

        faceViewer   = false;

        soundProfile      = NULL;
        particleEmitter   = NULL;
        soundProfileId    = 0;
        particleEmitterId = 0;
        explosionAnimation = 3;

        explosionScale.set(1.0f, 1.0f, 1.0f);
        playSpeed = 1.0f;

        memset( emitterList, 0, sizeof( emitterList ) );
        memset( emitterIDList, 0, sizeof( emitterIDList ) );
        memset( debrisList, 0, sizeof( debrisList ) );
        memset( debrisIDList, 0, sizeof( debrisIDList ) );

        debrisThetaMin = 0.0f;
        debrisThetaMax = 90.0f;
        debrisPhiMin = 0.0f;
        debrisPhiMax = 360.0f;
        debrisNum = 1;
        debrisNumVariance = 0;
        debrisVelocity = 2.0f;
        debrisVelocityVariance = 0.0f;

        memset( explosionList, 0, sizeof( explosionList ) );
        memset( explosionIDList, 0, sizeof( explosionIDList ) );

        delayMS = 0;
        delayVariance = 0;
        lifetimeMS = 1000;
        lifetimeVariance = 0;
        offset = 0.0f;

        shockwave = NULL;
        shockwaveID = 0;
        shockwaveOnTerrain = false;

        shakeCamera = false;
        camShakeFreq.set( 10.0f, 10.0f, 10.0f );
        camShakeAmp.set( 1.0f, 1.0f, 1.0f );
        camShakeDuration = 1.5f;
        camShakeRadius = 10.0f;
        camShakeFalloff = 10.0f;

        for( U32 i=0; i<EC_NUM_TIME_KEYS; i++ )
        {
          times[i] = 1.0f;
        }
        times[0] = 0.0f;

        for( U32 j=0; j<EC_NUM_TIME_KEYS; j++ )
        {
          sizes[j].set( 1.0f, 1.0f, 1.0f );
        }

        //
        lightStartRadius = lightEndRadius = 0.0f;
        lightStartColor.set(1.0f,1.0f,1.0f);
        lightEndColor.set(1.0f,1.0f,1.0f);
   }

   virtual ~ExplosionData()
   {

   }

    virtual S32 shouldEnumerate(void* ptr, int elementIdx)
    {
        if (ptr == &times)
            return true;
        if (ptr == &sizes)
            return true;

      return GameBaseData::shouldEnumerate(ptr, elementIdx);
    }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);


     dtsFileName = readStringTableString(stream);

     if (stream->readFlag())
        soundProfileId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     else
        soundProfileId = 0;

     if (stream->readFlag())
        particleEmitterId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     else
        particleEmitterId = 0;

     particleDensity = stream->readInt(14);
     stream->read(&particleRadius);
     faceViewer = stream->readFlag();

     if(stream->readFlag())
     {
        explosionScale.x = stream->readInt(16) / 100.0f;
        explosionScale.y = stream->readInt(16) / 100.0f;
        explosionScale.z = stream->readInt(16) / 100.0f;
     }
     else
        explosionScale.set(1,1,1);

     playSpeed = stream->readInt(14) / 20.0f;
     debrisThetaMin = stream->readRangedU32(0, 180);
     debrisThetaMax = stream->readRangedU32(0, 180);
     debrisPhiMin = stream->readRangedU32(0, 360);
     debrisPhiMax = stream->readRangedU32(0, 360);
     debrisNum = stream->readRangedU32(0, 1000);
     debrisNumVariance = stream->readRangedU32(0, 1000);

     debrisVelocity = stream->readInt(14) / 10.0f;
     debrisVelocityVariance = stream->readRangedU32(0, 10000) / 10.0f;
     delayMS = stream->readInt(16) << 5;
     delayVariance = stream->readInt(16) << 5;
     lifetimeMS = stream->readInt(16) << 5;
     lifetimeVariance = stream->readInt(16) << 5;

     stream->read(&offset);

     shakeCamera = stream->readFlag();
     stream->read(&camShakeFreq.x);
     stream->read(&camShakeFreq.y);
     stream->read(&camShakeFreq.z);
     stream->read(&camShakeAmp.x);
     stream->read(&camShakeAmp.y);
     stream->read(&camShakeAmp.z);
     stream->read(&camShakeDuration);
     stream->read(&camShakeRadius);
     stream->read(&camShakeFalloff);


     for( S32 j=0; j<EC_NUM_DEBRIS_TYPES; j++ )
     {
        if( stream->readFlag() )
        {
           debrisIDList[j] = (S32) stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
        }
     }

     U32 i;
     for( i=0; i<EC_NUM_EMITTERS; i++ )
     {
        if( stream->readFlag() )
        {
           emitterIDList[i] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
        }
     }

     for( S32 k=0; k<EC_MAX_SUB_EXPLOSIONS; k++ )
     {
        if( stream->readFlag() )
        {
           explosionIDList[k] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
        }
     }

     U32 count = stream->readRangedU32(0, EC_NUM_TIME_KEYS);

     for( i=0; i<count; i++ )
        times[i] = stream->readFloat(8);

     for( i=0; i<count; i++ )
     {
        sizes[i].x = stream->readRangedU32(0, 16000) / 100.0f;
        sizes[i].y = stream->readRangedU32(0, 16000) / 100.0f;
        sizes[i].z = stream->readRangedU32(0, 16000) / 100.0f;
     }

     //
     lightStartRadius = stream->readFloat(8) * MaxLightRadius;
     lightEndRadius = stream->readFloat(8) * MaxLightRadius;
     lightStartColor.red = stream->readFloat(7);
     lightStartColor.green = stream->readFloat(7);
     lightStartColor.blue = stream->readFloat(7);
     lightEndColor.red = stream->readFloat(7);
     lightEndColor.green = stream->readFloat(7);
     lightEndColor.blue = stream->readFloat(7);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("explosionShape", STStringField, Offset(dtsFileName, ExplosionData));
    fields.addField("soundProfile", SimWeakRefField, Offset(soundProfileId, ExplosionData));
    fields.addField("faceViewer", BoolField, Offset(faceViewer, ExplosionData));

    fields.addField("particleEmitter", SimWeakRefField, Offset(particleEmitterId, ExplosionData));

    fields.addField("particleDensity", S32Field, Offset(particleDensity, ExplosionData));
    fields.addField("particleRadius", F32Field, Offset(particleRadius, ExplosionData));
    fields.addField("explosionScale", Point3FField, Offset(explosionScale, ExplosionData));
    fields.addField("playSpeed", F32Field, Offset(playSpeed, ExplosionData));

    fields.addField("emitter", SimWeakRefField, Offset(emitterIDList, ExplosionData), EC_NUM_EMITTERS);
    fields.addField("debris", SimWeakRefField, Offset(debrisIDList, ExplosionData), EC_NUM_DEBRIS_TYPES);
    
    fields.addField("debrisThetaMin", F32Field, Offset(debrisThetaMin, ExplosionData));
    fields.addField("debrisThetaMax", F32Field, Offset(debrisThetaMax, ExplosionData));
    fields.addField("debrisPhiMin", F32Field, Offset(debrisPhiMin, ExplosionData));
    fields.addField("debrisPhiMax",F32Field,  Offset(debrisPhiMax, ExplosionData));
    fields.addField("debrisNum", S32Field, Offset(debrisNum, ExplosionData));
    fields.addField("debrisNumVariance", S32Field, Offset(debrisNumVariance, ExplosionData));
    fields.addField("debrisVelocity", F32Field, Offset(debrisVelocity, ExplosionData));
    fields.addField("debrisVelocityVariance", F32Field, Offset(debrisVelocityVariance, ExplosionData));
    
    fields.addField("subExplosion", SimWeakRefField, Offset(explosionIDList, ExplosionData), EC_MAX_SUB_EXPLOSIONS);

    fields.addField("delayMS", S32Field, Offset(delayMS, ExplosionData));
    fields.addField("delayVariance", S32Field, Offset(delayVariance, ExplosionData));
    fields.addField("lifetimeMS", S32Field, Offset(lifetimeMS, ExplosionData));
    fields.addField("lifetimeVariance", S32Field, Offset(lifetimeVariance, ExplosionData));
    fields.addField("offset", F32Field, Offset(offset, ExplosionData));
    
    fields.addField("times", F32Field, Offset(times, ExplosionData), EC_NUM_TIME_KEYS);
    fields.addField("sizes", Point3FField, Offset(sizes, ExplosionData), EC_NUM_TIME_KEYS);

    fields.addField("shakeCamera", BoolField, Offset(shakeCamera, ExplosionData));
    fields.addField("camShakeFreq", Point3FField, Offset(camShakeFreq, ExplosionData));
    fields.addField("camShakeAmp", Point3FField, Offset(camShakeAmp, ExplosionData));
    fields.addField("camShakeDuration", F32Field, Offset(camShakeDuration, ExplosionData));
    fields.addField("camShakeRadius", F32Field, Offset(camShakeRadius, ExplosionData));
    fields.addField("camShakeFalloff", F32Field, Offset(camShakeFalloff, ExplosionData));

    fields.addField("lightStartRadius", F32Field, Offset(lightStartRadius, ExplosionData));
    fields.addField("lightEndRadius", F32Field, Offset(lightEndRadius, ExplosionData));
    fields.addField("lightStartColor", ColorFField, Offset(lightStartColor, ExplosionData));
    fields.addField("lightEndColor", ColorFField, Offset(lightEndColor, ExplosionData));
  };

   virtual const char* getClassName()
   {
    return "ExplosionData";
   }
};
