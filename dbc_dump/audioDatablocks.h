/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

// OK
class AudioDescription : public SimDataBlock
{
typedef SimDataBlock Parent;

public:

	struct Desc
	{
	   float volume;   // @ 0x48
	   bool isLooping;
	   bool isStreaming;
	   bool is3D;

	   F32 refDist;
	   F32 maxDist;
	   U32 coneInsideAngle;
	   U32 coneOutsideAngle;
	   F32 coneOutsideVolume;
	   Point3F coneVector; // @ 0x64
	   F32 envLevel;

	   S32 loopCount;
	   S32 minLoopGap;
	   S32 maxLoopGap;
	   S32 type;
   };

	 Desc mDesc;
	 

   AudioDescription()
   {
      mDesc.volume = 1;
      mDesc.isLooping = false;
      mDesc.isStreaming = false;
      mDesc.is3D = false;
      mDesc.refDist = 1;
      mDesc.maxDist = 100;
      mDesc.coneInsideAngle = 360;
      mDesc.coneOutsideAngle = 360;
      mDesc.coneOutsideVolume = 1;
      mDesc.coneVector = Point3F(0,0,1);
      mDesc.envLevel = 0;
      mDesc.loopCount = -1;
      mDesc.minLoopGap = 0;
      mDesc.maxLoopGap = 0;
      mDesc.type = 0;
   }

   virtual ~AudioDescription()
   {

   }

   virtual void packData(BitStream* stream)
   {
   	//
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);
   fields.addField("volume",            F32Field,     Offset(mDesc.volume, AudioDescription));
   fields.addField("isLooping",         BoolField,    Offset(mDesc.isLooping, AudioDescription));
   fields.addField("isStreaming",       BoolField,    Offset(mDesc.isStreaming, AudioDescription));
   fields.addField("is3D",              BoolField,    Offset(mDesc.is3D, AudioDescription));
   fields.addField("referenceDistance", F32Field,     Offset(mDesc.refDist, AudioDescription));
   fields.addField("maxDistance",       F32Field,     Offset(mDesc.maxDist, AudioDescription));
   fields.addField("coneInsideAngle",   S32Field,      Offset(mDesc.coneInsideAngle, AudioDescription));
   fields.addField("coneOutsideAngle",  S32Field,      Offset(mDesc.coneOutsideAngle, AudioDescription));
   fields.addField("coneOutsideVolume", F32Field,     Offset(mDesc.coneOutsideVolume, AudioDescription));
   fields.addField("coneVector",        Point3FField,  Offset(mDesc.coneVector, AudioDescription));
   fields.addField("environmentLevel",  F32Field,     Offset(mDesc.envLevel, AudioDescription));
   fields.addField("loopCount",         S32Field,      Offset(mDesc.loopCount, AudioDescription));
   fields.addField("minLoopGap",        S32Field,      Offset(mDesc.minLoopGap, AudioDescription));
   fields.addField("maxLoopGap",        S32Field,      Offset(mDesc.maxLoopGap, AudioDescription));
   fields.addField("type",              S32Field,      Offset(mDesc.type, AudioDescription));
   }

   virtual void unpackData(BitStream* stream)
   {
		mDesc.volume = stream->readFloat(6);
		mDesc.isLooping = stream->readFlag();
		mDesc.isStreaming = false;
		mDesc.is3D = false;

		mDesc.loopCount=-1;
		mDesc.minLoopGap=0;
		mDesc.maxLoopGap=0;
		mDesc.type = 0;

		if (mDesc.isLooping)
		{
			stream->read(&mDesc.loopCount);
			stream->read(&mDesc.minLoopGap);
			stream->read(&mDesc.maxLoopGap);
		}

		mDesc.isStreaming = stream->readFlag();
		mDesc.is3D = stream->readFlag();

		if (mDesc.is3D)
		{
			stream->read(&mDesc.refDist);
			stream->read(&mDesc.maxDist);
			mDesc.coneInsideAngle = stream->readInt(9);
			mDesc.coneOutsideAngle = stream->readInt(9);
			mDesc.coneOutsideVolume = stream->readFloat(6);
			stream->readNormalVector(&mDesc.coneVector, 8);
			stream->read(&mDesc.envLevel);
      }

		mDesc.type = stream->readInt(3);
   }

   virtual const char* getClassName()
   {
    return "AudioDescription";
   }
};

class AudioProfile : public SimDataBlock
{
typedef SimDataBlock Parent;

public:

   U32 mDescID;
   U32 mEnvID;
   bool mPreload;

   StringTableEntry mFilename;

   AudioProfile()
   {
      mDescID = 0;
      mEnvID = 0;
      mFilename = NULL;
      mPreload = false;
   }

   virtual ~AudioProfile()
   {

   }

   virtual void packData(BitStream* stream)
   {
   	//
   }

   virtual void unpackData(BitStream* stream)
   {
	   mDescID = 0;
	   mEnvID = 0;

	   if (stream->readFlag())
	      mDescID = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
	   if (stream->readFlag())
	      mEnvID = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
	   
      mFilename = readStringTableString(stream);
      mPreload = stream->readFlag();
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("filename",    STStringField,       Offset(mFilename, AudioProfile));
      fields.addField("description", SimWeakRefField,   Offset(mDescID, AudioProfile));
      fields.addField("environment", SimWeakRefField,   Offset(mEnvID, AudioProfile));
      fields.addField("preload",     BoolField,          Offset(mPreload, AudioProfile));
   }

   virtual const char* getClassName()
   {
    return "AudioProfile";
   }
};
