/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

#include "common.h"
#include "simKlass.h"

class SimDataBlock
{
public:

  // simobject
  Torque::SimGroup*   mGroup;  ///< SimGroup we're contained in, if any.

   // dictionary information stored on the object
   StringTableEntry objectName;
   Torque::SimObject*       nextNameObject;
   Torque::SimObject*       nextManagerNameObject;
   Torque::SimObject*       nextIdObject;

   BitSet32    mFlags;

   Torque::Notify*     mNotifyList;

   U32 mId;
   Torque::Namespace*  mNameSpace;
   U32         mTypeMask;
   Torque::SimFieldDictionary *mFieldDictionary;    ///< Storage for dynamic fields.

   bool mCanSaveFieldDictionary; ///< true if dynamic fields (added at runtime) should be saved, defaults to true
   StringTableEntry mInternalName; ///< Stores object Internal Name

   // datablock

    S32 modifiedKey;
    bool mServerSide;
    U32 mRefCount;
    bool mTransmit;


    // Field enumeration API
    virtual S32 shouldEnumerate(void* ptr, int elementIdx)
    {
      if (elementIdx >= 0)
      {
        uint32_t* uptr = (uint32_t*)ptr;
        if ((*uptr) == 0) // TODO: "empty" param instead
          return false;
      }
      return true;
    }
    //


    bool shouldEnumSTList(void* ptr, int elementIdx)
    {
        StringTableEntry val = ((StringTableEntry*)ptr)[elementIdx];
        if (val == NULL || val == SimpleStringTable::inst()->getSTEntry(""))
          return false;
        return true;
    }

   SimDataBlock() : mId(0)
   {

   }

   virtual ~SimDataBlock()
   {

   }

   virtual const char* getClassName()=0;/*
   {
    return "SimDataBlock";
   }*/

   virtual void packData(BitStream* stream)
   {

   }

   virtual void unpackData(BitStream* stream)
   {

   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    fields.addField("id", U32Field, Offset(mId, SimDataBlock));
    fields.addFuncField("class", [](int elementIdx, void*ptr){
      return ((SimDataBlock*)ptr)->getClassName();
    },
    [](void*ptr){
      return -1;
    });
   }

   virtual const char* getDynamicFieldValue(const char* key)
   {
      return "";
   }

   virtual void enumerateDynamicFields(std::vector<SimFieldInfo> &fields)
   {
   }

   virtual void dump()
   {

   }

   virtual void dumpJSON()
   {

   }


	static inline StringTableEntry readStringTableString(BitStream *s)
	{
		char str[256];
		str[0] = '\0';
		s->readString(str);
		return SimpleStringTable::getSTEntry(str);
	} 
};

template<typename T> SimDataBlock* doReadFuncTPL(BitStream &s)
{
   T* klass = new T();
   klass->unpackData(&s);
   if (true)
   {
      return klass;
   }
   else
   {
      delete klass;
      return NULL;
   }
}


