/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

class AudioProfile;

// OK
class LightningData : public GameBaseData
{
typedef GameBaseData Parent;

public:
   enum {
      MaxThunders = 8,
      MaxTextures = 8
   };

    AudioProfile *thunderSounds[MaxThunders];
    AudioProfile *strikeSound;
    StringTableEntry strikeTextureNames[MaxTextures];
    S32 thunderSoundIds[MaxThunders];
    S32 strikeSoundID;
    TextureHandle strikeTextures[MaxTextures];
    U32 numThunders;
    bool dynamic;

   LightningData()
   {
    memset(thunderSounds, '\0', sizeof(thunderSounds));
    strikeSound = NULL;
    strikeSoundID = -1;
    numThunders = 0;
    dynamic = false;
    memset(strikeTextureNames, '\0', sizeof(strikeTextureNames));
    memset(thunderSoundIds, '\0', sizeof(thunderSoundIds));
    memset(strikeTextures, '\0', sizeof(strikeTextures));

   }

   virtual ~LightningData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);

     for (U32 i = 0; i < MaxThunders; i++) 
     {
        if (stream->readFlag())
           thunderSoundIds[i] = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
        else
           thunderSoundIds[i] = -1;
     }
     
     for (U32 i = 0; i < MaxTextures; i++) 
     {
        strikeTextureNames[i] = readStringTableString(stream);
     }

     if (stream->readFlag())
        strikeSoundID = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     else
        strikeSoundID = -1;

      dynamic = stream->readFlag();
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);

   fields.addField("strikeSound",     SimWeakRefField,        Offset(strikeSoundID,        LightningData));
   fields.addField("thunderSounds",   SimWeakRefField,        Offset(thunderSoundIds,      LightningData), MaxThunders);
   fields.addField("strikeTextures",  STStringField,          Offset(strikeTextureNames, LightningData), MaxTextures);
  }

   virtual const char* getClassName()
   {
    return "LightningData";
   }
};

// OK
class PrecipitationData : public GameBaseData
{
typedef GameBaseData Parent;

public:

    AudioProfile *soundProfile;
    S32 soundProfileId;
    StringTableEntry mDropName;
    StringTableEntry mSplashName;
    F32 mDropSize;
    F32 mSplashSize;
    bool mUseTrueBillboards;
    S32 mSplashMS;

   PrecipitationData()
   {
      soundProfile = NULL;
      soundProfileId = 0;
      mDropName = SimpleStringTable::getSTEntry("");
      mSplashName = SimpleStringTable::getSTEntry("");
      mDropSize = 0.5;
      mSplashSize = 0.5;
      mUseTrueBillboards = true;
      mSplashMS = 250;
   }

   virtual ~PrecipitationData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);

     if (stream->readFlag())
        soundProfileId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     else
        soundProfileId = 0;

     mDropName = readStringTableString(stream);
     mSplashName = readStringTableString(stream);

     stream->read(&mDropSize);
     stream->read(&mSplashSize);
     stream->read(&mSplashMS);
     mUseTrueBillboards = stream->readFlag();
   }

   virtual const char* getClassName()
   {
    return "PrecipitationData";
   }


   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);

   fields.addField("soundProfile",      SimWeakRefField, Offset(soundProfile,       PrecipitationData));
   fields.addField("dropTexture",       STStringField,        Offset(mDropName,          PrecipitationData));
   fields.addField("splashTexture",     STStringField,        Offset(mSplashName,        PrecipitationData));
   fields.addField("dropSize",          F32Field,             Offset(mDropSize,          PrecipitationData));
   fields.addField("splashSize",        F32Field,             Offset(mSplashSize,        PrecipitationData));
   fields.addField("splashMS",          S32Field,             Offset(mSplashMS,          PrecipitationData));
   fields.addField("useTrueBillboards", BoolField,            Offset(mUseTrueBillboards, PrecipitationData));
  }
};

// OK
class WeatherLightningData : public GameBaseData
{
typedef GameBaseData Parent;

public:
   enum {
      MaxSounds = 4,
      MaxStrikeTextures = 6,
      MaxFlashTextures = 4,
      MaxFuzzyTextures = 2
   };

    U32 numStrikes;
    StringTableEntry strikeTextureNames[MaxStrikeTextures];
    TextureHandle strikeTextures[MaxStrikeTextures];
    U32 numFlashes;
    StringTableEntry flashTextureNames[MaxFlashTextures];
    TextureHandle flashTextures[MaxFlashTextures];
    U32 numFuzzes;
    StringTableEntry fuzzyTextureNames[MaxFuzzyTextures];
    TextureHandle fuzzyTextures[MaxFuzzyTextures];
    S32 strikeSoundId;
    AudioProfile *strikeSound;
    U32 numSounds;
    S32 thunderSoundIds[MaxSounds];
    AudioProfile *thunderSounds[MaxSounds];
    
   WeatherLightningData()
   {
    memset(strikeTextureNames, '\0', sizeof(strikeTextureNames));
    memset(strikeTextures, '\0', sizeof(strikeTextures));
    memset(flashTextureNames, '\0', sizeof(flashTextureNames));
    memset(flashTextures, '\0', sizeof(flashTextures));
    memset(fuzzyTextureNames, '\0', sizeof(fuzzyTextureNames));
    memset(fuzzyTextures, '\0', sizeof(fuzzyTextures));
    memset(thunderSoundIds, '\0', sizeof(thunderSoundIds));
    memset(thunderSounds, '\0', sizeof(thunderSounds));

    strikeSoundId = 0;
    strikeSound = NULL;
    
    numStrikes = (U32)-1; // ??
    numSounds = 0;
    numFlashes = (U32)-1; // ??
    numFuzzes = (U32)-1;  // ??
   }

   virtual ~WeatherLightningData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);

     U32 i;
     for(i = 0; i < MaxStrikeTextures; i++)
        strikeTextureNames[i] = readStringTableString(stream);
     for(i = 0; i < MaxFlashTextures; i++)
        flashTextureNames[i] = readStringTableString(stream);
     for(i = 0; i < MaxFuzzyTextures; i++)
        fuzzyTextureNames[i] = readStringTableString(stream);
       
     if(stream->readFlag())
        strikeSoundId = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
     else
        strikeSoundId = -1;   
     
     for(i = 0; i < MaxSounds; i++)
     {
        if(stream->readFlag())
           thunderSoundIds[i] = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
        else
           thunderSoundIds[i] = -1;
     }
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);
   
   fields.addField("strikeTextures", STStringField, Offset(strikeTextureNames, WeatherLightningData), MaxStrikeTextures);
   fields.addField("flashTextures",  STStringField, Offset(flashTextureNames,  WeatherLightningData), MaxFlashTextures);
   fields.addField("fuzzyTextures",  STStringField, Offset(fuzzyTextureNames,  WeatherLightningData), MaxFuzzyTextures);
   
   fields.addField("strikeSound",    SimWeakRefField, Offset(strikeSoundId,   WeatherLightningData));
   fields.addField("thunderSounds",  SimWeakRefField, Offset(thunderSoundIds, WeatherLightningData), MaxSounds);
   }

   virtual const char* getClassName()
   {
    return "WeatherLightningData";
   }
};
