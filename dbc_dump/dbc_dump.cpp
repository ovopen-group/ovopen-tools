/*
 ovopen-tools
 Copyright (C) 2020  mangofusi
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU Affero General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU Affero General Public License for more details.
 
 You should have received a copy of the GNU Affero General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "common.h"
#include "dataBlocks.h"

enum EventConstants
{
   MaxPacketDataSize = 1500,    ///< Maximum allowed size of a packet.
   MaxConsoleLineSize = 512     ///< Maximum allowed size of a console line.
};

#include <iostream>     // std::cin, std::cout
#include <fstream>      // std::filebuf

#include "math/mAngAxis.h"
#include "math/mMatrix.h"
#include "math/mQuat.h"
#include "ts/tsShape.h"
#include "ts/tsMaterial.h"
#include "ts/tsMaterialManager.h"
#include "ts/tsRenderState.h"
#include "optparse.h"
#include "jansson.h"


DTShape::TSMeshRenderer* DTShape::TSMeshRenderer::create()
{
   return NULL;
}

DTShape::TSMaterialManager* DTShape::TSMaterialManager::instance()
{
   return NULL;
}

std::string gBasePath = "";

PacketReadClass* findClassInfo(U32 idx, U32 typeID);

int main(int argc, char **argv)
{
   std::filebuf fb;
   char buffer[1024*1024*3];
   char strBuffer[1024];
   uint32_t sz = 0;
   char str[256];
   
   SimKlassInfo::buildKlassLookup();
   
   U32 lastSequenceNumber = 0xFFFFFF;
   
   TSKlassDumper dumper;
   JSONKlassDumper json_dumper;
   
   optparse::OptionParser parser = optparse::OptionParser().description("dbc_dump");
   
   parser.add_option("-p", "--path").dest("path").set_default("")
   .help("Set base path of game folder");
   parser.add_option("-j", "--json").dest("use_json").set_default(false).action("store_true")
   .help("Set base path of game folder");
   
   const optparse::Values options = parser.parse_args(argc, argv);
   const std::vector<std::string> args = parser.args();
   auto bp = options.get("path");
   gBasePath = (const char *)bp;
   bool json_format = (bool)options.get("use_json");
   
   for (int i=0; i<args.size(); i++)
   {
      //printf("Open %s\n", args[i].c_str());
      if (fb.open (args[i].c_str(), std::ios::in))
      {
         std::istream is(&fb);
         is.seekg (0, is.end);
         sz = is.tellg();
         is.seekg (0, is.beg);
         is.read(&buffer[0], sz);
         fb.close();
      }
      
      uint8_t* ptr = (uint8_t*)&buffer[0];
      
      uint16_t magic = ((uint16_t*)ptr)[0];
      ptr += 2;
      
      //printf("Magic=0x%x\n", magic);
      
      if (json_format)
      {
         printf("{\"objects\": [\n");
      }
      
      bool done_emit = true;
      while (*ptr++ && (ptr - (uint8_t*)&buffer[0]) < sz) // flag
      {
         uint32_t checksum;
         uint16_t dbID;
         uint16_t dbSize;
         char data[65536];
         
         if (!done_emit && json_format)
         {
            printf(",\n");
         }
         
         checksum = ((uint32_t*)ptr)[0];
         ptr += 4;
         dbID = ((uint16_t*)ptr)[0];
         ptr += 2;
         dbSize = ((uint16_t*)ptr)[0];
         ptr += 2;
         
         //printf("[%u/%u] DB %u size %u checksum=%u\n", ptr - (uint8_t*)&buffer[0], sz, dbID, dbSize, checksum);
         
         BitStream s(ptr, dbSize);
         str[0] = '\0';
         s.setStringBuffer(str);
         S32 classId = s.readClassId(NetClassTypeDataBlock, NetClassGroupGame);
         
         bool hasBuilderDesc = s.readFlag();
         StringTableEntry objName = NULL;
         
         if (hasBuilderDesc)
         {
            objName = SimDataBlock::readStringTableString(&s);
            //printf("Builder desc=%s\n", objName);
         }
         
         //printf("^^ classId=%i builderDesc=%s\n", classId, hasBuilderDesc ? "BUILDER" : "NO_BUILDER");
         
         PacketReadClass* klass = findClassInfo(classId, NetClassTypeDataBlock);
         if (!klass)
         {
            printf("WTF WTF WTF\n");
         }
         //printf("Klass=%s\n", klass->name);
         //klass = NULL;
         if (!klass)
         {
            fprintf(stderr, "Unknown class id %u!\n", classId);
         }
         else
         {
            //printf("^^ className=%s\n", klass->name);
         }
         
         if (klass && klass->readFunc != NULL)
         {
            SimDataBlock* block = klass->readFunc(s);
            if (!block)
            {
               fprintf(stderr, "ERROR: %s\n", klass->name);
            }
            //printf("stream pos=%i\n", s.getPosition());
            
            block->mId = dbID;
            if (objName) {
               //printf("OBJNAME SET %s\n", objName);
               block->objectName = objName;
            }
            
            if (json_format)
               json_dumper.dump(block, SimKlassInfo::lookupKlass(block->getClassName()));
            else
               dumper.dump(block, SimKlassInfo::lookupKlass(block->getClassName()));
         }
         else if (klass)
         {
            fprintf(stderr, "TOHANDLE: %s\n", klass->name);
         }
         
         ptr += dbSize;
         done_emit = false;
      }
      
      //printf("EOF=%u\n", ptr - (uint8_t*)&buffer[0]);
   }
   
   if (json_format)
   {
      printf("]}\n");
   }
   
   return 0;
}

