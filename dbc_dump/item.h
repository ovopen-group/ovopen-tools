/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

// OK

DECLARE_ENUM_TYPE(ItemData_LightTypeField);

class ItemData : public ShapeBaseData
{
typedef ShapeBaseData Parent;

public:
    F32 friction;
    F32 elasticity;
    bool sticky;
    F32 gravityMod;
    F32 maxVelocity;
    S32 dynamicTypeField;
    StringTableEntry pickUpName;
    bool lightOnlyStatic;
    S32 lightType;
    ColorF lightColor;
    S32 lightTime;
    F32 lightRadius;

    enum LightType
   {
      NoLight = 0,
      ConstantLight,
      PulsingLight,

      NumLightTypes,
   };


   ItemData()
   {
    friction = 0;
    elasticity = 0;
    sticky = false;
    gravityMod = 1;
    maxVelocity = -1;
    dynamicTypeField = 0;
    pickUpName = "an item";
    lightOnlyStatic = false;
    lightType = 0;
    lightColor = ColorF(1,1,1,1);
    lightTime = 1000;
    lightRadius = 10;
   }

   virtual ~ItemData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);
     
     friction = stream->readFloat(10);
     elasticity = stream->readFloat(10);
     sticky = stream->readFlag();

     if(stream->readFlag())
        gravityMod = stream->readSignedFloat(11);
     else
        gravityMod = 1.0;

     if(stream->readFlag())
        stream->read(&maxVelocity);
     else
        maxVelocity = -1;

     if(stream->readFlag())
     {
        lightType = stream->readInt(2);
        lightColor.red = stream->readFloat(7);
        lightColor.green = stream->readFloat(7);
        lightColor.blue = stream->readFloat(7);
        lightColor.alpha = stream->readFloat(7);
        stream->read(&lightTime);
        stream->read(&lightRadius);
        lightOnlyStatic = stream->readFlag();
     }
     else
        lightType = NoLight;
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("pickUpName", STStringField, Offset(pickUpName, ItemData));

    fields.addField("friction", F32Field, Offset(friction, ItemData));
    fields.addField("elasticity", F32Field, Offset(elasticity, ItemData));

    fields.addField("sticky", BoolField, Offset(sticky, ItemData));
    fields.addField("gravityMod", F32Field, Offset(gravityMod, ItemData));
    fields.addField("maxVelocity", F32Field, Offset(maxVelocity, ItemData));

    fields.addField("dynamicType", S32Field, Offset(dynamicTypeField, ItemData));
    fields.addField("lightType", ItemData_LightTypeField, Offset(lightType, ItemData));
    fields.addField("lightColor", ColorFField, Offset(lightColor, ItemData));
    fields.addField("lightTime", S32Field, Offset(lightTime, ItemData));
    fields.addField("lightRadius", F32Field, Offset(lightRadius, ItemData));
    
    fields.addField("lightOnlyStatic", BoolField, Offset(lightOnlyStatic, ItemData));

   }


   virtual const char* getClassName()
   {
    return "ItemData";
   }
};
