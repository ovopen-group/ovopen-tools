/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "fxLight.h"

// OK
class sgLightObjectData : public fxLightData
{
typedef fxLightData Parent;

public:
    bool sgStatic;
    bool sgSpot;
    F32 sgSpotAngle;
    bool sgAdvancedLightingModel;
    bool sgEffectsDTSObjects;
    bool sgCastsShadows;
    bool sgDiffuseRestrictZone;
    bool sgAmbientRestrictZone;
    F32 sgLocalAmbientAmount;
    bool sgSmoothSpotLight;
    bool sgDoubleSidedAmbient;
    StringTableEntry sgLightingModelName;
    bool sgUseNormals;
    
   sgLightObjectData()
   {
      sgStatic = false;
      sgSpot = false;
      sgSpotAngle = 1;
      sgAdvancedLightingModel = true;
      sgEffectsDTSObjects = true;
      sgCastsShadows = true;
      sgDiffuseRestrictZone = false;
      sgAmbientRestrictZone = false;
      sgLocalAmbientAmount = 0;
      sgSmoothSpotLight = false;
      sgDoubleSidedAmbient = false;
      sgLightingModelName = NULL;
      sgUseNormals = true;
   }

   virtual ~sgLightObjectData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("StaticLight", BoolField, Offset(sgStatic, sgLightObjectData));
    fields.addField("SpotLight", BoolField, Offset(sgSpot, sgLightObjectData));

    fields.addField("SpotAngle", F32Field, Offset(sgSpotAngle, sgLightObjectData));
    fields.addField("AdvancedLightingModel", BoolField, Offset(sgAdvancedLightingModel, sgLightObjectData));
    fields.addField("EffectsDTSObjects", BoolField, Offset(sgEffectsDTSObjects, sgLightObjectData));
    fields.addField("CastsShadows", BoolField, Offset(sgCastsShadows, sgLightObjectData));
    fields.addField("DiffuseRestrictZone", BoolField, Offset(sgDiffuseRestrictZone, sgLightObjectData));
    fields.addField("AmbientRestrictZone", BoolField, Offset(sgAmbientRestrictZone, sgLightObjectData));
    fields.addField("LocalAmbientAmount", F32Field, Offset(sgLocalAmbientAmount, sgLightObjectData));
    fields.addField("SmoothSpotLight", BoolField, Offset(sgSmoothSpotLight, sgLightObjectData));
    fields.addField("DoubleSidedAmbient", BoolField, Offset(sgDoubleSidedAmbient, sgLightObjectData));
    fields.addField("LightingModelName", STStringField, Offset(sgLightingModelName, sgLightObjectData));
    fields.addField("UseNormals", BoolField, Offset(sgUseNormals, sgLightObjectData));
  
   }


   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

  stream->read(&sgStatic);
  stream->read(&sgSpot);
  stream->read(&sgSpotAngle);
  stream->read(&sgAdvancedLightingModel);
  stream->read(&sgEffectsDTSObjects);
  stream->read(&sgCastsShadows);
  stream->read(&sgDiffuseRestrictZone);
  stream->read(&sgAmbientRestrictZone);
  stream->read(&sgLocalAmbientAmount);
  stream->read(&sgSmoothSpotLight);
  stream->read(&sgDoubleSidedAmbient);

  sgLightingModelName = readStringTableString(stream);
  stream->read(&sgUseNormals);
   }

   virtual const char* getClassName()
   {
    return "sgLightObjectData";
   }
};
