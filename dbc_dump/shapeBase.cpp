/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "common.h"
#include "dataBlocks.h"
#include "ts/tsShape.h"

static ShapeBaseData gShapeBaseDataProto;
static ShapeBaseImageData gShapeBaseImageDataProto;
static ShapeBaseImageData::StateData gDefaultStateData;

#define CHECK_DEFAULT_STATE_F(vv)\
if (elementIdx >= 0 && ptr == &vv) { return (vv[elementIdx] != gShapeBaseImageDataProto.vv[elementIdx]); }

S32 ShapeBaseImageData::shouldEnumerate(void* ptr, int elementIdx)
{
    CHECK_DEFAULT_STATE_F(stateName)
    CHECK_DEFAULT_STATE_F(stateTransitionLoaded)
    CHECK_DEFAULT_STATE_F(stateTransitionNotLoaded)
    CHECK_DEFAULT_STATE_F(stateTransitionAmmo)
    CHECK_DEFAULT_STATE_F(stateTransitionNoAmmo)
    CHECK_DEFAULT_STATE_F(stateTransitionTarget)
    CHECK_DEFAULT_STATE_F(stateTransitionNoTarget)
    CHECK_DEFAULT_STATE_F(stateTransitionWet)
    CHECK_DEFAULT_STATE_F(stateTransitionNotWet)
    CHECK_DEFAULT_STATE_F(stateTransitionTriggerUp)
    CHECK_DEFAULT_STATE_F(stateTransitionTriggerDown)
    CHECK_DEFAULT_STATE_F(stateTransitionTimeout)
    CHECK_DEFAULT_STATE_F(stateWaitForTimeout)
    CHECK_DEFAULT_STATE_F(stateTimeoutValue)
    CHECK_DEFAULT_STATE_F(stateFire)
    CHECK_DEFAULT_STATE_F(stateEjectShell)
    CHECK_DEFAULT_STATE_F(stateEnergyDrain)
    CHECK_DEFAULT_STATE_F(stateAllowImageChange)
    CHECK_DEFAULT_STATE_F(stateScaleAnimation)
    CHECK_DEFAULT_STATE_F(stateDirection)
    CHECK_DEFAULT_STATE_F(stateLoaded)
    CHECK_DEFAULT_STATE_F(stateSpin)
    CHECK_DEFAULT_STATE_F(stateRecoil)
    CHECK_DEFAULT_STATE_F(stateSequence)
    CHECK_DEFAULT_STATE_F(stateSequenceRandomFlash)
    //CHECK_DEFAULT_STATE_F(stateSound)
    if (elementIdx >= 0 && ptr == &stateSound[0]) { return (stateSound[elementIdx].mId != gShapeBaseImageDataProto.stateSound[elementIdx].mId); }
    CHECK_DEFAULT_STATE_F(stateScript)
    //CHECK_DEFAULT_STATE_F(stateEmitter)
    if (elementIdx >= 0 && ptr == &stateEmitter[0]) { return (stateEmitter[elementIdx].mId != gShapeBaseImageDataProto.stateEmitter[elementIdx].mId); }
    CHECK_DEFAULT_STATE_F(stateEmitterTime)
    CHECK_DEFAULT_STATE_F(stateEmitterNode)
    CHECK_DEFAULT_STATE_F(stateIgnoreLoadedForReady)

   if (ptr == &stateWaitForTimeout[0])
   {
     if (stateWaitForTimeout[elementIdx] != gShapeBaseImageDataProto.stateWaitForTimeout[elementIdx])
      return true;

   return false;
   }

   if (elementIdx >= 0)
   {
     uint32_t* uptr = (uint32_t*)ptr;
     if ((*uptr) == 0) // TODO: "empty" param instead
       return false;
   }
   return true;
}


BaseEnumType::EnumTable sIDLight[] = {
   { ShapeBaseImageData::NoLight,           "NoLight" },
   { ShapeBaseImageData::ConstantLight,     "ConstantLight" },
   { ShapeBaseImageData::PulsingLight,      "PulsingLight" },
   { ShapeBaseImageData::WeaponFireLight,   "WeaponFireLight" }
};
IMPLEMENT_ENUM_TYPE(ShapeBaseImageData_LightTypeField, sIDLight)

BaseEnumType::EnumTable sIDLoaded[] = {
   { ShapeBaseImageData::StateData::IgnoreLoaded, "Ignore" },
   { ShapeBaseImageData::StateData::Loaded,       "Loaded" },
   { ShapeBaseImageData::StateData::NotLoaded,    "Empty" },
};
IMPLEMENT_ENUM_TYPE(ShapeBaseImageData_LoadedStateField, sIDLoaded)

BaseEnumType::EnumTable sIDSpin[] = {
   { ShapeBaseImageData::StateData::IgnoreSpin,"Ignore" },
   { ShapeBaseImageData::StateData::NoSpin,    "Stop" },
   { ShapeBaseImageData::StateData::SpinUp,    "SpinUp" },
   { ShapeBaseImageData::StateData::SpinDown,  "SpinDown" },
   { ShapeBaseImageData::StateData::FullSpin,  "FullSpeed" },
};
IMPLEMENT_ENUM_TYPE(ShapeBaseImageData_SpinStateField, sIDSpin)

BaseEnumType::EnumTable sIDRecoil[] = {
   { ShapeBaseImageData::StateData::NoRecoil,     "NoRecoil" },
   { ShapeBaseImageData::StateData::LightRecoil,  "LightRecoil" },
   { ShapeBaseImageData::StateData::MediumRecoil, "MediumRecoil" },
   { ShapeBaseImageData::StateData::HeavyRecoil,  "HeavyRecoil" },
};
IMPLEMENT_ENUM_TYPE(ShapeBaseImageData_RecoilStateField, sIDRecoil)



// TODO MOVE THESE

BaseEnumType::EnumTable sItemLight[] = {
    { ItemData::NoLight, "NoLight"},
    { ItemData::ConstantLight, "ConstantLight"},
    { ItemData::PulsingLight, "SINGLE_USPulsingLightE_STACK"},
};
IMPLEMENT_ENUM_TYPE(ItemData_LightTypeField, sItemLight)

BaseEnumType::EnumTable sPowerupType[] = {
    { PowerupData::SINGLE_USE_NO_COMB, "SINGLE_USE_NO_COMB"},
    { PowerupData::SINGLE_USE_REPLACE, "SINGLE_USE_REPLACE"},
    { PowerupData::SINGLE_USE_STACK, "SINGLE_USE_STACK"},
    { PowerupData::TIMED_NO_COMB, "TIMED_NO_COMB"},
    { PowerupData::TIMED_REPLACE, "TIMED_REPLACE"},
    { PowerupData::TIMED, "TIMED"},
    { PowerupData::UNLIMITED, "UNLIMITED"},
};
IMPLEMENT_ENUM_TYPE(PowerupData_TypeField, sPowerupType)

extern std::string gBasePath;


void ShapeBaseData::unpackData(BitStream* stream)
{
   Parent::unpackData(stream);
   
   computeCRC = stream->readFlag();
   if(computeCRC)
      stream->read(&mCRC);

   shadowEnable = stream->readFlag();
   shadowCanMove = stream->readFlag();
   shadowCanAnimate = stream->readFlag();
   
   cloakTexName = readStringTableString(stream);
   debrisShapeName = readStringTableString(stream);
   shapeName = readStringTableString(stream);

   if (shapeName[0] != '\0')
   {
     char buffer[4096];
     snprintf(buffer, sizeof(buffer), "%s/%s", gBasePath.c_str(), shapeName);
     DTShape::Path basePath(buffer);

     shape = DTShape::TSShape::createFromPath(basePath);
/*
     if (shape)
     {
      printf("Shape %s loaded!\n", shapeName);
     }
     else
     {
      printf("Shape %s not loaded!\n", shapeName);
     }*/
   }

   if(stream->readFlag())
      stream->read(&mass);
   else
      mass = gShapeBaseDataProto.mass;

   if(stream->readFlag())
      stream->read(&drag);
   else
      drag = gShapeBaseDataProto.drag;

   if(stream->readFlag())
      stream->read(&density);
   else
      density = gShapeBaseDataProto.density;

   if(stream->readFlag())
      stream->read(&maxEnergy);
   else
      maxEnergy = gShapeBaseDataProto.maxEnergy;

   if(stream->readFlag())
      stream->read(&maxArmor);
   else
      maxArmor = gShapeBaseDataProto.maxArmor;

   if(stream->readFlag())
      stream->read(&maxDamage);
   else
      maxDamage = gShapeBaseDataProto.maxDamage;

   if(stream->readFlag())
      stream->read(&cameraMaxDist);
   else
      cameraMaxDist = gShapeBaseDataProto.cameraMaxDist;

   if(stream->readFlag())
      stream->read(&cameraMinDist);
   else
      cameraMinDist = gShapeBaseDataProto.cameraMinDist;

   if(stream->readFlag())
      stream->read(&cameraDefaultFov);
   else
      cameraDefaultFov = gShapeBaseDataProto.cameraDefaultFov;

   //

   if(stream->readFlag())
      stream->read(&cameraMinFov);
   else
      cameraMinFov = gShapeBaseDataProto.cameraMinFov;

   if(stream->readFlag())
      stream->read(&cameraMaxFov);
   else
      cameraMaxFov = gShapeBaseDataProto.cameraMaxFov;

   observeThroughObject = stream->readFlag(); // @ 0x16e

   if( stream->readFlag() )
   {
      debrisID = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
   }

   emap = stream->readFlag(); // @ 0x244
   planeSort = stream->readFlag(); // @ 0x245  OV
   isInvincible = stream->readFlag(); // @ 0x249
   renderWhenDestroyed = stream->readFlag(); // @ 0x24a

   if( stream->readFlag() )
   {
      explosionID = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
   }

   if( stream->readFlag() )
   {
      underwaterExplosionID = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
   }

   if( stream->readFlag() )
   {
      damageEmitterID = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
   }

   inheritEnergyFromMount = stream->readFlag(); // @ 0x24b
   firstPersonOnly = stream->readFlag(); // @ 0x246
   useEyePoint = stream->readFlag(); // @ 0x247
}

void ShapeBaseImageData::unpackData(BitStream* stream)
{
   Parent::unpackData(stream);

   mIsBase = stream->readFlag();

   if (mIsBase) // IMPORTANT: base can't have any stuff in it!
      return;
 
   computeCRC = stream->readFlag();
   if(computeCRC)
      stream->read(&mCRC);

   emap = stream->readFlag();

   shapeName = readStringTableString(stream);

   if (shapeName[0] != '\0')
   {
     char buffer[4096];
     snprintf(buffer, sizeof(buffer), "%s/%s", gBasePath.c_str(), shapeName);
     DTShape::Path basePath(buffer);

     //printf("SHAPE PATH %s\n", basePath.getFullPath().c_str());

     shape = DTShape::TSShape::createFromPath(basePath);

     /*if (shape)
     {
      printf("Shape %s loaded!\n", shapeName);
     }
     else
     {
      printf("Shape %s not loaded!\n", shapeName);
     }*/
   }

   stream->read(&mountPoint);
   if (stream->readFlag())
      mountOffset.identity();
   else
      stream->readAffineTransform(&mountOffset);
   if (stream->readFlag())
      eyeOffset.identity();
   else
      stream->readAffineTransform(&eyeOffset);

   correctMuzzleVector = stream->readFlag();
   firstPerson = stream->readFlag();
   stream->read(&mass);
   usesEnergy = stream->readFlag();
   stream->read(&minEnergy);
   hasFlash = stream->readFlag();

   projectile = (stream->readFlag() ?
                 stream->readRangedU32(DataBlockObjectIdFirst,
                                                        DataBlockObjectIdLast) : 0);

   cloakable = stream->readFlag();
   useReticle = stream->readFlag(); // OV
   allowZoom = stream->readFlag();  // OV

   toolIdx = stream->readInt(0x4);
   moveAimOff = stream->readFloat(0x5); // OV
   standAimOff = stream->readFloat(0x5); // OV


   lightType = stream->readRangedU32(0, NumLightTypes-1);
   if(lightType != NoLight)
   {
      stream->read(&lightRadius);
      stream->read(&lightTime);
      lightColor.red = stream->readFloat(7);
      lightColor.green = stream->readFloat(7);
      lightColor.blue = stream->readFloat(7);
      lightColor.alpha = stream->readFloat(7);
   }

   mathRead( *stream, &shellExitDir );
   stream->read(&shellExitVariance);
   stream->read(&shellVelocity);

   if(stream->readFlag())
   {
      casingID = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
   }

   for (U32 i = 0; i < MaxStates; i++) {
      if (stream->readFlag()) {
         StateData& s = state[i];
         s.name = readStringTableString(stream);

         s.transition.loaded[0] = stream->readInt(NumStateBits) - 1;
         s.transition.loaded[1] = stream->readInt(NumStateBits) - 1;
         s.transition.ammo[0] = stream->readInt(NumStateBits) - 1;
         s.transition.ammo[1] = stream->readInt(NumStateBits) - 1;
         s.transition.target[0] = stream->readInt(NumStateBits) - 1;
         s.transition.target[1] = stream->readInt(NumStateBits) - 1;
         s.transition.wet[0] = stream->readInt(NumStateBits) - 1;
         s.transition.wet[1] = stream->readInt(NumStateBits) - 1;
         s.transition.trigger[0] = stream->readInt(NumStateBits) - 1;
         s.transition.trigger[1] = stream->readInt(NumStateBits) - 1;
         s.transition.timeout = stream->readInt(NumStateBits) - 1;
         if(stream->readFlag())
            stream->read(&s.timeoutValue);
         else
            s.timeoutValue = gDefaultStateData.timeoutValue;

         s.waitForTimeout = stream->readFlag();
         s.fire = stream->readFlag();
         s.ejectShell = stream->readFlag();
         s.scaleAnimation = stream->readFlag();
         s.direction = stream->readFlag();
         if(stream->readFlag())
            stream->read(&s.energyDrain);
         else
            s.energyDrain = gDefaultStateData.energyDrain;

         s.loaded = (StateData::LoadedState)stream->readInt(StateData::NumLoadedBits);
         s.spin = (StateData::SpinState)stream->readInt(StateData::NumSpinBits);
         s.recoil = (StateData::RecoilState)stream->readInt(StateData::NumRecoilBits);
         if(stream->readFlag())
            s.sequence = stream->readSignedInt(16);
         else
            s.sequence = gDefaultStateData.sequence;

         if(stream->readFlag())
            s.sequenceVis = stream->readSignedInt(16);
         else
            s.sequenceVis = gDefaultStateData.sequenceVis;

         s.flashSequence = stream->readFlag();
         s.ignoreLoadedForReady = stream->readFlag();

         if (stream->readFlag()) {
            s.emitter = stream->readRangedU32(DataBlockObjectIdFirst,
                                                                     DataBlockObjectIdLast);
          //printf("EMITTER SET!!! %u\n", s.emitter.mId);
            stream->read(&s.emitterTime);
            stream->read(&s.emitterNode);
         }
         else
            s.emitter = 0;
         s.sound = stream->readFlag()? stream->readRangedU32(DataBlockObjectIdFirst,
                                                                             DataBlockObjectIdLast): 0;

      }
   }

   #define LOOKUP_TRANSITION(tname, outName) outName = tname >= 0 ? state[tname].name : NULL; 

   for (U32 i = 0; i < MaxStates; i++) {
      StateData& s = state[i];
      if (s.name == NULL || s.name[0] == '\0')
         continue;


      LOOKUP_TRANSITION(s.transition.loaded[0], stateTransitionLoaded[i])
      LOOKUP_TRANSITION(s.transition.loaded[1], stateTransitionNotLoaded[i])
      LOOKUP_TRANSITION(s.transition.ammo[0], stateTransitionAmmo[i])
      LOOKUP_TRANSITION(s.transition.ammo[1], stateTransitionNoAmmo[i])
      LOOKUP_TRANSITION(s.transition.target[0], stateTransitionTarget[i])
      LOOKUP_TRANSITION(s.transition.target[1], stateTransitionNoTarget[i])
      LOOKUP_TRANSITION(s.transition.wet[0], stateTransitionWet[i])
      LOOKUP_TRANSITION(s.transition.wet[1], stateTransitionNotWet[i])
      LOOKUP_TRANSITION(s.transition.trigger[0], stateTransitionTriggerUp[i])
      LOOKUP_TRANSITION(s.transition.trigger[1], stateTransitionTriggerDown[i])
      LOOKUP_TRANSITION(s.transition.timeout, stateTransitionTimeout[i])

      stateTimeoutValue[i] = s.timeoutValue;
      stateWaitForTimeout[i] = s.waitForTimeout;
      stateFire[i] = s.fire;
      stateEjectShell[i] = s.ejectShell;
      stateScaleAnimation[i] = s.scaleAnimation;
      stateDirection[i] = s.direction;
      stateEnergyDrain[i] = s.energyDrain;
      stateLoaded[i] = s.loaded;
      stateSpin[i] = s.spin;
      stateRecoil[i] = s.recoil;
      stateAllowImageChange[i] = s.allowImageChange;
      stateName[i] = s.name;
      

      stateSequence[i] = NULL;
      if (s.sequence >= 0 && shape)
      {
        stateSequence[i] = SimpleStringTable::inst()->getSTEntry(shape->getSequenceName(s.sequence));
      }

      stateEmitterNode[i] = NULL;
      if (s.emitterNode >= 0 && shape)
      {
        stateEmitterNode[i] = SimpleStringTable::inst()->getSTEntry(shape->getNodeName(s.emitterNode));
      }

      stateSequenceRandomFlash[i] = s.flashSequence;
      stateIgnoreLoadedForReady[i] = s.ignoreLoadedForReady;
      if (s.emitter.mId != 0)
      {
         stateEmitter[i].mId = s.emitter.mId;
         stateEmitterTime[i] = s.emitterTime;
      }
      stateSound[i] = s.sound.mId;
      stateScript[i] = NULL;

      if (s.fire && i == 3)
      {
         stateScript[3] = "onFire";
      }
      else if (strcasecmp(s.name, "DecInv") == 0)
      {
         stateScript[i] = "onDecInv";
      }

   }
   statesLoaded = true;
}



