/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

class ExplosionData;
class ParticleEmitterData;

// OK
class DebrisData : public GameBaseData
{
typedef GameBaseData Parent;

public:

     enum DebrisDataConst
   {
      DDC_NUM_EMITTERS = 2
   };


    F32 velocity;
    F32 velocityVariance;
    F32 friction;
    F32 elasticity;
    F32 lifetime;
    F32 lifetimeVariance;
    U32 numBounces;
    U32 bounceVariance;
    F32 minSpinSpeed;
    F32 maxSpinSpeed;
    bool render2D;
    bool explodeOnMaxBounce;
    bool staticOnMaxBounce;
    bool snapOnMaxBounce;
    bool fade;
    bool useRadiusMass;
    F32 baseRadius;
    F32 gravModifier;
    F32 terminalVelocity;
    bool ignoreWater;
    const char *shapeName;
    Resource<TSShape> shape;
    StringTableEntry textureName;
    TextureHandle texture;
    S32 explosionId;
    ExplosionData *explosion;
    SimWeakRef<ParticleEmitterData> emitterList[DDC_NUM_EMITTERS];
    S32 emitterIDList[DDC_NUM_EMITTERS];

   DebrisData()
   {
    velocity = 0;
    velocityVariance = 0;
    friction = 0.200000003;
    elasticity = 0.300000012;
    lifetime = 3;
    lifetimeVariance = 0;
    numBounces = 0;
    bounceVariance = 0;
    minSpinSpeed = 0;
    maxSpinSpeed = 0;
    render2D = false;
    explodeOnMaxBounce = false;
    staticOnMaxBounce = false;
    snapOnMaxBounce = false;
    fade = true;
    useRadiusMass = false;
    baseRadius = 1;
    gravModifier = 1;
    terminalVelocity = 0;
    ignoreWater = true;
    shapeName = NULL;
    textureName = NULL;
    explosionId = 0;
    explosion = NULL;
    memset(emitterList, '\0', sizeof(emitterList));
    memset(emitterIDList, '\0', sizeof(emitterIDList));
   }

   virtual ~DebrisData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {

    Parent::unpackData(stream);

    stream->read(&elasticity);
    stream->read(&friction);
    stream->read(&numBounces);
    stream->read(&bounceVariance);
    stream->read(&minSpinSpeed);
    stream->read(&maxSpinSpeed);
    stream->read(&render2D);
    stream->read(&explodeOnMaxBounce);
    stream->read(&staticOnMaxBounce);
    stream->read(&snapOnMaxBounce);
    stream->read(&lifetime);
    stream->read(&lifetimeVariance);
    stream->read(&minSpinSpeed);
    stream->read(&maxSpinSpeed);
    stream->read(&velocity);
    stream->read(&velocityVariance);
    stream->read(&fade);
    stream->read(&useRadiusMass);
    stream->read(&baseRadius);
    stream->read(&gravModifier);
    stream->read(&terminalVelocity);
    stream->read(&ignoreWater);

    textureName = readStringTableString(stream);
    shapeName   = readStringTableString(stream);

    for( int i=0; i<DDC_NUM_EMITTERS; i++ )
    {
      if( stream->readFlag() )
      {
        emitterIDList[i] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
      }
    }

    if(stream->readFlag())
    {
      explosionId = (S32)stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
    }
    else
    {
      explosionId = 0;
    }

   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);

    fields.addField("texture", STStringField, Offset(textureName, DebrisData));
    fields.addField("shapeFile", STStringField, Offset(shapeName, DebrisData));

    fields.addField("render2D", BoolField, Offset(render2D, DebrisData));

    fields.addField("emitters", SimWeakRefField, Offset(emitterIDList, DebrisData), DDC_NUM_EMITTERS);
    fields.addField("explosion", SimWeakRefField, Offset(explosionId, DebrisData));

    fields.addField("elasticity", F32Field, Offset(elasticity, DebrisData));
    fields.addField("friction", F32Field, Offset(friction, DebrisData));

    fields.addField("numBounces", S32Field, Offset(numBounces, DebrisData));
    fields.addField("bounceVariance", S32Field, Offset(bounceVariance, DebrisData));
    fields.addField("minSpinSpeed", F32Field, Offset(minSpinSpeed, DebrisData));
    fields.addField("maxSpinSpeed", F32Field, Offset(maxSpinSpeed, DebrisData));
    fields.addField("gravModifier", F32Field, Offset(gravModifier, DebrisData));
    fields.addField("terminalVelocity", F32Field, Offset(terminalVelocity, DebrisData));
    fields.addField("velocity", F32Field, Offset(velocity, DebrisData));
    fields.addField("velocityVariance", F32Field, Offset(velocityVariance, DebrisData));
    fields.addField("lifetime", F32Field, Offset(lifetime, DebrisData));
    fields.addField("lifetimeVariance", F32Field, Offset(lifetimeVariance, DebrisData));
    fields.addField("useRadiusMass", BoolField, Offset(useRadiusMass, DebrisData));
    fields.addField("baseRadius", F32Field, Offset(baseRadius, DebrisData));

    fields.addField("explodeOnMaxBounce", BoolField, Offset(explodeOnMaxBounce, DebrisData));
    fields.addField("staticOnMaxBounce", BoolField, Offset(staticOnMaxBounce, DebrisData));
    fields.addField("snapOnMaxBounce", BoolField, Offset(snapOnMaxBounce, DebrisData));
    fields.addField("fade", BoolField, Offset(fade, DebrisData));
    fields.addField("ignoreWater", BoolField, Offset(ignoreWater, DebrisData));
  }


   virtual const char* getClassName()
   {
    return "DebrisData";
   }
};
