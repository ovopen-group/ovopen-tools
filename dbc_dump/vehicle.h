/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

class ParticleEmitterData;

// OK
class VehicleData : public ShapeBaseData
{
typedef ShapeBaseData Parent;

public:

   struct Body {
      enum Sounds {
         SoftImpactSound,
         HardImpactSound,
         MaxSounds,
      };

        S32 sound[MaxSounds]; // sound IDS
        F32 restitution;
        F32 friction;
    };
    VehicleData::Body body;


   enum VehicleConsts
   {
      VC_NUM_DUST_EMITTERS = 1,
      VC_NUM_DAMAGE_EMITTER_AREAS = 2,
      VC_NUM_DAMAGE_LEVELS = 2,
      VC_NUM_BUBBLE_EMITTERS = 1,
      VC_NUM_DAMAGE_EMITTERS = VC_NUM_DAMAGE_LEVELS + VC_NUM_BUBBLE_EMITTERS,
      VC_NUM_SPLASH_EMITTERS = 2,
      VC_BUBBLE_EMITTER = VC_NUM_DAMAGE_EMITTERS - VC_NUM_BUBBLE_EMITTERS,
   };

  enum Sounds {
      ExitWater,
      ImpactSoft,
      ImpactMedium,
      ImpactHard,
      Wake,
      MaxSounds
   };

    S32 waterSound[MaxSounds]; // sound IDS
    F32 exitSplashSoundVel;
    F32 softSplashSoundVel;
    F32 medSplashSoundVel;
    F32 hardSplashSoundVel;
    F32 minImpactSpeed; // @ 0x280
    F32 softImpactSpeed;
    F32 hardImpactSpeed;
    F32 minRollSpeed;
    F32 maxSteeringAngle;
    F32 collDamageThresholdVel;
    F32 collDamageMultiplier;
    bool cameraRoll;
    F32 cameraLag;
    F32 cameraDecay;
    F32 cameraOffset;
    F32 minDrag;
    F32 maxDrag;
    S32 integration;
    F32 collisionTol;
    F32 contactTol;
    Point3F massCenter; // @ 0x2c0
    Point3F massBox;
    F32 jetForce;
    F32 jetEnergyDrain;
    F32 minJetEnergy;
    ParticleEmitterData *dustEmitter;
    S32 dustID;
    F32 triggerDustHeight;
    F32 dustHeight;
    ParticleEmitterData *damageEmitterList[VC_NUM_DAMAGE_EMITTERS];
    Point3F damageEmitterOffset[VC_NUM_DAMAGE_EMITTER_AREAS];
    S32 damageEmitterIDList[VC_NUM_DAMAGE_EMITTERS];
    F32 damageLevelTolerance[VC_NUM_DAMAGE_LEVELS];
    F32 numDmgEmitterAreas;
    ParticleEmitterData *splashEmitterList[VC_NUM_SPLASH_EMITTERS];
    S32 splashEmitterIDList[VC_NUM_SPLASH_EMITTERS];
    F32 splashFreqMod;
    F32 splashVelEpsilon;
    S32 mCC;
    S32 mPP;
    S32 mTier;
    StringTableEntry mDispName; // @ 0x354
    StringTableEntry mDesc;
    StringTableEntry mIconFile;
    StringTableEntry mTextureName;

   VehicleData()
   {
     shadowEnable = true;
     shadowCanMove = true;
     shadowCanAnimate = true;

     body.friction = 0;
     body.restitution = 1;

     minImpactSpeed = 25;
     softImpactSpeed = 25;
     hardImpactSpeed = 50;
     minRollSpeed = 0;
     maxSteeringAngle = 0.785; // 45 deg.

     cameraRoll = true;
     cameraLag = 0;
     cameraDecay = 0;
     cameraOffset = 0;

     minDrag = 0;
     maxDrag = 0;
     integration = 1;
     collisionTol = 0.1;
     contactTol = 0.1;
     massCenter.set(0,0,0);
     massBox.set(0,0,0);

     drag = 0.7;
     density = 4;

     jetForce = 500;
     jetEnergyDrain =  0.8;
     minJetEnergy = 1;

     for (S32 i = 0; i < Body::MaxSounds; i++)
        body.sound[i] = 0;

     dustEmitter = NULL;
     dustID = 0;
     triggerDustHeight = 3.0;
     dustHeight = 1.0;

     memset( damageEmitterList, 0, sizeof( damageEmitterList ) );
     memset( damageEmitterIDList, 0, sizeof( damageEmitterIDList ) );
     memset( damageLevelTolerance, 0, sizeof( damageLevelTolerance ) );
     memset( splashEmitterList, 0, sizeof( splashEmitterList ) );
     memset( splashEmitterIDList, 0, sizeof( splashEmitterIDList ) );

     numDmgEmitterAreas = 0;

     splashFreqMod = 300.0;
     splashVelEpsilon = 0.50;
     exitSplashSoundVel = 2.0;
     softSplashSoundVel = 1.0;
     medSplashSoundVel = 2.0;
     hardSplashSoundVel = 3.0;

     genericShadowLevel = Vehicle_GenericShadowLevel;
     noShadowLevel = Vehicle_NoShadowLevel;

     memset(waterSound, 0, sizeof(waterSound));

     collDamageThresholdVel = 20;
     collDamageMultiplier   = 0.05;

     mCC = 0;
     mPP = 0;
     mTier = 0;
     mDispName = NULL;
     mDesc = NULL;
     mIconFile = NULL;
     mTextureName = NULL;
   }

   virtual ~VehicleData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
   Parent::unpackData(stream);

   stream->read(&body.restitution);
   stream->read(&body.friction);

   for (U32 i = 0; i < Body::MaxSounds; i++) {
      body.sound[i] = 0;
      if (stream->readFlag())
         body.sound[i] = stream->readRangedU32(DataBlockObjectIdFirst,
                                                              DataBlockObjectIdLast);
   }

   stream->read(&minImpactSpeed);
   stream->read(&softImpactSpeed);
   stream->read(&hardImpactSpeed);
   stream->read(&minRollSpeed);
   stream->read(&maxSteeringAngle);

   stream->read(&maxDrag);
   stream->read(&minDrag);
   stream->read(&integration);
   stream->read(&collisionTol);
   stream->read(&contactTol);
   mathRead(*stream,&massCenter);
   mathRead(*stream,&massBox);

   stream->read(&jetForce);
   stream->read(&jetEnergyDrain);
   stream->read(&minJetEnergy);

   cameraRoll = stream->readFlag();
   stream->read(&cameraLag);
   stream->read(&cameraDecay);
   stream->read(&cameraOffset);

   stream->read( &triggerDustHeight );
   stream->read( &dustHeight );

   stream->read( &numDmgEmitterAreas );

   stream->read(&exitSplashSoundVel);
   stream->read(&softSplashSoundVel);
   stream->read(&medSplashSoundVel);
   stream->read(&hardSplashSoundVel);

   // write the water sound profiles
   for(U32  i = 0; i < MaxSounds; i++)
   {
      if(stream->readFlag())
      {
         U32 id = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
         waterSound[i] = id;
      }
    }

   if( stream->readFlag() )
   {
      dustID = (S32) stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
   }

   for ( U32 i = 0; i < VC_NUM_DAMAGE_EMITTERS; i++)
   {
      if( stream->readFlag() )
      {
         damageEmitterIDList[i] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
      }
   }

   for ( U32 i = 0; i < VC_NUM_SPLASH_EMITTERS; i++)
   {
      if( stream->readFlag() )
      {
         splashEmitterIDList[i] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
      }
   }

   for( U32 j=0; j<VC_NUM_DAMAGE_EMITTER_AREAS; j++ )
   {
      stream->read( &damageEmitterOffset[j].x );
      stream->read( &damageEmitterOffset[j].y );
      stream->read( &damageEmitterOffset[j].z );
   }

   for( U32 k=0; k<VC_NUM_DAMAGE_LEVELS; k++ )
   {
      stream->read( &damageLevelTolerance[k] );
   }

   stream->read(&splashFreqMod);
   stream->read(&splashVelEpsilon);

   stream->read(&collDamageThresholdVel);
   stream->read(&collDamageMultiplier);

   stream->read(&mCC);
   stream->read(&mPP);

   if (stream->readFlag()) mTextureName = readStringTableString(stream);
   if (stream->readFlag()) mIconFile = readStringTableString(stream);
   if (stream->readFlag()) mDesc = readStringTableString(stream);
   mDispName = readStringTableString(stream);
   
   mTier = stream->readInt(0x3);
   }


    virtual S32 shouldEnumerate(void* ptr, int elementIdx)
    {
        if (ptr == &damageLevelTolerance)
            return true;
          if (ptr == &damageEmitterOffset)
            return true;

      return ShapeBaseData::shouldEnumerate(ptr, elementIdx);
    }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("jetForce", F32Field, Offset(jetForce, VehicleData));
    fields.addField("jetEnergyDrain", F32Field, Offset(jetEnergyDrain, VehicleData));
    fields.addField("minJetEnergy", F32Field, Offset(minJetEnergy, VehicleData));
    fields.addField("massCenter", Point3FField, Offset(massCenter, VehicleData));
    fields.addField("massBox", Point3FField, Offset(massBox, VehicleData));
    fields.addField("bodyRestitution", F32Field, Offset(body.restitution, VehicleData));
    fields.addField("bodyFriction", F32Field, Offset(body.friction, VehicleData));

    fields.addField("softImpactSound", SimWeakRefField, Offset(body.sound[Body::SoftImpactSound], VehicleData));
    fields.addField("hardImpactSound", SimWeakRefField, Offset(body.sound[Body::HardImpactSound], VehicleData));
    fields.addField("minImpactSpeed", F32Field, Offset(minImpactSpeed, VehicleData));
    fields.addField("softImpactSpeed", F32Field, Offset(softImpactSpeed, VehicleData));
    fields.addField("hardImpactSpeed", F32Field, Offset(hardImpactSpeed, VehicleData));
    fields.addField("minRollSpeed", F32Field, Offset(minRollSpeed, VehicleData));
    fields.addField("maxSteeringAngle", F32Field, Offset(maxSteeringAngle, VehicleData));
    fields.addField("maxDrag", F32Field, Offset(maxDrag, VehicleData));
    fields.addField("minDrag", F32Field, Offset(minDrag, VehicleData));
    fields.addField("integration", S32Field, Offset(integration, VehicleData));
    fields.addField("collisionTol", F32Field, Offset(collisionTol, VehicleData));
    fields.addField("contactTol", F32Field, Offset(contactTol, VehicleData));
    fields.addField("cameraRoll", BoolField, Offset(cameraRoll, VehicleData));
    fields.addField("cameraLag", F32Field, Offset(cameraLag, VehicleData));
    fields.addField("cameraDecay", F32Field, Offset(cameraDecay, VehicleData));
    fields.addField("cameraOffset", F32Field, Offset(cameraOffset, VehicleData));

    fields.addField("dustEmitter", SimWeakRefField, Offset(dustID, VehicleData));
    fields.addField("triggerDustHeight", F32Field, Offset(triggerDustHeight, VehicleData));
    fields.addField("dustHeight", F32Field, Offset(dustHeight, VehicleData));

    fields.addField("damageEmitter", SimWeakRefField, Offset(damageEmitterIDList, VehicleData), VC_NUM_DAMAGE_EMITTERS);
    fields.addField("splashEmitter", SimWeakRefField, Offset(splashEmitterIDList, VehicleData), VC_NUM_SPLASH_EMITTERS);
    fields.addField("damageEmitterOffset", Point3FField, Offset(damageEmitterOffset, VehicleData), VC_NUM_DAMAGE_EMITTER_AREAS);
    fields.addField("damageLevelTolerance", F32Field, Offset(damageLevelTolerance, VehicleData), VC_NUM_DAMAGE_LEVELS);
    fields.addField("numDmgEmitterAreas", F32Field, Offset(numDmgEmitterAreas, VehicleData));

    fields.addField("splashFreqMod", F32Field, Offset(splashFreqMod, VehicleData));
    fields.addField("splashVelEpsilon", F32Field, Offset(splashVelEpsilon, VehicleData));

    fields.addField("exitSplashSoundVelocity", F32Field, Offset(exitSplashSoundVel, VehicleData));
    fields.addField("softSplashSoundVelocity", F32Field, Offset(softSplashSoundVel, VehicleData));
    fields.addField("mediumSplashSoundVelocity", F32Field, Offset(medSplashSoundVel, VehicleData));
    fields.addField("hardSplashSoundVelocity", F32Field, Offset(hardSplashSoundVel, VehicleData));
    fields.addField("exitingWater", SimWeakRefField, Offset(waterSound[ExitWater], VehicleData));
    fields.addField("impactWaterEasy",SimWeakRefField, Offset(waterSound[ImpactSoft], VehicleData));
    fields.addField("impactWaterMedium", SimWeakRefField, Offset(waterSound[ImpactMedium], VehicleData));
    fields.addField("impactWaterHard", SimWeakRefField, Offset(waterSound[ImpactHard], VehicleData));
    fields.addField("waterWakeSound", SimWeakRefField, Offset(waterSound[Wake], VehicleData));

    fields.addField("collDamageThresholdVel", F32Field, Offset(collDamageThresholdVel, VehicleData));
    fields.addField("collDamageMultiplier", F32Field, Offset(collDamageMultiplier, VehicleData));

    fields.addField("textureFile", STStringField, Offset(mTextureName, VehicleData));
    fields.addField("CC", S32Field, Offset(mCC, VehicleData));
    fields.addField("PP", S32Field, Offset(mPP, VehicleData));
    fields.addField("displayName", STStringField, Offset(mDispName, VehicleData));
    fields.addField("desc", STStringField, Offset(mDesc, VehicleData));
    fields.addField("tier", S32Field,  Offset(mTier, VehicleData));
    fields.addField("iconFile", STStringField, Offset(mIconFile, VehicleData));
   }

   virtual const char* getClassName()
   {
    return "VehicleData";
   }
};
