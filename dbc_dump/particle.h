/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "common.h"
#include "gameBase.h"

enum ParticleConsts
{
  PC_COLOR_KEYS = 4,
  PC_SIZE_KEYS = 4,
};

#define MaxParticleSize 50.0f

// OK
class ParticleData : public SimDataBlock
{
typedef SimDataBlock Parent;

public:

   enum PDConst
   {
      PDC_MAX_TEX = 50
   };

    F32 dragCoefficient;
    F32 windCoefficient;
    F32 gravityCoefficient;
    F32 inheritedVelFactor;
    F32 constantAcceleration;
    S32 lifetimeMS;
    S32 lifetimeVarianceMS;
    F32 spinSpeed;
    F32 spinRandomMin;
    F32 spinRandomMax;
    bool useInvAlpha;
    bool allowLighting;
    bool animateTexture;
    U32 numFrames;
    U32 framesPerSec;
    ColorF colors[PC_COLOR_KEYS];
    F32 sizes[PC_COLOR_KEYS];
    F32 times[4];
    StringTableEntry textureNameList[PDC_MAX_TEX];
    //TextureHandle textureList[PDC_MAX_TEX];
    bool bSandBoxTexturesLoaded;

   ParticleData()
   {
      dragCoefficient = 0;
      windCoefficient = 1;
      gravityCoefficient = 0;
      inheritedVelFactor = 0;
      constantAcceleration = 0;
      lifetimeMS = 1000;
      lifetimeVarianceMS = 0;
      spinSpeed = 0;
      spinRandomMin = 0;
      spinRandomMax = 0;
      useInvAlpha = false;
      allowLighting = false;
      animateTexture = false;
      numFrames = 1;
      framesPerSec = 1;

      for (int i=0; i<PC_COLOR_KEYS; i++)
      {
        colors[i] = ColorF(1,1,1,1);
        sizes[i] = 1;
      }

      times[0] = 0;
      times[1] = 1;
      times[2] = 2;
      times[3] = 2;

      memset(textureNameList, '\0', sizeof(textureNameList));
      bSandBoxTexturesLoaded = false;
    }

   virtual ~ParticleData()
   {
   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream);


    virtual S32 shouldEnumerate(void* ptr, int elementIdx)
    {
      if (ptr == &textureNameList)
      {
        return shouldEnumSTList(ptr, elementIdx);
      }
      if (ptr == &colors)
        return true;
      if (ptr == &sizes)
        return true;
      if (ptr == &times)
        return true;

      return true;
    }




   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("dragCoefficient", F32Field, Offset(dragCoefficient, ParticleData));
    fields.addField("windCoefficient", F32Field, Offset(windCoefficient, ParticleData));
    fields.addField("gravityCoefficient", F32Field, Offset(gravityCoefficient, ParticleData));
    fields.addField("inheritedVelFactor", F32Field, Offset(inheritedVelFactor, ParticleData));
    fields.addField("constantAcceleration", F32Field, Offset(constantAcceleration, ParticleData));

    fields.addField("lifetimeMS", S32Field, Offset(lifetimeMS, ParticleData));
    fields.addField("lifetimeVarianceMS", S32Field, Offset(lifetimeVarianceMS, ParticleData));
    fields.addField("spinSpeed", F32Field, Offset(spinSpeed, ParticleData));
    fields.addField("spinRandomMin", F32Field, Offset(spinRandomMin, ParticleData));
    fields.addField("spinRandomMax", F32Field, Offset(spinRandomMax, ParticleData));

    fields.addField("useInvAlpha", BoolField, Offset(useInvAlpha, ParticleData));
    fields.addField("animateTexture", BoolField, Offset(animateTexture, ParticleData));
    fields.addField("framesPerSec", S32Field, Offset(framesPerSec, ParticleData));

    fields.addField("textureName", STStringField, Offset(textureNameList, ParticleData));
    fields.addField("animTexName", STStringField, Offset(textureNameList, ParticleData), PDC_MAX_TEX);
    fields.addField("colors", ColorFField, Offset(colors, ParticleData), PC_COLOR_KEYS);
    fields.addField("sizes", F32Field, Offset(sizes, ParticleData), PC_SIZE_KEYS);
    fields.addField("times", F32Field, Offset(times, ParticleData), 4);

    fields.addField("allowLighting", BoolField, Offset(allowLighting, ParticleData));
   }

   virtual const char* getClassName()
   {
    return "ParticleData";
   }
};

// OK
class ParticleEmitterNodeData : public GameBaseData
{
typedef GameBaseData Parent;

public:

	    F32 timeMultiple;

   ParticleEmitterNodeData()
   {
    timeMultiple = 1;
   }

   virtual ~ParticleEmitterNodeData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);
      stream->read(&timeMultiple);
   }




   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);
      fields.addField("timeMultiple", F32Field, Offset(timeMultiple, ParticleEmitterNodeData));
   }

   virtual const char* getClassName()
   {
    return "ParticleEmitterNodeData";
   }
};

// OK
class ParticleEmitterData : public GameBaseData
{
typedef GameBaseData Parent;

public:

    S32 ejectionPeriodMS; // @ 0x50
    S32 periodVarianceMS; // @ 0x54
    F32 ejectionVelocity;
    F32 velocityVariance;
    F32 ejectionOffset;   // @ 0x60
    F32 thetaMin;
    F32 thetaMax;
    F32 phiReferenceVel;
    F32 phiVariance;
    U32 lifetimeMS;
    U32 lifetimeVarianceMS;
    bool overrideAdvance; // @ 0x7c
    bool orientParticles;
    bool orientOnVelocity;
    bool useEmitterSizes;
    bool useEmitterColors;
    StringTableEntry particleString; // @ 0x84
    Vector<ParticleData*> particleDataBlocks;
    Vector<U32> dataBlockIds;


   ParticleEmitterData()
   {
      ejectionPeriodMS = 100;
      periodVarianceMS = 0;
      ejectionVelocity = 2;
      velocityVariance = 1;
      ejectionOffset = 0;;
      thetaMin = 0;
      thetaMax = 90;
      phiReferenceVel = 0;
      phiVariance = 360;
      lifetimeMS = 0;
      lifetimeVarianceMS = 0;
      overrideAdvance = false;
      orientParticles = false;
      orientOnVelocity = true;
      useEmitterSizes = false;
      useEmitterColors = false;
      particleString = NULL;
   }

   virtual ~ParticleEmitterData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream);



   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);
      
    fields.addField("ejectionPeriodMS", S32Field, Offset(ejectionPeriodMS, ParticleEmitterData));
    fields.addField("periodVarianceMS", S32Field, Offset(periodVarianceMS, ParticleEmitterData));

    fields.addField("ejectionVelocity", F32Field, Offset(ejectionVelocity, ParticleEmitterData));
    fields.addField("velocityVariance", F32Field, Offset(velocityVariance, ParticleEmitterData));
    fields.addField("ejectionOffset", F32Field, Offset(ejectionOffset, ParticleEmitterData));
    fields.addField("thetaMin", F32Field, Offset(thetaMin, ParticleEmitterData));
    fields.addField("thetaMax", F32Field, Offset(thetaMax, ParticleEmitterData));
    fields.addField("phiReferenceVel", F32Field, Offset(phiReferenceVel, ParticleEmitterData));
    fields.addField("phiVariance", F32Field, Offset(phiVariance, ParticleEmitterData));

    fields.addField("overrideAdvance", BoolField, Offset(overrideAdvance, ParticleEmitterData));
    fields.addField("orientParticles", BoolField, Offset(orientParticles, ParticleEmitterData));
    fields.addField("orientOnVelocity", BoolField, Offset(orientOnVelocity, ParticleEmitterData));
    fields.addField("particles", STStringField, Offset(particleString, ParticleEmitterData));
    fields.addField("lifetimeMS", S32Field, Offset(lifetimeMS, ParticleEmitterData));
    fields.addField("lifetimeVarianceMS", S32Field, Offset(lifetimeVarianceMS, ParticleEmitterData));
    fields.addField("useEmitterSizes", BoolField, Offset(useEmitterSizes, ParticleEmitterData));
    fields.addField("useEmitterColors", BoolField, Offset(useEmitterColors, ParticleEmitterData));

   }

   virtual const char* getClassName()
   {
    return "ParticleEmitterData";
   }
};


