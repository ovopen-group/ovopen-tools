/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

// OK
class ClothingData : public GameBaseData
{
typedef GameBaseData Parent;

public:
    Box3F mRenderBounds;
    StringTableEntry mShapeName;  // @ 0x68
    StringTableEntry mSkinFile;   // @ 0x6c
    Resource<TSShape> mShape;     // @ 0x70
    S32 mType;                    // @ 0x74
    StringTableEntry mIconName;   // @ 0x78
    S32 mTier;                    // @ 0x7c
    bool mDualSex;                // @ 0x80
    StringTableEntry mSex;        // @ 0x84
    bool mServer;                 // @ 0x88
    StringTableEntry mDispName;   // @ 0x8c
    StringTableEntry mDesc;       // @ 0x90
    U32 mCC;                      // @ 0x94
    U32 mPP;                      // @ 0x98
    S32 mRootSequence;            // @ 0x9c

   ClothingData()
   {
    mRenderBounds = Box3F();
    mShapeName = NULL;
    mSkinFile = NULL;
    mType = 0;
    mIconName = NULL;
    mTier = 0;
    mDualSex = false;
    mSex = NULL;
    mServer = false;
    mDispName = NULL;
    mDesc = NULL;
    mCC = 0;
    mPP = 0;
    mRootSequence = -1;
   }

   virtual ~ClothingData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);
      // CHECKED

      mShapeName = readStringTableString(stream);

      if (stream->readFlag()) {
        mSkinFile = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mIconName = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mDesc = readStringTableString(stream);
      }

      mType = stream->readInt(5);
      mTier = stream->readInt(3);
      mSex = readStringTableString(stream);
      mDualSex = stream->readFlag();
      mDispName = readStringTableString(stream);

      stream->read(&mCC);
      stream->read(&mPP);
    }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("shapeFile",    STStringField,       Offset(mShapeName, ClothingData));
      fields.addField("skinFile",    STStringField,       Offset(mSkinFile, ClothingData));
      fields.addField("iconFile",    STStringField,       Offset(mIconName, ClothingData));
      
      fields.addField("type",    S32Field,       Offset(mType, ClothingData));
      fields.addField("tier",    S32Field,       Offset(mTier, ClothingData));

      fields.addField("sex",    STStringField,       Offset(mSex, ClothingData));
      fields.addField("dualSex",    BoolField,       Offset(mDualSex, ClothingData));

      fields.addField("displayName",    STStringField,       Offset(mDispName, ClothingData));
      fields.addField("desc",    STStringField,       Offset(mDesc, ClothingData));

      fields.addField("CC",    U32Field,       Offset(mCC, ClothingData));
      fields.addField("PP",    U32Field,       Offset(mPP, ClothingData));
   }

   virtual const char* getClassName()
   {
    return "ClothingData";
   }
};
