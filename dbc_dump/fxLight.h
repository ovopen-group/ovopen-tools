/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

// OK
class fxLightData : public GameBaseData
{
typedef GameBaseData Parent;

public:
    StringTableEntry mFlareTextureName;
    bool mLightOn;
    F32 mRadius;
    F32 mBrightness;
    ColorF mColour;
    bool mFlareOn;
    bool mFlareTP;
    ColorF mFlareColour;
    bool mConstantSizeOn;
    F32 mConstantSize;
    F32 mNearSize;
    F32 mFarSize;
    F32 mNearDistance;
    F32 mFarDistance;
    F32 mFadeTime;
    U32 mBlendMode;
    bool mLinkFlare;
    bool mLinkFlareSize;
    ColorF mMinColour;
    ColorF mMaxColour;
    F32 mMinBrightness;
    F32 mMaxBrightness;
    F32 mMinRadius;
    F32 mMaxRadius;
    Point3F mStartOffset;
    Point3F mEndOffset;
    F32 mMinRotation;
    F32 mMaxRotation;
    bool mSingleColourKeys;
    StringTableEntry mRedKeys;
    StringTableEntry mGreenKeys;
    StringTableEntry mBlueKeys;
    StringTableEntry mBrightnessKeys;
    StringTableEntry mRadiusKeys;
    StringTableEntry mOffsetKeys;
    StringTableEntry mRotationKeys;
    F32 mColourTime;
    F32 mBrightnessTime;
    F32 mRadiusTime;
    F32 mOffsetTime;
    F32 mRotationTime;
    bool mLerpColour;
    bool mLerpBrightness;
    bool mLerpRadius;
    bool mLerpOffset;
    bool mLerpRotation;
    bool mUseColour;
    bool mUseBrightness;
    bool mUseRadius;
    bool mUseOffsets;
    bool mUseRotation;
    
   fxLightData()
   {
      // Datablock Light.
      mLightOn        = true;
      mRadius         = 10.0f;
      mBrightness       = 1.0f;
      mColour         .set(1,1,1);

      // Datablock Sun-Light.
      // Datablock Flare.
      mFlareOn        = false;
      mFlareTP        = true;
      mFlareTextureName   = SimpleStringTable::inst()->getSTEntry("");
      mConstantSizeOn     = false;
      mConstantSize     = 1.0f;
      mNearSize       = 3.0f;
      mFarSize        = 0.5f;
      mNearDistance     = 10.0f;
      mFarDistance      = 30.0f;
      mFadeTime       = 0.1f;
      mFlareColour      .set(1,1,1);
      mBlendMode        = 0;

      // Datablock Animation.
      mMinColour        .set(0,0,0);
      mMaxColour        .set(1,1,1);
      mMinBrightness      = 0.0f;
      mMaxBrightness      = 1.0f;
      mMinRadius        = 0.1f;
      mMaxRadius        = 20.0f;
      mStartOffset      .set(-5,0,0);
      mEndOffset        .set(+5,0,0);
      mMinRotation      = 0;
      mMaxRotation      = 359;
      mSingleColourKeys   = true;
      mRedKeys        = SimpleStringTable::inst()->getSTEntry("AZA");
      mGreenKeys        = SimpleStringTable::inst()->getSTEntry("AZA");
      mBlueKeys       = SimpleStringTable::inst()->getSTEntry("AZA");
      mBrightnessKeys     = SimpleStringTable::inst()->getSTEntry("AZA");
      mRadiusKeys       = SimpleStringTable::inst()->getSTEntry("AZA");
      mOffsetKeys       = SimpleStringTable::inst()->getSTEntry("AZA");
      mRotationKeys     = SimpleStringTable::inst()->getSTEntry("AZA");
      mColourTime       = 5.0f;
      mBrightnessTime     = 5.0f;
      mRadiusTime       = 5.0f;
      mOffsetTime       = 5.0f;
      mRotationTime     = 5.0f;
      mLerpColour       = true;
      mLerpBrightness     = true;
      mLerpRadius       = true;
      mLerpOffset       = true;
      mLerpRotation     = true;
      mUseColour        = false;
      mUseBrightness      = false;
      mUseRadius        = false;
      mUseOffsets       = false;
      mUseRotation      = false;
      mLinkFlare        = true;
      mLinkFlareSize      = false;
   }

   virtual ~fxLightData()
   {

   }
    virtual S32 shouldEnumerate(void* ptr, int elementIdx)
    {
      return true;
    }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      stream->read(&mLightOn);
      stream->read(&mRadius);
      stream->read(&mBrightness);
      stream->read(&mColour);

      mFlareTextureName = readStringTableString(stream);
      stream->read(&mFlareColour);
      stream->read(&mFlareOn);
      stream->read(&mFlareTP);
      stream->read(&mConstantSizeOn);
      stream->read(&mConstantSize);
      stream->read(&mNearSize);
      stream->read(&mFarSize);
      stream->read(&mNearDistance);
      stream->read(&mFarDistance);
      stream->read(&mFadeTime);
      stream->read(&mBlendMode);

      mUseColour        = stream->readFlag();
      mUseBrightness      = stream->readFlag();
      mUseRadius        = stream->readFlag();
      mUseOffsets       = stream->readFlag();
      mUseRotation      = stream->readFlag();
      mLinkFlare        = stream->readFlag();
      mLinkFlareSize      = stream->readFlag();

      stream->read(&mMinColour);
      stream->read(&mMaxColour);
      stream->read(&mMinBrightness);
      stream->read(&mMaxBrightness);
      stream->read(&mMinRadius);
      stream->read(&mMaxRadius);
      stream->read(&mStartOffset.x);
      stream->read(&mStartOffset.y);
      stream->read(&mStartOffset.z);
      stream->read(&mEndOffset.x);
      stream->read(&mEndOffset.y);
      stream->read(&mEndOffset.z);
      stream->read(&mMinRotation);
      stream->read(&mMaxRotation);

      mSingleColourKeys   = stream->readFlag();
      mRedKeys        = readStringTableString(stream);
      mGreenKeys        = readStringTableString(stream);
      mBlueKeys       = readStringTableString(stream);
      mBrightnessKeys     = readStringTableString(stream);
      mRadiusKeys       = readStringTableString(stream);
      mOffsetKeys       = readStringTableString(stream);
      mRotationKeys     = readStringTableString(stream);

      stream->read(&mColourTime);
      stream->read(&mBrightnessTime);
      stream->read(&mRadiusTime);
      stream->read(&mOffsetTime);
      stream->read(&mRotationTime);
      
      mLerpColour       = stream->readFlag();
      mLerpBrightness     = stream->readFlag();
      mLerpRadius       = stream->readFlag();
      mLerpOffset       = stream->readFlag();
      mLerpRotation     = stream->readFlag();
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("LightOn", BoolField, Offset(mLightOn, fxLightData));
    fields.addField("Radius", F32Field, Offset(mRadius, fxLightData));
    fields.addField("Brightness", F32Field, Offset(mBrightness, fxLightData));
    fields.addField("Colour", ColorFField, Offset(mColour, fxLightData));

    fields.addField("FlareOn", BoolField, Offset(mFlareOn, fxLightData));
    fields.addField("FlareTP", BoolField, Offset(mFlareTP, fxLightData));
    fields.addField("FlareBitmap", STStringField, Offset(mFlareTextureName, fxLightData));
    fields.addField("FlareColour", ColorFField, Offset(mFlareColour, fxLightData));
    fields.addField("ConstantSizeOn", BoolField, Offset(mConstantSizeOn, fxLightData));
    fields.addField("ConstantSize", F32Field, Offset(mConstantSize, fxLightData));
    fields.addField("NearSize", F32Field, Offset(mNearSize, fxLightData));
    fields.addField("FarSize", F32Field, Offset(mFarSize, fxLightData));
    fields.addField("NearDistance", F32Field, Offset(mNearDistance, fxLightData));
    fields.addField("FarDistance", F32Field, Offset(mFarDistance, fxLightData));
    fields.addField("FadeTime", F32Field, Offset(mFadeTime, fxLightData));
    fields.addField("BlendMode", S32Field, Offset(mBlendMode, fxLightData));

    fields.addField("AnimColour", BoolField, Offset(mUseColour, fxLightData));
    fields.addField("AnimBrightness", BoolField, Offset(mUseBrightness, fxLightData));
    fields.addField("AnimRadius", BoolField, Offset(mUseRadius, fxLightData));
    fields.addField("AnimOffsets", BoolField, Offset(mUseOffsets, fxLightData));
    fields.addField("AnimRotation", BoolField, Offset(mUseRotation, fxLightData));
    fields.addField("LinkFlare", BoolField, Offset(mLinkFlare, fxLightData));
    fields.addField("LinkFlareSize", BoolField, Offset(mLinkFlareSize, fxLightData));
    fields.addField("MinColour", ColorFField, Offset(mMinColour, fxLightData));
    fields.addField("MaxColour", ColorFField, Offset(mMaxColour, fxLightData));
    fields.addField("MinBrightness", F32Field, Offset(mMinBrightness, fxLightData));
    fields.addField("MaxBrightness", F32Field, Offset(mMaxBrightness, fxLightData));
    fields.addField("MinRadius", F32Field, Offset(mMinRadius, fxLightData));
    fields.addField("MaxRadius", F32Field, Offset(mMaxRadius, fxLightData));

    fields.addField("StartOffset", Point3FField, Offset(mStartOffset, fxLightData));
    fields.addField("EndOffset", Point3FField, Offset(mEndOffset, fxLightData));
    fields.addField("MinRotation", F32Field,Offset(mMinRotation, fxLightData));
    fields.addField("MaxRotation", F32Field,Offset(mMaxRotation, fxLightData));
    fields.addField("SingleColourKeys", BoolField, Offset(mSingleColourKeys, fxLightData));

    fields.addField("RedKeys", STStringField, Offset(mRedKeys, fxLightData));
    fields.addField("GreenKeys", STStringField, Offset(mGreenKeys, fxLightData));
    fields.addField("BlueKeys", STStringField, Offset(mBlueKeys, fxLightData));
    fields.addField("BrightnessKeys", STStringField, Offset(mBrightnessKeys, fxLightData));
    fields.addField("RadiusKeys", STStringField, Offset(mRadiusKeys, fxLightData));
    fields.addField("OffsetKeys", STStringField, Offset(mOffsetKeys, fxLightData));
    fields.addField("RotationKeys", STStringField, Offset(mRotationKeys, fxLightData));
    fields.addField("ColourTime", F32Field, Offset(mColourTime, fxLightData));
    fields.addField("BrightnessTime", F32Field, Offset(mBrightnessTime, fxLightData));
    fields.addField("RadiusTime", F32Field, Offset(mRadiusTime, fxLightData));
    fields.addField("OffsetTime", F32Field, Offset(mOffsetTime, fxLightData));
    fields.addField("RotationTime", F32Field, Offset(mRotationTime, fxLightData));
    fields.addField("LerpColour", BoolField, Offset(mLerpColour, fxLightData));
    fields.addField("LerpBrightness", BoolField, Offset(mLerpBrightness, fxLightData));
    fields.addField("LerpRadius", BoolField, Offset(mLerpRadius, fxLightData));
    fields.addField("LerpOffset", BoolField, Offset(mLerpOffset, fxLightData));
    fields.addField("LerpRotation", BoolField, Offset(mLerpRotation, fxLightData));
  };

   virtual const char* getClassName()
   {
    return "fxLightData";
   }
};
