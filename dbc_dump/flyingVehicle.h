/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "vehicle.h"

class AudioProfile;
class ParticleEmitterData;

// OK
class FlyingVehicleData : public VehicleData
{
typedef VehicleData Parent;

public:

     enum Sounds {
      JetSound,
      EngineSound,
      MaxSounds
   };

   enum Jets {
      ForwardJetEmitter,
      BackwardJetEmitter,
      DownwardJetEmitter,
      TrailEmitter,
      MaxJetEmitters
   };

    SimWeakRef<AudioProfile> sound[MaxSounds];
    SimWeakRef<ParticleEmitterData> jetEmitter[MaxJetEmitters];
    F32 minTrailSpeed;     // @ 0x37c
    F32 maneuveringForce;  // @ 0x380
    F32 steeringRollForce; // @ 0x384
    F32 rotationalDrag;    // @ 0x388
    F32 airFoilSurfArea;   // @ 0x38c
    F32 dragSurfaceArea;   // @ 0x390
    //ClippedPolyList rigidBody;
    S32 surfaceCount;
    F32 maxSpeed;
    S32 jetNode[10];

   FlyingVehicleData()
   {
     maneuveringForce = 0;
     //horizontalSurfaceForce = 0;
     //verticalSurfaceForce = 0;
     //autoInputDamping = 1;
     //steeringForce = 1;
     steeringRollForce = 1;
     //rollForce = 1;
     //autoAngularForce = 0;
     rotationalDrag = -0.00000000000231202796;
     airFoilSurfArea = 1;
     dragSurfaceArea = 1;
     //autoLinearForce = 0;
     //maxAutoSpeed = 0;
     //hoverHeight = 2;
     //createHoverHeight = 2;
     maxSteeringAngle = M_PI;
     minTrailSpeed = 1;
     maxSpeed = 100;

     for (S32 k = 0; k < 10; k++)
        jetNode[k] = -1;

     for (S32 j = 0; j < MaxJetEmitters; j++)
        jetEmitter[j] = 0;

     for (S32 i = 0; i < MaxSounds; i++)
        sound[i] = 0;

     //vertThrustMultiple = 1.0;
   }

   virtual ~FlyingVehicleData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

     for (S32 i = 0; i < MaxSounds; i++) {
        sound[i] = 0;
        if (stream->readFlag())
        {
           sound[i] = stream->readRangedU32(DataBlockObjectIdFirst,
                                                           DataBlockObjectIdLast);
        }
     }

     for (S32 j = 0; j < MaxJetEmitters; j++) {
        jetEmitter[j] = 0;
        if (stream->readFlag())
        {
           jetEmitter[j] = stream->readRangedU32(DataBlockObjectIdFirst,
                                                                       DataBlockObjectIdLast);
        }
     }

     // NOTE: seems simplified from TGE...
     stream->read(&maneuveringForce);
     stream->read(&steeringRollForce);
     stream->read(&rotationalDrag);
     stream->read(&minTrailSpeed);
     stream->read(&airFoilSurfArea);
     stream->read(&dragSurfaceArea);
   }

   virtual const char* getClassName()
   {
    return "FlyingVehicleData";
   }
};
