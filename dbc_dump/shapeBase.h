/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"
#include "ts/tsShape.h"

class AudioProfile;
class ExplosionData;
class ParticleEmitterData;
class ProjectileData;

DECLARE_ENUM_TYPE(ShapeBaseImageData_LightTypeField);
DECLARE_ENUM_TYPE(ShapeBaseImageData_LoadedStateField);
DECLARE_ENUM_TYPE(ShapeBaseImageData_SpinStateField);
DECLARE_ENUM_TYPE(ShapeBaseImageData_RecoilStateField);

#define Player_GenericShadowLevel 0.4f
#define Player_NoShadowLevel 0.01f
#define Vehicle_GenericShadowLevel 0.7f
#define Vehicle_NoShadowLevel 0.2f
#define Item_GenericShadowLevel 0.4f
#define Item_NoShadowLevel 0.01f
#define StaticShape_GenericShadowLevel 0.7f
#define StaticShape_NoShadowLevel 0.2f
#define RigidShape_GenericShadowLevel 0.7f
#define RigidShape_NoShadowLevel 0.2f


// OK
class SplashData : public GameBaseData
{
typedef GameBaseData Parent;

public:

   enum Constants
   {
      NUM_EMITTERS = 3,
      NUM_TIME_KEYS = 4,
      NUM_TEX = 2,
   };

    AudioProfile *soundProfile;
    S32 soundProfileId;
    ParticleEmitterData *emitterList[NUM_EMITTERS];
    S32 emitterIDList[NUM_EMITTERS];
    S32 delayMS;
    S32 delayVariance;
    S32 lifetimeMS;
    S32 lifetimeVariance;
    Point3F scale;
    F32 width;
    F32 height;
    U32 numSegments;
    F32 velocity;
    F32 acceleration;
    F32 texWrap;
    F32 texFactor;
    F32 ejectionFreq;
    F32 ejectionAngle;
    F32 ringLifetime;
    F32 startRadius;
    F32 times[NUM_TIME_KEYS];
    ColorF colors[NUM_TIME_KEYS];
    StringTableEntry textureName[NUM_TEX];
    //TextureHandle textureHandle[NUM_TEX];
    ExplosionData *explosion;
    S32 explosionId;


   SplashData()
   {
      soundProfile = NULL;
      soundProfileId = 0;
      memset(emitterList, '\0', sizeof(emitterList));
      memset(emitterIDList, '\0', sizeof(emitterIDList));
      delayMS = 0;
      delayVariance = 0;
      lifetimeMS = 1000;
      lifetimeVariance = 0;
      scale = Point3F(1,1,1);
      width = 4;
      height = 0;
      numSegments = 10;
      velocity = 5;
      acceleration = 0;
      texWrap = 1;
      texFactor = 3;
      ejectionFreq = 5;
      ejectionAngle = 45;
      ringLifetime = 1;
      startRadius = 0.5;
      times[0] = 0;
      times[1] = 1;
      times[2] = 1;
      times[3] = 1;
      for (int i=0; i<4; i++) { colors[i] = ColorF(1,1,1,1); }
      memset(textureName, '\0', sizeof(textureName));
      explosion = NULL;
      explosionId = 0;
   }

   virtual ~SplashData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);

     mathRead(*stream, &scale);
     stream->read(&delayMS);
     stream->read(&delayVariance);
     stream->read(&lifetimeMS);
     stream->read(&lifetimeVariance);
     stream->read(&width);
     stream->read(&numSegments);
     stream->read(&velocity);
     stream->read(&height);
     stream->read(&acceleration);
     stream->read(&texWrap);
     stream->read(&texFactor);
     stream->read(&ejectionFreq);
     stream->read(&ejectionAngle);
     stream->read(&ringLifetime);
     stream->read(&startRadius);

     if( stream->readFlag() )
     {
        explosionId = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
     }

     U32 i;
     for( i=0; i<NUM_EMITTERS; i++ )
     {
        if( stream->readFlag() )
        {
           emitterIDList[i] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
        }
     }

     for( i=0; i<NUM_TIME_KEYS; i++ )
     {
        stream->read( &colors[i] );
     }

     for( i=0; i<NUM_TIME_KEYS; i++ )
     {
        stream->read( &times[i] );
     }

     for( i=0; i<NUM_TEX; i++ )
     {
        textureName[i] = readStringTableString(stream);
     }
   }

   virtual const char* getClassName()
   {
    return "SplashData";
   }
};

class ExplosionData;
class ParticleEmitterData;
class DebrisData;

// OK
class ShapeBaseData : public GameBaseData
{
typedef GameBaseData Parent;

public:

  enum
  {
    NumHudRenderImages = 8,
    NumMountPoints = 32
  };

    bool shadowEnable; // @ 0x50
    bool shadowCanMove; // @ 0x51
    bool shadowCanAnimate; // @ 0x52
    StringTableEntry shapeName; // @ 0x54
    StringTableEntry cloakTexName;// @ 0x58
    DebrisData *debris;
    S32 debrisID; // @ 0x60
    StringTableEntry debrisShapeName; // @ 0x64
    Resource<TSShape> debrisShape;
    ExplosionData *explosion;
    S32 explosionID; // @ 0x70
    ExplosionData *underwaterExplosion; // @ 0x74
    S32 underwaterExplosionID; // @ 0x78
    ParticleEmitterData *damageEmitter; // + OV
    S32 damageEmitterID; // @ 0x80         + OV
    F32 mass; // @ 0x84
    F32 drag; // @ 0x88
    F32 density; // @ 0x8c
    F32 maxEnergy; // @ 0x90
    F32 maxDamage; // @ 0x94
    F32 maxArmor; // @ 0x98
    F32 repairRate;
    F32 disabledLevel;
    F32 destroyedLevel;
    S32 shieldEffectLifetimeMS;
    F32 cameraMaxDist; // @ 0xac
    F32 cameraMinDist; // @ 0xb0
    F32 cameraDefaultFov; // @ 0xb4
    F32 cameraMinFov; // @ 0xb8
    F32 cameraMaxFov; // @ 0xbc
    //Resource<TSShape> shape; // @ 0xc0
    DTShape::TSShapeRef shape;

    U32 mCRC; // @ 0xc4
    bool computeCRC; //   @ 0xc8
    S32 eyeNode;     // @ 0xcc
    S32 cameraNode;  // @ 0xd0
    S32 targetNode;  // @ 0xd4 OV  not read
    S32 fpgunNode;   // @ 0xd8 OV  not read
    S32 shadowNode;  // @ 0xdc OV  not read
    S32 mountPointNode[NumMountPoints];
    S32 debrisDetail;
    S32 damageSequence;
    S32 hulkSequence;
    bool canControl; // @ 0x16c
    bool canObserve;
    bool observeThroughObject;
    StringTableEntry hudImageNameFriendly[NumHudRenderImages];
    StringTableEntry hudImageNameEnemy[NumHudRenderImages];
    TextureHandle hudImageFriendly[NumHudRenderImages];
    TextureHandle hudImageEnemy[NumHudRenderImages];
    bool hudRenderCenter[NumHudRenderImages];
    bool hudRenderModulated[NumHudRenderImages];
    bool hudRenderAlways[NumHudRenderImages];
    bool hudRenderDistance[NumHudRenderImages];
    bool hudRenderName[NumHudRenderImages];
    Vector<S32> collisionDetails;
    Vector<Box3F> collisionBounds;
    Vector<S32> LOSDetails;
    F32 genericShadowLevel;
    F32 noShadowLevel;

    bool emap; // @ 0x244
    bool planeSort; // @ 0x245    OV
    bool firstPersonOnly; // @ 0x246
    bool useEyePoint; // @ 0x247
    bool aiAvoidThis; // @ 0x248
    bool isInvincible; // @ 0x249
    bool renderWhenDestroyed; // @ 0x24a
    bool inheritEnergyFromMount; // @ 0x24b

   ShapeBaseData()
   {
     shadowEnable = false;
     shadowCanMove = false;
     shadowCanAnimate = false;
     
     shapeName = "";
     cloakTexName = "";
     mass = 1;
     drag = 0;
     density = 1;
     maxEnergy = 0;
     maxDamage = 1.0;
     maxArmor = 0;
     disabledLevel = 1.0;
     destroyedLevel = 1.0;
     repairRate = 0.0033;
     eyeNode = -1;
     shadowNode = -1;
     cameraNode = -1;
     targetNode = -1;
     fpgunNode = -1;
     damageSequence = -1;
     hulkSequence = -1;
     cameraMaxDist = 15; // checked
     cameraMinDist = 0; // checked
     cameraDefaultFov = 90.f;
     cameraMinFov = 5.f;
     cameraMaxFov = 120.f;
     emap = false;
     aiAvoidThis = false;
     isInvincible = false;
     renderWhenDestroyed = true;
     debris = 0;
     debrisID = 0;
     debrisShapeName = NULL;
     explosion = 0;
     explosionID = 0;
     underwaterExplosion = 0;
     underwaterExplosionID = 0;
     firstPersonOnly = false;
     useEyePoint = false;

     explosionID = 0;
     underwaterExplosionID = 0;
     damageEmitterID = 0;
     damageEmitter = 0;
     debrisID = 0;
     debrisShapeName = "";
     explosion = 0;

     observeThroughObject = false;
     computeCRC = false;

     genericShadowLevel = 2.0f;
     noShadowLevel = 2.0f;

     emap = false;
     planeSort = false;
     firstPersonOnly = false;
     useEyePoint = false;
     aiAvoidThis = false;
     isInvincible = false;
     renderWhenDestroyed = true;

     inheritEnergyFromMount = false;

     debrisDetail = -1;
     damageSequence = -1;
     hulkSequence = -1;
     canControl = true;
     canObserve = true;

     for (U32 i=0; i<NumMountPoints; i++)
     {
      mountPointNode[i] = -1;
     }

     for(U32 j = 0; j < NumHudRenderImages; j++)
     {
        hudImageNameFriendly[j] = 0;
        hudImageNameEnemy[j] = 0;
        hudRenderCenter[j] = false;
        hudRenderModulated[j] = false;
        hudRenderAlways[j] = false;
        hudRenderDistance[j] = false;
        hudRenderName[j] = false;
     }
   }

   virtual ~ShapeBaseData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream);

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("shadowEnable", BoolField, Offset(shadowEnable, ShapeBaseData));
    fields.addField("shadowCanMove", BoolField, Offset(shadowCanMove, ShapeBaseData));
    fields.addField("shadowCanAnimate", BoolField, Offset(shadowCanAnimate, ShapeBaseData));

    fields.addField("shapeFile", STStringField, Offset(shapeName, ShapeBaseData));
    fields.addField("cloakTexture", STStringField, Offset(cloakTexName, ShapeBaseData));
    fields.addField("emap", BoolField, Offset(emap, ShapeBaseData));
    fields.addField("planesort", BoolField, Offset(planeSort, ShapeBaseData));

    fields.addField("explosion", SimWeakRefField, Offset(explosionID, ShapeBaseData));
    fields.addField("underwaterExplosion", SimWeakRefField, Offset(underwaterExplosionID, ShapeBaseData));
    fields.addField("debris", SimWeakRefField, Offset(debrisID, ShapeBaseData));
    fields.addField("renderWhenDestroyed", BoolField, Offset(renderWhenDestroyed, ShapeBaseData));
    fields.addField("debrisShapeName", STStringField, Offset(debrisShapeName, ShapeBaseData));
    fields.addField("damageEmitter", SimWeakRefField, Offset(damageEmitterID, ShapeBaseData));


    fields.addField("mass", F32Field, Offset(mass, ShapeBaseData));
    fields.addField("drag", F32Field, Offset(drag, ShapeBaseData));
    fields.addField("density", F32Field, Offset(density, ShapeBaseData));

    fields.addField("maxEnergy", F32Field, Offset(maxEnergy, ShapeBaseData));
    fields.addField("maxDamage", F32Field, Offset(maxDamage, ShapeBaseData));
    fields.addField("maxArmor", F32Field, Offset(maxArmor, ShapeBaseData));
    fields.addField("disabledLevel", F32Field, Offset(disabledLevel, ShapeBaseData));
    fields.addField("destroyedLevel", F32Field, Offset(destroyedLevel, ShapeBaseData));
    fields.addField("repairRate", F32Field, Offset(repairRate, ShapeBaseData));
    fields.addField("inheritEnergyFromMount", BoolField, Offset(inheritEnergyFromMount, ShapeBaseData));
    fields.addField("isInvincible", BoolField, Offset(isInvincible, ShapeBaseData));

    fields.addField("cameraMaxDist", F32Field, Offset(cameraMaxDist, ShapeBaseData));
    fields.addField("cameraMinDist", F32Field, Offset(cameraMinDist, ShapeBaseData));
    fields.addField("cameraDefaultFov", F32Field, Offset(cameraDefaultFov, ShapeBaseData));
    fields.addField("cameraMinFov", F32Field, Offset(cameraMinFov, ShapeBaseData));
    fields.addField("cameraMaxFov", F32Field, Offset(cameraMaxFov, ShapeBaseData));
    fields.addField("firstPersonOnly", BoolField, Offset(firstPersonOnly, ShapeBaseData));
    fields.addField("useEyePoint", BoolField, Offset(useEyePoint, ShapeBaseData));
    fields.addField("observeThroughObject", BoolField, Offset(observeThroughObject, ShapeBaseData));

    fields.addField("hudImageName", STStringField, Offset(hudImageNameFriendly, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudImageNameFriendly", STStringField, Offset(hudImageNameFriendly, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudImageNameEnemy", STStringField, Offset(hudImageNameEnemy, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudRenderCenter", BoolField, Offset(hudRenderCenter, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudRenderModulated", BoolField, Offset(hudRenderModulated, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudRenderAlways", BoolField, Offset(hudRenderAlways, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudRenderDistance", BoolField, Offset(hudRenderDistance, ShapeBaseData), NumHudRenderImages);
    fields.addField("hudRenderName", BoolField, Offset(hudRenderName, ShapeBaseData), NumHudRenderImages);
    
    fields.addField("aiAvoidThis", BoolField, Offset(aiAvoidThis, ShapeBaseData));
    fields.addField("computeCRC", BoolField, Offset(computeCRC, ShapeBaseData));
   }

   virtual const char* getClassName()
   {
    return "ShapeBaseData";
   }
};


// OK
class ShapeBaseImageData : public GameBaseData
{
typedef GameBaseData Parent;

public:

   enum Constants {
      MaxStates    = 31,            // 
                                    // 
      NumStateBits = 5,
   };
   enum LightType {
      NoLight = 0,
      ConstantLight,
      PulsingLight,
      WeaponFireLight,
      NumLightTypes
   };
   struct StateData {
      StateData()
      {
         name = 0;
         transition.loaded[0] = transition.loaded[1] = -1;
         transition.ammo[0] = transition.ammo[1] = -1;
         transition.target[0] = transition.target[1] = -1;
         transition.trigger[0] = transition.trigger[1] = -1;
         transition.wet[0] = transition.wet[1] = -1;
         transition.timeout = -1;
         waitForTimeout = true;
         timeoutValue = 0;
         fire = false;
         energyDrain = 0;
         allowImageChange = true;
         loaded = IgnoreLoaded;
         spin = IgnoreSpin;
         recoil = NoRecoil;
         flashSequence = false;
         sequence = -1;
         sequenceVis = -1;
         sound = 0;
         emitter = 0;
         emitterNode = 0;
         emitterTime = 0; // NOTE: Don't think these emitter* things are actually set
         script = 0;
         ignoreLoadedForReady = false;
      }

      const char* name;             // 

      struct Transition {
         S32 loaded[2];             // 
         S32 ammo[2];               // 
         S32 target[2];             // 
         S32 trigger[2];            // 
         S32 wet[2];                // 
         S32 timeout;               // 
      } transition;
      bool ignoreLoadedForReady;

      bool fire;                    // 
      bool ejectShell;              // 
      bool allowImageChange;        // 
                                    //
                                    // 
                                    // 
                                    // 
                                    // 
      bool scaleAnimation;          // 
      bool direction;               // 
      bool waitForTimeout;          // 
                                    // 
      F32 timeoutValue;             // 
                                    // 
      F32 energyDrain;              // 
                                    //
                                    // 
                                    // 
      enum LoadedState {
         IgnoreLoaded,              // 
         Loaded,                    // 
         NotLoaded,                 // 
         NumLoadedBits = 3          // 
      } loaded;                     // 
      enum SpinState {
         IgnoreSpin,                // 
         NoSpin,                    // 
         SpinUp,                    // 
         SpinDown,                  // 
         FullSpin,                  // 
         NumSpinBits = 3            // 
      } spin;                       // 
      enum RecoilState {
         NoRecoil,
         LightRecoil,
         MediumRecoil,
         HeavyRecoil,
         NumRecoilBits = 3
      } recoil;                     // 
                                    //
                                    // 
                                    // 
                                    // 
                                    // 
      bool flashSequence;           // 
                                    //
                                    // 
                                    // 
      S32 sequence;                 // 
                                    //
                                    //
      S32 sequenceVis;              // 
      const char* script;           // 
                                    // 
      SimWeakRef<ParticleEmitterData> emitter; // 
                                    // 
      SimWeakRef<AudioProfile> sound;
      F32 emitterTime;              //<
      S32 emitterNode;
   };

   bool mIsBase; // OV   @ 0x50

   const char*             fireStateName; // @ 0x54

   const char*             stateName                  [MaxStates];

   const char*             stateTransitionLoaded      [MaxStates];
   const char*             stateTransitionNotLoaded   [MaxStates];
   const char*             stateTransitionAmmo        [MaxStates];
   const char*             stateTransitionNoAmmo      [MaxStates];
   const char*             stateTransitionTarget      [MaxStates];
   const char*             stateTransitionNoTarget    [MaxStates];
   const char*             stateTransitionWet         [MaxStates];
   const char*             stateTransitionNotWet      [MaxStates];
   const char*             stateTransitionTriggerUp   [MaxStates];
   const char*             stateTransitionTriggerDown [MaxStates];
   const char*             stateTransitionTimeout     [MaxStates];
   F32                     stateTimeoutValue          [MaxStates];
   bool                    stateWaitForTimeout        [MaxStates];

   bool                    stateFire                  [MaxStates];
   bool                    stateEjectShell            [MaxStates];
   F32                     stateEnergyDrain           [MaxStates];
   bool                    stateAllowImageChange      [MaxStates];
   bool                    stateScaleAnimation        [MaxStates];
   bool                    stateDirection             [MaxStates];
   StateData::LoadedState  stateLoaded                [MaxStates];
   StateData::SpinState    stateSpin                  [MaxStates];
   StateData::RecoilState  stateRecoil                [MaxStates];
   const char*             stateSequence              [MaxStates];
   bool                    stateSequenceRandomFlash   [MaxStates];
   bool                    stateIgnoreLoadedForReady  [MaxStates];

   SimWeakRef<AudioProfile>           stateSound                 [MaxStates];
   const char*             stateScript                [MaxStates];

   SimWeakRef<ParticleEmitterData>    stateEmitter               [MaxStates];
   F32                     stateEmitterTime           [MaxStates];
   const char*             stateEmitterNode           [MaxStates];
   
    bool emap;
    bool correctMuzzleVector;
    bool firstPerson;
    bool useEyeOffset;
    StringTableEntry shapeName;
    U32 mountPoint;
    MatrixF mountOffset;
    MatrixF eyeOffset;
    SimWeakRef<ProjectileData> projectile;
    F32 mass;
    bool usesEnergy;
    F32 minEnergy;
    bool accuFire;
    bool cloakable;
    bool useReticle; // OV @ 0xd1a
    bool allowZoom;  // OV @ 0xd1b
    S32 toolIdx;     // OV @ 0xd1c
    F32 moveAimOff;  // OV @ 0xd20
    F32 standAimOff; // OV @ 0xd24
    S32 lightType;
    ColorF lightColor;
    S32 lightTime;
    F32 lightRadius;
    //Resource<TSShape> shape;
    DTShape::TSShapeRef shape;
    U32 mCRC;
    bool computeCRC;
    MatrixF mountTransform;
    S32 retractNode;
    S32 muzzleNode;
    S32 ejectNode;
    S32 emitterNode;
    S32 spinSequence;
    S32 ambientSequence;
    bool isAnimated;
    bool hasFlash;
    S32 fireState;
    SimWeakRef<DebrisData> casing;
    S32 casingID;
    Point3F shellExitDir;
    F32 shellExitVariance;
    F32 shellVelocity;


   StateData state[MaxStates];   // 
   bool      statesLoaded;       // 


   ShapeBaseImageData()
   {
       emap = false;
       mIsBase = false;
       fireStateName = NULL; // TODO: is this used?

        useReticle = false;
        allowZoom = false;
        toolIdx = 0;     // verified
        moveAimOff = 0;  // verified
        standAimOff = 0; // verified

        retractNode = 0;
        muzzleNode = 0;
        ejectNode = 0;
        emitterNode = 0;
        spinSequence = 0;
        ambientSequence = 0;
        isAnimated = false;
        hasFlash = false;


       mountPoint = 0;
       mountOffset.identity();
       eyeOffset.identity();
       correctMuzzleVector = true;
       firstPerson = true;
       useEyeOffset = false;
       mass = 0;

       usesEnergy = false;
       minEnergy = 2;
       accuFire = false;

       projectile = 0;

       cloakable = true;

       lightType = ShapeBaseImageData::NoLight;
       lightColor.set(1.f,1.f,1.f,1.f);
       lightTime = 1000;
       lightRadius = 10.f;

       mountTransform.identity();
       shapeName = "";
       fireState = -1;
       computeCRC = false;

       //
       for (int i = 0; i < MaxStates; i++) {
          stateName[i] = 0;
          stateTransitionLoaded[i] = 0;
          stateTransitionNotLoaded[i] = 0;
          stateTransitionAmmo[i] = 0;
          stateTransitionNoAmmo[i] = 0;
          stateTransitionTarget[i] = 0;
          stateTransitionNoTarget[i] = 0;
          stateTransitionWet[i] = 0;
          stateTransitionNotWet[i] = 0;
          stateTransitionTriggerUp[i] = 0;
          stateTransitionTriggerDown[i] = 0;
          stateTransitionTimeout[i] = 0;
          stateWaitForTimeout[i] = true;
          stateTimeoutValue[i] = 0;
          stateFire[i] = false;
          stateEjectShell[i] = false;
          stateEnergyDrain[i] = 0;
          stateAllowImageChange[i] = true;
          stateScaleAnimation[i] = true;
          stateDirection[i] = true;
          stateLoaded[i] = StateData::IgnoreLoaded;
          stateSpin[i] = StateData::IgnoreSpin;
          stateRecoil[i] = StateData::NoRecoil;
          stateSequence[i] = 0;
          stateSequenceRandomFlash[i] = false;
          stateSound[i] = 0;
          stateScript[i] = 0;
          stateEmitter[i] = 0;
          stateEmitterTime[i] = 0;
          stateEmitterNode[i] = 0;
          stateIgnoreLoadedForReady[i] = false;
       }
       statesLoaded = false;

       casing = 0;
       casingID = 0;
       shellExitDir.set( 1.0, 0.0, 1.0 );
       shellExitDir.normalize();
       shellExitVariance = 20.0;
       shellVelocity = 1.0;
   }

   virtual ~ShapeBaseImageData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream);


   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("isBase", BoolField, Offset(mIsBase, ShapeBaseImageData));
    fields.addField("emap", BoolField, Offset(emap, ShapeBaseImageData));
    fields.addField("shapeFile", STStringField, Offset(shapeName, ShapeBaseImageData));
    fields.addField("projectile", SimWeakRefField, Offset(projectile, ShapeBaseImageData));
    fields.addField("cloakable", BoolField, Offset(cloakable, ShapeBaseImageData));
    fields.addField("useReticle", BoolField, Offset(useReticle, ShapeBaseImageData));
    fields.addField("allowZoom", BoolField, Offset(allowZoom, ShapeBaseImageData));
    fields.addField("toolIdx", S32Field, Offset(toolIdx, ShapeBaseImageData));
    fields.addField("moveAimOff", F32Field, Offset(moveAimOff, ShapeBaseImageData));
    fields.addField("standAimOff", F32Field, Offset(standAimOff, ShapeBaseImageData));
    fields.addField("mountPoint", S32Field, Offset(mountPoint, ShapeBaseImageData));

    fields.addField("offset", MatrixPositionField, Offset(mountOffset, ShapeBaseImageData));
    fields.addField("rotation", MatrixRotationField, Offset(mountOffset, ShapeBaseImageData));

    fields.addField("eyeOffset", MatrixPositionField, Offset(eyeOffset, ShapeBaseImageData));
    fields.addField("eyeRotation", MatrixRotationField, Offset(eyeOffset, ShapeBaseImageData));
    
    fields.addField("correctMuzzleVector", BoolField, Offset(correctMuzzleVector, ShapeBaseImageData));
    
    fields.addField("firstPerson", BoolField, Offset(firstPerson, ShapeBaseImageData));
    fields.addField("mass", F32Field, Offset(mass, ShapeBaseImageData));
    fields.addField("usesEnergy", BoolField, Offset(usesEnergy, ShapeBaseImageData));
    fields.addField("minEnergy", F32Field, Offset(minEnergy, ShapeBaseImageData));
    fields.addField("accuFire", BoolField, Offset(accuFire, ShapeBaseImageData));

    fields.addField("lightType", ShapeBaseImageData_LightTypeField, Offset(lightType, ShapeBaseImageData));

    fields.addField("lightColor", ColorFField, Offset(lightColor, ShapeBaseImageData));
    fields.addField("lightTime", S32Field, Offset(lightTime, ShapeBaseImageData));
    fields.addField("lightRadius", F32Field, Offset(lightRadius, ShapeBaseImageData));

    fields.addField("casing", SimWeakRefField, Offset(casingID, ShapeBaseImageData));
    fields.addField("shellExitDir", Point3FField, Offset(shellExitDir, ShapeBaseImageData));
    fields.addField("shellExitVariance", F32Field, Offset(shellExitVariance, ShapeBaseImageData));
    fields.addField("shellVelocity", F32Field, Offset(shellVelocity, ShapeBaseImageData));



    fields.addField("stateName", STStringField, Offset(stateName, ShapeBaseImageData), MaxStates);

    fields.addField("stateTransitionOnLoaded", STStringField, Offset(stateTransitionLoaded, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnNotLoaded", STStringField, Offset(stateTransitionNotLoaded, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnAmmo", STStringField, Offset(stateTransitionAmmo, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnNoAmmo", STStringField, Offset(stateTransitionNoAmmo, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnTarget", STStringField, Offset(stateTransitionTarget, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnNoTarget", STStringField, Offset(stateTransitionNoTarget, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnWet", STStringField, Offset(stateTransitionWet, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnNotWet", STStringField, Offset(stateTransitionNotWet, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnTriggerUp", STStringField, Offset(stateTransitionTriggerUp, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnTriggerDown", STStringField, Offset(stateTransitionTriggerDown, ShapeBaseImageData), MaxStates);
    fields.addField("stateTransitionOnTimeout", STStringField, Offset(stateTransitionTimeout, ShapeBaseImageData), MaxStates);
    
    fields.addField("stateTimeoutValue", F32Field, Offset(stateTimeoutValue, ShapeBaseImageData), MaxStates);
    fields.addField("stateWaitForTimeout", BoolField, Offset(stateWaitForTimeout, ShapeBaseImageData), MaxStates);
    fields.addField("stateFire", BoolField, Offset(stateFire, ShapeBaseImageData), MaxStates);
    fields.addField("stateEjectShell", BoolField, Offset(stateEjectShell, ShapeBaseImageData), MaxStates);
    fields.addField("stateEnergyDrain", F32Field, Offset(stateEnergyDrain, ShapeBaseImageData), MaxStates);
    fields.addField("stateAllowImageChange", BoolField, Offset(stateAllowImageChange, ShapeBaseImageData), MaxStates);
    fields.addField("stateDirection", BoolField, Offset(stateDirection, ShapeBaseImageData), MaxStates);

    fields.addField("stateLoadedFlag", ShapeBaseImageData_LoadedStateField, Offset(stateLoaded, ShapeBaseImageData), MaxStates);
    fields.addField("stateSpinThread", ShapeBaseImageData_SpinStateField, Offset(stateSpin, ShapeBaseImageData), MaxStates);
    fields.addField("stateRecoil", ShapeBaseImageData_RecoilStateField, Offset(stateRecoil, ShapeBaseImageData), MaxStates);

    fields.addField("stateSequence", STStringField, Offset(stateSequence, ShapeBaseImageData), MaxStates);
    fields.addField("stateSequenceRandomFlash", BoolField, Offset(stateSequenceRandomFlash, ShapeBaseImageData), MaxStates);
    fields.addField("stateSound", SimWeakRefField, Offset(stateSound, ShapeBaseImageData), MaxStates);
    fields.addField("stateScript", STStringField, Offset(stateScript, ShapeBaseImageData), MaxStates);
    fields.addField("stateEmitter", SimWeakRefField, Offset(stateEmitter, ShapeBaseImageData), MaxStates);
    fields.addField("stateEmitterTime", F32Field, Offset(stateEmitterTime, ShapeBaseImageData), MaxStates);
    fields.addField("stateEmitterNode", STStringField, Offset(stateEmitterNode, ShapeBaseImageData), MaxStates);
    fields.addField("stateIgnoreLoadedForReady", BoolField, Offset(stateIgnoreLoadedForReady, ShapeBaseImageData), MaxStates);
    
    fields.addField("computeCRC", BoolField, Offset(computeCRC, ShapeBaseImageData));
   }


    virtual S32 shouldEnumerate(void* ptr, int elementIdx);

   virtual const char* getClassName()
   {
    return "ShapeBaseImageData";
   }
};

class MissionMarkerData : public ShapeBaseData
{
typedef ShapeBaseData Parent;

public:
   MissionMarkerData()
   {
   }

   virtual ~MissionMarkerData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);
   }

   virtual void unpackData(BitStream* stream)
   {
    Parent::unpackData(stream);
   }

   virtual const char* getClassName()
   {
    return "MissionMarkerData";
   }
};
