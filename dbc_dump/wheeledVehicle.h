/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "vehicle.h"

class ParticleEmitterData;
class AudioProfile;


class WheeledVehicleTire : public SimDataBlock
{
typedef SimDataBlock Parent;

public:
    StringTableEntry shapeName; // @ 0x48
    F32 mass;                   // @ 0x4c
    F32 kineticFriction;        // @ 0x50
    F32 staticFriction;         // @ 0x54
    F32 restitution;            // @ 0x58
    F32 lateralForce;           // @ 0x5c
    F32 lateralDamping;         // @ 0x60
    F32 lateralRelaxation;      // @ 0x64
    F32 longitudinalForce;      // @ 0x68
    F32 longitudinalDamping;    // @ 0x6c
    F32 longitudinalRelaxation; // @ 0x70
    Resource<TSShape> shape;
    F32 radius;                 // @ 0x74

   WheeledVehicleTire()
   {
    shapeName = NULL;
    mass = 0;
    kineticFriction = 0.5;
    staticFriction = 1;
    restitution = 1;
    lateralForce = 10;
    lateralDamping = 1;
    lateralRelaxation = 1;
    longitudinalForce = 10;
    longitudinalDamping = 1;
    longitudinalRelaxation = 1;
    radius = 0.6;
   }

   virtual ~WheeledVehicleTire()
   {
   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
     Parent::unpackData(stream);

     shapeName = readStringTableString(stream);
     stream->read(&kineticFriction);
     stream->read(&mass);
     stream->read(&staticFriction);
     stream->read(&radius);
     stream->read(&restitution);
     stream->read(&lateralForce);
     stream->read(&lateralDamping);
     stream->read(&lateralRelaxation);
     stream->read(&longitudinalForce);
     stream->read(&longitudinalDamping);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("shapeFile", STStringField, Offset(shapeName, WheeledVehicleTire));
    fields.addField("radius", F32Field, Offset(radius, WheeledVehicleTire));
    fields.addField("staticFriction", F32Field, Offset(staticFriction, WheeledVehicleTire));
    fields.addField("kineticFriction", F32Field, Offset(kineticFriction, WheeledVehicleTire));
    fields.addField("restitution", F32Field, Offset(restitution, WheeledVehicleTire));
    fields.addField("lateralForce", F32Field, Offset(lateralForce, WheeledVehicleTire));
    fields.addField("lateralDamping", F32Field, Offset(lateralDamping, WheeledVehicleTire));
    fields.addField("lateralRelaxation", F32Field, Offset(lateralRelaxation, WheeledVehicleTire));
    fields.addField("longitudinalForce", F32Field, Offset(longitudinalForce, WheeledVehicleTire));
    fields.addField("longitudinalDamping", F32Field, Offset(longitudinalDamping, WheeledVehicleTire));
    fields.addField("longitudinalRelaxation", F32Field, Offset(longitudinalRelaxation, WheeledVehicleTire));
   }

   virtual const char* getClassName()
   {
    return "WheeledVehicleTire";
   }
};

// OK
class WheeledVehicleSpring : public SimDataBlock
{
typedef SimDataBlock Parent;

public:
    F32 length;
    F32 force;
    F32 damping;
    F32 antiSway;

   WheeledVehicleSpring()
   {
     length = 1;
     force = 10;
     damping = 1;
     antiSway = 1;
   }

   virtual ~WheeledVehicleSpring()
   {
   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
       Parent::unpackData(stream);

       stream->read(&length);
       stream->read(&force);
       stream->read(&damping);
       stream->read(&antiSway);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("length", F32Field, Offset(length, WheeledVehicleSpring));
    fields.addField("force", F32Field, Offset(force, WheeledVehicleSpring));
    fields.addField("damping", F32Field, Offset(damping, WheeledVehicleSpring));
    fields.addField("antiSwayForce", F32Field, Offset(antiSway, WheeledVehicleSpring));
   }

   virtual const char* getClassName()
   {
    return "WheeledVehicleSpring";
   }
};

// OK
class WheeledVehicleData : public VehicleData
{
typedef VehicleData Parent;

public:
    struct Wheel {
        S32 opposite;
        Point3F pos;
        S32 springNode;
        S32 springSequence;
        F32 springLength;
    };

    enum
    {
      JetSound,
      EngineSound,
      SquealSound,
      WheelImpactSound,
      MaxSounds
    };

    SimWeakRef<AudioProfile> sound[MaxSounds];
    SimWeakRef<ParticleEmitterData> tireEmitter;
    SimWeakRef<ParticleEmitterData> jetEmitter;
    S32 jetEmitterNode;
    F32 maxWheelSpeed;
    F32 engineTorque;
    F32 engineBrake;
    F32 brakeTorque;
    WheeledVehicleData::Wheel wheel[8];
    U32 wheelCount;
    //ClippedPolyList rigidBody;
    S32 brakeLightSequence;
    S32 steeringSequence;


   WheeledVehicleData()
   {
     tireEmitter = 0;
     maxWheelSpeed = 40;
     engineTorque = 1;
     engineBrake = 1;
     brakeTorque = 1;
     brakeLightSequence = -1;

     for (S32 i = 0; i < MaxSounds; i++)
        sound[i] = 0;
   }

   virtual ~WheeledVehicleData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      tireEmitter = stream->readFlag() ?
        stream->readRangedU32(DataBlockObjectIdFirst,
           DataBlockObjectIdLast): 0; 

      jetEmitter = stream->readFlag() ?
        stream->readRangedU32(DataBlockObjectIdFirst,
           DataBlockObjectIdLast): 0;

      for (S32 i = 0; i < MaxSounds; i++)
      {
        sound[i] = stream->readFlag() ?
           stream->readRangedU32(DataBlockObjectIdFirst,
              DataBlockObjectIdLast): 0;
      }

      stream->read(&maxWheelSpeed);
      stream->read(&engineTorque);
      stream->read(&engineBrake);
      stream->read(&brakeTorque);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("jetSound", SimWeakRefField, Offset(sound[JetSound], WheeledVehicleData));
    fields.addField("engineSound", SimWeakRefField, Offset(sound[EngineSound], WheeledVehicleData));
    fields.addField("squealSound", SimWeakRefField, Offset(sound[SquealSound], WheeledVehicleData));
    fields.addField("WheelImpactSound", SimWeakRefField, Offset(sound[WheelImpactSound], WheeledVehicleData));

    fields.addField("tireEmitter", SimWeakRefField, Offset(tireEmitter, WheeledVehicleData));
    fields.addField("jetEmitter", SimWeakRefField, Offset(jetEmitter, WheeledVehicleData));
    fields.addField("maxWheelSpeed", F32Field, Offset(maxWheelSpeed, WheeledVehicleData));

    fields.addField("engineTorque", F32Field, Offset(engineTorque, WheeledVehicleData));
    fields.addField("engineBrake", F32Field, Offset(engineBrake, WheeledVehicleData));
    fields.addField("brakeTorque", F32Field, Offset(brakeTorque, WheeledVehicleData));
   }

   virtual const char* getClassName()
   {
    return "WheeledVehicleData";
   }
};
