/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "player.h"

class AudioProfile;

// OK
class AIPlayerData : public PlayerData
{
typedef PlayerData Parent;

public:
    F32 mMoveTolerance;           // @ 0x372c NEVER TRANSMITTED
    F32 mWalkRate;                // @ 0x3730 NEVER TRANSMITTED
    S32 mNumTrickAnimations;      // @ 0x3734
    S32 mNumIdleAnimations;       // @ 0x3738
    SimWeakRef<AudioProfile> mTriggerSound;  // @ 0x373c AudioProfile
    SimWeakRef<AudioProfile> mTriggerSound2; // @ 0x3740 AudioProfile

   AIPlayerData()
   {
    mMoveTolerance = 0.25;
    mWalkRate = 0.5;
    mNumTrickAnimations = 0;
    mNumIdleAnimations = 0;
    mTriggerSound = 0;
    mTriggerSound2 = 0;
   }

   virtual ~AIPlayerData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      mNumTrickAnimations = stream->readInt(3);
      mNumIdleAnimations = stream->readInt(3);

      if (stream->readFlag()) {
        mTriggerSound = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
      }

      if (stream->readFlag()) {
        mTriggerSound2 = stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
      }
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("moveTolerance",    F32Field,       Offset(mMoveTolerance, AIPlayerData));
      fields.addField("numTrickAnimations",    S32Field,       Offset(mNumTrickAnimations, AIPlayerData));
      fields.addField("numIdleAnimations",    S32Field,       Offset(mNumIdleAnimations, AIPlayerData));
      fields.addField("triggerSound",    SimWeakRefField,       Offset(mTriggerSound, AIPlayerData));
      fields.addField("triggerSound2",    SimWeakRefField,       Offset(mTriggerSound2, AIPlayerData));
      fields.addField("walkRate",    F32Field,       Offset(mWalkRate, AIPlayerData));
  }

   virtual const char* getClassName()
   {
    return "AIPlayerData";
   }
};
