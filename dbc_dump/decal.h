/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

class ExplosionData;
class ParticleEmitterData;

// OK
class DecalData : public GameBaseData
{
typedef GameBaseData Parent;

public:

    F32 sizeX;
    F32 sizeY;
    StringTableEntry textureName;
    bool selfIlluminated;
    U32 lifeSpan;
    //TextureHandle textureHandle;

   DecalData()
   {
    sizeX = 1;
    sizeY = 1;
    textureName = NULL;
    selfIlluminated = false;
    lifeSpan = 5000;
   }

   virtual ~DecalData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      stream->read(&sizeX);
      stream->read(&sizeY);
      textureName = readStringTableString(stream);

      stream->read(&selfIlluminated);
      stream->read(&lifeSpan);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);

   fields.addField("sizeX",       F32Field,       Offset(sizeX,       DecalData));
   fields.addField("sizeY",       F32Field,       Offset(sizeY,       DecalData));
   fields.addField("textureName", STStringField,  Offset(textureName, DecalData));

  fields.addField("SelfIlluminated", BoolField, Offset(selfIlluminated, DecalData));
  fields.addField("LifeSpan", S32Field, Offset(lifeSpan, DecalData));
  }

   virtual const char* getClassName()
   {
    return "DecalData";
   }
};
