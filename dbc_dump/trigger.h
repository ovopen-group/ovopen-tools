/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "gameBase.h"

// OK
class TriggerData : public GameBaseData
{
typedef GameBaseData Parent;

public:
    S32 tickPeriodMS;
    
   TriggerData()
   {
      tickPeriodMS = 100;
   }

   virtual ~TriggerData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);
      stream->read(&tickPeriodMS);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("tickPeriodMS",    S32Field,       Offset(tickPeriodMS, TriggerData));
    }

   virtual const char* getClassName()
   {
    return "TriggerData";
   }
};
