/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "staticShape.h"

// OK
class ShoulderPetData : public StaticShapeData
{
typedef StaticShapeData Parent;

public:
    S32 mCC;   // @ 0x25c
    S32 mPP;   // @ 0x260
    S32 mTier; // @ 0x264
    StringTableEntry mDispName;    // @ 0x268
    StringTableEntry mDesc;        // @ 0x26c
    StringTableEntry mIconFile;    // @ 0x270
    StringTableEntry mTextureName; // @ 0x274
    
   ShoulderPetData()
   {
      mCC = 0;
      mPP = 0;
      mTier = 0;
      mDispName = NULL;
      mDesc = NULL;
      mIconFile = NULL;
      mTextureName = NULL;
   }

   virtual ~ShoulderPetData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

      stream->read(&mCC);
      stream->read(&mPP);

      if (stream->readFlag()) {
        mTextureName = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mIconFile = readStringTableString(stream);
      }
      if (stream->readFlag()) {
        mDesc = readStringTableString(stream);
      }
      mDispName = readStringTableString(stream);
      mTier = stream->readInt(3);
   }

   virtual const char* getClassName()
   {
    return "ShoulderPetData";
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

      fields.addField("textureFile",    STStringField,       Offset(mTextureName, ShoulderPetData));
      
      fields.addField("CC",    U32Field,       Offset(mCC, ShoulderPetData));
      fields.addField("PP",    U32Field,       Offset(mPP, ShoulderPetData));

      fields.addField("displayName",    STStringField,       Offset(mDispName, ShoulderPetData));
      fields.addField("desc",    STStringField,       Offset(mDesc, ShoulderPetData));
      
      fields.addField("tier",    S32Field,       Offset(mTier, ShoulderPetData));

      fields.addField("iconFile",    STStringField,       Offset(mIconFile, ShoulderPetData));
   }
};
