/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

class DecalData;

// OK
class PlayerData : public ShapeBaseData
{
typedef ShapeBaseData Parent;

public:

    enum
    {
      RecoverDelayBits = 7,
      JumpDelayBits = 7,
      ImpactBits = 3,

      NUM_SPLASH_EMITTERS = 3,
      BUBBLE_EMITTER = 2,

      NumRecoilSequences = 3,
      NumSpineNodes = 6,
      NumTableActionAnims = 512
    };

    bool renderFirstPerson; // @ 0x24c
    F32 pickupRadius;
    F32 maxTimeScale; // @ 0x254
    F32 minLookAngle; // @ 0x258
    F32 maxLookAngle;
    F32 runForce;         // @ 0x260
    F32 runEnergyDrain;   // @ 0x264
    F32 minRunEnergy;     // @ 0x268
    F32 maxForwardSpeed;  // @ 0x26c
    F32 maxBackwardSpeed; // @ 0x270
    F32 maxSideSpeed; // @ 0x274
    F32 swimForce; // @ 0x278
    F32 maxUnderwaterForwardSpeed;
    F32 maxUnderwaterBackwardSpeed; // @ 0x280
    F32 maxUnderwaterSideSpeed; // @ 0x284
    F32 maxStepHeight; // @ 0x288
    F32 runSurfaceAngle; // @ 0x28c
    F32 horizMaxSpeed; // @ 0x290
    F32 horizResistSpeed; // @ 0x294
    F32 horizResistFactor;
    F32 upMaxSpeed; // @ 0x29c
    F32 upResistSpeed; // @ 0x2a0
    F32 upResistFactor;
    S32 recoverDelay; // @ 0x2a8
    F32 recoverRunForceScale; // @ 0x2ac
    F32 jumpForce; // @ 0x2b0
    F32 jumpEnergyDrain;
    F32 minJumpEnergy;
    F32 minJumpSpeed; // @ 0x2bc
    F32 maxJumpSpeed;
    F32 jumpSurfaceAngle;
    S32 jumpDelay;  // @ 0x2c8
    F32 airControl; // @ 0x2cc
    F32 boxHeadPercentage;
    F32 boxTorsoPercentage;
    S32 boxHeadLeftPercentage;
    S32 boxHeadRightPercentage;
    S32 boxHeadBackPercentage;
    S32 boxHeadFrontPercentage;
    F32 minImpactSpeed; // @ 0x2e8
    F32 decalOffset; // @ 0x2ec
    F32 decalOffsetY;
    F32 groundImpactMinSpeed;
    VectorF groundImpactShakeFreq;
    VectorF groundImpactShakeAmp;
    F32 groundImpactShakeDuration; // @ 0x310
    F32 groundImpactShakeFalloff; // @ 0x31

   enum Sounds {
      FootSoft,
      FootHard,
      FootMetal,
      FootSnow,
      FootGrass, // ++
      FootWood,  // ++
      FootShallowSplash,
      FootWading,
      FootUnderWater,
      FootBubbles,   // 10
      MoveBubbles,
      WaterBreath,
      ImpactSoft,
      ImpactHard,
      ImpactMetal,
      ImpactGrass, // ++
      ImpactWood, // ++
      ImpactSnow,
      ImpactWaterEasy,
      ImpactWaterMedium, // 20
      ImpactWaterHard,
      ExitWater,
      MaxSounds // 22
   };


    SimWeakRef<AudioProfile> sound[MaxSounds];
    Point3F boxSize; // @ 0x370
    Point3F swimBoxSize; // @ 0x37c

    struct ActionAnimation {
        const char *name;
        S32 sequence;
        VectorF dir;
        F32 speed;
        bool velocityScale;
        bool death;
    };

    ActionAnimation actionList[NumTableActionAnims];
    U32 actionCount; // @ 0x3b88
    U32 lookAction;
    S32 spineNode[NumSpineNodes];
    S32 pickupDelta;
    F32 runSurfaceCos;
    F32 jumpSurfaceCos;
    S32 recoilSequence[NumRecoilSequences];
    SimWeakRef<ParticleEmitterData> footPuffEmitter;
    S32 footPuffID; // @ 0x3bc4
    S32 footPuffNumParts;
    F32 footPuffRadius;
    SimWeakRef<DecalData> decalData;
    S32 decalID; // @ 0x3bd4
    SimWeakRef<ParticleEmitterData> dustEmitter;
    S32 dustID;
    SimWeakRef<ParticleEmitterData> doubleEmitter;
    S32 doubleID; // @ 0x3be4
    SimWeakRef<SplashData> splash;
    S32 splashId;
    F32 splashVelocity; // @ 0x3bf0
    F32 splashAngle;
    F32 splashFreqMod;
    F32 splashVelEpsilon;
    F32 medSplashSoundVel; // @ 0x3c00
    F32 hardSplashSoundVel;
    F32 exitSplashSoundVel;
    F32 footSplashHeight;
    SimWeakRef<ParticleEmitterData> splashEmitterList[NUM_SPLASH_EMITTERS];
    S32 splashEmitterIDList[NUM_SPLASH_EMITTERS];
    S32 fpMeshHideNum; // @ 0x3c28

   PlayerData()
   {
    memset(actionList, '\0', sizeof(actionList));
    actionCount = 0;
    lookAction = 0;
    pickupDelta = 0;
    runSurfaceCos = 0;
    jumpSurfaceCos = 0;



   shadowEnable = true;
   shadowCanMove = true;
   shadowCanAnimate = true;

   renderFirstPerson = true;
   pickupRadius = 0.0f;
   minLookAngle = -1.4f;
   maxLookAngle = 1.4f;
   //maxFreelookAngle = 3.0f;
   maxTimeScale = 1.5f;

   mass = 9.0f;
   maxEnergy =  60.0f;

   runForce = 40.0f * 9.0f;
   runEnergyDrain = 0.0f;
   minRunEnergy = 0.0f;
   maxForwardSpeed = 6.0f;
   maxBackwardSpeed = 5.0f;
   maxSideSpeed = 6.0f;
   swimForce = 495;

   maxUnderwaterForwardSpeed = 6.0f;
   maxUnderwaterBackwardSpeed = 5.0f;
   maxUnderwaterSideSpeed = 6.0f;

   maxStepHeight = 0.5f;
   runSurfaceAngle = 45.0f;

   recoverDelay = 16;
   recoverRunForceScale = 1.0f;

   jumpForce = 85.0f;
   jumpEnergyDrain = 0.0f;
   minJumpEnergy = 0.0f;
   jumpSurfaceAngle = 78.0f;
   jumpDelay = 10;
   airControl = 0.100000001;
   minJumpSpeed = 500.0f;
   maxJumpSpeed = 2.0f * minJumpSpeed;

   horizMaxSpeed = 80.0f;
   horizResistSpeed = 38.0f;
   horizResistFactor = 1.0f;

   upMaxSpeed = 80.0f;
   upResistSpeed = 38.0f;
   upResistFactor = 1.0f;

   minImpactSpeed = 8.0f;

   decalData      = 0;
   decalID        = 0;
   decalOffset      = 0.0f;
   decalOffsetY = 0;

   lookAction = 0;

   // size of bounding box
   boxSize.set(1.0f, 1.0f, 2.3f );
   swimBoxSize.set(1.0f, 2.3f, 1.0f );

   // location of head, torso, legs
   boxHeadPercentage = 0.85f;
   boxTorsoPercentage = 0.55f;

   // damage locations
   boxHeadLeftPercentage  = 0;
   boxHeadRightPercentage = 1;
   boxHeadBackPercentage  = 0;
   boxHeadFrontPercentage = 1;

   for (S32 i = 0; i < MaxSounds; i++)
      sound[i] = 0;

   footPuffEmitter = 0;
   footPuffID = 0;
   footPuffNumParts = 10;
   footPuffRadius = .25f;

   dustEmitter = 0;
   dustID = 0;
   doubleEmitter = 0;
   doubleID = 0;

   splash = 0;
   splashId = 0;
   splashVelocity = 1.0f;
   splashAngle = 45.0f;
   splashFreqMod = 300.0f;
   splashVelEpsilon = 0.25f;
   //bubbleEmitTime = 0.4f;

   medSplashSoundVel = 2.0f;
   hardSplashSoundVel = 3.0f;
   exitSplashSoundVel = 2.0f;
   footSplashHeight = 0.1f;

   memset( splashEmitterList, 0, sizeof( splashEmitterList ) );
   memset( splashEmitterIDList, 0, sizeof( splashEmitterIDList ) );

   genericShadowLevel = Player_GenericShadowLevel;
   noShadowLevel = Player_NoShadowLevel;

   groundImpactMinSpeed = 10.0f;
   groundImpactShakeFreq.set( 10.0f, 10.0f, 10.0f );
   groundImpactShakeAmp.set( 5.0f, 5.0f, 5.0f );
   groundImpactShakeDuration = 1.0f;
   groundImpactShakeFalloff = 10.0f;


   fpMeshHideNum = -1;
   }

   virtual ~PlayerData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
       Parent::unpackData(stream);

       renderFirstPerson = stream->readFlag();

       // NOTE: everything seems to use flags here
       if (stream->readFlag()) stream->read(&minLookAngle);
       if (stream->readFlag()) stream->read(&maxLookAngle);
       if (stream->readFlag()) stream->read(&maxTimeScale);
       if (stream->readFlag()) stream->read(&maxStepHeight);

       if (stream->readFlag()) stream->read(&runForce);
       if (stream->readFlag()) stream->read(&runEnergyDrain);
       if (stream->readFlag()) stream->read(&minRunEnergy);
       if (stream->readFlag()) stream->read(&maxForwardSpeed);
       if (stream->readFlag()) stream->read(&maxBackwardSpeed);

       if (stream->readFlag()) stream->read(&maxSideSpeed);
       if (stream->readFlag()) stream->read(&runSurfaceAngle);
       if (stream->readFlag()) stream->read(&swimForce);

       if (stream->readFlag()) stream->read(&maxUnderwaterForwardSpeed);
       if (stream->readFlag()) stream->read(&maxUnderwaterBackwardSpeed);
       if (stream->readFlag()) stream->read(&maxUnderwaterSideSpeed);

       if (stream->readFlag()) stream->read(&recoverDelay);
       if (stream->readFlag()) stream->read(&recoverRunForceScale);

       if (stream->readFlag()) stream->read(&jumpForce);
       if (stream->readFlag()) stream->read(&jumpEnergyDrain);
       if (stream->readFlag()) stream->read(&minJumpEnergy);
       if (stream->readFlag()) stream->read(&minJumpSpeed);
       if (stream->readFlag()) stream->read(&maxJumpSpeed);
       if (stream->readFlag()) stream->read(&jumpSurfaceAngle);
       if (stream->readFlag()) jumpDelay = stream->readInt(JumpDelayBits);
       if (stream->readFlag()) stream->read(&airControl);

       if (stream->readFlag()) stream->read(&horizMaxSpeed);
       if (stream->readFlag()) stream->read(&horizResistSpeed);
       if (stream->readFlag()) stream->read(&horizResistFactor);

       if (stream->readFlag()) stream->read(&upMaxSpeed);
       if (stream->readFlag()) stream->read(&upResistSpeed);
       if (stream->readFlag()) stream->read(&upResistFactor);

       if (stream->readFlag()) stream->read(&splashVelocity);
       if (stream->readFlag()) stream->read(&splashAngle);
       if (stream->readFlag()) stream->read(&splashFreqMod);
       if (stream->readFlag()) stream->read(&splashVelEpsilon);

       if (stream->readFlag()) stream->read(&medSplashSoundVel);
       if (stream->readFlag()) stream->read(&hardSplashSoundVel);
       if (stream->readFlag()) stream->read(&exitSplashSoundVel);
       if (stream->readFlag()) stream->read(&footSplashHeight);

       if (stream->readFlag()) stream->read(&minImpactSpeed);

       for ( S32 i = 0; i < MaxSounds; i++ ) {
          sound[i] = 0;
          if (stream->readFlag())
             sound[i] = stream->readRangedU32(DataBlockObjectIdFirst,
                                                             DataBlockObjectIdLast);
       }

       stream->read(&boxSize.x);
       stream->read(&boxSize.y);
       stream->read(&boxSize.z);

       if( stream->readFlag() )
       {
          footPuffID = (S32) stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
       }

       // Flagged reads in OV
       if (stream->readFlag()) stream->read(&footPuffNumParts);
       if (stream->readFlag()) stream->read(&footPuffRadius);

       if( stream->readFlag() )
       {
          decalID = (S32) stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
       }

       stream->read(&decalOffset);
       stream->read(&decalOffsetY);

       if( stream->readFlag() )
       {
          dustID = (S32) stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
       }

       if( stream->readFlag() )
       {
          doubleID = (S32) stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
       }

       if (stream->readFlag())
       {
          splashId = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
       }

       for( S32 i=0; i<NUM_SPLASH_EMITTERS; i++ )
       {
          if( stream->readFlag() )
          {
             splashEmitterIDList[i] = stream->readRangedU32( DataBlockObjectIdFirst, DataBlockObjectIdLast );
          }
       }

       if (stream->readFlag()) stream->read(&groundImpactMinSpeed);
       if (stream->readFlag()) stream->read(&groundImpactShakeFreq.x);
       if (stream->readFlag()) stream->read(&groundImpactShakeFreq.y);
       if (stream->readFlag()) stream->read(&groundImpactShakeFreq.z);
       if (stream->readFlag()) stream->read(&groundImpactShakeAmp.x);
       if (stream->readFlag()) stream->read(&groundImpactShakeAmp.y);
       if (stream->readFlag()) stream->read(&groundImpactShakeAmp.z);
       if (stream->readFlag()) stream->read(&groundImpactShakeDuration);
       if (stream->readFlag()) stream->read(&groundImpactShakeFalloff);
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);
       fields.addField("renderFirstPerson", BoolField, Offset(renderFirstPerson, PlayerData));

    fields.addField("pickupRadius", F32Field, Offset(pickupRadius, PlayerData));
    fields.addField("minLookAngle", F32Field, Offset(minLookAngle, PlayerData));
    fields.addField("maxLookAngle", F32Field, Offset(maxLookAngle, PlayerData));
    fields.addField("maxTimeScale", F32Field, Offset(maxTimeScale, PlayerData));
    fields.addField("maxStepHeight", F32Field, Offset(maxStepHeight, PlayerData));
    fields.addField("runForce", F32Field, Offset(runForce, PlayerData));
    fields.addField("runEnergyDrain", F32Field, Offset(runEnergyDrain, PlayerData));
    fields.addField("minRunEnergy", F32Field, Offset(minRunEnergy, PlayerData));
    fields.addField("maxForwardSpeed", F32Field, Offset(maxForwardSpeed, PlayerData));
    fields.addField("maxBackwardSpeed", F32Field, Offset(maxBackwardSpeed, PlayerData));
    fields.addField("maxSideSpeed", F32Field, Offset(maxSideSpeed, PlayerData));
    fields.addField("maxUnderwaterForwardSpeed", F32Field, Offset(maxUnderwaterForwardSpeed, PlayerData));
    fields.addField("maxUnderwaterBackwardSpeed", F32Field, Offset(maxUnderwaterBackwardSpeed, PlayerData));
    fields.addField("maxUnderwaterSideSpeed", F32Field, Offset(maxUnderwaterSideSpeed, PlayerData));
    fields.addField("runSurfaceAngle", F32Field, Offset(runSurfaceAngle, PlayerData));
    fields.addField("minImpactSpeed", F32Field, Offset(minImpactSpeed, PlayerData));

    fields.addField("recoverDelay", S32Field, Offset(recoverDelay, PlayerData));
    fields.addField("recoverRunForceScale", F32Field, Offset(recoverRunForceScale, PlayerData));

    fields.addField("jumpForce", F32Field, Offset(jumpForce, PlayerData));
    fields.addField("jumpEnergyDrain", F32Field, Offset(jumpEnergyDrain, PlayerData));
    fields.addField("minJumpEnergy", F32Field, Offset(minJumpEnergy, PlayerData));
    fields.addField("minJumpSpeed", F32Field, Offset(minJumpSpeed, PlayerData));
    fields.addField("maxJumpSpeed", F32Field, Offset(maxJumpSpeed, PlayerData));
    fields.addField("jumpSurfaceAngle", F32Field, Offset(jumpSurfaceAngle, PlayerData));
    fields.addField("jumpDelay", S32Field, Offset(jumpDelay, PlayerData));

    fields.addField("airControl", F32Field, Offset(airControl, PlayerData));
    fields.addField("swimForce", F32Field, Offset(swimForce, PlayerData));

    fields.addField("maxUnderwaterForwardSpeed", F32Field, Offset(maxUnderwaterForwardSpeed, PlayerData));
    fields.addField("maxUnderwaterBackwardSpeed", F32Field, Offset(maxUnderwaterBackwardSpeed, PlayerData));
    fields.addField("maxUnderwaterSideSpeed", F32Field, Offset(maxUnderwaterSideSpeed, PlayerData));

    fields.addField("boundingBox", Point3FField, Offset(boxSize, PlayerData));
    fields.addField("swimBoundingBox", Point3FField, Offset(swimBoxSize, PlayerData));

    fields.addField("boxHeadPercentage", F32Field, Offset(boxHeadPercentage, PlayerData));
    fields.addField("boxTorsoPercentage", F32Field, Offset(boxTorsoPercentage, PlayerData));
    fields.addField("boxHeadLeftPercentage", S32Field, Offset(boxHeadLeftPercentage, PlayerData));
    fields.addField("boxHeadRightPercentage", S32Field, Offset(boxHeadRightPercentage, PlayerData));
    fields.addField("boxHeadBackPercentage", S32Field, Offset(boxHeadBackPercentage, PlayerData));
    fields.addField("boxHeadFrontPercentage", S32Field, Offset(boxHeadFrontPercentage, PlayerData));

    fields.addField("horizMaxSpeed", F32Field, Offset(horizMaxSpeed, PlayerData));
    fields.addField("horizResistSpeed", F32Field, Offset(horizResistSpeed, PlayerData));
    fields.addField("horizResistFactor", F32Field, Offset(horizResistFactor, PlayerData));
    fields.addField("upMaxSpeed", F32Field, Offset(upMaxSpeed, PlayerData));
    fields.addField("upResistSpeed", F32Field, Offset(upResistSpeed, PlayerData));
    fields.addField("upResistFactor", F32Field, Offset(upResistFactor, PlayerData));


    fields.addField("decalData", SimWeakRefField, Offset(decalID, PlayerData));
    fields.addField("decalOffset", F32Field, Offset(decalOffset, PlayerData));
    fields.addField("decalOffsetY", F32Field, Offset(decalOffsetY, PlayerData));

    fields.addField("footPuffEmitter", SimWeakRefField, Offset(footPuffID, PlayerData));
    fields.addField("footPuffNumParts", S32Field, Offset(footPuffNumParts, PlayerData));
    fields.addField("footPuffRadius", F32Field, Offset(footPuffRadius, PlayerData));
    fields.addField("dustEmitter", SimWeakRefField, Offset(dustID, PlayerData));
    fields.addField("doubleEmitter", SimWeakRefField, Offset(doubleID, PlayerData));

    fields.addField("FootSoftSound", SimWeakRefField, Offset(sound[FootSoft], PlayerData));
    fields.addField("FootHardSound", SimWeakRefField, Offset(sound[FootHard], PlayerData));
    fields.addField("FootMetalSound", SimWeakRefField, Offset(sound[FootMetal], PlayerData));
    fields.addField("FootSnowSound", SimWeakRefField, Offset(sound[FootSnow], PlayerData));
    fields.addField("FootGrassSound", SimWeakRefField, Offset(sound[FootGrass], PlayerData));
    fields.addField("FootWoodSound", SimWeakRefField, Offset(sound[FootWood], PlayerData));
    fields.addField("FootShallowSound", SimWeakRefField, Offset(sound[FootShallowSplash], PlayerData));
    fields.addField("FootWadingSound", SimWeakRefField, Offset(sound[FootWading], PlayerData));
    fields.addField("FootUnderwaterSound", SimWeakRefField, Offset(sound[FootUnderWater], PlayerData));
    fields.addField("FootBubblesSound", SimWeakRefField, Offset(sound[FootBubbles], PlayerData));
    fields.addField("movingBubblesSound", SimWeakRefField, Offset(sound[MoveBubbles], PlayerData));
    fields.addField("waterBreathSound", SimWeakRefField, Offset(sound[WaterBreath], PlayerData));
    fields.addField("impactSoftSound", SimWeakRefField, Offset(sound[ImpactSoft], PlayerData));
    fields.addField("impactHardSound", SimWeakRefField, Offset(sound[ImpactHard], PlayerData));
    fields.addField("impactMetalSound", SimWeakRefField, Offset(sound[ImpactMetal], PlayerData));
    fields.addField("impactGrassSound", SimWeakRefField, Offset(sound[ImpactGrass], PlayerData));
    fields.addField("impactWoodSound", SimWeakRefField, Offset(sound[ImpactWood], PlayerData));
    fields.addField("impactSnowSound", SimWeakRefField, Offset(sound[ImpactSnow], PlayerData));

    fields.addField("mediumSplashSoundVelocity", F32Field, Offset(medSplashSoundVel, PlayerData));
    fields.addField("hardSplashSoundVelocity", F32Field, Offset(hardSplashSoundVel, PlayerData));
    fields.addField("exitSplashSoundVelocity", F32Field, Offset(exitSplashSoundVel, PlayerData));

    fields.addField("impactWaterEasy", SimWeakRefField, Offset(sound[ImpactWaterEasy], PlayerData));
    fields.addField("impactWaterMedium", SimWeakRefField, Offset(sound[ImpactWaterMedium], PlayerData));
    fields.addField("impactWaterHard", SimWeakRefField, Offset(sound[ImpactWaterHard], PlayerData));
    fields.addField("exitingWater", SimWeakRefField, Offset(sound[ExitWater], PlayerData));
    
    fields.addField("splash", SimWeakRefField, Offset(splashId, PlayerData));
    fields.addField("splashVelocity", F32Field, Offset(splashVelocity, PlayerData));
    fields.addField("splashAngle", F32Field, Offset(splashAngle, PlayerData));
    fields.addField("splashFreqMod", F32Field, Offset(splashFreqMod, PlayerData));
    fields.addField("splashVelEpsilon", F32Field, Offset(splashVelEpsilon, PlayerData));
    fields.addField("splashEmitter", SimWeakRefField, Offset(splashEmitterIDList, PlayerData), NUM_SPLASH_EMITTERS);
    fields.addField("footstepSplashHeight", F32Field, Offset(footSplashHeight, PlayerData));

    fields.addField("groundImpactMinSpeed", F32Field, Offset(groundImpactMinSpeed, PlayerData));
    fields.addField("groundImpactShakeFreq", Point3FField, Offset(groundImpactShakeFreq, PlayerData));
    fields.addField("groundImpactShakeAmp", Point3FField, Offset(groundImpactShakeAmp, PlayerData));
    fields.addField("groundImpactShakeDuration", F32Field, Offset(groundImpactShakeDuration, PlayerData));
    fields.addField("groundImpactShakeFalloff", F32Field, Offset(groundImpactShakeFalloff, PlayerData));

   }


   virtual const char* getClassName()
   {
    return "PlayerData";
   }
};
