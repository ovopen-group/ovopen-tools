/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "item.h"


DECLARE_ENUM_TYPE(PowerupData_TypeField);

// OK (mostly)
class PowerupData : public ItemData
{
typedef ItemData Parent;

public:
   enum PowerUpType {
    SINGLE_USE_NO_COMB=0,   // -3
    SINGLE_USE_REPLACE,     // -2
    SINGLE_USE_STACK,       // -1
    TIMED_NO_COMB,          // 0
    TIMED_REPLACE,          // 1
    TIMED,                  // 2
    UNLIMITED,              // 3
    PowerUpType_NUMBITS
};

    PowerupData::PowerUpType type; // @ 0x288
    StringTableEntry iconPath; // @ 0x28c
    U8 sortType;   // @ 0x290
    U8 sortOrder;  // @ 0x291
    S32 time;      // @ 0x294
    bool sortOver; // @ 0x298
    float speedMod; // @ 0x29c
    float jumpForceMod; // @ 0x2a0
    U8 jumpCount;  // @ 0x2a4

   PowerupData()
   {
    type = SINGLE_USE_NO_COMB;
    sortType = 0;
    sortOrder = 0;
    sortOver = false;
    time = 0;
    speedMod = 0;
    jumpForceMod = 0;
    jumpCount = 0;
   }

   virtual ~PowerupData()
   {

   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
    // TOCHECK
      Parent::unpackData(stream);

      iconPath = readStringTableString(stream);
      //printf("iconPath=%s\n", iconPath);
      //printf("class=%s\n", className);
      type = (PowerupData::PowerUpType)stream->readInt(0x3);
      //printf("f=%f, el=%f, t=%u\n", friction, elasticity, type);

      if (stream->readFlag())
      {
        stream->read(&speedMod);
        stream->read(&jumpForceMod);
        stream->read(&jumpCount);
      }

      if (stream->readFlag())
      {
        stream->read(&sortType);
        stream->read(&sortOrder);
        sortOver = stream->readFlag();
      }

      //printf("sortType=%u,sortOrder=%u,jumpCount=%u,jumpForceMod=%f,speedMod=%f\n", sortType, sortOrder, jumpCount, jumpForceMod, speedMod);

    if (type >= TIMED_NO_COMB && type < UNLIMITED) {
      time = stream->readRangedU32( 0x0, 0xffff ); // FIXED
    }
   }

   static void enumerateFields(SimKlassInfo& fields)
   {
    Parent::enumerateFields(fields);
   
    fields.addField("type", PowerupData_TypeField, Offset(type, PowerupData));
    fields.addField("iconPath", STStringField, Offset(iconPath, PowerupData));

    fields.addField("sortType", U8Field, Offset(sortType, PowerupData));
    fields.addField("sortOrder", U8Field, Offset(sortOrder, PowerupData));

    fields.addField("speedMod", F32Field, Offset(speedMod, PowerupData));
    fields.addField("jumpForceMod", F32Field, Offset(jumpForceMod, PowerupData));
    fields.addField("jumpCount", U8Field, Offset(jumpCount, PowerupData));
    
    fields.addField("time", S32Field, Offset(time, PowerupData));
   }

   virtual const char* getClassName()
   {
    return "PowerupData";
   }
};
