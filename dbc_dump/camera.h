/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "shapeBase.h"

// OK
class CameraData : public ShapeBaseData
{
typedef ShapeBaseData Parent;

public:

   // Nothing extra

   CameraData()
   {

   }

   virtual ~CameraData()
   {

   }

   virtual void packData(BitStream* stream)
   {
      Parent::packData(stream);
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);
   }

   virtual const char* getClassName()
   {
    return "CameraData";
   }
};