/*
ovopen-tools
Copyright (C) 2020  mangofusi

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include "vehicle.h"

class AudioProfile;
class ParticleEmitterData;

// OK
class HoverVehicleData : public VehicleData
{
typedef VehicleData Parent;

public:

    enum Sounds {
      JetSound,
      EngineSound,
      FloatSound,
      MaxSounds
   };

   enum Jets {
      ForwardJetEmitter,
      BackwardJetEmitter,
      DownwardJetEmitter,
      MaxJetEmitters
   };

    enum JetNodes {
      ForwardJetNode,
      ForwardJetNode1,
      BackwardJetNode,
      BackwardJetNode1,
      DownwardJetNode,
      DownwardJetNode1,
      
      MaxJetNodes,
      MaxDirectionJets = 2,
      ThrustJetStart = ForwardJetNode,
      MaxTrails = 4,
   };

    SimWeakRef<AudioProfile> sound[MaxSounds];
    SimWeakRef<ParticleEmitterData> jetEmitter[MaxJetEmitters];
    S32 jetNode[MaxJetNodes];
    F32 dragForce;
    F32 vertFactor;
    F32 floatingThrustFactor;
    F32 mainThrustForce;
    F32 reverseThrustForce;
    F32 strafeThrustForce;
    F32 turboFactor;
    F32 stabLenMin;
    F32 stabLenMax;
    F32 stabSpringConstant;
    F32 stabDampingConstant;
    F32 gyroDrag;
    F32 normalForce;
    F32 restorativeForce;
    F32 steeringForce;
    F32 rollForce;
    F32 pitchForce;
    F32 floatingGravMag;
    F32 brakingForce;
    F32 brakingActivationSpeed;
    ParticleEmitterData *dustTrailEmitter;
    S32 dustTrailID;
    Point3F dustTrailOffset;
    F32 triggerTrailHeight;
    F32 dustTrailFreqMod;
    F32 maxThrustSpeed;

   HoverVehicleData()
   {
     dragForce            = 0;
     vertFactor           = 0.25;
     floatingThrustFactor = 0.15;

     mainThrustForce    = 0;
     reverseThrustForce = 0;
     strafeThrustForce  = 0;
     turboFactor        = 1.0;

     stabLenMin = 0.5;
     stabLenMax = 2.0;
     stabSpringConstant  = 30;
     stabDampingConstant = 10;

     gyroDrag = 10;
     normalForce = 30;
     restorativeForce = 10;
     steeringForce = 25;
     rollForce = 2.5;
     pitchForce = 2.5;

     dustTrailEmitter = NULL;
     dustTrailID = 0;
     dustTrailOffset.set( 0.0, 0.0, 0.0 );
     dustTrailFreqMod = 15.0;
     triggerTrailHeight = 2.5;

     floatingGravMag = 1;
     brakingForce = 0;
     brakingActivationSpeed = 0;

     maxThrustSpeed = 0;

     for (S32 k = 0; k < MaxJetNodes; k++)
        jetNode[k] = -1;

     for (S32 j = 0; j < MaxJetEmitters; j++)
        jetEmitter[j] = 0;

     for (S32 i = 0; i < MaxSounds; i++)
        sound[i] = 0;
   }

   virtual ~HoverVehicleData()
   {

   }

   static void enumerateFields(SimKlassInfo& fields)
   {
      Parent::enumerateFields(fields);

    fields.addField("dragForce", F32Field, Offset(dragForce, HoverVehicleData));
    fields.addField("vertFactor", F32Field, Offset(vertFactor, HoverVehicleData));
    fields.addField("floatingThrustFactor", F32Field, Offset(floatingThrustFactor, HoverVehicleData));
    fields.addField("mainThrustForce", F32Field, Offset(mainThrustForce, HoverVehicleData));
    fields.addField("reverseThrustForce", F32Field, Offset(reverseThrustForce, HoverVehicleData));
    fields.addField("strafeThrustForce", F32Field, Offset(strafeThrustForce, HoverVehicleData));
    fields.addField("turboFactor", F32Field, Offset(turboFactor, HoverVehicleData));
    fields.addField("stabLenMin", F32Field, Offset(stabLenMin, HoverVehicleData));
    fields.addField("stabLenMax", F32Field, Offset(stabLenMax, HoverVehicleData));
    fields.addField("stabSpringConstant", F32Field, Offset(stabSpringConstant, HoverVehicleData));
    fields.addField("stabDampingConstant", F32Field, Offset(stabDampingConstant, HoverVehicleData));
    fields.addField("gyroDrag", F32Field, Offset(gyroDrag, HoverVehicleData));
    fields.addField("normalForce", F32Field, Offset(normalForce, HoverVehicleData));
    fields.addField("restorativeForce", F32Field, Offset(restorativeForce, HoverVehicleData));
    fields.addField("steeringForce", F32Field, Offset(steeringForce, HoverVehicleData));
    fields.addField("rollForce", F32Field, Offset(rollForce, HoverVehicleData));
    fields.addField("pitchForce", F32Field, Offset(pitchForce, HoverVehicleData));

    fields.addField("jetSound", SimWeakRefField, Offset(sound[JetSound], HoverVehicleData));
    fields.addField("engineSound", SimWeakRefField, Offset(sound[EngineSound], HoverVehicleData));
    fields.addField("floatSound", SimWeakRefField, Offset(sound[FloatSound], HoverVehicleData));

    fields.addField("dustTrailEmitter", SimWeakRefField, Offset(dustTrailID, HoverVehicleData));
    fields.addField("dustTrailOffset", Point3FField, Offset(dustTrailOffset, HoverVehicleData));
    fields.addField("triggerTrailHeight", F32Field, Offset(triggerTrailHeight, HoverVehicleData));
    fields.addField("dustTrailFreqMod", F32Field, Offset(dustTrailFreqMod, HoverVehicleData));
    fields.addField("floatingGravMag", F32Field, Offset(floatingGravMag, HoverVehicleData));
    fields.addField("brakingForce", F32Field, Offset(brakingForce, HoverVehicleData));
    fields.addField("brakingActivationSpeed", F32Field, Offset(brakingActivationSpeed, HoverVehicleData));

    fields.addField("forwardJetEmitter", SimWeakRefField, Offset(jetEmitter[ForwardJetEmitter], HoverVehicleData));
   }

   virtual void packData(BitStream* stream)
   {
   }

   virtual void unpackData(BitStream* stream)
   {
      Parent::unpackData(stream);

       stream->read(&dragForce);
       stream->read(&vertFactor);
       stream->read(&floatingThrustFactor);
       stream->read(&mainThrustForce);
       stream->read(&reverseThrustForce);
       stream->read(&strafeThrustForce);
       stream->read(&turboFactor);
       stream->read(&stabLenMin);
       stream->read(&stabLenMax);
       stream->read(&stabSpringConstant);
       stream->read(&stabDampingConstant);
       stream->read(&gyroDrag);
       stream->read(&normalForce);
       stream->read(&restorativeForce);
       stream->read(&steeringForce);
       stream->read(&rollForce);
       stream->read(&pitchForce);
       mathRead(*stream, &dustTrailOffset);
       stream->read(&triggerTrailHeight);
       stream->read(&dustTrailFreqMod);

       for (S32 i = 0; i < MaxSounds; i++) {
          sound[i] = 0;
          if (stream->readFlag())
          {
             sound[i] = stream->readRangedU32(DataBlockObjectIdFirst,
                                                   DataBlockObjectIdLast);
          }
       }

       for (S32 j = 0; j < MaxJetEmitters; j++) {
          jetEmitter[j] = 0;
          if (stream->readFlag())
          {
             jetEmitter[j] = stream->readRangedU32(DataBlockObjectIdFirst,
                                                                         DataBlockObjectIdLast);
          }
       }

       if( stream->readFlag() )
       {
          dustTrailID = (S32) stream->readRangedU32(DataBlockObjectIdFirst, DataBlockObjectIdLast);
       }
       stream->read(&floatingGravMag);
       stream->read(&brakingForce);
       stream->read(&brakingActivationSpeed);
   }

   virtual const char* getClassName()
   {
    return "HoverVehicleData";
   }
};
